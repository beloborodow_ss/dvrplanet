BlowPipeEmail is a professional high-performance bulk email sender software 
for targeted bulk email marketing campaigns and email lists building at your 
desktop. BlowPipeEmail empowers you to send personalized HTML email messages 
easily

The source code is available on request: alcinoe@arkadia.com