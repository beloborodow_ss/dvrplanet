unit Accel;

interface

uses
  System.SysUtils, System.Classes, System.Types,
  Vcl.Controls, Vcl.Graphics,
  GR32_Image, GR32_Layers, GR32, GR32_VPR, GR32_Lines;

type
  TAccelerometer = class(TCustomImage32)
  private
    FVectorLayer: TBitmapLayer;
    FGridLayer:   TBitmapLayer;
    FModule: real;
    FAngle: real;
    FXValue: real;
    FYValue: real;
    procedure SetAngle(const Value: real);
    procedure SetModule(const Value: real);
    procedure BeginDrawing;
    procedure EndDrawing;
    procedure DrawCircle(amp: real);
    procedure DrawArrow(amp, theta: real);
    procedure LoadPNGFromResource(Dst: TBitmap32; ResName: string);
    procedure SetXValue(const Value: real);
    procedure SetYValue(const Value: real);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property Module: real read FModule write SetModule;
    property Angle:  real read FAngle  write SetAngle;
    property XValue: real read FXValue write SetXValue; //�� ���������� ��� ����
    property YValue: real read FYValue write SetYValue;
  end;

procedure Register;

implementation

uses
  Math, GR32_PNG, GR32_VectorUtils, GR32_PolygonsEx;

{$R 'accel.res'}

{$I ..\AkenIndi.inc}

const
  cXCenter = 70;
  cYCenter = 70;

procedure Register;
begin
  RegisterComponents('Graphics32', [TAccelerometer]);
end;

{ TAccelerometer }

procedure TAccelerometer.BeginDrawing;
begin
  BeginUpdate;
  FVectorLayer.Bitmap.Clear(0);
end;

procedure TAccelerometer.EndDrawing;
begin
  EndUpdate;
  Invalidate;
end;

constructor TAccelerometer.Create(AOwner: TComponent);
begin
  inherited;

  AutoSize := true;
  LoadPNGFromResource(Bitmap, 'ACCELBACK');
  Bitmap.DrawMode := dmBlend;


  FVectorLayer := TBitmapLayer.Create(Layers);
  with FVectorLayer do begin
    Bitmap.DrawMode := dmBlend;
    Bitmap.SetSizeFrom(Self.Bitmap);
    Location := FloatRect(0, 0, Self.Bitmap.Width, Self.Bitmap.Height);
  end;


  FGridLayer   := TBitmapLayer.Create(Layers);
  LoadPNGFromResource(FGridLayer.Bitmap, 'ACCELGRID');
  with FGridLayer do begin
    Bitmap.DrawMode := dmBlend;
    Location := FloatRect(0, 0, Self.Bitmap.Width, Self.Bitmap.Height);
  end;

  FModule := 0;
  DrawCircle(0.0);

  FAngle := 0.0;
  DrawArrow(0.0, 0.0);
end;

destructor TAccelerometer.Destroy;
begin
  //FArrowLayer.Free;

  inherited;
end;

procedure TAccelerometer.DrawArrow(amp, theta: real);
var
  r: real;
  x0,y0,x,y: UInt16;
begin
  DrawCircle(0.0);
  if FModule < 10 then Exit;

  with TLine32.Create do
  try
    //AntialiasMode := am4times;

    EndStyle := esSquared;
    ArrowEnd.Style := asFourPoint;
    ArrowEnd.Color := clAkGreenLight;
    ArrowEnd.Size := 8;
    ArrowEnd.Pen.Width := 1;
    ArrowEnd.Pen.Color   := clAkGreenLight;

    ArrowStart.Style := asNone;
    //ArrowStart.Color := clAkGreenLight;
    //ArrowStart.Size := 6;
    //ArrowStart.Pen.Width := 2.0;
    //ArrowStart.Pen.Color := clAkGreenLight;

    r := Round(amp * 64/100 + 3); //��� ������=3, ���� ������=67, ��� ����=0-100
    x0 := 70;
    y0 := 70;

    x := Round(x0 + r * Sin(theta));
    y := Round(y0 - r * Cos(theta));

    //SetPoints([FixedPoint(70,74), FixedPoint(70,72-r)]);
    SetPoints([FixedPoint(70,70), FixedPoint(x,y)]);

    Draw(FVectorLayer.Bitmap, 3, clAkGreenLight);
  finally
    free;
  end;
end;

procedure TAccelerometer.DrawCircle(amp: real);
var
  r: UInt16;
  aofp: TArrayOfFloatPoint;
begin
  r := Round(amp * 64/100 + 3); //��� ������=3, ���� ������=67, ��� ����=0-100

  aofp := Ellipse(70, 70, r, r);
  PolyLineFS(FVectorLayer.Bitmap, aofp, clAkGreenLight, True, 5);
end;

procedure TAccelerometer.LoadPNGFromResource(Dst: TBitmap32; ResName: string);
var
  aResStream: TResourceStream;
  aPNG32: TPortableNetworkGraphic32;
begin
  aResStream := TResourceStream.Create(HInstance, ResName, RT_RCDATA);
  try
    aPNG32 := TPortableNetworkGraphic32.Create;
    try
      aPNG32.LoadFromStream(aResStream);
      aPNG32.AssignTo(Dst);
    finally
      aPNG32.Free;
    end;
  finally
    aResStream.Free;
  end
  ;
end;

procedure TAccelerometer.SetAngle(const Value: real);
begin
  if Value <> FAngle then begin
    FAngle := Value;
    BeginDrawing;
    DrawCircle(FModule);
    DrawArrow(FModule, FAngle);
    EndDrawing;
  end;
end;

procedure TAccelerometer.SetModule(const Value: real);
begin
  if Value <> FModule then begin
    if Value>100 then
      FModule := 100
    else
      FModule := Value
    ;
    BeginDrawing;
    DrawCircle(FModule);
    DrawArrow(FModule, FAngle);
    EndDrawing;
  end;
end;

procedure TAccelerometer.SetXValue(const Value: real);
begin
  if Value <> FXValue then begin
    FXValue := Value;

    FModule := Sqrt(Sqr(FXValue) + Sqr(FYValue));
    FAngle  := ArcTan2(FYValue, FXValue);

    BeginDrawing;
    DrawCircle(FModule);
    DrawArrow(FModule, FAngle);
    EndDrawing;
  end;
end;

procedure TAccelerometer.SetYValue(const Value: real);
begin
  FYValue := Value;
end;

end.
