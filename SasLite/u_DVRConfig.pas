unit u_DVRConfig;

interface

uses
  i_ConfigDataProvider,
  i_ConfigDataWriteProvider,
  i_DVRConfig,
  u_ConfigDataElementBase;

type
  TDVRConfig = class(TConfigDataElementBase, IDVRConfig)
  private
    FSnapShotFolder: string;
    FUseVideoAccel: boolean;
  protected
    procedure DoReadConfig(const AConfigData: IConfigDataProvider); override;
    procedure DoWriteConfig(const AConfigData: IConfigDataWriteProvider); override;
  private
    function GetSnapShotFolder: string;
    procedure SetSnapShotFolder(AValue: string);

    function  GetUseVideoAccel: boolean;
    procedure SetUseVideoAccel(AValue: boolean);
  public
    constructor Create;
  end;

implementation

uses
  u_ConfigProviderHelpers;

{ TDVRConfig }

constructor TDVRConfig.Create;
begin
  inherited Create;
  FSnapShotFolder := 'c:\';
  FUseVideoAccel := True;
end;

procedure TDVRConfig.DoReadConfig(const AConfigData: IConfigDataProvider);
begin
  inherited;
  if AConfigData <> nil then begin
    FSnapShotFolder := AConfigData.ReadString('SnapShotFolder', FSnapShotFolder);
    FUseVideoAccel := AConfigData.ReadBool('UseVideoAccel', FUseVideoAccel);
    SetChanged;
  end;
end;

procedure TDVRConfig.DoWriteConfig(
  const AConfigData: IConfigDataWriteProvider
);
begin
  inherited;
  AConfigData.WriteAnsiString('SnapShotFolder', FSnapShotFolder);
  AConfigData.WriteBool      ('UseVideoAccel',  FUseVideoAccel);
end;

function TDVRConfig.GetSnapShotFolder: string;
begin
  LockRead;
  try
    Result := FSnapShotFolder;
  finally
    UnlockRead;
  end;
end;

function TDVRConfig.GetUseVideoAccel: boolean;
begin
  LockRead;
  try
    Result := FUseVideoAccel;
  finally
    UnlockRead;
  end;
end;

procedure TDVRConfig.SetSnapShotFolder(AValue: string);
begin
  LockWrite;
  try
    if FSnapShotFolder <> AValue then begin
      FSnapShotFolder := AValue;
      SetChanged;
    end;
  finally
    UnlockWrite;
  end;
end;

procedure TDVRConfig.SetUseVideoAccel(AValue: boolean);
begin
  LockWrite;
  try
    if FUseVideoAccel <> AValue then begin
      FUseVideoAccel := AValue;
      SetChanged;
    end;
  finally
    UnlockWrite;
  end;
end;

end.
