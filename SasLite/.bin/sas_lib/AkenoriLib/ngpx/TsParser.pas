unit TsParser;

interface

uses
  SysUtils,
  Classes,
  i_DVRGPSLib,
  DateUtils;

type
  TTsHeader = record
    SyncByte: Byte;
    TransportError: boolean;
    PayloadUnitStart: boolean;
    TransportPriority: boolean;
    PID: Word;
    TransportScramblingControl: Byte;
    AdaptationFieldControl: Byte;
    AdaptationFieldLength: Byte;
    ContinuityCounter: Byte;
  end;

  TTsPayload = array [0..187] of Byte;

  TTsPacket = record
    Header: TTsHeader;
    Payload: TTsPayload;
  end;

  // *****[Below]
  TGpsPrivateData = record
    size: Int32;
    flags: Word;
    latitude: Double;
    longitude: Double;
    altitude: Double;
    speed: Single;
    bearing: Single;
    accuracy: Single;
    timestamp: Int64;
  end;
  // *****[Below]
  TSensorPrivateData = record
    accelX: Single;
    accelY: Single;
    accelZ: Single;
    status: Byte;
    res: array [0..2] of Byte;
  end;
  // *****[Below]
  TAmpdHeader = record
    ampd: Int32;
    size: Int32;
    flags1: Int32;
    flags2: Int32;
  end;
  // *****[Below]
  TPesHeader = record
    id: Word;
    size: Byte;
  end;



  TTsParser = class
  private
    fStream: TFileStream;
    fCurFrameNum: Integer;
  public
    constructor Create(const AFileName: string);
    destructor Destroy; override;
    function GetNextFrame(out AFrame: TTsPacket): boolean;
    function GetTrackSeg: TTrackSeg;
  end;

implementation

{ TTsParser }

constructor TTsParser.Create(const AFileName: string);
begin
  inherited Create;

  fCurFrameNum := 0;

  fStream := TFileStream.Create(AFileName, fmOpenRead);
end;

destructor TTsParser.Destroy;
begin
  fStream.Free;

  inherited;
end;

function TTsParser.GetNextFrame(out AFrame: TTsPacket): boolean;
var
  i: integer;
  FrameArr: array [0..187] of Byte;
begin
  Result := false;

  if (fStream.Size - fStream.Position) >= SizeOf(FrameArr) then begin
    Result := true;
    fStream.ReadBuffer(FrameArr, SizeOf(FrameArr));

    AFrame.Header.SyncByte := FrameArr[0];
    AFrame.Header.TransportError    := ((FrameArr[1] and $80) <> 0);
    AFrame.Header.PayloadUnitStart  := ((FrameArr[1] and $40) <> 0);
    AFrame.Header.TransportPriority := ((FrameArr[1] and $20) <> 0);
    AFrame.Header.PID := ((FrameArr[1] and $1F) shl 8) + FrameArr[2];
    AFrame.Header.TransportScramblingControl := FrameArr[3] shr 6;
    AFrame.Header.AdaptationFieldControl     := (FrameArr[3] and $30) shr 4;
    AFrame.Header.ContinuityCounter          := FrameArr[3] and $0F;

    if      (AFrame.Header.AdaptationFieldControl = 2) then
      AFrame.Header.AdaptationFieldLength := 184
    else if (AFrame.Header.AdaptationFieldControl = 3) then
      AFrame.Header.AdaptationFieldLength := 1+FrameArr[4]
    else
      AFrame.Header.AdaptationFieldLength := 0
    ;

    //for i:= 4 to 187 do AFrame.Payload[187-i] := FrameArr[i];
    //for i:= 4 to 187 do AFrame.Payload[i-4] := FrameArr[i];
    for i:= 0 to 187 do AFrame.Payload[i] := FrameArr[i];
  end
  ;
end;

function TTsParser.GetTrackSeg: TTrackSeg;
var
  i: integer;
  ATsPacket: TTsPacket;
  pDbl: ^Double;
  pSngl: ^Single;
  pUInt64: ^UInt64;
  //aErr: boolean;

  // *****[Below]
  cnt: Integer;
  cntMax: Integer;
  pPesHdr: ^TPesHeader;
  pAmpdHdr: ^TAmpdHeader;
  pGpsPD: ^TGpsPrivateData;
  pSensPD: ^TSensorPrivateData;
  trackPointTmp : TTrackPoint;
begin
  fStream.Position := 0;
  i := 0;

  while GetNextFrame(ATsPacket) do
  begin
    if (ATsPacket.Header.SyncByte = $47) and (ATsPacket.Header.PID = $102) then
    begin
      //i := i + 1;
      //SetLength(Result, i);
      //with Result[i-1] do
      with trackPointTmp do
      begin
        Latitude  := 99.99;//0.0
        Longitude := 199.99;//0.0
        Altitude :=  0.0;
        Speed :=     0.0;
        Course :=    0.0;
        Accuracy :=  0.0;
        Time := IncSecond(EncodeDateTime(1970, 01, 01, 0, 0, 0, 0), i);

        cnt:= 4;// header
        cnt:= cnt + 2;// adaptation field
        cnt:= cnt + ATsPacket.Header.AdaptationFieldLength + 1;// adaptation field
        cntMax:= cnt;
        pPesHdr:= @ATsPacket.Payload[cnt];
        cnt:= cnt + 16;// PES header
        pAmpdHdr:= @ATsPacket.Payload[cnt];
        cntMax:= cntMax + 3 + pPesHdr.size;// PES header
        cnt:= cnt + SizeOf(TAmpdHeader);// AMPD header
        repeat
          if(pAmpdHdr.size = 56) then
          begin
            pGpsPD:= @ATsPacket.Payload[cnt];
            cnt:= cnt + SizeOf(TGpsPrivateData);

            Latitude:= pGpsPD.latitude;
            Longitude:= pGpsPD.longitude;
            Altitude:= pGpsPD.altitude;
            Speed:= Round(3.6 * pGpsPD.speed);
            Course:= pGpsPD.bearing;
            Accuracy:= pGpsPD.accuracy;
            //Time:= pGpsPD.timestamp;
            Time := EncodeDateTime(1970, 01, 01, 0, 0, 0, 0);
            Time := IncMilliSecond(Time, pGpsPD.timestamp);
          end
          else if(pAmpdHdr.size = 16) then
          begin
            pSensPD:= @ATsPacket.Payload[cnt];
            cnt:= cnt + SizeOf(TSensorPrivateData);

            AccelX:= pSensPD.accelX;
            AccelY:= pSensPD.accelY;
            AccelZ:= pSensPD.accelZ;
          end;
          pAmpdHdr:= @ATsPacket.Payload[cnt];
          cnt:= cnt + SizeOf(TAmpdHeader);// AMPD header
        until(cnt >= cntMax);

      end;

      if ((trackPointTmp.Latitude < 99.99) and (trackPointTmp.Longitude < 199.99)) then
      begin
        i := i + 1;
        SetLength(Result, i);
        Result[i-1]:= trackPointTmp;
      end
      ;
    end;
    ;
  end;
end;

end.

