// CodeGear C++Builder
// Copyright (c) 1995, 2007 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Tb2k_d11.pas' rev: 11.00

#ifndef Tb2k_d11HPP
#define Tb2k_d11HPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Tb2version.hpp>	// Pascal unit
#include <Tb2toolbar.hpp>	// Pascal unit
#include <Tb2consts.hpp>	// Pascal unit
#include <Tb2dock.hpp>	// Pascal unit
#include <Tb2extitems.hpp>	// Pascal unit
#include <Tb2item.hpp>	// Pascal unit
#include <Tb2common.hpp>	// Pascal unit
#include <Tb2hook.hpp>	// Pascal unit
#include <Tb2toolwindow.hpp>	// Pascal unit
#include <Tb2mru.hpp>	// Pascal unit
#include <Tb2anim.hpp>	// Pascal unit
#include <Tb2mdi.hpp>	// Pascal unit
#include <Tb2acc.hpp>	// Pascal unit
#include <Variants.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit
#include <Varutils.hpp>	// Pascal unit
#include <Typinfo.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <Inifiles.hpp>	// Pascal unit
#include <Registry.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Multimon.hpp>	// Pascal unit
#include <Actnlist.hpp>	// Pascal unit
#include <Helpintfs.hpp>	// Pascal unit
#include <Printers.hpp>	// Pascal unit
#include <Flatsb.hpp>	// Pascal unit
#include <Uxtheme.hpp>	// Pascal unit
#include <Themes.hpp>	// Pascal unit
#include <Dwmapi.hpp>	// Pascal unit
#include <Graphutil.hpp>	// Pascal unit
#include <Extctrls.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit
#include <Clipbrd.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Menus.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Tb2k_d11
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------

}	/* namespace Tb2k_d11 */
using namespace Tb2k_d11;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Tb2k_d11
