unit Speedo;

interface

uses
  System.SysUtils, System.Classes, System.Types,
  Vcl.Controls, Vcl.Graphics,
  GR32_Image, GR32_Layers, GR32;

type
  TSpeedometer = class(TCustomImage32)
  private
    { Private declarations }
    fBitmapList: TBitmap32List;
    FArrowLayer: TBitmapLayer;
    FValue: UInt16;
    procedure SetValue(const Value: UInt16);
    procedure LoadPNGFromResource(Dst: TBitmap32; ResName: string);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property Value: UInt16 read FValue write SetValue default 0;
  end;

procedure Register;

implementation

uses
  Math, GR32_PNG;

{$R 'speedo.res'}

{$I ..\AkenIndi.inc}

const
  cGradNum = 90;
  cMaxVal  = 240;

procedure Register;
begin
  RegisterComponents('Graphics32', [TSpeedometer]);
end;

{ TSpeedometer }

constructor TSpeedometer.Create(AOwner: TComponent);
var
  aBM32: TBitmap32;
  i: integer;
begin
  inherited;

  AutoSize := true;
  FArrowLayer := TBitmapLayer.Create(Layers);
  fBitmapList := TBitmap32List.Create(Self);

  LoadPNGFromResource(Bitmap, 'SPEEDOBACK');

  for i := 0 to cGradNum-1 do begin
    aBM32 := fBitmapList.Bitmaps.Add.Bitmap;
    LoadPNGFromResource(aBM32, 'arrow_'+Format('%2.2d', [i]));
  end
  ;

  Value := 0;
end;

{
constructor TSpeedometer.Create(AOwner: TComponent);
var
  aPngImg: TPngImage;
  aBM32: TBitmap32;
  i: integer;
begin
  inherited;

  //Height := 129;
  //Width := 257;
  AutoSize := true;

  FArrowLayer := TBitmapLayer.Create(Layers);

  fBitmapList := TBitmap32List.Create(Self);

  aPngImg := TPngImage.Create;
  try
    aPngImg.LoadFromResourceName(HInstance, 'SPEEDOBACK');
    Bitmap.Assign(aPngImg);

    for i := 0 to cGradNum-1 do begin
      aPngImg.LoadFromResourceName(HInstance, 'arrow_'+Format('%2.2d', [i]));
      aBM32 := fBitmapList.Bitmaps.Add.Bitmap;
      aBM32.Assign(aPngImg);

      IntensityToAlpha(aBM32 , aBM32);
      Invert(aBM32 , aBM32, [ccAlpha]);
    end
    ;
  finally
    aPngImg.Free;
  end;

  Value := 0;
end;
}

destructor TSpeedometer.Destroy;
begin
  //FArrowLayer.Free;

  inherited;
end;

procedure TSpeedometer.LoadPNGFromResource(Dst: TBitmap32; ResName: string);
var
  aResStream: TResourceStream;
  aPNG32: TPortableNetworkGraphic32;
begin
  aResStream := TResourceStream.Create(HInstance, ResName, RT_RCDATA);
  try
    aPNG32 := TPortableNetworkGraphic32.Create;
    try
      aPNG32.LoadFromStream(aResStream);
      aPNG32.AssignTo(Dst);
    finally
      aPNG32.Free;
    end;
  finally
    aResStream.Free;
  end
  ;
end;

procedure TSpeedometer.SetValue(const Value: UInt16);
var
  idx: integer;
  L: TFloatRect;
begin
  FValue := Min(cMaxVal, Value);
  idx := Round((cGradNum-1) * FValue / cMaxVal);

  BeginUpdate;

  with FArrowLayer do begin
    Bitmap := fBitmapList.Bitmaps[idx].Bitmap;

    Bitmap.DrawMode := dmBlend;
    Bitmap.MasterAlpha := 255;

    //Bitmap.DrawMode := dmTransparent;
    //Bitmap.OuterColor := clWhite32;

    L.Left := 0;
    L.Top := 0;
    L.Right := Bitmap.Width;
    L.Bottom := Bitmap.Height;
    Location := L;
  end;

  EndUpdate;
  Invalidate;
end;

end.
