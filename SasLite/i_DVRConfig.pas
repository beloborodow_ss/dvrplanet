unit i_DVRConfig;

interface

uses
  i_ConfigDataElement;
  {_Bitmap32Static,
  i_ActiveMapsConfig,
  i_ViewPortState,
  i_NavigationToPoint,
  i_MainFormBehaviourByGPSConfig,
  i_MainGeoCoderConfig,
  i_KeyMovingConfig,
  i_MapMovingConfig,
  i_MapZoomingConfig,
  i_DownloadUIConfig,
  i_WindowPositionConfig,
  i_LastSearchResultConfig,
  i_MainFormLayersConfig;}

type
  IDVRConfig = interface(IConfigDataElement)
    ['{527490C4-8A55-444E-B510-4D2894D3C7B6}']
    function  GetSnapShotFolder: string;
    procedure SetSnapShotFolder(AValue: string);
    property  SnapShotFolder: string read GetSnapShotFolder write SetSnapShotFolder;

    function  GetUseVideoAccel: boolean;
    procedure SetUseVideoAccel(AValue: boolean);
    property  UseVideoAccel: boolean read GetUseVideoAccel write SetUseVideoAccel;
  end;

implementation

end.
