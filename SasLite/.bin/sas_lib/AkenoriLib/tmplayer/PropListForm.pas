unit PropListForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.ValEdit, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls;

type
  TfmPropList = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    ValueListEditor1: TValueListEditor;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Clear;
    function ParseLine(const LineToParse: string): boolean;
    function ValByName(const AName: string): string;
    function NameExist(const AName: string): boolean;
  end;

var
  fmPropList: TfmPropList;

implementation

{$R *.dfm}

{ TfmPropList }

procedure TfmPropList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

procedure TfmPropList.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmPropList.Clear;
begin
  ValueListEditor1.Strings.Clear;
end;

function TfmPropList.NameExist(const AName: string): boolean;
var i: integer;
begin
  Result :=  ValueListEditor1.FindRow(AName, i);
end;

function TfmPropList.ParseLine(const LineToParse: string): boolean;

  function VO: boolean;
  var
    ltp: string;
    P, I, J, R: integer;
  begin  // VO: [directx] 352x240 => 352x240 Planar YV12
    Result := false;
    ltp := LineToParse;
    if not(Copy(ltp, 1, 5) = 'VO: [') then Exit;

    P := Pos(' => ', ltp);
    if P = 0 then Exit;
    System.Delete(ltp, 1, P + 3);
    P := Pos(' ', ltp);
    if P = 0 then Exit;

    SetLength(ltp, P - 1); //��������� �� �������?
    P := Pos('x', ltp);
    if P = 0 then Exit;
    Val(Copy(ltp, 1, P - 1), I, R);

    if not(R = 0) or (I < 16) or not(I < 4096) then Exit;

    Val(Copy(ltp, P + 1, 5), J, R);
    if not(R = 0) or (J < 16) or not(J < 4096) then Exit;

    ValueListEditor1.InsertRow('VIDEONATIVE_WIDTH', IntToStr(I), true);
    ValueListEditor1.InsertRow('VIDEONATIVE_HEIGHT', IntToStr(J), true);

    Result := true;
  end;

var
   InputLine, KName, KValue: string;
   L, P: integer;
begin
  InputLine := Trim(LineToParse);
  Result := VO;
  if Result then Exit;

  L := Length(InputLine);
  if L < 6 then Exit; // min. 'ID_?=?'
  if not(Copy(InputLine, 1, 3) = 'ID_') then Exit;
  P := Pos('=', InputLine);
  if P < 5 then Exit;
  if P = L then Exit;
  KName := Trim(Copy(InputLine, 1, P - 1));
  System.Delete(KName, 1, 3);

  System.Delete(InputLine, 1, P);
  KValue := Trim(InputLine);
  if KValue = '' then Exit;

  ValueListEditor1.InsertRow(KName, KValue, true);

  Result := true;
end;

function TfmPropList.ValByName(const AName: string): string;
begin
  Result := ValueListEditor1.Values[AName];
end;

end.
