﻿1. Клонируем репозиторий в папку: $(BDS)\lib_sas
    где, $(BDS) - путь до установленной Delphi, например:
    - c:\Program Files\CodeGear\RAD Studio\5.0\ (Delphi 2007)
    - c:\Program Files\Embarcadero\RAD Studio\9.0\ (Delphi XE2)
    (в этой папке должны быть такие папки как bin, lib и т.д)

2. В IDE дописываем пути к компонентам (Tools->Options->Library Win32->Library path):
$(BDS)\lib_sas\alcinoe\source;$(BDS)\lib_sas\graphics32\Source;$(BDS)\lib_sas\tb2k;$(BDS)\lib_sas\tbx;$(BDS)\lib_sas\tb2k\source;$(BDS)\lib_sas\EmbeddedWB\source;$(BDS)\lib_sas\vpr;$(BDS)\lib_sas\PascalScript\Source;$(BDS)\lib_sas\vsagps\Runtime;$(BDS)\lib_sas\vsagps\Public;$(BDS)\lib_sas\ccr-exif

3. Устанавливаем дизайн-тайм компонеты в следующем порядке:
Delphi 2007:
  - $(BDS)\lib_sas\EmbeddedWB\Packages\EmbeddedWebBrowser_D2007.dpk
  - $(BDS)\lib_sas\graphics32\Source\Packages\GR32_DSGN_RS2007.dpk
  - $(BDS)\lib_sas\tb2k\Packages\tb2kdsgn_d11.dpk
  - $(BDS)\lib_sas\tbx\Packages\tbxdsgn_d11.dpk
Delphi XE2:
  - $(BDS)\lib_sas\EmbeddedWB\Packages\EmbeddedWebBrowser_XE2.dpk (EmbeddedWebBrowser_XE2.bpl -> Install)
  - $(BDS)\lib_sas\graphics32\Source\Packages\GR32_DSGN_RSXE2_INSTALL.groupproj (GR32_DSGN_RSXE2.bpl -> Install)
  - $(BDS)\lib_sas\tb2k\Packages\tb2kdsgn_d16_install.groupproj (tb2kdsgn_d16.bpl -> Install)
  - $(BDS)\lib_sas\tbx\Packages\TBX_DXE2_Design_Install.groupproj (TBX_DXE2_Design.bpl -> Install)