unit TimeSliderLib;

interface

uses
   Classes, System.SysUtils;

type

  TTSVertScale = record
    MaxSpeed:  Integer;
    Steps:     Integer;
    MaxAccel:  Integer;
  end;

  TTSHorzScale = record
    Duration:  Integer;  //in seconds
    Steps:     Integer;
  end;

  TTSDataSample = record
    Speed: Real;
    Accel: Real;
  end;

  TTSChartData = class
  private
    FData: array of TTSDataSample;
    FTimeStep: Real;
    FMaxSpeed: Real;
    FMaxAccel: Real;
    function GetCount: UInt16;
    function GetDataSample(Index: Integer): TTSDataSample;
    function GetMaxTime: Real;
  public
    procedure GenerateTestData(TestAmpl: Real = 176.0; SampleCount: UInt16 = 1000);
    procedure SetData(aData: array of TTSDataSample; aTimeStep: Real);
    function GetVertScales(ScaleLength, MinLabelHeight: Integer): TTSVertScale;
    function GetHorzScale(ScaleLength, MinLabelWidth: Integer): TTSHorzScale;
    function GetNearestTime(aPos: Real): Real;

    property Count:    UInt16  read GetCount;
    property TimeStep: Real read FTimeStep;
    property MaxSpeed: Real read FMaxSpeed;
    property MaxAccel: Real read FMaxAccel;
    property MaxTime: Real read GetMaxTime;
    property DataSample[Index: Integer]: TTSDataSample read GetDataSample;

    constructor CreateChartData;
    destructor Destroy; override;
  end;

implementation
uses
  Math;

function GetDecRoundedVertScale(MaxDataValue: Real; ScaleLength, MinLabelHeight: Integer): TTSVertScale;
var
  MinLabelHeightBV: Real;
  MinDecRoundStep: Integer;
  NSteps: Integer;
begin
  MaxDataValue := Max(MaxDataValue, 1);

  if ScaleLength > 0 then begin
    MinLabelHeightBV := MinLabelHeight * MaxDataValue / ScaleLength;
    MinDecRoundStep := Ceil(MinLabelHeightBV / 10) * 10; //13 -> 20; 10 -> 10; 10.1 -> 20
    NSteps := Ceil(MaxDataValue / MinDecRoundStep);

    with Result do begin
      MaxSpeed := MinDecRoundStep * NSteps;
      Steps := NSteps;
      MaxAccel := 10;
      while (MaxAccel mod Steps <> 0) do
        inc(MaxAccel)
      ;
    end
    ;
  end else begin //������� �������: ����� ���-�� ����?
    with Result do begin
      MaxSpeed := Ceil(MaxDataValue/10)*10;
      Steps := 1;
      MaxAccel := 10;
    end
    ;
  end
  ;
end;

function GetDecRoundedHorzScale(MaxDataValue: Real; ScaleLength, MinLabelWidth: Integer): TTSHorzScale;
var
  MinLabelWidthByVal: Real;
  MinDecRoundStep: Integer;
  NSteps: Integer;
begin
  MaxDataValue := Max(MaxDataValue, 1);

  if ScaleLength > 0 then begin
    MinLabelWidthByVal := MinLabelWidth * MaxDataValue / ScaleLength;
    MinDecRoundStep := Ceil(MinLabelWidthByVal / 10) * 10; //13 -> 20; 10 -> 10; 10.1 -> 20
    NSteps := Ceil(MaxDataValue / MinDecRoundStep);

    with Result do begin
      Duration := MinDecRoundStep * NSteps;
      Steps := NSteps;
    end
    ;
  end else begin  //������� �������: ����� ���-�� ����?
    with Result do begin
      Duration := Ceil(MaxDataValue/10)*10;
      Steps := 1;
    end
    ;
  end
  ;
end;

{ TTSChartData }

constructor TTSChartData.CreateChartData;
begin
  FData := nil;
  FMaxSpeed := 0;
  FMaxAccel := 0;

  GenerateTestData(176, 500);
end;

destructor TTSChartData.Destroy;
begin

  inherited;
end;

procedure TTSChartData.GenerateTestData(TestAmpl: Real = 176.0; SampleCount: UInt16 = 1000);
var
  i: Integer;
  x, xDiap, xMult: Real;
const
  cSpan = 20.0;
begin
  SetLength(FData, SampleCount);
  FTimeStep := 0.1;
  xDiap := SampleCount*FTimeStep;
  xMult := cSpan / xDiap;

  for i := 0 to High(FData) do begin
    x := (i - High(FData) div 2) * xMult;
    if x=0 then x := FTimeStep;

    FData[i].Speed := TestAmpl * (Sin(x)/x + 0.3)/1.3;
    FData[i].Accel := 10.0 * (Sin(x)/x + 0.2)/1.2;
  end;

  FMaxSpeed := TestAmpl;
  FMaxAccel := 10.0;
end;

function TTSChartData.GetCount: UInt16;
begin
  Result := Length(FData);
end;

function TTSChartData.GetDataSample(Index: Integer): TTSDataSample;
begin
  if (Index > High(FData)) or (Index < 0) then raise Exception.Create('Property index is out of bounds!');
  Result := FData[Index];
end;

function TTSChartData.GetHorzScale(ScaleLength, MinLabelWidth: Integer): TTSHorzScale;
begin
  Result := GetDecRoundedHorzScale(Count*TimeStep, ScaleLength, MinLabelWidth);
end;

function TTSChartData.GetMaxTime: Real;
begin
  Result := Count*TimeStep;
end;

function TTSChartData.GetNearestTime(aPos: Real): Real;
begin
  if aPos < 0 then
    Result := 0
  else if aPos > MaxTime then
    Result := MaxTime
  else
    Result := Round(aPos / TimeStep)*TimeStep;
  ;
end;

function TTSChartData.GetVertScales(ScaleLength, MinLabelHeight: Integer): TTSVertScale;
begin
  Result := GetDecRoundedVertScale(MaxSpeed, ScaleLength, MinLabelHeight);
end;

procedure TTSChartData.SetData(aData: array of TTSDataSample;
  aTimeStep: Real);
var
  i: integer;
begin
  SetLength(FData, Length(aData));
  FTimeStep := aTimeStep;
  FMaxSpeed := 0;
  FMaxAccel := 0;
  for i := 0 to High(FData) do begin
    FData[i].Speed := Abs(aData[i].Speed);
    FData[i].Accel := Abs(aData[i].Accel);
    FMaxSpeed := Max(FMaxSpeed, FData[i].Speed);
    FMaxAccel := Max(FMaxAccel, FData[i].Accel);
  end
  ;
end;

initialization

end.
