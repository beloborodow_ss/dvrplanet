unit u_DvrPlayList;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  StrUtils,
  SyncObjs,
  mplayer_lib,
  i_DVRGPSLib,
  TsParser,
  fr_DvrPlayList,
  fr_DvrButtons,
  DateUtils, Math,
  Vcl.ActnList,
  i_DVRConfig,
  AbBar, AbRMeter, AbVCInd, _GClass, AbVBar,
  Speedo, Accel, TimeSlider, TimeSliderLib,
  GpxHelperUnit,
  gnugettext;

type
  TDVRTrackStatus = (dtsNew, dtsFailed, dtsGPSPrepared, dtsGPSMissing, dtsVideoOnly, dtsCompleteTrack);

  TDVRTrack = class
  strict private
    FLock: TCriticalSection;
    FVideoFile: string;
    FGPSData: TTrackSeg;
    FMapProvider: IDVRMapProvider;
    FStatus: TDVRTrackStatus;
    FStartTime: TDateTime;
    FDistance: UInt16;
    FMaxAccel: real;
    FDuration: UInt16;
    FMaxSpeed: real;
    FAvgSpeed: real;
    procedure CalcMaxMinAvg;
    function GetCount: UInt16;
    function GetFileExt: string;
    function GetStatus: TDVRTrackStatus;
    procedure SetStatus(aNewStatus: TDVRTrackStatus);

    procedure InterpolateGPSData;
  public
    FOwner: TListItem;
    procedure Lock;
    function TryLock: boolean;
    procedure UnLock;
    property Status: TDVRTrackStatus read GetStatus write SetStatus;
    property VideoFile: string read FVideoFile;
    property FileExt: string read GetFileExt;
    property GPSData: TTrackSeg read FGPSData;
    property Count: UInt16 read GetCount;
    property StartTime: TDateTime read FStartTime;
    property Duration: UInt16 read FDuration;
    property Distance: UInt16 read FDistance;
    property AvgSpeed: real read FAvgSpeed;
    property MaxSpeed: real read FMaxSpeed;
    property MaxAccel: real read FMaxAccel;
    function PrepareGPSData: TDVRTrackStatus;
    procedure ReportWrongVideo(const aErr: string);
    constructor Create(AOwner: TListItem; const AVideoFile: string; aMapProvider: IDVRMapProvider);
    destructor Destroy; override;
  end;

  TDvrPlayList = class;

  TBackgroundThread = class(TThread)
  strict private
    FOwner: TDvrPlayList;
  public
    constructor CreateBackgroundThread(Owner: TDvrPlayList);
    procedure Execute; override;
  end;


  TDvrPlayList = class
  strict private
    { Private declarations }
    FPlayer: TMPlayer;
    FDVRListFrame: TfrDVRPlayList;
    FDVRButtonsFrame: TfrDVRButtons;

    //-----------------------------
    FMapProvider: IDVRMapProvider;
    FGPSCursorDrawer: IDVRCursorDrawer;
    FGPSRingsDrawer: IDVRCursorDrawer;
    FDVRConfig: IDVRConfig;
    //-----------------------------
    FTimeSlider: TTimeSlider;

    FSpeedometer: TSpeedometer;
    FAccelerometer: TAccelerometer;
    FAccelZInd: TAbVBar;
    //-----------------------------
    FCurNPoint: integer;
    FCurGPSTrack: TTrackSeg;
    //FCurDVRTrack: IVectorDataItemLine; //? ���� �� ���� �����...
   //-----------------------------
    procedure lvSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure staOpenExecute(Sender: TObject);

    procedure staClearExecute(Sender: TObject);
    procedure staClearUpdate(Sender: TObject);

    procedure staDeleteExecute(Sender: TObject);
    procedure staDeleteUpdate(Sender: TObject);

    procedure staScreenShotExecute(Sender: TObject);

    procedure staPlayPauseExecute(Sender: TObject);
    procedure staPlayPauseUpdate(Sender: TObject);

    procedure staMuteExecute(Sender: TObject);
    procedure staMuteUpdate(Sender: TObject);

    procedure staFullScreenExecute(Sender: TObject);

    procedure staStepForwardExecute(Sender: TObject);
    procedure staStepForwardUpdate(Sender: TObject);

    procedure staStepBackwardExecute(Sender: TObject);
    procedure staStepBackwardUpdate(Sender: TObject);

    procedure staNextTrackExecute(Sender: TObject);
    procedure staNextTrackUpdate(Sender: TObject);

    procedure staPrevTrackExecute(Sender: TObject);
    procedure staPrevTrackUpdate(Sender: TObject);


    procedure staUniNPUpdate(Sender: TObject);
    procedure staUniUpdate(Sender: TObject);

    procedure staSpeed025Execute(Sender: TObject);
    procedure staSpeed05Execute(Sender: TObject);
    procedure staSpeedNormalExecute(Sender: TObject);
    procedure staSpeedx2Execute(Sender: TObject);
    procedure staSpeedx4Execute(Sender: TObject);
    procedure staSpeedXUpdate(Sender: TObject);

    procedure SliderUpdate(const NewPos: Real);
    procedure PlayerChangePosition(const PosMS: Cardinal; const PosPC: Real);
    procedure TimeSliderUserChange(Sender: TObject; const NewPosition: Real);
    //-----------------------------
    function MsecElapsed(APointNum: integer): Cardinal;
    function FileFormatSupported(aFileName: string): boolean;
    procedure StopPlayback;
    procedure PlayItem(aItem: TListItem);
    procedure PlaybackCompleeted(Sender: TObject);
    procedure BeginFromTheBegining;
    procedure BeginFrom(idx: integer);
    procedure PlotSpeedAndAccelChart;
  public
    { Public declarations }
    function AddFiles(aPlayList: TStrings): boolean; overload;
    function AddFiles(aPlayList: array of string): boolean; overload;
    function Get1stUnprepTrack: TDVRTrack;
    procedure UpdateItemView(aItem: TListItem);
    procedure PlayDemo;
    constructor Create(
      aMapProvider: IDVRMapProvider;
      aGPSCursorDrawer, aGPSRingsDrawer: IDVRCursorDrawer;
      aDVRConfig: IDVRConfig;
      aPlayer: TMPlayer;
      aDVRListFrame: TfrDVRPlayList;
      aDVRButtonsFrame: TfrDVRButtons;
      aTimeSlider: TTimeSlider;
      aSpeedometer: TSpeedometer;
      aAccelerometer: TAccelerometer;
      aAccelZInd: TAbVBar
    );
    destructor Destroy; override;
  end;

implementation

procedure SetSubItem(AItem: TListItem; AIndex: Integer; const AValue: string);
var
  i: Integer;
begin
  if AIndex < AItem.SubItems.Count then begin
    AItem.SubItems.Strings[AIndex] := AValue;
  end else begin
    for i := AItem.SubItems.Count to AIndex - 1 do begin
      AItem.SubItems.Add('');
    end;
    AItem.SubItems.Add(AValue);
  end;
end;

procedure ClearItem(AItem: TListItem); //������� �������???
var ATrack: TDVRTrack;
begin
  aTrack := TDVRTrack(AItem.Data);

  aTrack.Free;

  aItem.Data := nil;
end;

function GetAccelModule(aTrackPoint: TTrackPoint): real;
begin
  Result :=
    Sqrt(Sqr(aTrackPoint.AccelX - cx0)
    +    Sqr(aTrackPoint.AccelY - cy0)
    +    Sqr(aTrackPoint.AccelZ - cz0))
  ;
end;

function GetNmeaFromFile(const AFileName: string): TTrackSeg;

  function GetNthSubstr(const S: string; N: integer; Delim: string = ','): string;
  //����.: ������ � ����� ������ ��������� �� Delim
    function PosExt(const SubStr,S: string; N: Cardinal = 1): integer;
    var
      i, Offset: integer;
    begin
      Result := 0;
      Offset := 0;
      for i:=1 to N do begin
        Offset := PosEx(SubStr,S,Offset+1);
        if Offset<1 then Exit;
      end
      ;
      Result := Offset;
    end;
  var
    ibeg,iend: integer;
  begin
    Result := '';
    if (N<1) or (Length(S) < 1) or (Length(Delim) < 1) then Exit;
    ibeg := PosExt(Delim,S,N-1);
    if (ibeg<1) and (N>1) then Exit; //����� ��������� � ������� ������ 1, � ����� ������������ �� ������
    ibeg := IfThen(ibeg<1,1,ibeg+Length(Delim));
    iend := PosExt(Delim, S, N);
    iend := IfThen(iend<1, Length(S), iend-1);
    Result := Copy(S,ibeg,iend-ibeg+1);
  end;

  function GetRealFromStrDef(const StrVal: string; const DefVal: real=0.0): real;
  var
    fs: TFormatSettings;
  begin
    fs := TFormatSettings.Create; // record, so no need to free
    fs.DecimalSeparator := '.';

    Result := StrToFloatDef(StrVal, DefVal, fs);
  end;

  function GetDegreeDecMinFromNmea(AVal: string): real;
  var VDegrees, VMinutes: integer;
  begin
    Result := GetRealFromStrDef(AVal);

    VDegrees := Trunc(Result/100);

    VMinutes := Trunc(Result - VDegrees*100);

    Result := VDegrees + (VMinutes + Frac(Result))/60; //shifts two digits right and converts minutes from babylonian to arabian: f/100 * 100/60 = f/60
  end;

  function GetDateTimeFromNmea(const ADat, ATim: string): TDateTime;
  begin
    try
      Result := EncodeDateTime(StrToInt(Copy(ADat, 5, 2)), StrToInt(Copy(ADat, 3, 2)), StrToInt(Copy(ADat, 1, 2))
                             , StrToInt(Copy(ATim, 1, 2)), StrToInt(Copy(ATim, 3, 2)), StrToInt(Copy(ATim, 5, 2)), StrToInt(Copy(ATim, 8, 2))*10);
    except
      Result := 0.0;
    end;
  end;


var
  VSL: TStringList;
  i, j: integer;
begin
  Result := nil;
  VSL := TStringList.Create;
  try
    VSL.LoadFromFile(AFileName);
    SetLength(Result, VSL.Count);
    j := 0;
    for i:=0 to VSL.Count-1 do
      if (GetNthSubstr(VSL.Strings[i], 1) = '$GPRMC') and (GetNthSubstr(VSL.Strings[i], 3) = 'A') then begin
        with Result[j] do begin
          Latitude := GetDegreeDecMinFromNmea(GetNthSubstr(VSL.Strings[i], 4));
          Longitude := GetDegreeDecMinFromNmea(GetNthSubstr(VSL.Strings[i], 6));
          Time := GetDateTimeFromNmea(GetNthSubstr(VSL.Strings[i], 10), GetNthSubstr(VSL.Strings[i], 2));
          Altitude := 0.0;
          Satellites := 0;
          Speed := GetRealFromStrDef(GetNthSubstr(VSL.Strings[i], 8));
          Course := GetRealFromStrDef(GetNthSubstr(VSL.Strings[i], 9));
        end;
        inc(j);
      end
    ;
    SetLength(Result, j);
  finally
    VSL.Free;
  end;
end;

{ TDvrPlayList }

constructor TDvrPlayList.Create(
  aMapProvider: IDVRMapProvider;
  aGPSCursorDrawer, aGPSRingsDrawer: IDVRCursorDrawer;
  aDVRConfig: IDVRConfig;
  aPlayer: TMPlayer;
  aDVRListFrame: TfrDVRPlayList;
  aDVRButtonsFrame: TfrDVRButtons;
  aTimeSlider: TTimeSlider;
  aSpeedometer: TSpeedometer;
  aAccelerometer: TAccelerometer;
  aAccelZInd: TAbVBar
);
begin
  inherited Create;

  FPlayer := aPlayer;
  FDVRListFrame := aDVRListFrame;
  FDVRButtonsFrame := aDVRButtonsFrame;

  FMapProvider := aMapProvider;
  FGPSCursorDrawer := aGPSCursorDrawer;
  FGPSRingsDrawer := aGPSRingsDrawer;

  FDVRConfig := aDVRConfig;

  FTimeSlider := aTimeSlider;
  FSpeedometer := aSpeedometer;
  FAccelerometer := aAccelerometer;
  FAccelZInd := aAccelZInd;

  FDVRListFrame.lvDVRFileList.OnSelectItem := lvSelectItem;


  FDVRButtonsFrame.staAddFilesToPlayList.OnExecute := staOpenExecute;

  FDVRButtonsFrame.staClearPlayList.OnExecute := staClearExecute;
  FDVRButtonsFrame.staClearPlayList.OnUpdate := staClearUpdate;
  FDVRButtonsFrame.staRemoveFileFromPlayList.OnExecute := staDeleteExecute;
  FDVRButtonsFrame.staRemoveFileFromPlayList.OnUpdate := staDeleteUpdate;

  FDVRButtonsFrame.staPlayPause.OnExecute := staPlayPauseExecute;
  FDVRButtonsFrame.staPlayPause.OnUpdate := staPlayPauseUpdate;

  FDVRButtonsFrame.staMute.OnExecute := staMuteExecute;
  FDVRButtonsFrame.staMute.OnUpdate := staMuteUpdate;

  FDVRButtonsFrame.staScreenShot.OnExecute := staScreenShotExecute;
  FDVRButtonsFrame.staScreenShot.OnUpdate := staUniUpdate;

  FDVRButtonsFrame.staFullScreenVideo.OnExecute := staFullScreenExecute;
  FDVRButtonsFrame.staFullScreenVideo.OnUpdate := staUniNPUpdate;

  FDVRButtonsFrame.staStepForward.OnExecute  := staStepForwardExecute;
  FDVRButtonsFrame.staStepForward.OnUpdate   := staStepForwardUpdate;
  FDVRButtonsFrame.staStepBackward.OnExecute := staStepBackwardExecute;
  FDVRButtonsFrame.staStepBackward.OnUpdate  := staStepBackwardUpdate;

  FDVRButtonsFrame.staNextTrack.OnExecute := staNextTrackExecute;
  FDVRButtonsFrame.staNextTrack.OnUpdate  := staNextTrackUpdate;
  FDVRButtonsFrame.staPrevTrack.OnExecute := staPrevTrackExecute;
  FDVRButtonsFrame.staPrevTrack.OnUpdate  := staPrevTrackUpdate;

  FDVRButtonsFrame.staSpeed025.OnExecute    := staSpeed025Execute;
  FDVRButtonsFrame.staSpeed025.OnUpdate     := staSpeedXUpdate;
  FDVRButtonsFrame.staSpeed05.OnExecute     := staSpeed05Execute;
  FDVRButtonsFrame.staSpeed05.OnUpdate      := staSpeedXUpdate;
  FDVRButtonsFrame.staSpeedNormal.OnExecute := staSpeedNormalExecute;
  FDVRButtonsFrame.staSpeedNormal.OnUpdate  := staSpeedXUpdate;
  FDVRButtonsFrame.staSpeedx2.OnExecute     := staSpeedx2Execute;
  FDVRButtonsFrame.staSpeedx2.OnUpdate      := staSpeedXUpdate;
  FDVRButtonsFrame.staSpeedx4.OnExecute     := staSpeedx4Execute;
  FDVRButtonsFrame.staSpeedx4.OnUpdate      := staSpeedXUpdate;

  FPlayer.OnPlayEnd := PlaybackCompleeted;
  FPlayer.OnSliderUpdate := SliderUpdate;
  FPlayer.OnChangePosition := PlayerChangePosition;
  FPlayer.MPlayerPath := ExtractFilePath(ParamStr(0))+'mplayer';
  //FPlayer.LogVisible := true;//debug


  FTimeSlider.OnUserChange := TimeSliderUserChange;

  FCurNPoint := -1;

  //FBackgroundThread := TBackgroundThread.CreateBackgroundThread(Self);
  TBackgroundThread.CreateBackgroundThread(Self);
end;

destructor TDvrPlayList.Destroy;
begin
  //FBackgroundThread.Terminate;

  inherited;
end;

function TDvrPlayList.AddFiles(aPlayList: TStrings): boolean;
var
  i, lFirstAdded: integer;
  aNewItem: TListItem;
begin
  if FileFormatSupported(aPlayList[0]) then begin

    lFirstAdded := FDVRListFrame.lvDVRFileList.Items.Count;

    for i := 0 to aPlayList.Count-1 do begin
      aNewItem := FDVRListFrame.lvDVRFileList.Items.Add;

      aNewItem.Caption := ExtractFileName(aPlayList.Strings[i]); //���

      aNewItem.Data := TDVRTrack.Create(aNewItem, aPlayList.Strings[i], FMapProvider);

      UpdateItemView(aNewItem);
    end;

    Application.ProcessMessages;//???

    //BeginFromTheBegining;
    BeginFrom(lFirstAdded);

    Result := true;
  end else
    Result := false
  ;
end;

function TDvrPlayList.AddFiles(aPlayList: array of string): boolean; //overloaded version for demo playback support
var
  i: integer;
  sl: TStringList;
begin
  if Length(aPlayList)<1 then Exit;

  sl := TStringList.Create;
  try
    for i := Low(aPlayList) to High(aPlayList) do
      sl.Add(aPlayList[i])
    ;
    Result := AddFiles(sl);
  finally
    sl.Free;
  end;
end;

procedure TDvrPlayList.BeginFrom(idx: integer);
begin
  if FDVRListFrame.lvDVRFileList.Items.Count > idx then
    FDVRListFrame.lvDVRFileList.Items[idx].Selected := true
  ;
end;

procedure TDvrPlayList.BeginFromTheBegining;
begin
  if FDVRListFrame.lvDVRFileList.Items.Count > 0 then
    FDVRListFrame.lvDVRFileList.Items[0].Selected := true
  ;
end;

function TDvrPlayList.FileFormatSupported(aFileName: string): boolean;
var ext: string;
begin
  ext := LowerCase(ExtractFileExt(aFileName));
  Result :=
     (ext = '.ts')
  or (ext = '.avi')
  or (ext = '.mp4')
  or (ext = '.mov')
  ;
end;

function TDvrPlayList.Get1stUnprepTrack: TDVRTrack;
//���������� ������ �� �������� ������ (�������������) � ������ ����� Synchronize, ��� ��� ��� ��������� � �����������...
//���������� ������, ����������� ����� Synchronize �� ����������� ������� �������� �� �������� ������ ???
var
  i: integer;
  aTrack: TDVRTrack;
begin
  Result := nil;
  for i := 0 to FDVRListFrame.lvDVRFileList.Items.Count-1 do begin
    aTrack := TDVRTrack(FDVRListFrame.lvDVRFileList.Items[i].Data);
    if aTrack.Status = dtsNew then begin
      Result := aTrack;
      Break;
    end
    ;
  end
  ;
end;

procedure TDvrPlayList.PlayerChangePosition(const PosMS: Cardinal; const PosPC: Real);

  function GetPointNumToShow(CurrentTime: Cardinal): integer;
  begin
    Result := -1;

    if Assigned(FCurGPSTrack) and (High(FCurGPSTrack)>=0) then begin //������� ������� ����� ����� � gpx/nmea-����� �� �������
    //(���������: ������ ����� � ����������� ��������� �� �������)
      while (MsecElapsed(Result+1) <= CurrentTime) do Result := Result + 1;
    end
    ;
  end;

var
  npoint: integer;
begin
  if Application.Terminated then Exit;
  npoint := GetPointNumToShow(PosMS);
  if (npoint <> FCurNPoint) and (npoint >= Low(FCurGPSTrack)) and (npoint <= High(FCurGPSTrack)) then begin
    FCurNPoint := npoint;
    FGPSCursorDrawer.DrawDVRMark(FCurGPSTrack[npoint]);
    FGPSRingsDrawer.DrawDVRMark(FCurGPSTrack[npoint]);

    if FDVRButtonsFrame.staCenterCursorOnMap.Checked then
      FMapProvider.CenterPointOnMap(FCurGPSTrack[npoint])
    ;
    FSpeedometer.Value := Round(FCurGPSTrack[npoint].Speed);

    {FAviaHorizon.Course := FCurGPSTrack[npoint].Course;
    FAviaHorizon.Pitch := FCurGPSTrack[npoint].aSngl1 - cx0;
    FAviaHorizon.Roll := -(FCurGPSTrack[npoint].aSngl2 - cy0)*5;
    }
    FAccelerometer.DecXValue :=  10*(FCurGPSTrack[npoint].AccelX - cx0);
    FAccelerometer.DecYValue := -10*(FCurGPSTrack[npoint].AccelY - cy0);

    FAccelZInd.Value := FCurGPSTrack[npoint].AccelZ - cz0;

    {FAccelModuleInd.Value := Sqrt(Sqr(FCurGPSTrack[npoint].aSngl1 - cx0)
                    + Sqr(FCurGPSTrack[npoint].aSngl2 - cy0)
                    + Sqr(FCurGPSTrack[npoint].aSngl3 - cz0)
    );}
  end
  ;
end;

procedure TDvrPlayList.SliderUpdate(const NewPos: Real);
begin
  //FTrackBar.Position := Round(NewPos*10);
  //FTrackBar.Position := Round(NewPos*FTrackBar.Max/100);

  FTimeSlider.Position := NewPos * FTimeSlider.MaxTime / 100;
end;

procedure TDvrPlayList.staClearExecute(Sender: TObject);
var
  i: integer;
begin
  StopPlayback;
  for i := 0 to FDVRListFrame.lvDVRFileList.Items.Count-1 do
    ClearItem(FDVRListFrame.lvDVRFileList.Items[i])
  ;
  FDVRListFrame.lvDVRFileList.Clear;
end;

procedure TDvrPlayList.staClearUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (FDVRListFrame.lvDVRFileList.Items.Count > 0);
end;

procedure TDvrPlayList.staDeleteExecute(Sender: TObject);
var
  vItem: TListItem;
begin
  FDVRButtonsFrame.staRemoveFileFromPlayList.OnExecute := nil;
  try
    vItem := FDVRListFrame.lvDVRFileList.Selected;
    if Assigned(vItem) then begin
      if FDVRListFrame.lvDVRFileList.Items.Count = 1 then
        StopPlayback
      else
        PlaybackCompleeted(nil)
      ;
      ClearItem(FDVRListFrame.lvDVRFileList.Items[vItem.Index]);
      vItem.Delete;
    end;
  finally
    FDVRButtonsFrame.staRemoveFileFromPlayList.OnExecute := staDeleteExecute;
  end;
end;

procedure TDvrPlayList.staDeleteUpdate(Sender: TObject);
var
  vItem: TListItem;
begin
  vItem := FDVRListFrame.lvDVRFileList.Selected;
  (Sender as TAction).Enabled := Assigned(vItem);
end;

procedure TDvrPlayList.staFullScreenExecute(Sender: TObject);
begin
  FPlayer.FullScreen := true;
end;

procedure TDvrPlayList.staMuteExecute(Sender: TObject);
begin
  FPlayer.Mute := not FPlayer.Mute;
end;

procedure TDvrPlayList.staMuteUpdate(Sender: TObject);
begin
  if FPlayer.Active then
    (Sender as TAction).Enabled := not FPlayer.Paused
  else
    (Sender as TAction).Enabled := false
  ;

  if FPlayer.Mute then begin
    (Sender as TAction).ImageIndex := 15;
    (Sender as TAction).Caption := 'Sound on';
  end else begin
    (Sender as TAction).ImageIndex := 14;
    (Sender as TAction).Caption := 'Sound off';
  end
  ;
end;

procedure TDvrPlayList.staOpenExecute(Sender: TObject);
begin
  with FDVRListFrame.odDVR do begin
    Options := [ofAllowMultiSelect, ofFileMustExist];
    Filter := 'Video files|*.ts;*.avi;*.mp4;*.mov|All files|*.*';
    FilterIndex := 1;
    //InitialDir := ExtractFilePath(FPlayer.MediaAddr);
    //InitialDir := GetCurrentDir;
    FileName := FPlayer.MediaAddr;

    if Execute then AddFiles(Files);
  end
  ;
end;

procedure TDvrPlayList.staPlayPauseExecute(Sender: TObject);
begin
  FPlayer.Paused := not FPlayer.Paused;
end;

procedure TDvrPlayList.staPlayPauseUpdate(Sender: TObject);
begin
  if FPlayer.Active then begin
    (Sender as TAction).Enabled := true;
    if FPlayer.Paused then
      (Sender as TAction).ImageIndex := 0
    else
      (Sender as TAction).ImageIndex := 1
    ;
  end else begin
    (Sender as TAction).Enabled := false;
  end;
end;

procedure TDvrPlayList.staScreenShotExecute(Sender: TObject);
begin
  if FPlayer.Active then FPlayer.DoScreenShot;
end;

procedure TDvrPlayList.staSpeed025Execute(Sender: TObject);
begin
  FPlayer.Speed := 0.25;
end;

procedure TDvrPlayList.staSpeed05Execute(Sender: TObject);
begin
  FPlayer.Speed := 0.5;
end;

procedure TDvrPlayList.staSpeedNormalExecute(Sender: TObject);
begin
  FPlayer.Speed := 1;
end;

procedure TDvrPlayList.staSpeedx4Execute(Sender: TObject);
begin
    FPlayer.Speed := 4;
end;

procedure TDvrPlayList.staSpeedXUpdate(Sender: TObject);
begin
  if      Sender = FDVRButtonsFrame.staSpeed025 then
    (Sender as TAction).Checked := (FPlayer.Speed = 0.25)
  else if Sender = FDVRButtonsFrame.staSpeed05 then
    (Sender as TAction).Checked := (FPlayer.Speed = 0.50)
  else if Sender = FDVRButtonsFrame.staSpeedNormal then
    (Sender as TAction).Checked := (FPlayer.Speed = 1.0)
  else if Sender = FDVRButtonsFrame.staSpeedx2 then
    (Sender as TAction).Checked := (FPlayer.Speed = 2.0)
  else if Sender = FDVRButtonsFrame.staSpeedx4 then
    (Sender as TAction).Checked := (FPlayer.Speed = 4.0)
  ;

  (Sender as TAction).Enabled := FPlayer.Active;
end;

procedure TDvrPlayList.staSpeedx2Execute(Sender: TObject);
begin
    FPlayer.Speed := 2;
end;

procedure TDvrPlayList.staStepBackwardExecute(Sender: TObject);
begin
  if NOT FPlayer.Paused then begin
    //FPlayer.Speed := FPlayer.Speed - 0.5;
    FPlayer.SeekBy(-10)
  end else
    FPlayer.SeekBy(-1)
  ;
end;

procedure TDvrPlayList.staStepBackwardUpdate(Sender: TObject);
begin
  if FPlayer.Active then begin
    (Sender as TAction).Enabled := true;
    if NOT FPlayer.Paused then
      (Sender as TAction).Caption := 'Rewind by 10 s'
    else
      (Sender as TAction).Caption := 'Rewind by 1 s'
    ;
  end else
    (Sender as TAction).Enabled := false
  ;
end;

procedure TDvrPlayList.staStepForwardExecute(Sender: TObject);
begin
  if NOT FPlayer.Paused then begin
    //FPlayer.Speed := FPlayer.Speed + 0.5;
    FPlayer.SeekBy(10)
  end else
    FPlayer.Send_FrameStep
  ;
end;

procedure TDvrPlayList.staStepForwardUpdate(Sender: TObject);
begin
  if FPlayer.Active then begin
    (Sender as TAction).Enabled := true;
    if NOT FPlayer.Paused then
      (Sender as TAction).Caption := 'Skip by 10 s'
    else
      (Sender as TAction).Caption := 'Skip by 1 frame'
    ;
  end else
    (Sender as TAction).Enabled := false
  ;
end;

procedure TDvrPlayList.staPrevTrackExecute(Sender: TObject);
//���������� ���� �� ������ �� �����
var
  vItem: TListItem;
  i: integer;
begin
  if NOT FPlayer.Paused then begin
    i := 0;
    vItem := FDVRListFrame.lvDVRFileList.Selected;
    if VItem <> nil then begin
      i := FDVRListFrame.lvDVRFileList.Selected.Index-1;
      if i < 0 then
        i := FDVRListFrame.lvDVRFileList.Items.Count-1
      ;
    end;
    if Assigned(FDVRListFrame.lvDVRFileList.Items[i]) then
      FDVRListFrame.lvDVRFileList.Items[i].Selected := true
    ;
  end else
    FPlayer.SeekBy(-10)
  ;
end;

procedure TDvrPlayList.staPrevTrackUpdate(Sender: TObject);
begin
  if FPlayer.Active then begin
    (Sender as TAction).Enabled := true;
    if NOT FPlayer.Paused then
      (Sender as TAction).Caption := 'Previous track'
    else
      (Sender as TAction).Caption := 'Rewind by 10 s'
    ;
  end else
    (Sender as TAction).Enabled := false
  ;
end;

procedure TDvrPlayList.staNextTrackExecute(Sender: TObject);
begin
  if NOT FPlayer.Paused then begin
    PlaybackCompleeted(nil);
  end else
    FPlayer.SeekBy(10)
  ;
end;

procedure TDvrPlayList.staNextTrackUpdate(Sender: TObject);
begin
  if FPlayer.Active then begin
    (Sender as TAction).Enabled := true;
    if NOT FPlayer.Paused then
      (Sender as TAction).Caption := 'Next track'
    else
      (Sender as TAction).Caption := 'Skip by 10 s'
    ;
  end else
    (Sender as TAction).Enabled := false
  ;
end;

procedure TDvrPlayList.staUniNPUpdate(Sender: TObject);
begin
  if FPlayer.Active then
    (Sender as TAction).Enabled := not FPlayer.Paused
  else
    (Sender as TAction).Enabled := false
  ;
end;

procedure TDvrPlayList.staUniUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := FPlayer.Active;
end;

procedure TDvrPlayList.StopPlayback;
begin
  if FPlayer.Active then begin
    FPlayer.OnPlayEnd := nil;
    try
      FPlayer.CloseMedia;
      Application.ProcessMessages;
    finally
      FPlayer.OnPlayEnd := PlaybackCompleeted;
    end;
  end;
end;

procedure TDvrPlayList.TimeSliderUserChange(Sender: TObject; const NewPosition: Real);
var PP: real;
begin
  PP := NewPosition/FTimeSlider.MaxTime * 100;

  FPlayer.SetPercentPositionFromASlider(PP);
end;

procedure TDvrPlayList.UpdateItemView(aItem: TListItem);
//����� ���������� � ������� � ������� ������� ������ lock-unlock
var
  ATrack: TDVRTrack;
begin
  try
    ATrack := TDVRTrack(aItem.Data);
    if not (ATrack is TDVRTrack) then Exit;
  except
    Exit;
  end;

    SetSubItem(aItem, 7, ATrack.VideoFile);             //������ ����
    case ATrack.Status of
      dtsNew: begin
        SetSubItem(aItem, 0, '?');
        SetSubItem(aItem, 1, '?');
        SetSubItem(aItem, 2, '?');
        SetSubItem(aItem, 3, '?');
        SetSubItem(aItem, 4, '?');
        SetSubItem(aItem, 5, '?');
        SetSubItem(aItem, 6, '?');
      end;
      dtsFailed: begin
        SetSubItem(aItem, 0, 'err!');
        SetSubItem(aItem, 1, 'err!');
        SetSubItem(aItem, 2, 'err!');
        SetSubItem(aItem, 3, 'err!');
        SetSubItem(aItem, 4, 'err!');
        SetSubItem(aItem, 5, 'err!');
        SetSubItem(aItem, 6, 'err!');
      end;
      dtsGPSMissing, dtsVideoOnly: begin
        SetSubItem(aItem, 0, '-');
        SetSubItem(aItem, 1, '-');
        SetSubItem(aItem, 2, '-');
        SetSubItem(aItem, 3, '-');
        SetSubItem(aItem, 4, '-');
        SetSubItem(aItem, 5, '-');
        SetSubItem(aItem, 6, '-');
      end;
      dtsGPSPrepared, dtsCompleteTrack: begin
        SetSubItem(aItem, 0, IntToStr(ATrack.Count+1));
        SetSubItem(aItem, 1, DateTimeToStr(ATrack.StartTime));
        SetSubItem(aItem, 2, IntToStr(ATrack.Count));
        SetSubItem(aItem, 3, FormatFloat('#,##0;;', ATrack.Distance));
        SetSubItem(aItem, 4, FloatToStr(ATrack.MaxSpeed));
        SetSubItem(aItem, 5, FloatToStr(ATrack.AvgSpeed));
        SetSubItem(aItem, 6, IfThen((ATrack.MaxAccel>cAccelTreshold), 'Yes', ''));
      end;
    end;

end;

procedure TDvrPlayList.lvSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  FDVRListFrame.lvDVRFileList.OnSelectItem := nil;
  try
    if Selected and Assigned(Item) then PlayItem(Item);
    Application.ProcessMessages;
  finally
    FDVRListFrame.lvDVRFileList.OnSelectItem := lvSelectItem;
  end;
end;

function TDvrPlayList.MsecElapsed(APointNum: integer): Cardinal;
begin
  if (APointNum >= Low(FCurGPSTrack)) and (APointNum <= High(FCurGPSTrack))  then
    Result := MilliSecondsBetween(FCurGPSTrack[APointNum].Time, FCurGPSTrack[0].Time)
  else
    Result := High(Result)
  ;
end;

procedure TDvrPlayList.PlayItem(aItem: TListItem);
//threadsafe! ���������� � ������� ������
var
  ATrack: TDVRTrack;
begin
  ATrack := TDVRTrack(aItem.Data);

  ATrack.Lock;
  try
    if (ATrack.Status = dtsNew) then begin
      ATrack.Status := ATrack.PrepareGPSData;
      UpdateItemView(aItem);
    end;

    if not (ATrack.Status in [dtsNew, dtsFailed]) then begin
      StopPlayback;

      if ATrack.Status in [dtsGPSPrepared, dtsCompleteTrack] then begin
        FCurGPSTrack := ATrack.GPSData;
        //FCurDVRTrack := FMapProvider.ReCreateDVRPathMark(ATrack.GPSData);
        FMapProvider.ReCreateDVRPathMark(ATrack.GPSData);
        PlotSpeedAndAccelChart;

        //FTrackBar.Max := ATrack.Count*10;
      end else begin
        FCurGPSTrack := nil;
        //FCurDVRTrack := nil;

        //FTrackBar.Max := 60*10; //to do ���������� �������� ������������ �����
      end
      ;

      aItem.MakeVisible(false);

      FPlayer.MediaAddr := ATrack.VideoFile;
      try
        FDVRConfig.LockRead;
        try
          //edSnapshotPath.Text := GState.MainFormConfig.DVRConfig.SnapShotFolder;
          FPlayer.UseAccel := FDVRConfig.UseVideoAccel;
          FPlayer.SnapshotFolder := FDVRConfig.SnapShotFolder;
        finally
          FDVRConfig.UnlockRead;
        end;

        FPlayer.OpenMedia;

      except
        on E: Exception do begin
          ATrack.ReportWrongVideo(E.Message);
          //PlaybackCompleeted(nil);
        end;
      end;
    end else begin //���-�� �� ���, ���������
      // to do: PlaybackCompleeted(nil);
    end
    ;
  finally
    ATrack.UnLock;
  end;
end;

procedure TDvrPlayList.PlotSpeedAndAccelChart;
var
  i: integer;
  ar: array of TTSDataSample;
begin
    SetLength(ar, Length(FCurGPSTrack));
    for i := Low(FCurGPSTrack) to High(FCurGPSTrack) do begin
      ar[i].Speed := FCurGPSTrack[i].Speed;
      ar[i].Accel := GetAccelModule(FCurGPSTrack[i]);
    end
     //AddSinglePoint(FCurGPSTrack[i].Speed)
    ;
    FTimeSlider.SetData(ar, 1/CMult);

  {FChartView.BeginUpdate;
  FChartView.Panes[0].Series.Clear;
  with FChartView.Panes[0].Series.Add do begin
    LegendText := 'Speed, km/h';
    YAxis.Position := yLeft;
    YAxis.MajorFont.Color := clRed;
    LineColor := clRed;

    for i := Low(FCurGPSTrack) to High(FCurGPSTrack) do
     AddSinglePoint(FCurGPSTrack[i].Speed)
    ;
    AutoRange := arEnabledZeroBased;
  end;
  with FChartView.Panes[0].Series.Add do begin
    LegendText := 'Acceleration, m/s^2';
    YAxis.Position := yRight;
    YAxis.MajorFont.Color := clLime;
    LineColor := clLime;

    for i := Low(FCurGPSTrack) to High(FCurGPSTrack) do
     AddSinglePoint(GetAccelModule(FCurGPSTrack[i]))
    ;
    AutoRange := arDisabled;
    Maximum := 10.0;
    Minimum := 0.0;
  end;
  FChartView.Panes[0].Range.RangeFrom := Low(FCurGPSTrack);
  FChartView.Panes[0].Range.RangeTo := High(FCurGPSTrack);
  FChartView.EndUpdate;
  }
end;

procedure TDvrPlayList.PlaybackCompleeted(Sender: TObject);
//���������� ���� �� ������ �� �����
var
  vItem: TListItem;
  i: integer;
begin
  i := 0;
  vItem := FDVRListFrame.lvDVRFileList.Selected;
  if VItem <> nil then begin
    i := FDVRListFrame.lvDVRFileList.Selected.Index+1;
    if i > FDVRListFrame.lvDVRFileList.Items.Count-1 then
      i := 0
    ;
  end;
  if Assigned(FDVRListFrame.lvDVRFileList.Items[i]) then begin
    FPlayer.OnPlayEnd := nil;
    try
      FDVRListFrame.lvDVRFileList.Items[i].Selected := true;
    finally
      FPlayer.OnPlayEnd := PlaybackCompleeted;
    end;
  end
  ;
end;


procedure TDvrPlayList.PlayDemo;
var
  demopath: string;
  sr: TSearchRec;
  FileAttrs: Integer;
  i: integer;
  afiles: array of string;
begin
  i := 0;
  demopath := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)))+'_demo\';
  FileAttrs := 0;
  if FindFirst(demopath+'*.ts', FileAttrs, sr) = 0 then begin
    repeat
      inc(i);
      SetLength(afiles, i);
      afiles[i-1] := demopath + sr.Name;
    until FindNext(sr) <> 0
    ;
    FindClose(sr);
  end
  ;
  AddFiles(afiles);
end;

{ TDVRTrack }

procedure TDVRTrack.CalcMaxMinAvg;
var i: integer;
begin
  FAvgSpeed := 0;
  FMaxSpeed := 0;
  FMaxAccel := 0;
  for i:=0 to Count-1 do begin
    FAvgSpeed := FAvgSpeed + FGPSData[i].Speed;
    FMaxSpeed :=  Max(FMaxSpeed, FGPSData[i].Speed);
    FMaxAccel :=  Max(FMaxAccel, GetAccelModule(FGPSData[i]));
  end
  ;
  FAvgSpeed := Round(FAvgSpeed / Count);
end;

constructor TDVRTrack.Create(AOwner: TListItem; const AVideoFile: string; aMapProvider: IDVRMapProvider);
begin
  inherited Create;

  FLock := TCriticalSection.Create;
  FOwner := AOwner;
  FVideoFile := AVideoFile;
  FStatus := dtsNew;
  FMapProvider := aMapProvider;
end;

destructor TDVRTrack.Destroy;
begin
  Lock;
  try

    inherited Destroy;
  finally
    UnLock;
    FLock.Free;
  end;
end;

function TDVRTrack.GetCount: UInt16;
begin
  if Assigned(FGPSData) then // and (FStatus in [dtsGPSPrepared, dtsCompleteTrack])
    Result := Length(FGPSData)
  else
    Result := 0
  ;
end;

function TDVRTrack.GetFileExt: string;
begin
  Result := LowerCase(ExtractFileExt(FVideoFile));
end;

function TDVRTrack.PrepareGPSData: TDVRTrackStatus;
var
  aTSParser: TTsParser;
  afn: string;
  aGpxHelper: TGpxHelper;
begin
  FGPSData := nil;
  Result := dtsVideoOnly;

  if (Length(FGPSData) = 0) then begin
    afn := ChangeFileExt(FVideoFile, '.nmea');
    if FileExists(afn) then begin
      FGPSData := GetNmeaFromFile(afn);
      Result := dtsGPSMissing;
    end
    ;
  end
  ;
  if (Length(FGPSData) = 0) and (FileExt = '.ts') then begin
    aTSParser := TTsParser.Create(FVideoFile);
    try
      FGPSData := aTSParser.GetTrackSeg;
      Result := dtsGPSMissing;
    finally
      aTSParser.Free;
    end;
  end
  ;
  if Length(FGPSData) = 0 then begin
    afn := ChangeFileExt(FVideoFile, '.gpx');
    if FileExists(afn) then begin
      try
        aGpxHelper := TGpxHelper.CreateGpx1stTrackSeg(nil, afn);
        FGPSData := aGpxHelper.TrackSeg1;
        Result := dtsGPSMissing;
      finally
        FreeAndNil(aGpxHelper);
      end;
    end
    ;
  end
  ;

  if Length(FGPSData) > 0 then begin
    InterpolateGPSData;

    FStartTime := FGPSData[0].Time;
    FDistance := Round(FMapProvider.GetDVRPathLength(FGPSData));
    CalcMaxMinAvg;


    Result := dtsGPSPrepared;
  end
  ;
end;

procedure TDVRTrack.ReportWrongVideo(const aErr: string);
begin
  SetStatus(dtsFailed);
  ShowMessage('ReportWrongVideo: '+aErr);
end;

procedure TDVRTrack.SetStatus(aNewStatus: TDVRTrackStatus);
begin
  //FLock.Acquire;
  FStatus := aNewStatus;
  //FLock.Release;
end;

function TDVRTrack.TryLock: boolean;
begin
  Result := FLock.TryEnter;
end;

procedure TDVRTrack.UnLock;
begin
  FLock.Release;
end;

function TDVRTrack.GetStatus: TDVRTrackStatus;
begin
  //FLock.Acquire;
  Result := FStatus;
  //FLock.Release;
end;

procedure TDVRTrack.InterpolateGPSData;
//���� ���� ������ ����� � 10 ��� ����...
//���� 2 �����, ����� (2-1)*10 = 10 �����
var
  artmp: TTrackSeg;
  i,j,k: integer;
begin
  if Count < 2 then Exit;

  SetLength(artmp, (Length(FGPSData)-1) * cMult);

  for i := Low(artmp) to High(artmp) do begin
    j := i div cMult;

    k := i mod cMult;
    if j<High(FGPSData) then begin
      artmp[i].Latitude  := FGPSData[j].Latitude  + (FGPSData[j+1].Latitude  - FGPSData[j].Latitude) / cMult * k;
      artmp[i].Longitude := FGPSData[j].Longitude + (FGPSData[j+1].Longitude - FGPSData[j].Longitude) / cMult * k;
      artmp[i].Time      := FGPSData[j].Time      + (FGPSData[j+1].Time      - FGPSData[j].Time) / cMult * k;
      artmp[i].Speed     := FGPSData[j].Speed     + (FGPSData[j+1].Speed     - FGPSData[j].Speed) / cMult * k;
      //artmp[i].Course    := FGPSData[j].Course    + (FGPSData[j+1].Course    - FGPSData[j].Course) / cMult * k;
      artmp[i].Course     := FGPSData[j+1].Course;
      artmp[i].Accuracy  := FGPSData[j].Accuracy  + (FGPSData[j+1].Accuracy  - FGPSData[j].Accuracy) / cMult * k;
      artmp[i].AccelX    := FGPSData[j].AccelX    + (FGPSData[j+1].AccelX    - FGPSData[j].AccelX) / cMult * k;
      artmp[i].AccelY    := FGPSData[j].AccelY    + (FGPSData[j+1].AccelY    - FGPSData[j].AccelY) / cMult * k;
      artmp[i].AccelZ    := FGPSData[j].AccelZ    + (FGPSData[j+1].AccelZ    - FGPSData[j].AccelZ) / cMult * k;
    end else begin
      artmp[i].Latitude   := FGPSData[j].Latitude;
      artmp[i].Longitude  := FGPSData[j].Longitude;
      artmp[i].Time       := FGPSData[j].Time;
      artmp[i].Speed      := FGPSData[j].Speed;
      artmp[i].Course     := FGPSData[j].Course;
      artmp[i].Accuracy   := FGPSData[j].Accuracy;
      artmp[i].AccelX     := FGPSData[j].AccelX;
      artmp[i].AccelY     := FGPSData[j].AccelY;
      artmp[i].AccelZ     := FGPSData[j].AccelZ;
    end
    ;
  end
  ;
  FGPSData := artmp;
end;

procedure TDVRTrack.Lock;
begin
  FLock.Acquire;
end;

{ TBackgroundThread }

constructor TBackgroundThread.CreateBackgroundThread(Owner: TDvrPlayList);
begin
  inherited Create;

  FOwner := Owner;
  FreeOnTerminate := True;
end;

procedure TBackgroundThread.Execute;
var
  aTrack: TDVRTrack;
  aErrLog: TextFile;
begin
  while not Terminated do
  try
    Sleep(50); // to reduce cpu load...
    Synchronize(
      procedure
      begin
        aTrack := FOwner.Get1stUnprepTrack;
      end)
    ;

    if Assigned(aTrack) then begin
      if aTrack.TryLock then begin
        try
          aTrack.Status := aTrack.PrepareGPSData;
        finally
          aTrack.UnLock;
        end;
      end
      ;

      Synchronize(
      procedure
      begin
        FOwner.UpdateItemView(aTrack.FOwner);
      end)
      ;
    end
    ;
  except
    on E: Exception do begin
      AssignFile(aErrLog, 'err1.txt');
      Rewrite(aErrLog);
      Writeln(aErrLog, E.Message);
      CloseFile(aErrLog);
    end;
  end
  ;
end;

end.
