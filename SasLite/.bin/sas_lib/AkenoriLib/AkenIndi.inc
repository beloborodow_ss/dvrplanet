const
  clAkGrayLight      = $FFe0e0e0;//$FF9fa2a8
  clAkGrayDark       = $FF525962;//$FF363636
  clAkGrayUltraDark  = $FF42484f;//
  clAkRed            = clRed32;
  clAkGreenLight     = $FF81D626;
  clAkGreenDark      = $FF369600;
  clAkLight          = $FFFFFFFF;
