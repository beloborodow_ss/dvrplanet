object fmLog: TfmLog
  Left = 272
  Top = 148
  BorderStyle = bsSizeToolWin
  Caption = 'Log player'
  ClientHeight = 668
  ClientWidth = 473
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 300
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnl: TPanel
    Left = 0
    Top = 634
    Width = 473
    Height = 34
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 233
    DesignSize = (
      473
      34)
    object btnClear: TButton
      Left = 316
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'C&lear'
      TabOrder = 0
      OnClick = btnClearClick
    end
    object btnClose: TButton
      Left = 396
      Top = 4
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Close'
      TabOrder = 1
      OnClick = btnCloseClick
    end
  end
  object mm: TMemo
    Left = 0
    Top = 0
    Width = 473
    Height = 634
    Align = alClient
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
    ExplicitHeight = 233
  end
end
