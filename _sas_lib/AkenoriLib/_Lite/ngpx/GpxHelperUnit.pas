unit GpxHelperUnit;

interface

uses
  SysUtils,
  Classes,
  NativeXml,
  i_DVRGPSLib, TsParser;

const
  // Do not localize these
  sNamespace  = 'http://www.topografix.com/GPX/1/1';
  sVersion = '1.1';
  sCreator = 'SwathGen';
  sSchemaInstance = 'http://www.w3.org/2001/XMLSchema-instance';
  sSchemaLocation = sNamespace+' '+
                    'http://www.topografix.com/GPX/1/1/gpx.xsd';

  sNNRoute = 'rte';
  sNNWaipoint = 'wpt';
  sNNTrack = 'trk';
  sNNExtentions = 'extensions';
  sNNMetadata = 'metadata';

resourcestring
  sNoGpxRoot       = '<%s> has no gpx root !';
  sWrongGpxXmlns   = '<%s> root has a wrong xmlns attribute';
  sWrongGpxVersion = '<%s> is not a version "1.1" gpx file !';
  sNoGpxTrackpoints = '<%s> has no track segments with trackpoints!';

type
  EGpxException = class(Exception)
  end;

type
  TGpxHelper = class(TNativeXml)
  private
    FTrackSeg1: TTrackSeg;
  public
    constructor CreateNewGpx(AOwner: TComponent);
    constructor CreateGpx1stTrackSeg(AOwner: TComponent; const AFileName: string);
    constructor CreateNmeaTrackSeg(AOwner: TComponent; const AFileName: string);
    constructor CreateTSTrackSeg(AOwner: TComponent; const AFileName: string);
    procedure NewGpx;
    procedure AssignToStrings(AStrings:TStrings);

    procedure LoadGpxFromFile(const AFileName: string);
    procedure SaveGpxToFile(const AFileName: string);

    function GetRootNodeListByName(AName: string): TsdNodeList;
    function Get1stTrackSeg: TTrackSeg;
    function GetGpxInfo: string;

    function GetNmeaFromFile(const AFileName: string): TTrackSeg;
    property TrackSeg1: TTrackSeg read FTrackSeg1;
  end;

  TGpxNodeHelper = class helper for TXmlNode
    function Element(const AName: string): TXmlNode;
  end;

implementation

{ TGpxHelper }

uses StrUtils, Math, DateUtils;

constructor TGpxHelper.CreateGpx1stTrackSeg(AOwner: TComponent; const AFileName: string);
begin
  inherited Create(AOwner);
  LoadGpxFromFile(AFileName);
  FTrackSeg1 := Get1stTrackSeg;
end;

function TGpxHelper.GetNmeaFromFile(const AFileName: string): TTrackSeg;

  function GetNthSubstr(const S: string; N: integer; Delim: string = ','): string;
  //����.: ������ � ����� ������ ��������� �� Delim
    function PosExt(const SubStr,S: string; N: Cardinal = 1): integer;
    var
      i, Offset: integer;
    begin
      Result := 0;
      Offset := 0;
      for i:=1 to N do begin
        Offset := PosEx(SubStr,S,Offset+1);
        if Offset<1 then Exit;
      end
      ;
      Result := Offset;
    end;
  var
    ibeg,iend: integer;
  begin
    Result := '';
    if (N<1) or (Length(S) < 1) or (Length(Delim) < 1) then Exit;
    ibeg := PosExt(Delim,S,N-1);
    if (ibeg<1) and (N>1) then Exit; //����� ��������� � ������� ������ 1, � ����� ������������ �� ������
    ibeg := IfThen(ibeg<1,1,ibeg+Length(Delim));
    iend := PosExt(Delim, S, N);
    iend := IfThen(iend<1, Length(S), iend-1);
    Result := Copy(S,ibeg,iend-ibeg+1);
  end;

  function GetRealFromStrDef(const StrVal: string; const DefVal: real=0.0): real;
  var
    fs: TFormatSettings;
  begin
    fs := TFormatSettings.Create; // record, so no need to free
    fs.DecimalSeparator := '.';

    Result := StrToFloatDef(StrVal, DefVal, fs);
  end;

  function GetDegreeDecMinFromNmea(AVal: string): real;
  var VDegrees, VMinutes: integer;
  begin
    Result := GetRealFromStrDef(AVal);

    VDegrees := Trunc(Result/100);

    VMinutes := Trunc(Result - VDegrees*100);

    Result := VDegrees + (VMinutes + Frac(Result))/60; //shifts two digits right and converts minutes from babylonian to arabian: f/100 * 100/60 = f/60
  end;

  function GetDateTimeFromNmea(const ADat, ATim: string): TDateTime;
  begin
    try
      Result := EncodeDateTime(StrToInt(Copy(ADat, 5, 2)), StrToInt(Copy(ADat, 3, 2)), StrToInt(Copy(ADat, 1, 2))
                             , StrToInt(Copy(ATim, 1, 2)), StrToInt(Copy(ATim, 3, 2)), StrToInt(Copy(ATim, 5, 2)), StrToInt(Copy(ATim, 8, 2))*10);
    except
      Result := 0.0;
    end;
  end;


var
  VSL: TStringList;
  i, j: integer;
begin
  Result := nil;
  VSL := TStringList.Create;
  try
    VSL.LoadFromFile(AFileName);
    SetLength(Result, VSL.Count);
    j := 0;
    for i:=0 to VSL.Count-1 do
      if ((GetNthSubstr(VSL.Strings[i], 1) = '$GPRMC') or (GetNthSubstr(VSL.Strings[i], 1) = '$GNRMC')) and (GetNthSubstr(VSL.Strings[i], 3) = 'A') then begin
        with Result[j] do begin
          Latitude := GetDegreeDecMinFromNmea(GetNthSubstr(VSL.Strings[i], 4));
          Longitude := GetDegreeDecMinFromNmea(GetNthSubstr(VSL.Strings[i], 6));
          Time := GetDateTimeFromNmea(GetNthSubstr(VSL.Strings[i], 10), GetNthSubstr(VSL.Strings[i], 2));
          Altitude := 0.0;
          Satellites := 0;
          Speed := GetRealFromStrDef(GetNthSubstr(VSL.Strings[i], 8));
          Course := GetRealFromStrDef(GetNthSubstr(VSL.Strings[i], 9));
        end;
        inc(j);
      end
    ;
    SetLength(Result, j);
  finally
    VSL.Free;
  end;
end;

constructor TGpxHelper.CreateNmeaTrackSeg(AOwner: TComponent; const AFileName: string);
begin
  inherited Create(AOwner);

  FTrackSeg1 := GetNmeaFromFile(AFileName);
end;

constructor TGpxHelper.CreateTSTrackSeg(AOwner: TComponent;
  const AFileName: string);
var
  aTSParser: TTsParser;
begin
  inherited Create(AOwner);
  aTSParser := TTsParser.Create(AFileName);
  try
    FTrackSeg1 := aTSParser.GetTrackSeg;
  finally
    aTSParser.Free;
  end;
end;

constructor TGpxHelper.CreateNewGpx(AOwner: TComponent);
begin
  inherited Create(AOwner);
  //
  NewGpx;
end;

function TGpxHelper.GetGpxInfo: string;
const
  cLF = #10#13;
  cMsg  = 'metadata node count : %u'+cLF+
          'wpt node count : %u'+cLF+
          'rte node count : %u'+cLF+
          'trk node count : %u'+cLF+
          'extensions node count : %u';
var
  lMetadataCount: Integer;
  lWaypointsCount: Integer;
  lRoutesCount: Integer;
  lTracksCount: Integer;
  lExtensions: Integer;
begin
  lMetadataCount := IfThen(Assigned(Root.Element(sNNMetadata)),1,0);

  with GetRootNodeListByName(sNNWaipoint) do
  try
    lWaypointsCount := Count;
  finally
    Free
  end;

  with GetRootNodeListByName(sNNRoute) do
  try
    lRoutesCount := Count;
  finally
    Free
  end;

  with GetRootNodeListByName(sNNTrack) do
  try
    lTracksCount := Count;
  finally
    Free
  end;

  lExtensions := IfThen(Assigned(Root.Element(sNNExtentions)),1,0);

  Result := Format(cMsg,[lMetadataCount,lWaypointsCount,lRoutesCount,lTracksCount,lExtensions]);
end;

procedure TGpxHelper.NewGpx;
begin
  New;

  Root.Name := 'gpx';

  Root.NodesAdd(
    [
      AttrText('xmlns', sNamespace),
      AttrText('version', sVersion),
      AttrText('creator', sCreator),
      AttrText('xmlns:xsi',sSchemaInstance),
      AttrText('xsi:schemaLocation', sSchemaLocation),

      // Metadata
      NodeNew(sNNMetadata,
        [
          NodeNewAttr('bounds',
            [
              AttrText('minlat','90.00000000'),
              AttrText('minlon','180.00000000'),
              AttrText('maxlat','-90.00000000'),
              AttrText('maxlon','-180.00000000')
            ]
          ),
          NodeNew(sNNExtentions)
        ]
      ),

      // Waypoints

      // Routes

      // Tracks

      NodeNew(sNNExtentions)
    ]
  );
end;

function TGpxHelper.GetRootNodeListByName(AName: string): TsdNodeList;
begin
  Result := TsdNodeList.Create(False);

  Root.NodesByName(AName, Result);
end;

function TGpxHelper.Get1stTrackSeg: TTrackSeg;
var
  aNode: TXmlNode;
  aNodeList: TsdNodeList;
  i: integer;
begin
  Result := nil;

  aNode := Root.FindNode('/gpx/trk/trkseg/trkpt');
  if Assigned(aNode) then begin
    aNode := aNode.Parent;
    aNodeList := TsdNodeList.Create(false);
    try
      aNode.NodesByName('trkpt', aNodeList);
      SetLength(Result, aNodeList.Count);
      for i := 0 to aNodeList.Count-1 do begin
        Result[i].Latitude := aNodeList.Items[i].ReadAttributeFloat('lat');
        Result[i].Longitude := aNodeList.Items[i].ReadAttributeFloat('lon');
        Result[i].Time := aNodeList.Items[i].NodeByName('time').ValueAsDateTime;
        Result[i].Altitude := aNodeList.Items[i].NodeByName('ele').ValueAsFloat;

        try
          Result[i].Satellites := aNodeList.Items[i].NodeByName('sat').ValueAsInteger;
          Result[i].Speed := aNodeList.Items[i].NodeByName('speed').ValueAsFloat;
          Result[i].Course := aNodeList.Items[i].NodeByName('course').ValueAsFloat;
        except
          Result[i].Satellites := 0;
          Result[i].Speed := 0.0;
          Result[i].Course := 0.0;
        end;
      end;
    finally
      FreeAndNil(aNodeList);
    end;

  end;
end;

procedure TGpxHelper.LoadGpxFromFile(const AFileName: string);
var
  lGpx: TNativeXml;
  lFileName: TFileName;
begin
  lFileName := ExtractFileName(AFileName);

  lGpx := TNativeXml.Create(Self);

  lGpx.LoadFromFile(AFileName);

  try
    //*****[Below] - ���������� ���� xml ���� �� �����������
    if (lGpx.Root = nil) then
      raise EGpxException.CreateFmt(sNoGpxRoot,[lFileName])
    else
    //-----[Below]
    if lGpx.Root.Name<>'gpx' then
      raise EGpxException.CreateFmt(sNoGpxRoot,[lFileName])
//    else if lGpx.Root.AttributeValueByName['xmlns']<>sNamespace then
//      raise EGpxException.CreateFmt(sWrongGpxXmlns,[lFileName])
//    else if lGpx.Root.AttributeValueByName['version']<>sVersion then
//      raise EGpxException.CreateFmt(sWrongGpxVersion,[lFileName])
    else if not Assigned(lGpx.Root.FindNode('/gpx/trk/trkseg/trkpt')) then
      raise EGpxException.CreateFmt(sNoGpxTrackpoints,[lFileName])
    else
      Self.ReadFromString(lGpx.WriteToString) // <<<
  finally
    lGpx.Free
  end;

end;

procedure TGpxHelper.SaveGpxToFile(const AFileName: string);
begin
  ChangeFileExt(AFileName,'gpx');

  SaveToFile(AFileName);
end;

procedure TGpxHelper.AssignToStrings(AStrings:TStrings);
var
  lXmlFormat: TXmlFormatType;
  lIndentString: string;
begin
  // Save states
  lXmlFormat := XmlFormat;
  lIndentString := IndentString;

  XmlFormat := xfReadable;
  IndentString := '  ';

  AStrings.Text := WriteToString;

  // Restore states
  XmlFormat := lXmlFormat;
  IndentString := lIndentString;
end;

{ TGpxNodeHelper }

function TGpxNodeHelper.Element(const AName: string): TXmlNode;
begin
  Result := NodeByName(UTF8String(AName));
end;

end.
