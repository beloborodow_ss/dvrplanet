unit fr_DvrPlayList;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  mplayer_lib,
  i_DVRGPSLib,
  TsParser,
  fr_DvrButtons;

type
  TDVRTrackStatus = (dtsNew, dtsFailed, dtsGPSPrepared, dtsGPSMissing, dtsVideoOnly, dtsCompleteTrack);

  TDVRTrack = class
  private
    FOwner: TListItem;
    FVideoFile: string;
    FGPSData: TTrackSeg;
    FStatus: TDVRTrackStatus;
    FStartTime: TDateTime;
    FDistance: UInt16;
    FMaxAccel: UInt16;
    FDuration: UInt16;
    FMaxSpeed: UInt16;
    function GetCount: UInt16;
    procedure SetStatus(aNewStatus: TDVRTrackStatus);
    function GetFileExt: string;
  public
    property VideoFile: string read FVideoFile;
    property FileExt: string read GetFileExt;
    property Status: TDVRTrackStatus read FStatus;
    property Count: UInt16 read GetCount;
    property StartTime: TDateTime read FStartTime;
    property Duration: UInt16 read FDuration;
    property Distance: UInt16 read FDistance;
    property MaxSpeed: UInt16 read FMaxSpeed;
    property MaxAccel: UInt16 read FMaxAccel;
  public
    procedure PrepareGPSData;
    procedure ReportWrongVideo(const aErr: string);
    constructor Create(AOwner: TListItem; const AVideoFile: string);
    destructor Destroy; override;
  end;

  TfrDvrPlayList = class(TFrame)
    lvDVRFileList: TListView;
    odDVR: TOpenDialog;
    procedure lvDVRFileListSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
  private
    { Private declarations }
    FPlayer: TMPlayer;
    FDVRButtons: TfrDVRButtons;

    FGPSCursorDrawer: IDVRCursorDrawer;
    FGPSRingsDrawer: IDVRCursorDrawer;

    FTrackBar: TTrackBar;
    procedure StopPlayback;
    procedure PlayItem(aItem: TListItem);
    procedure UpdateItemView(aItem: TListItem);
    function FileFormatSupported(aFileName: string): boolean;
  public
    procedure staOpenExecute(Sender: TObject);
    procedure staClearExecute(Sender: TObject);
    procedure PlaybackCompleeted(Sender: TObject);
    procedure SliderUpdate(const NewPos: Real);
    procedure PlayerChangePosition(const PosMS: Cardinal; const PosPC: Real);
    procedure TrackBarChange(Sender: TObject);
  public
    { Public declarations }
    procedure Init(aPlayer: TMPlayer; aDVRButtons: TfrDVRButtons;
      aGPSCursorDrawer: IDVRCursorDrawer;
      aGPSRingsDrawer: IDVRCursorDrawer;
      aTrackBar: TTrackBar
    );
    function AddFiles(aPlayList: TStrings): boolean; overload;
    function AddFiles(aPlayList: array of string): boolean; overload;
    procedure BeginFromTheBegining;
  end;

implementation

{$R *.dfm}

procedure SetSubItem(AItem: TListItem; AIndex: Integer; const AValue: string);
var
  i: Integer;
begin
  if AIndex < AItem.SubItems.Count then begin
    AItem.SubItems.Strings[AIndex] := AValue;
  end else begin
    for i := AItem.SubItems.Count to AIndex - 1 do begin
      AItem.SubItems.Add('');
    end;
    AItem.SubItems.Add(AValue);
  end;
end;

procedure ClearItem(AItem: TListItem);
var ATrack: TDVRTrack;
begin
  aTrack := TDVRTrack(AItem.Data);
  aTrack.Free;
  aItem.Data := nil;
end;

{ TfrDvrFileList1 }

procedure TfrDvrPlayList.Init(aPlayer: TMPlayer; aDVRButtons: TfrDVRButtons;
  aGPSCursorDrawer: IDVRCursorDrawer;
  aGPSRingsDrawer: IDVRCursorDrawer;
  aTrackBar: TTrackBar
);
begin
  FPlayer := aPlayer;
  FDVRButtons := aDVRButtons;

  FGPSCursorDrawer := aGPSCursorDrawer;
  FGPSRingsDrawer := aGPSRingsDrawer;

  FTrackBar := aTrackBar;


  FDVRButtons.staAddFilesToPlayList.OnExecute := staOpenExecute;
  FDVRButtons.staClearPlayList.OnExecute := staClearExecute;

  FPlayer.OnPlayEnd := PlaybackCompleeted;
  FPlayer.OnSliderUpdate := SliderUpdate;
  FPlayer.OnChangePosition := PlayerChangePosition;

  FTrackBar.OnChange := TrackBarChange;
end;

function TfrDvrPlayList.AddFiles(aPlayList: TStrings): boolean;
var
  i: integer;
  aNewItem: TListItem;
begin
  if FileFormatSupported(aPlayList[0]) then begin
    for i := 0 to aPlayList.Count-1 do begin
      aNewItem := lvDVRFileList.Items.Add;

      aNewItem.Caption := ExtractFileName(aPlayList.Strings[i]); //���

      aNewItem.Data := TDVRTrack.Create(aNewItem, aPlayList.Strings[i]);

      UpdateItemView(aNewItem);
    end;

    BeginFromTheBegining;

    Result := true;
  end else
    Result := false
  ;
end;

function TfrDvrPlayList.AddFiles(aPlayList: array of string): boolean; //overloaded version for demo playback support
var
  i: integer;
  sl: TStringList;
begin
  sl := TStringList.Create;
  try
    for i := Low(aPlayList) to High(aPlayList) do
      sl.Add(aPlayList[i])
    ;
    Result := AddFiles(sl);
  finally
    sl.Free;
  end;
end;

procedure TfrDvrPlayList.BeginFromTheBegining;
begin
  if lvDVRFileList.Items.Count > 0 then
    lvDVRFileList.Items[0].Selected := true
  ;
end;

function TfrDvrPlayList.FileFormatSupported(aFileName: string): boolean;
var ext: string;
begin
  ext := LowerCase(ExtractFileExt(aFileName));
  Result :=
     (ext = '.ts')
  or (ext = '.avi')
  or (ext = '.mp4')
  or (ext = '.mov')
  ;
end;

procedure TfrDvrPlayList.PlayerChangePosition(const PosMS: Cardinal; const PosPC: Real);
begin
  if Application.Terminated then Exit;
end;

procedure TfrDvrPlayList.SliderUpdate(const NewPos: Real);
begin
  FTrackBar.Position := Round(NewPos*10);
end;

procedure TfrDvrPlayList.staClearExecute(Sender: TObject);
var
  i: integer;
begin
  StopPlayback;
  for i := 0 to lvDVRFileList.Items.Count-1 do
    ClearItem(lvDVRFileList.Items[i])
  ;
  lvDVRFileList.Clear;
end;

procedure TfrDvrPlayList.staOpenExecute(Sender: TObject);
begin
  with odDVR do begin
    Options := [ofAllowMultiSelect, ofFileMustExist];
    Filter := '�����-�����|*.ts;*.avi;*.mp4;*.mov|��� �����|*.*';
    FilterIndex := 1;
    InitialDir := ExtractFilePath(FPlayer.MediaAddr);
    FileName := FPlayer.MediaAddr;

    if Execute then AddFiles(Files);
  end
  ;
end;

procedure TfrDvrPlayList.StopPlayback;
begin
  if FPlayer.Active then begin
    FPlayer.OnPlayEnd := nil;
    try
      FPlayer.CloseMedia;
      Application.ProcessMessages;
    finally
      FPlayer.OnPlayEnd := PlaybackCompleeted;
    end;
  end;
end;

procedure TfrDvrPlayList.TrackBarChange(Sender: TObject);
var PP: real;
begin
  PP := FTrackBar.Position/10;
  FPlayer.SetPercentPositionFromASlider(PP);
end;

procedure TfrDvrPlayList.UpdateItemView(aItem: TListItem);
var
  ATrack: TDVRTrack;
begin
  ATrack := TDVRTrack(aItem.Data);
  SetSubItem(aItem, 6, ATrack.FVideoFile);             //������ ����

  case ATrack.Status of
    dtsNew: begin
      SetSubItem(aItem, 0, '?');
      SetSubItem(aItem, 1, '?');
      SetSubItem(aItem, 2, '?');
      SetSubItem(aItem, 3, '?');
      SetSubItem(aItem, 4, '?');
      SetSubItem(aItem, 5, '?');
    end;
    dtsFailed: begin
      SetSubItem(aItem, 0, 'err!');
      SetSubItem(aItem, 1, 'err!');
      SetSubItem(aItem, 2, 'err!');
      SetSubItem(aItem, 3, 'err!');
      SetSubItem(aItem, 4, 'err!');
      SetSubItem(aItem, 5, 'err!');
    end;
    dtsGPSMissing, dtsVideoOnly: begin
      SetSubItem(aItem, 0, '-');
      SetSubItem(aItem, 1, '-');
      SetSubItem(aItem, 2, '-');
      SetSubItem(aItem, 3, '-');
      SetSubItem(aItem, 4, '-');
      SetSubItem(aItem, 5, '-');
    end;
    dtsGPSPrepared, dtsCompleteTrack: begin
      SetSubItem(aItem, 0, '');
      SetSubItem(aItem, 1, '');
      SetSubItem(aItem, 2, IntToStr(ATrack.Count));
      SetSubItem(aItem, 3, '');
      SetSubItem(aItem, 4, '');
      SetSubItem(aItem, 5, '');
    end;
  end;
end;

procedure TfrDvrPlayList.lvDVRFileListSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  if Selected and Assigned(Item) then begin
    PlayItem(Item);
  end
  ;
end;

procedure TfrDvrPlayList.PlayItem(aItem: TListItem);
var
  ATrack: TDVRTrack;
begin
  ATrack := TDVRTrack(aItem.Data);

  if (ATrack.Status = dtsNew) then begin
    ATrack.PrepareGPSData;
    UpdateItemView(aItem);
  end;

  if not (ATrack.Status in [dtsNew, dtsFailed]) then begin
    StopPlayback;
    FPlayer.MediaAddr := ATrack.VideoFile;
    try
      FPlayer.OpenMedia;

    except
      on E: Exception do begin
        ATrack.ReportWrongVideo(E.Message);
        UpdateItemView(aItem);

        PlaybackCompleeted(nil);
      end;
    end;
  end else begin //���-�� �� ���, ���������
    PlaybackCompleeted(nil);
  end
  ;
end;

procedure TfrDvrPlayList.PlaybackCompleeted(Sender: TObject);
//���������� ���� �� ������ �� �����
var
  vItem: TListItem;
  i: integer;
begin
  i := 0;
  vItem := lvDVRFileList.Selected;
  if VItem <> nil then begin
    i := lvDVRFileList.Selected.Index+1;
    if i > lvDVRFileList.Items.Count-1 then
      i := 0
    ;
  end;
  if Assigned(lvDVRFileList.Items[i]) then
    lvDVRFileList.Items[i].Selected := true
  ;
end;

{ TDVRTrack }

constructor TDVRTrack.Create(AOwner: TListItem; const AVideoFile: string);
begin
  inherited Create;

  FOwner := AOwner;
  FVideoFile := AVideoFile;
  FStatus := dtsNew;
end;

destructor TDVRTrack.Destroy;
begin

  inherited;
end;

function TDVRTrack.GetCount: UInt16;
begin
  if Assigned(FGPSData) and (FStatus in [dtsGPSPrepared, dtsCompleteTrack]) then
    Result := Length(FGPSData)
  else
    Result := 0
  ;
end;

function TDVRTrack.GetFileExt: string;
begin
  Result := LowerCase(ExtractFileExt(FVideoFile));
end;

procedure TDVRTrack.PrepareGPSData;
var
  aTSParser: TTsParser;
begin
  SetStatus(dtsGPSMissing);
  if FileExt = '.ts' then begin
    aTSParser := TTsParser.Create(FVideoFile);
    try
      FGPSData := aTSParser.GetTrackSeg;
      if Length(FGPSData)>0 then
        SetStatus(dtsGPSPrepared)
      ;
    finally
      aTSParser.Free;
    end;
  end else begin
  end;
end;

procedure TDVRTrack.ReportWrongVideo(const aErr: string);
begin
  SetStatus(dtsFailed);
end;

procedure TDVRTrack.SetStatus(aNewStatus: TDVRTrackStatus);
begin
  FStatus := aNewStatus;
end;

end.
