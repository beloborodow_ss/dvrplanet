object frmMain: TfrmMain
  Left = 488
  Top = 165
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Akenori.DVR.Planet'
  ClientHeight = 712
  ClientWidth = 1251
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = False
  Position = poDefault
  ShowHint = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnMouseWheel = FormMouseWheel
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object map: TImage32
    Left = 105
    Top = 59
    Width = 276
    Height = 348
    Align = alClient
    Bitmap.CombineMode = cmMerge
    Bitmap.ResamplerClassName = 'TLinearResampler'
    BitmapAlign = baTopLeft
    Color = clSilver
    ParentColor = False
    RepaintMode = rmOptimizer
    Scale = 1.000000000000000000
    ScaleMode = smNormal
    TabOrder = 0
    OnDblClick = mapDblClick
    OnMouseLeave = mapMouseLeave
  end
  object TBDock: TTBXDock
    Left = 0
    Top = 0
    Width = 1251
    Height = 59
    PopupMenu = TBXPopupPanels
    Visible = False
    object TBMainToolBar: TTBXToolbar
      Left = 0
      Top = 25
      DockPos = -6
      DockRow = 1
      Images = PanelsImageList
      Stretch = True
      TabOrder = 0
      Visible = False
      Caption = 'Main'
      object TBmove: TTBXItem
        Checked = True
        ImageIndex = 8
        Images = PanelsImageList
        Options = [tboDefault]
        OnClick = TBmoveClick
        Caption = ''
        Hint = 'Move'
      end
      object TBRectSave: TTBXSubmenuItem
        DropdownCombo = True
        ImageIndex = 10
        Images = PanelsImageList
        Options = [tboShowHint]
        Visible = False
        OnClick = TBRectSaveClick
        Caption = ''
        Hint = 'Selection manager'
      end
      object TBCalcRas: TTBXItem
        AutoCheck = True
        ImageIndex = 9
        Images = PanelsImageList
        OnClick = TBCalcRasClick
        Caption = ''
        Hint = 'Distance calculation'
      end
      object TBXSeparatorItem4: TTBXSeparatorItem
        Caption = ''
        Hint = ''
      end
      object TBMapZap: TTBXSubmenuItem
        DisplayMode = nbdmImageAndText
        ImageIndex = 7
        Images = PanelsImageList
        LinkSubitems = NFillMap
        Options = [tboDropdownArrow, tboShowHint]
        Visible = False
        Caption = ''
        Hint = 'Cached tiles map'
      end
      object TBGoTo: TTBXSubmenuItem
        DropdownCombo = True
        ImageIndex = 11
        Images = PanelsImageList
        Options = [tboShowHint]
        OnClick = TBSubmenuItem1Click
        Caption = ''
        Hint = 'Go to'
      end
      object TBXSeparatorItem5: TTBXSeparatorItem
        Caption = ''
        Hint = ''
      end
      object TBFullSize: TTBXItem
        AutoCheck = True
        ImageIndex = 4
        Images = PanelsImageList
        OnClick = TBFullSizeClick
        Caption = ''
        Hint = 'Full screen (F11)'
      end
    end
    object SrcToolbar: TTBXToolbar
      Left = 224
      Top = 25
      DockPos = 224
      DockRow = 1
      Stretch = True
      TabOrder = 1
      Caption = 'Sources'
      object TBSrc: TTBXSubmenuItem
        ImageIndex = 0
        Images = PanelsImageList
        LinkSubitems = NSources
        Options = [tboDropdownArrow]
        Visible = False
        Caption = ''
        Hint = 'Select data source'
      end
      object TBSMB: TTBXSubmenuItem
        DisplayMode = nbdmImageAndText
        ImageIndex = 3
        Images = PanelsImageList
        Options = [tboDropdownArrow]
        Caption = ''
        Hint = 'Selected basemap'
      end
      object TBLayerSel: TTBXSubmenuItem
        ImageIndex = 3
        Images = PanelsImageList
        Options = [tboDropdownArrow]
        Visible = False
        Caption = ''
        Hint = 'Select overlay layers'
      end
    end
    object TBMarksToolbar: TTBXToolbar
      Left = 363
      Top = 25
      DockPos = 363
      DockRow = 1
      Images = PanelsImageList
      LinkSubitems = NMarksGroup
      Stretch = True
      TabOrder = 2
      Visible = False
      Caption = 'Placemarks'
    end
    object GPSToolbar: TTBXToolbar
      Left = 504
      Top = 25
      DockPos = 504
      DockRow = 1
      Images = PanelsImageList
      Stretch = True
      TabOrder = 3
      Visible = False
      Caption = 'GPS'
      object TBGPSconn: TTBXItem
        AutoCheck = True
        ImageIndex = 14
        Images = PanelsImageList
        OnClick = TBGPSconnClick
        Caption = ''
        Hint = 'Connect to GPS receiver'
      end
      object TBGPSPath: TTBXSubmenuItem
        AutoCheck = True
        DropdownCombo = True
        ImageIndex = 6
        Images = PanelsImageList
        OnClick = TBGPSPathClick
        Caption = ''
        Hint = 'Show GPS track'
        object tbitmSaveCurrentPositionToolbar: TTBXItem
          ImageIndex = 15
          Images = MenusImageList
          OnClick = tbitmSaveCurrentPositionClick
          Caption = 'Add Placemark'
          Hint = ''
        end
        object TBXSeparatorItem16: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object tbitmGPSTrackSaveToMarks: TTBXItem
          ImageIndex = 25
          Images = MenusImageList
          OnClick = tbitmGPSTrackSaveToMarksClick
          Caption = 'Add Track to Database'
          Hint = ''
        end
        object TBXSeparatorItem17: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object TBItemDelTrack: TTBXItem
          ImageIndex = 35
          Images = MenusImageList
          OnClick = TBItemDelTrackClick
          Caption = 'Delete Track'
          Hint = ''
        end
      end
      object TBGPSToPoint: TTBXSubmenuItem
        AutoCheck = True
        DropdownCombo = True
        ImageIndex = 5
        Images = PanelsImageList
        OnClick = TBGPSToPointClick
        Caption = ''
        Hint = 'Follow GPS Position'
        object TBGPSToPointCenter: TTBXItem
          AutoCheck = True
          OnClick = TBGPSToPointCenterClick
          Caption = 'Centered GPS Position'
          Hint = ''
        end
      end
    end
    object TBExit: TTBXToolbar
      Left = 807
      Top = 25
      DockPos = 807
      DockRow = 1
      TabOrder = 4
      Visible = False
      object TBXExit: TTBXItem
        ImageIndex = 29
        Images = MenusImageList
        OnClick = TrayItemQuitClick
        Caption = ''
        Hint = 'Quit'
      end
    end
    object TBXMainMenu: TTBXToolbar
      Left = 0
      Top = 0
      CloseButton = False
      DockPos = -6
      MenuBar = True
      ProcessShortCuts = True
      ShrinkMode = tbsmWrap
      Stretch = True
      TabOrder = 5
      Visible = False
      Caption = 'Main Menu'
      object NOperations: TTBXSubmenuItem
        Caption = '&Operations'
        Hint = ''
        object tbitmCreateShortcut: TTBXItem
          Images = MenusImageList
          Visible = False
          OnClick = tbitmCreateShortcutClick
          Caption = 'Create Shortcut'
          Hint = ''
        end
        object tbitmOpenFile: TTBXItem
          ImageIndex = 34
          Images = MenusImageList
          OnClick = tbitmOpenFileClick
          Caption = 'Open...'
          Hint = ''
        end
        object TBXSeparatorItem6: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object NZoomIn: TTBXItem
          ImageIndex = 23
          Images = MenusImageList
          ShortCut = 33
          OnClick = NzoomInClick
          Caption = 'Zoom In'
          Hint = ''
        end
        object NZoomOut: TTBXItem
          ImageIndex = 24
          Images = MenusImageList
          ShortCut = 34
          OnClick = NZoomOutClick
          Caption = 'Zoom Out'
          Hint = ''
        end
        object TBXSeparatorItem7: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object tbitmGoToModal: TTBXItem
          ImageIndex = 11
          Images = MenusImageList
          ShortCut = 16455
          OnClick = TBSubmenuItem1Click
          Caption = 'Go to...'
          Hint = ''
        end
        object NCalcRast: TTBXItem
          ImageIndex = 9
          Images = MenusImageList
          ShortCut = 16460
          OnClick = NCalcRastClick
          Caption = 'Distance Calculation'
          Hint = ''
        end
        object TBXSeparatorItem8: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object TBXSeparatorItem9: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object tbitmQuit: TTBXItem
          ImageIndex = 29
          Images = MenusImageList
          OnClick = tbitmQuitClick
          Caption = 'Quit'
          Hint = ''
        end
      end
      object NView: TTBXSubmenuItem
        Caption = '&View'
        Hint = ''
        object NPanels: TTBXSubmenuItem
          Images = MenusImageList
          Caption = 'Toolbars'
          Hint = ''
          object NMainToolBarShow: TTBXVisibilityToggleItem
            Control = TBMainToolBar
            Images = MenusImageList
            Caption = 'Main'
            Hint = ''
          end
          object NZoomToolBarShow: TTBXVisibilityToggleItem
            Control = ZoomToolBar
            Images = MenusImageList
            Caption = 'Zoom'
            Hint = ''
          end
          object NsrcToolBarShow: TTBXVisibilityToggleItem
            Control = SrcToolbar
            Images = MenusImageList
            Caption = 'Sources'
            Hint = ''
          end
          object NGPSToolBarShow: TTBXVisibilityToggleItem
            Control = GPSToolbar
            Images = MenusImageList
            Visible = False
            Caption = 'GPS'
            Hint = ''
          end
          object TBXVisibilityToggleItem1: TTBXVisibilityToggleItem
            Control = TBMarksToolbar
            Images = MenusImageList
            Caption = 'Placemarks'
            Hint = ''
          end
          object TBXVisibilityToggleItem2: TTBXVisibilityToggleItem
            Control = TBXToolBarSearch
            Images = MenusImageList
            Caption = 'Search'
            Hint = ''
          end
          object NSearchResults: TTBXVisibilityToggleItem
            Control = TBSearchWindow
            Images = MenusImageList
            Visible = False
            Caption = 'Search Results'
            Hint = ''
          end
          object TBXVisibilityToggleItem3: TTBXVisibilityToggleItem
            Control = FillDates
            Images = MenusImageList
            Visible = False
            Caption = 'Time Interval'
            Hint = ''
          end
          object NSensors: TTBXSubmenuItem
            AutoCheck = True
            DropdownCombo = True
            Images = MenusImageList
            Visible = False
            OnClick = NSensorsClick
            Caption = 'Sensors'
            Hint = ''
            object NFillMap: TTBXSubmenuItem
              ImageIndex = 7
              Images = MenusImageList
              Visible = False
              OnClick = NFillMapClick
              Caption = 'Cached Tiles Map'
              Hint = ''
              object TBFillingTypeMap: TTBXSubmenuItem
                Images = MenusImageList
                Options = [tboDropdownArrow]
                Caption = 'Show for...'
                Hint = ''
              end
              object TBXSeparatorItem11: TTBXSeparatorItem
                Caption = ''
                Hint = ''
              end
              object TBXToolPalette1: TTBXToolPalette
                ColCount = 5
                Images = ScalesImageList
                PaletteOptions = []
                RowCount = 7
                OnCellClick = TBXToolPalette1CellClick
                Caption = ''
                Hint = ''
              end
              object TBXSeparatorItem20: TTBXSeparatorItem
                Caption = ''
                Hint = ''
              end
              object NFillMode1: TTBXItem
                Tag = 99
                Checked = True
                GroupIndex = 2
                Images = MenusImageList
                RadioItem = True
                OnClick = NFillMode1Click
                Caption = 'Mark Nonexistent Tiles'
                Hint = ''
              end
              object NFillMode2: TTBXItem
                Tag = 99
                GroupIndex = 2
                Images = MenusImageList
                RadioItem = True
                OnClick = NFillMode2Click
                Caption = 'Mark Existing Tiles'
                Hint = ''
              end
              object NFillMode3: TTBXItem
                Tag = 99
                GroupIndex = 2
                Images = MenusImageList
                RadioItem = True
                OnClick = NFillMode3Click
                Caption = 'Use Age Gradient'
                Hint = ''
              end
              object TBXSeparatorItem21: TTBXSeparatorItem
                Caption = ''
                Hint = ''
              end
              object NShowFillDates: TTBXItem
                Tag = 99
                GroupIndex = 1
                Images = MenusImageList
                OnClick = NShowFillDatesClick
                Caption = 'Within Time Interval'
                Hint = ''
              end
            end
          end
          object TBXVisibilityToggleItem7: TTBXVisibilityToggleItem
            Control = TBXDVRControlsPanel
            Caption = 'Playback controls'
            Hint = ''
          end
          object TBXVisibilityToggleItem6: TTBXVisibilityToggleItem
            Control = TBXPlayListPanel
            Caption = 'Playlist'
            Hint = ''
          end
          object TBXVisibilityToggleItem5: TTBXVisibilityToggleItem
            Control = TBXSliderPanel
            Caption = 'Time pos && Charts'
            Hint = ''
          end
          object TBXVisibilityToggleItem4: TTBXVisibilityToggleItem
            Control = TBXIndicatorPanel
            Caption = 'Indicators'
            Hint = ''
          end
          object TBXSeparatorItem18: TTBXSeparatorItem
            Caption = ''
            Hint = ''
          end
          object NBlock_toolbars: TTBXItem
            AutoCheck = True
            Images = MenusImageList
            OnClick = NBlock_toolbarsClick
            Caption = 'Lock Toolbars'
            Hint = ''
          end
          object TBXItem1: TTBXItem
            OnClick = TBXItem1Click
            Caption = 'Full screen off'
            Hint = ''
          end
        end
        object tbsbmInterface: TTBXSubmenuItem
          Images = MenusImageList
          Caption = 'Interface'
          Hint = ''
          object Showstatus: TTBXItem
            AutoCheck = True
            Images = MenusImageList
            ShortCut = 32851
            OnClick = ShowstatusClick
            Caption = 'Status Bar'
            Hint = ''
          end
          object ShowMiniMap: TTBXItem
            AutoCheck = True
            Images = MenusImageList
            ShortCut = 32845
            OnClick = ShowMiniMapClick
            Caption = 'Overview Map'
            Hint = ''
          end
          object ShowLine: TTBXItem
            AutoCheck = True
            Images = MenusImageList
            ShortCut = 32844
            OnClick = ShowLineClick
            Caption = 'Scale Legend'
            Hint = ''
          end
        end
        object NShowGran: TTBXSubmenuItem
          ImageIndex = 3
          Images = MenusImageList
          Visible = False
          OnClick = NShowGranClick
          Caption = 'Tile Boundaries'
          Hint = ''
          object N000: TTBXItem
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = 'No'
            Hint = ''
          end
          object N001: TTBXItem
            Tag = 100
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = 'Current Zoom'
            Hint = ''
          end
          object N002: TTBXItem
            Tag = 2
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = '2'
            Hint = ''
          end
          object N003: TTBXItem
            Tag = 3
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = '3'
            Hint = ''
          end
          object N004: TTBXItem
            Tag = 4
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = '4'
            Hint = ''
          end
          object N005: TTBXItem
            Tag = 5
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = '5'
            Hint = ''
          end
          object N006: TTBXItem
            Tag = 6
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = '6'
            Hint = ''
          end
          object N007: TTBXItem
            Tag = 7
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = '7'
            Hint = ''
          end
          object tbitmTileGrid1p: TTBXItem
            Tag = 101
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = 'Current Zoom + 1'
            Hint = ''
          end
          object tbitmTileGrid2p: TTBXItem
            Tag = 102
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = 'Current Zoom + 2'
            Hint = ''
          end
          object tbitmTileGrid3p: TTBXItem
            Tag = 103
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = 'Current Zoom + 3'
            Hint = ''
          end
          object tbitmTileGrid4p: TTBXItem
            Tag = 104
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = 'Current Zoom + 4'
            Hint = ''
          end
          object tbitmTileGrid5p: TTBXItem
            Tag = 105
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = N000Click
            Caption = 'Current Zoom + 5'
            Hint = ''
          end
        end
        object DegreedLinesSubMenu: TTBXSubmenuItem
          Images = MenusImageList
          Visible = False
          Caption = 'Lat/Lon Grid'
          Hint = ''
          object NDegScale0: TTBXItem
            AutoCheck = True
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = 'No'
            Hint = ''
          end
          object NDegScale1000000: TTBXItem
            Tag = 1000000000
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = '10'#176
            Hint = ''
          end
          object NDegScale500000: TTBXItem
            Tag = 500000000
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = '5'#176
            Hint = ''
          end
          object NDegScale200000: TTBXItem
            Tag = 200000000
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = '2'#176
            Hint = ''
          end
          object NDegScale100000: TTBXItem
            Tag = 100000000
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = '1'#176
            Hint = ''
          end
          object NDegScale50000: TTBXItem
            Tag = 50000000
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = '0.5'#176
            Hint = ''
          end
          object NDegScale25000: TTBXItem
            Tag = 25000000
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = '0.25'#176
            Hint = ''
          end
          object NDegScale10000: TTBXItem
            Tag = 12500000
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = '0.125'#176
            Hint = ''
          end
          object TBXSeparatorItem22: TTBXSeparatorItem
            Caption = ''
            Hint = ''
          end
          object NDegScaleUser: TTBXItem
            Tag = 1
            GroupIndex = 1
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = 'User defined'
            Hint = ''
          end
          object NDegValue: TTBXEditItem
            OnAcceptText = NDegValueAcceptText
            Caption = ''
            Hint = ''
          end
          object TBSeparatorItem2: TTBSeparatorItem
            Caption = ''
            Hint = ''
          end
          object NDegScaleAuto: TTBXItem
            Tag = -100000000
            GroupIndex = 1
            Images = MenusImageList
            RadioItem = True
            OnClick = NDegScale0Click
            Caption = 'Auto'
            Hint = ''
          end
        end
        object tbxsbmProjection: TTBXSubmenuItem
          Images = MenusImageList
          Visible = False
          Caption = 'Projection'
          Hint = ''
        end
        object TBXSeparatorItem10: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object NFoolSize: TTBXItem
          AutoCheck = True
          ImageIndex = 4
          Images = MenusImageList
          ShortCut = 122
          OnClick = TBFullSizeClick
          Caption = 'Full Screen'
          Hint = ''
        end
        object NGoToCur: TTBXItem
          AutoCheck = True
          Checked = True
          Images = MenusImageList
          OnClick = NGoToCurClick
          Caption = 'Zoom to Cursor'
          Hint = ''
        end
        object Nbackload: TTBXItem
          AutoCheck = True
          Checked = True
          Images = MenusImageList
          OnClick = NbackloadClick
          Caption = 'Use Maps from Lower Zooms'
          Hint = ''
        end
        object NbackloadLayer: TTBXItem
          AutoCheck = True
          Checked = True
          Images = MenusImageList
          OnClick = NbackloadLayerClick
          Caption = 'Use Layers from Lower Zooms'
          Hint = ''
        end
        object Nanimate: TTBXItem
          AutoCheck = True
          Checked = True
          Images = MenusImageList
          OnClick = NanimateClick
          Caption = 'Zoom Animation'
          Hint = ''
        end
        object NAnimateMove: TTBXItem
          AutoCheck = True
          Checked = True
          Images = MenusImageList
          Visible = False
          OnClick = NAnimateMoveClick
          Caption = 'Inertial Movement'
          Hint = ''
        end
        object tbitmGauge: TTBXItem
          AutoCheck = True
          Images = MenusImageList
          Visible = False
          OnClick = tbitmGaugeClick
          Caption = 'Azimuth Circle'
          Hint = ''
        end
        object Ninvertcolor: TTBXItem
          AutoCheck = True
          Images = MenusImageList
          ShortCut = 32846
          Visible = False
          OnClick = NinvertcolorClick
          Caption = 'Night Mode (Color Inversion)'
          Hint = ''
        end
        object tbitmNavigationArrow: TTBXItem
          Images = MenusImageList
          Visible = False
          OnClick = tbitmNavigationArrowClick
          Caption = 'Navigation Arrow'
          Hint = ''
        end
        object tbitmShowDebugInfo: TTBXItem
          Images = MenusImageList
          Visible = False
          OnClick = tbitmShowDebugInfoClick
          Caption = 'Debug Info'
          Hint = ''
        end
      end
      object NSources: TTBXSubmenuItem
        Caption = '&Source'
        Hint = ''
        object NSRCesh: TTBXItem
          Tag = 1
          AutoCheck = True
          GroupIndex = 1
          ImageIndex = 1
          Images = PanelsImageList
          RadioItem = True
          ShortCut = 32835
          OnAdjustFont = AdjustFont
          OnClick = NSRCinetClick
          Caption = 'Cache'
          Hint = ''
        end
        object NSRCinet: TTBXItem
          AutoCheck = True
          GroupIndex = 1
          ImageIndex = 0
          Images = PanelsImageList
          RadioItem = True
          ShortCut = 32841
          OnAdjustFont = AdjustFont
          OnClick = NSRCinetClick
          Caption = 'Internet'
          Hint = ''
        end
        object NSRCic: TTBXItem
          Tag = 2
          AutoCheck = True
          GroupIndex = 1
          ImageIndex = 2
          Images = PanelsImageList
          RadioItem = True
          ShortCut = 32834
          OnAdjustFont = AdjustFont
          OnClick = NSRCinetClick
          Caption = 'Internet && Cache'
          Hint = ''
        end
      end
      object NSMB: TTBXSubmenuItem
        LinkSubitems = TBSMB
        Caption = '&Maps'
        Hint = ''
      end
      object NLayerSel: TTBXSubmenuItem
        LinkSubitems = TBLayerSel
        Caption = 'Layers'
        Hint = ''
      end
      object NMarks: TTBXSubmenuItem
        Caption = 'Placemarks'
        Hint = ''
        object NMarksGroup: TTBGroupItem
          Caption = ''
          Hint = ''
          object TBAdd_Point: TTBXItem
            GroupIndex = 1
            ImageIndex = 15
            Images = PanelsImageList
            Options = [tboShowHint]
            Stretch = True
            OnClick = TBAdd_PointClick
            Caption = 'Add Placemark'
            Hint = 'Add new placemark'
          end
          object TBXSeparatorItem12: TTBXSeparatorItem
            Caption = ''
            Hint = ''
          end
          object tbitmPlacemarkManager: TTBXItem
            ImageIndex = 18
            Images = PanelsImageList
            Options = [tboShowHint]
            OnClick = tbitmPlacemarkManagerClick
            Caption = 'Placemark Manager'
            Hint = 'Placemark manager'
          end
          object TBHideMarks: TTBXItem
            AutoCheck = True
            ImageIndex = 19
            Images = PanelsImageList
            OnClick = TBHideMarksClick
            Caption = 'Hide All Placemarks'
            Hint = 'Hide all placemarks'
          end
        end
        object tbitmShowMarkCaption: TTBXItem
          AutoCheck = True
          Checked = True
          Images = PanelsImageList
          OnClick = tbitmShowMarkCaptionClick
          Caption = 'Placemark Names'
          Hint = ''
        end
      end
      object tbsbmGPS: TTBXSubmenuItem
        Visible = False
        Caption = 'GPS'
        Hint = ''
        object tbitmGPSConnect: TTBXItem
          AutoCheck = True
          ImageIndex = 14
          Images = MenusImageList
          ShortCut = 49223
          OnClick = TBGPSconnClick
          Caption = 'Connect to GPS Receiver'
          Hint = ''
        end
        object tbitmGPSTrackShow: TTBXItem
          AutoCheck = True
          ImageIndex = 6
          Images = MenusImageList
          ShortCut = 49236
          OnClick = TBGPSPathClick
          Caption = 'Show GPS Track'
          Hint = ''
        end
        object tbitmGPSCenterMap: TTBXItem
          AutoCheck = True
          ImageIndex = 5
          Images = MenusImageList
          OnClick = TBGPSToPointClick
          Caption = 'Follow GPS Position'
          Hint = ''
        end
        object tbitmGPSToPointCenter: TTBXItem
          AutoCheck = True
          Images = MenusImageList
          OnClick = TBGPSToPointCenterClick
          Caption = 'Centered GPS Position'
          Hint = ''
        end
        object tbsprtGPS1: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object tbitmSaveCurrentPosition: TTBXItem
          ImageIndex = 15
          Images = MenusImageList
          ShortCut = 49235
          OnClick = tbitmSaveCurrentPositionClick
          Caption = 'Add Placemark'
          Hint = ''
        end
        object tbitmGPSTrackSaveToDb: TTBXItem
          ImageIndex = 25
          Images = MenusImageList
          OnClick = tbitmGPSTrackSaveToMarksClick
          Caption = 'Add Track to Database'
          Hint = ''
        end
        object tbitmGPSTrackClear: TTBXItem
          ImageIndex = 35
          Images = MenusImageList
          OnClick = TBItemDelTrackClick
          Caption = 'Delete Track'
          Hint = ''
        end
        object tbsprtGPS2: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object tbitmPositionByGSM: TTBXItem
          Images = MenusImageList
          OnClick = tbitmPositionByGSMClick
          Caption = 'Get Locaton from GSM (Google Query)'
          Hint = ''
        end
        object TBXSeparatorItem19: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object tbitmGPSOptions: TTBXItem
          ImageIndex = 20
          Images = MenusImageList
          OnClick = tbitmGPSOptionsClick
          Caption = 'Options'
          Hint = ''
        end
      end
      object NParams: TTBXSubmenuItem
        OnPopup = NParamsPopup
        Caption = 'Settings'
        Hint = ''
        object NMapParams: TTBXItem
          Images = MenusImageList
          ShortCut = 49232
          OnClick = NMapParamsClick
          Caption = 'Map Settings'
          Hint = ''
        end
        object NLayerParams: TTBXSubmenuItem
          Images = MenusImageList
          Caption = 'Layer Settings'
          Hint = ''
        end
        object TBXSeparatorItem14: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object tbitmOptions: TTBXItem
          ImageIndex = 20
          Images = MenusImageList
          OnClick = tbitmOptionsClick
          Caption = 'Options'
          Hint = ''
        end
        object tbitmInterfaceOptions: TTBXItem
          Images = MenusImageList
          OnClick = tbitmOnInterfaceOptionsClick
          Caption = 'Interface Options'
          Hint = ''
        end
        object TBLang: TTBXSubmenuItem
          Images = MenusImageList
          Caption = 'Language'
          Hint = ''
        end
      end
      object tbsbmHelp: TTBXSubmenuItem
        Caption = '&Help'
        Hint = ''
        object tbitmOnlineHelp: TTBXItem
          ImageIndex = 26
          Images = MenusImageList
          ShortCut = 112
          OnClick = tbitmOnlineHelpClick
          Caption = 'Online Help (http://akenori.ru)'
          Hint = ''
        end
        object tbitmAbout: TTBXItem
          ImageIndex = 27
          Images = MenusImageList
          OnClick = tbitmAboutClick
          Caption = 'About'
          Hint = ''
        end
        object tbsprtHelp01: TTBXSeparatorItem
          Caption = ''
          Hint = ''
        end
        object tbitmOnlineHome: TTBXItem
          Images = MenusImageList
          OnClick = tbitmOnlineHomeClick
          Caption = 'Web Site (http://akenori.ru)'
          Hint = ''
        end
      end
    end
    object TBXToolBarSearch: TTBXToolbar
      Left = 413
      Top = 0
      DockPos = 413
      Options = [tboNoRotation]
      Stretch = True
      TabOrder = 6
      Visible = False
      Caption = 'Search'
      object TBXSelectSrchType: TTBXSubmenuItem
        Options = [tboDropdownArrow, tboNoRotation]
        Caption = 'Google'
        Hint = ''
      end
      object tbiSearch: TTBXComboBoxItem
        EditCaption = 'Search'
        EditWidth = 150
        Options = [tboNoRotation]
        OnAcceptText = TBXSearchEditAcceptText
        AutoComplete = False
        MaxVisibleItems = 20
        MinListWidth = 150
        Caption = ''
        Hint = ''
        EditCaption = 'Search'
      end
    end
    object FillDates: TTBXToolbar
      Left = 643
      Top = 0
      DefaultDock = TBDock
      DockPos = 643
      TabOrder = 7
      Visible = False
      Caption = 'FillDates'
      object TBControlItem7: TTBControlItem
        Control = Label1
        Caption = ''
        Hint = ''
      end
      object TBControlItem6: TTBControlItem
        Control = DateTimePicker1
        Caption = ''
        Hint = ''
      end
      object TBControlItem8: TTBControlItem
        Control = Label2
        Caption = ''
        Hint = ''
      end
      object TBControlItem9: TTBControlItem
        Control = DateTimePicker2
        Caption = ''
        Hint = ''
      end
      object Label1: TLabel
        Left = 0
        Top = 4
        Width = 42
        Height = 13
        Caption = 'Fill From '
      end
      object Label2: TLabel
        Left = 123
        Top = 4
        Width = 18
        Height = 13
        Caption = ' To '
      end
      object DateTimePicker1: TDateTimePicker
        Left = 42
        Top = 0
        Width = 81
        Height = 21
        Date = 40830.496065717590000000
        Time = 40830.496065717590000000
        TabOrder = 0
        OnChange = DateTimePicker1Change
      end
      object DateTimePicker2: TDateTimePicker
        Left = 141
        Top = 0
        Width = 81
        Height = 21
        Date = 40830.496065717590000000
        Time = 40830.496065717590000000
        TabOrder = 1
        OnChange = DateTimePicker2Change
      end
    end
  end
  object TBDockBottom: TTBXMultiDock
    Left = 0
    Top = 407
    Width = 1251
    Height = 305
    PopupMenu = TBXPopupPanels
    Position = dpBottom
    OnResize = TBDockBottomResize
    UseThemeColor = False
    object TBXSliderPanel: TTBXDockablePanel
      Left = 507
      Top = 0
      CloseButton = False
      CloseButtonWhenDocked = False
      DockedHeight = 301
      DockPos = 507
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ShowCaption = False
      ShowCaptionWhenDocked = False
      SplitWidth = 359
      TabOrder = 0
      Caption = 'Time pos && Charts'
      object TimeSlider1: TTimeSlider
        Left = 0
        Top = 0
        Width = 381
        Height = 301
        Align = alClient
        CaptSpeed = 'Speed, km/h'
        CaptAccel = 'Acceleration, m/s^2'
        CaptTimePos = 'Time, s:'
        LargeChange = 10.000000000000000000
        SmallChange = 1.000000000000000000
      end
    end
    object TBXPlayListPanel: TTBXDockablePanel
      Left = 892
      Top = 0
      CloseButton = False
      CloseButtonWhenDocked = False
      DockedHeight = 301
      DockPos = 892
      ShowCaption = False
      ShowCaptionWhenDocked = False
      SplitWidth = 316
      TabOrder = 1
      Caption = 'Playlist'
      inline frDvrPlayList1: TfrDvrPlayList
        Left = 0
        Top = 0
        Width = 339
        Height = 301
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 339
        ExplicitHeight = 301
        inherited lvDVRFileList: TListView
          Width = 339
          Height = 301
          Columns = <
            item
              Caption = 'Name'
              Width = 170
            end
            item
              Caption = 'Duration, s'
              Width = 65
            end
            item
              Caption = 'Date/Time'
              Width = 120
            end
            item
              Caption = 'Points'
              Width = 45
            end
            item
              Caption = 'Dist.,m'
            end
            item
              Caption = 'Max'
              Width = 45
            end
            item
              Caption = 'Avg'
              Width = 45
            end
            item
              Caption = 'Shock'
              Width = 40
            end
            item
              Caption = 'Full file path'
              Width = 150
            end>
          ExplicitWidth = 339
          ExplicitHeight = 301
        end
      end
    end
    object TBXIndicatorPanel: TTBXDockablePanel
      Left = 0
      Top = 0
      Color = clWhite
      CloseButton = False
      CloseButtonWhenDocked = False
      DockedHeight = 301
      DockPos = 0
      ShowCaption = False
      ShowCaptionWhenDocked = False
      SplitWidth = 480
      TabOrder = 2
      UseThemeColor = False
      Caption = 'Indicators'
      object AbVBar1: TAbVBar
        Left = 279
        Top = 13
        Width = 24
        Height = 148
        Digit = 505
        LimitUpper = 2.000000000000000000
        LimitLower = -2.000000000000000000
        SectorSettings.Offset = 0
        SectorSettings.WidthOffset = 0
        SignalSettings.Name1 = 'SignalName1'
        SignalSettings.Name2 = 'SignalName2'
        SignalSettings.ValueFormat = '##0.0'
        SignalSettings.ValueFrom = -10.000000000000000000
        SignalSettings.ValueTo = 10.000000000000000000
        SignalSettings.ValueUnit = '%'
        Value = 0.100000001490116100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        AutoSize = False
        FontValue.Charset = DEFAULT_CHARSET
        FontValue.Color = clLime
        FontValue.Height = -13
        FontValue.Name = 'System'
        FontValue.Style = []
        BarSettings.Style = bsBar
        BarSettings.Font.Charset = DEFAULT_CHARSET
        BarSettings.Font.Color = clBtnHighlight
        BarSettings.Font.Height = -11
        BarSettings.Font.Name = 'Tahoma'
        BarSettings.Font.Style = []
        BarSettings.Options = []
        BarSettings.Color = clLime
        BarSettings.BkColor = clBlack
        BarSettings.BkBrightness = 40.000000000000000000
        BarSettings.BkAuto = False
        BarSettings.Bevel.Style = bsLowered
        BarSettings.Bevel.Spacing = 1
        BarSettings.Bevel.BevelLine = blNone
        BarSettings.Bevel.Width = 1
        BarSettings.Bevel.Color = clBlack
        BarSettings.Bevel.SurfaceGrad.Visible = False
        BarSettings.Bevel.SurfaceGrad.Style = gsHorizontal1
        BarSettings.minHeight = 100
        BarSettings.minWidth = 9
        BevelInner.Style = bsLowered
        BevelInner.Spacing = 5
        BevelInner.BevelLine = blInner
        BevelInner.Width = 2
        BevelInner.SurfaceGrad.Visible = False
        BevelInner.SurfaceGrad.Style = gsHorizontal1
        BevelOuter.Style = bsRaised
        BevelOuter.Spacing = 5
        BevelOuter.BevelLine = blOuter
        BevelOuter.Width = 2
        BevelOuter.SurfaceGrad.Visible = False
        BevelOuter.SurfaceGrad.Style = gsHorizontal1
        BevelValue.Style = bsLowered
        BevelValue.Spacing = 0
        BevelValue.BevelLine = blNone
        BevelValue.Width = 2
        BevelValue.Color = clBlack
        BevelValue.SurfaceGrad.Visible = False
        BevelValue.SurfaceGrad.Style = gsHorizontal1
        Options = []
      end
      object Label8: TLabel
        Left = 370
        Top = 3
        Width = 56
        Height = 13
        Caption = 'Accel. (X,Y)'
      end
      object Label9: TLabel
        Left = 263
        Top = 3
        Width = 46
        Height = 13
        Caption = 'Accel. (Z)'
      end
      object Label3: TLabel
        Left = 119
        Top = 3
        Width = 30
        Height = 13
        Caption = 'Speed'
      end
      object Speedometer1: TSpeedometer
        Left = 3
        Top = 26
        Width = 256
        Height = 130
      end
      object Accelerometer1: TAccelerometer
        Left = 325
        Top = 16
        Width = 140
        Height = 140
      end
    end
  end
  object TBDockLeft: TTBXDock
    Left = 0
    Top = 59
    Width = 105
    Height = 348
    PopupMenu = TBXPopupPanels
    Position = dpLeft
    object ZoomToolBar: TTBXToolbar
      Left = 0
      Top = 0
      DockPos = -6
      Stretch = True
      TabOrder = 0
      OnDockChanging = ZoomToolBarDockChanging
      Caption = 'Zoom'
      object TBZoomIn: TTBXItem
        ImageIndex = 23
        Images = MenusImageList
        MinHeight = 29
        MinWidth = 29
        OnClick = NzoomInClick
        Caption = ''
        Hint = 'Zoom In'
      end
      object TBXSeparatorItem1: TTBXSeparatorItem
        Blank = True
        Size = 3
        Caption = ''
        Hint = ''
      end
      object TBControlItem1: TTBControlItem
        Control = ZSlider
        Caption = ''
        Hint = ''
      end
      object TBXSeparatorItem3: TTBXSeparatorItem
        Blank = True
        Size = 3
        Caption = ''
        Hint = ''
      end
      object TBZoom_out: TTBXItem
        ImageIndex = 24
        Images = MenusImageList
        MinHeight = 29
        MinWidth = 29
        OnClick = NZoomOutClick
        Caption = ''
        Hint = 'Zoom Out'
      end
      object TBXSeparatorItem2: TTBXSeparatorItem
        Blank = True
        Size = 4
        Caption = ''
        Hint = ''
      end
      object TBControlItem2: TTBControlItem
        Control = labZoom
        Caption = ''
        Hint = ''
      end
      object labZoom: TLabel
        Left = 29
        Top = 230
        Width = 14
        Height = 13
        Hint = 'Current Zoom'
        Alignment = taCenter
        Caption = 'z1'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Transparent = True
      end
      object ZSlider: TImage32
        Left = 2
        Top = 32
        Width = 25
        Height = 153
        AutoSize = True
        Bitmap.ResamplerClassName = 'TNearestResampler'
        BitmapAlign = baTopLeft
        Scale = 1.000000000000000000
        ScaleMode = smNormal
        TabOrder = 0
        OnMouseMove = ZSliderMouseMove
        OnMouseUp = ZSliderMouseUp
      end
    end
    object TBSearchWindow: TTBXDockablePanel
      Left = 33
      Top = 0
      DockedWidth = 68
      DockPos = -6
      DockRow = 2
      TabOrder = 1
      Visible = False
      OnClose = TBSearchWindowClose
      Caption = 'Search Results'
      object PanelSearch: TPanel
        Left = 0
        Top = 0
        Width = 68
        Height = 328
        Align = alClient
        AutoSize = True
        BevelOuter = bvNone
        TabOrder = 0
        object TBXDockForSearch: TTBXDock
          Left = 0
          Top = 0
          Width = 68
          Height = 9
        end
        object ScrollBoxSearchWindow: TScrollBox
          Left = 0
          Top = 9
          Width = 68
          Height = 319
          HorzScrollBar.Visible = False
          VertScrollBar.Smooth = True
          VertScrollBar.Tracking = True
          Align = alClient
          Color = clWhite
          ParentColor = False
          TabOrder = 1
        end
      end
    end
    object TBEditPath: TTBXToolbar
      Left = 0
      Top = 240
      DockPos = 240
      TabOrder = 2
      OnClose = TBEditPathClose
      object TBEditPathDel: TTBXItem
        ImageIndex = 36
        Images = MenusImageList
        OnClick = TBEditPathDelClick
        Caption = ''
        Hint = 'Delete Point'
      end
      object TBEditPathLabel: TTBXItem
        AutoCheck = True
        Checked = True
        ImageIndex = 37
        Images = MenusImageList
        OnClick = TBEditPathLabelClick
        Caption = ''
        Hint = 'Show/Hide Captions'
      end
      object TBEditMagnetDraw: TTBXItem
        AutoCheck = True
        ImageIndex = 41
        Images = MenusImageList
        OnClick = TBEditMagnetDrawClick
        Caption = ''
        Hint = 'Attract to The Existing Markers'
      end
      object tbitmFitEditToScreen: TTBXItem
        ImageIndex = 43
        Images = MenusImageList
        OnClick = tbitmFitEditToScreenClick
        Caption = ''
        Hint = 'Fit to screen'
      end
      object TBEditSelectPolylineRadiusCap1: TTBXLabelItem
        Margin = 2
        Caption = 'Radius'
        Hint = ''
      end
      object TBControlItem4: TTBControlItem
        Control = TBEditSelectPolylineRadius
        Caption = ''
        Hint = ''
      end
      object TBEditSelectPolylineRadiusCap2: TTBXLabelItem
        Margin = 2
        Caption = 'm'
        Hint = ''
      end
      object TBEditPathMarsh: TTBXSubmenuItem
        ImageIndex = 39
        Images = MenusImageList
        Options = [tboDropdownArrow]
        Caption = ''
        Hint = 'Route Calculation'
      end
      object TBEditPathOk: TTBXItem
        FontSettings.Bold = tsTrue
        FontSettings.Color = clNavy
        FontSettings.Name = 'Arial'
        ImageIndex = 38
        Images = MenusImageList
        Options = [tboImageAboveCaption, tboNoRotation, tboSameWidth]
        OnClick = TBEditPathOkClick
        Caption = ''
        Hint = 'Manage Selection'
      end
      object tbitmSaveMark: TTBXSubmenuItem
        DropdownCombo = True
        ImageIndex = 25
        Images = MenusImageList
        OnClick = TBEditPathSaveClick
        Caption = ''
        Hint = 'Add to Database'
        object tbitmSaveMarkAsNew: TTBXItem
          OnClick = tbitmSaveMarkAsNewClick
          Caption = 'Save as new'
          Hint = 'Save placemark in Database as new'
        end
      end
      object TBEditSelectPolylineRadius: TSpinEdit
        Left = 29
        Top = 98
        Width = 61
        Height = 22
        MaxValue = 100000
        MinValue = 1
        TabOrder = 0
        Value = 100000
        OnChange = TBEditSelectPolylineRadiusChange
      end
    end
  end
  object TBDockRight: TTBXMultiDock
    Left = 381
    Top = 59
    Width = 870
    Height = 348
    PopupMenu = TBXPopupPanels
    Position = dpRight
    OnResize = TBDockRightResize
    object TBXMplayerPanel: TTBXDockablePanel
      Left = 0
      Top = 0
      DockedWidth = 866
      DockPos = 0
      FloatingWidth = 285
      FloatingHeight = 128
      ShowCaptionWhenDocked = False
      SplitHeight = 369
      TabOrder = 0
      Caption = 'TBXDockablePanel1'
      object MPlayer1: TMPlayer
        Left = 0
        Top = 0
        Width = 866
        Height = 288
        Align = alClient
        BevelOuter = bvNone
        Color = 16448250
        ParentBackground = False
        TabOrder = 0
        PropFormVisible = False
      end
    end
    object TBXDVRControlsPanel: TTBXDockablePanel
      Left = 0
      Top = 292
      MaxClientHeight = 36
      MinClientHeight = 36
      CaptionRotation = dpcrAlwaysVert
      CloseButtonWhenDocked = False
      DblClickUndock = True
      DockedWidth = 866
      DockPos = 292
      Resizable = False
      ShowCaptionWhenDocked = False
      SplitHeight = 42
      TabOrder = 1
      Caption = 'Playback controls'
      inline frDVRButtons1: TfrDVRButtons
        Left = 0
        Top = 0
        Width = 866
        Height = 36
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 866
        ExplicitHeight = 36
        inherited TBXDock1: TTBXDock
          Width = 866
          ExplicitWidth = 866
          inherited TBXToolbar1: TTBXToolbar
            Left = 4
            DockPos = 4
            ExplicitLeft = 4
            Caption = 'TBXToolbar1'
          end
          inherited TBXToolbar2: TTBXToolbar
            Caption = 'TBXToolbar2'
          end
          inherited TBXToolbar3: TTBXToolbar
            Caption = 'TBXToolbar3'
          end
          inherited TBXToolbar4: TTBXToolbar
            Caption = 'TBXToolbar2'
          end
        end
      end
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.hlg'
    Filter = 'Selections|*.hlg'
    Left = 161
    Top = 84
  end
  object SaveLink: TSaveDialog
    DefaultExt = 'lnk'
    Filter = 'Shortcut|*.lnk'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 113
    Top = 212
  end
  object TBXPopupMenuSensors: TTBXPopupMenu
    LinkSubitems = NSensors
    Left = 298
    Top = 193
  end
  object OpenSessionDialog: TOpenDialog
    DefaultExt = '*.sls'
    Filter = 
      'All compatible formats (*.kml,*.plt,*.kmz,*.sls,*.hlg,*.gpx,*.jp' +
      'g,*.ts,*.mp4,*.mov)|*.kml;*.plt;*.kmz;*.sls;*.hlg;*.gpx;*.jpg;*.' +
      'ts;*.mp4;*.mov|Google KML files (*.kml)|*.kml|OziExplorer Track ' +
      'Point File Version 2.1 (*.plt)|*.plt|Google KMZ files (*.kmz)|*.' +
      'kmz|Download session (*.sls)|*.sls|Selection (*.hlg)|*.hlg|GPS E' +
      'xchange files (*.gpx)|*.gpx|JPEG Image with Exif (*.jpg)|*.jpg|'#1042 +
      #1080#1076#1077#1086'-'#1092#1072#1081#1083#1099'|*.ts;*.mp4;*.mov'
    Options = [ofAllowMultiSelect, ofEnableSizing]
    Left = 272
    Top = 264
  end
  object PanelsImageList: TTBXImageList
    Height = 24
    Width = 24
    Left = 48
    Top = 176
    PngDIB = {
      1600000089504E470D0A1A0A0000000D49484452000000180000021008060000
      009BBBAEBC00004952494441547801ED7D055C5549F8F69C5B70695404094545
      4CEC4ECCB5BB3050B163D535508C552C30776D8C55C1D6750DD0B5BBD17515D7
      552C14BB10692EDCFB3DEFDCF01682E8FFFB7DB1F737CF997AE77DA7CECC7B66
      E69C2BA8542AF63FF993E4817951D0F832F3BFD38CB1A7408E4694630C639688
      F3BF78F192DFE4A0A97E83060F0DF6EBD5772381DC1446714403102D2C539353
      09EA82D42B24647ECB9BB762BA88C5629944226172B9653AC2994291E5191FFF
      CC73D9F2954DCE9C39BB27286812053FC0E5226060CC95A0684A4A4A99A1C346
      4CF8EBC6DF7E4E859D627C7C2AEC512814CCCBCB2B9240EE8A157D763B39158A
      219A21A0A534E0EC061818C14C23FB4F0C9CDCFBC993272D4A97F63E307B56F0
      9E2143864D4D4A4EF658BD6AC5684A3D7CC4A865B63636F16BD786CD9D3337A4
      F5AD5BB77A787A7A1E5E303F643BE223009D11E95C6A47C993274FB98379337B
      7BFBA7F5EAD5BBBE73D7EE729F9292BC0A162A187BE9F2155702B9298CE2AA57
      AF7AC7C1DE3E0EBF1667CE9EA5121457B3525F8DDBA0DEC143871B0982489498
      F8A9E8BA75BF058B44222641FDBF7DF3AE12FC952819F9D12E6CCF9EBD93B2B3
      B319B919135864E4C1868D1A367C0E9AC70037C62560CF9F3FF7914A24AF6BD4
      A8B6B156AD1A1BD106D715590A56B66C993DB56BD5DC48A85AB5CA666A878205
      0AC4D44258F5EAD536A2033C7D86B49CABDEC5B8042C3535C5A9A887C7A58913
      C69F66F8054E9AECF42C23830D1E34F0989B9BEB0B049171ECD8A94B0F4BB9E5
      EBC0896ABAF1E327BA3D7CF4A80145EAC3A404D9D94AA6D4BBBB4582388BC2AC
      ACE4D9DA84C9C92992ACAC6C894AA9D206B14C854286304117A0719894402693
      BD7FF7EEBD7748E8025FEA61EF3F7CF0A03A5EF2CBD216724BCB644A97969E6E
      83B611A16739CF0B99EF2B08027BF3E66D290B0B8BF714AF0F1301AEAE2E314F
      9FC637BB70E1C2006A4030E28D7CE3C68D6E5959593CAD442A656234FEABD7AF
      7C5EBC78EE43196010E259ACD8514EA077311670A17DBB76EEBFFCBAB4490147
      C7383FBF9EE12881CDCE9DBBC6BBB8B8DCECD4B1C33E4ABB77EFBECEAF5EBFAE
      D4A347F78568E8E4EDDB77F4FB9090E0D9AE6D9B3388BF00E88CD91B6DD8F011
      7D1F3C78D8AC7AB56ABB4343E74575EFE137352929A9C8B6AD9BC7514ABF5E7D
      96D9D9D9C5EFDAB97D6ED094A9ADA3A3AFF52855AAD451DC885B111F01E88C71
      0928E2ECA2850B240103073B5FB97AB55B5FFF7E254A7979DDBC1A1DED3D7FC1
      A21644909D9D2D2F55CAEB26E2463F7BF6BC9A939353CCC205A13B11779A19FD
      CC958048EAE2E23539684ACBCB57AE76417DCBA82D50D77CB083004BA552C9B2
      95CA4CDC177B4243E61D06FD03E022606072124044340477C7D05164C7CE5DBE
      B801CB252727D3DCC06C6C6C9EBAB9B9DDE9D9A3FBE9264D1ABF04F12E201D30
      31E6AA484B9409C7163070078809BC26E63463EC1990A311E510E38FF03E1AF8
      32FC76EDFEBD449BB6EDA7B66ED36E3AB91144C69731A6A5A334F01A1A730278
      58CD5A75C2EBD56F184E362559BD3A6C04AAC81BE3BE5758D89A61144671FA34
      08E36961EB8C49803686EE622D282C4BA1B0229B80818EBBB5F16453B839E4D4
      C826C59D372FB44ED4C183FD197E6DDBB4D93465CAE44B701A9B08E3809C0410
      9D882E1A2861FB03FA26021E11A0354AAD43DF96E87B8CDCC609228CE2C96B4C
      4361061019F8FE073CFF09C8B552FFDFAB222AD1F780AEEA84F61D3AB3FDFBF6
      D080458C7511DFC1F11A3C8E48C05C9299A990F4EB3F6009638223690888C8B7
      A17109DAC5A3B56B56CF96C9A0C28193FCDDBBB71698A51CADACF81886A0AF33
      480B955EC11349A171A4A4A694209EAEAEAE723E54909E8511926566D21CC3E9
      BEEA9201CD6FCFEFBBFA51A22E5DBB87D3744A3CC9AF16C0549C39CDBB14F8B5
      2001DA34E4A66A5281278571010CE228F7F9AD7FAAA2AEDD7A0413437273050D
      3CC9CF0590C48CF40C28E00285E50B891F133D99E6A72D057985AAD56AB0EBD7
      AEFAB76EDD3628E1E3C732F92D05312350661D1D1CEE1E3A1415027F042F011C
      F19A0038BF9B89274EC633DAF7BAD9741391B604248CA08B20CFF7C0F7CA718E
      79F94F408E55A38DF8AF8AB43591A3FD5F15E55835DA0851878E5D981608A42A
      FB1E002BB5E1631126FEFF7BB50AAA0E6815EF2C4865B1B6B666A4597C2DA0A6
      309ACF099696960C331AB40AE2C9D45A05D516CD44A40D90FB6B01866CDBD608
      AE55F4EAED1F4EBCB43C781B3068002A15D68994BA25216D7C9E6CA55E3A72AB
      05A8785A2E809CB4E844E0A15F791189C42C60E090604A466ED2508827F9B900
      148065615D2E3B5B4A61F9425A5AAA27D3FC8817F124AFD0B4D90FECF8B1C3FE
      3DFD7A0761A92C47AD821A90D4424A445A604EED45D56363637D77C7F6AD21A0
      8D107C9B3463A74E1C6B0C8F07F045D3B67DC77022883AB0AF1FD9B980B48A53
      4283868D757467CF9CA46EABF31B39FAFCD0B27538851D397C88046C21770ED0
      290F02D61A72A0F91C7CFEDC1912DCA761A3C65CC0D933A7B880FA0D1A2911F7
      99D08C4BA85DA79E9960C62E5D3C4F4C795C9DBAF595F0FB738FE682B00884E9
      6810AC044C8C50AB565D93C0CB972FF81B076EDCB8A9DCB66D3BFA33FC7AF5EA
      B969C080FE77E0343611C60142F51AB50CC2A2AF5E1621A04FADDA75C3A9D750
      8FB972F962BFC64D9A2D410F298838AC600AEF4F9D3C3ECE980671D42E4AD83A
      23C9A9BB51B816440DE622809C04CA04D3C6934D81E6C0B56BE308D2B68DC396
      2F5F51E9F73D7F0CA4F01EDDBBAD1B3162780CB98D1061E467660510118488C8
      2654AB5E53097F5FB805808C0A619B11A6A341A041D5C0CF4D8E02786C1E2E10
      F2452A496E045F4C9D87483E27838E8AFAAD001B53430FE2FEA6C1F90E89304E
      29A180CE5DBA87D35447EEFC203D3D9DFDB1873F278B905E09E80C1740CC691E
      D6857E1F0717C605D00D44C82F5FBDB4EDC0C31ED0192E80EE44822EF42B1DDA
      B40B172EEE54B65CD9B8F0F0CD81A83679E481BDFDBEAB803265CBC46DDEBC35
      10F993D7AF5F2F1C36FBAE02366CD81488A9558EBDB5F03265CAC431FCB8806F
      5159C0034B1DEA8E9391912E6FD4A86178D932A5E3D6AC5D1BD8BE5D9B615C00
      2D601088383FD0A6A56A298B9C6FD8B82910ED22275E1A01D82EC1061D05E407
      5A7DAA5CD97271BFA19A20505EB76E5DBD36F806AD8E32A4447AB2A95AB09822
      AF57AF6E78B972E5E2187EBC044AECA8422ABCF933949E5252D76CDAA4099897
      8D5BB9726520F6DDB46DF07DAAA851A346605E2E6ECD9A7581D9D9AACF6D909E
      9E86E92F9B32912F902E4A097D2A948F0B5BB306CCB3E50D1BD6FFDC06E7CE9E
      263D8768BE09AB56AF067325EFAA3E152AC431FC44D08B22606FD103DBFDFB9E
      128D7C9B8441F7099FFEF38C2679F143EB08C36905E4BC4138315FBA6C39DDD1
      4CA851B336787F3657AF5CF287061786014CEEEBDB28BCA28F4FDCAAD5618158
      E8FB2A3F353814B37E02266F1DF76BD15744F0F4A1EDAB16CD9B8757ACE813B7
      72D56ACEBCB1AFEF57F9E98E9E3777CE4993491F73B4FF9429D39A54AA5C31EE
      975F96065249A8EBE5C7DFA37BF75D260250025A85EC00DB1EF816730D89EF08
      C821EC2F1AAA36F568A626FB2A7F5E04A8D9E6F3CAD596FF1D4B09FEF9CC606E
      C922482FF2DBB2759BCFC1837F8E04B51CA321ACFC1B6A5320BD79F3666B0706
      F467E2993367565DB070F16489446207C59FA9B0959E7FF68C89C462867D0FC9
      83870F2B74EDD2E9A08498E121A32004306CFC0F73752D924661F9C58B172FE5
      383E14868C5A939B0BC0A4CAB2F9A10C15F1DD8B4DE96EFDFA052C4389AC33B1
      BB51A74EEDAD3805B517910458ACDBECD9733B455FBBDE43862D15286D0F2222
      36CE4604E255BDD4BCE0C3D33817403392C0040AE1484C4C94C26B8D47A84C1C
      0C58DCB56BE7BB8838802D77256C7630EA40E4F4E9532DD6AD5BFFF6C4C9D3C3
      D233D2DD285C0BE2A775AB05E0100C4E8E409E3A98CA919696CE7080694B78C4
      E69F90E097EEDDBAFA812C424DC1BAAE5C1556EBF091A3010DEBD7DB7AEEFC85
      EE9A70CE430942F2131F2E00AD8E5A82178622DC5C5DD33AB46FB76CC89041D7
      2D2D2C52A1A9FDD4BC59B31FFF3C78C012F14E972F5F713E74E8CF007FFF3EBF
      B469D3FA018E4124215C6DC0031A85CECD05D0128C3A447D6DD1B275C4D1C387
      1EC33E079B356FDEF4AEA3A34326FCE9A08847D8EB458BE607424549847FE7F0
      E1431FC13ECD18B3050C66478D0025DA93DA00E241010676B08AC1BE099B7DFA
      9424231B3D832C8EB4D4349E161E8A7385CD99ABD79ED47CC8CD89A848463758
      471C8A691F326FAE082775EA1C3972B46F44F8C671C78E1EB606A30238C0E71C
      3C6B7670AF5E7E2BDAB76B2B5BB468498BB97367132FF42286122841A63614C8
      4F4461D4E30D44C1CF9E3F97E3C44D977921A1C938A8D77DF0E0418B0A172E9C
      DEB869F314C4A76075E675870EEDD76DDBBE63D4D3F8F8DFAF5DBFDE16E15100
      E74119D6BAD502B45ADDE792F19367172E5CEA57A776EDAD5DBB747ED0C8B7A9
      B60751DA9DFE7DFB481E3F8E3B72F9D295BE58E5FA7C73828756D323696A01BA
      E101B148EEE060AFC02A640216FB1C4F9D3ED35B347B6EE699D3271210751020
      D33674FEC2262865471A01ECEDEC9E50A01A2ABD2A527D56DFA176ABE319EB84
      6E9775E4F0C1B1DA008DED089B16706131366776F0513808B074A693D6455D9F
      DCBC042AAC1A8A25623660C0C0306DFD51E4D780CA4EBD8C063B1A3E148A2CAA
      2126222674EC8D08C4120993CA64F902D5805729AFD32E2E2ED7716E8E0E3751
      2FD554111A59FFCC1C09FD5A207DD6CA15CBC36FDEBCE53879F2946AF023A352
      15AF22B995FC19F44B771CB1FA5ABE3A7A6CEF4A70B86C224EF15866A3CA2552
      C95B1767E77412903864F0A0751B366CEC8D51D40B2978B5C1FE2A4355FCFAD5
      AB72D4B88E8E8EB1FEFE7DB7834106D7ECA0D13585C70DF89EE639989D3051BC
      A0D9958C3A78A8C79C3973A7A3E168F4045DCE86728C6EFD08C34830A8B60064
      7463458ED581F65010655E209188333574C498A0F1E6C12AFC8BAA3E503E0FA4
      6649722C811E350DDD4B21A413C01F8BF4E27275E645C07D7029056C628C8541
      4855400C779E4C5E04BC00A7FBE04825A1B1E80FF89B027932B90A78F3939002
      4ED178445489A157C1ED0AD803260625138C037315A049701976064026139777
      00034311C099165AA2F2465847F84BC0D61991CEA5E7009117A0DFA07710FD16
      2043934B32E2CBC0D31B1806775191C01AC03D1C1802BF0B6C6ECC0A404C5D60
      0E089D61634A652F60C702643271A9070C04C6038B805F807E4033A027403D4E
      0A5B3D5C93C308FFC0DF0BF80D42CA2177A970476633F60936193F5C468819AB
      08D0919E36F0239A25C0F6008600A5811C053C44E47DA01530118A5A59D859C0
      4EE039E0A961CCEB1F6E0B8495051E0014E603BB09605E001826227216709A31
      561A256805BB232003A20129606C0A6B02E261670276807901EFC6092A74CFE3
      20E8012C010A01F581760035AE256C0323869680001B6000D0190807A043D335
      17A01D9A82643740F51D0F9B725856AC660AAFDA64AB4FCF3646E61EA843987A
      CAD47ABE60DF42B5ED435555054D69E032701F0C2BC0F6802029DCA9709F00DE
      023A23E85C661CC839D575714455016C80694031E03E04EE84DD0442A93422B8
      3700FB90FB4FB0758622B8473573E60C8072C7FD605E1E8E1F8132C069C65804
      B00648079CC0D806280C771DC016B0071C904E045B67F43D33196377F58464C0
      FF0C388C5CBD06147053AE0FC07E8C12C431C6E87EB14015510917C37F089802
      2105607323E157C30B092923FC24DC43F00340DF50231745C016603D709A31E6
      9ACD580D08A1EA2C0F7720C24E33C6CE03E6BB29224888AEBAE0D79A4F701406
      C6A17ABC50AA5B708F00FE0663156C32125CE40037FA55C403F42E2642502D29
      884F005C801A008390EBB08703B7354288A725FCDC90873B72B8180841AE1341
      B71148011AA0AE29B724E40AFCC3817F01E22987CD0D79B8E30B979EDA38E416
      9964BFC11F029406E80E87C5CD455CA9BAEEC3D60910E0F96A839C138351A8B2
      B31856AE6819209CF8F932C62C9099C3B0F36F8AADDD55B858D80E1F630E0817
      0374531A4799FA91A36FD28988636E6D6007A2A510D40990C3FDD5263701D460
      A5C07513CB874E843439DE68144778814BBE7522A4FDB200F404EAEFD1D97818
      C25040A57545227BC0C4A00AA907998453229340A380CBF067006432717907E4
      A81361B0E4371FD1104CA422275E88788EDCA7C12646D4062750020F948498B7
      4678125003B0010E56CA7AD14F218886D6CD7A623D36E39C83932A05C1EC142E
      51E64A50171179D289B0FCB0B866D6D30B72A698F5AFC8D9ED88B4B4C3216959
      96A5AEF9C6E0B3D89C807F10D10BF80DA5C9592752A92A8904417E53ECEA2EC6
      13B18C65A7C60BF6AA4996AD6FD6B519591DE9AB0223055C0C0C983A20E00050
      0F88C070B010835C63B82B01958162A82E1AB2E1541B3C67BF5109421CC3BC00
      288171A8E265B0D565218716604823E62CF84F33333A915CA5A09B0F519F0D98
      3B697CF1B033011D8D081E0383C12B479DA854F6DB7620B6040C8C58ADBE5083
      0F404467201CE046E0D72F5C50654D11BD5BA2CAB6F750268A50D7EC9EA81013
      D32A2222B4063DEC19DC063A11FC79D28B6ED5533C7AFD5164E5F89018B3ECAB
      587F7B098615C0C043CC588E3A11E27316809C4B65AAAC1253D28EACB3576594
      F9C5B2214B676027481CD04E4790B8101AFF290451356F80DF44274298692353
      209897B75429C62D4E8BDCD35511D3A0BBE22613ABB21723E774F3E55927225E
      66DB2062EE81DEB62C63631BC5BF58385652DD5677B69F61850421402994201C
      B96F0C77079489A1145970C7023B8055E8A21F607323C1D831022E8A7C0CBB2A
      308829FE6A019B4C142E3D849933530BFF328334BDA2F06F01D603A7592E3A11
      E2791BAC2487197402E37D7AE19FE0A61B8C74A213C8E52D5425656E1D4A5019
      2511102F010C26266AA0D1085C0744031B015F86979F8D98D3735A0AC2130017
      A00640EACA75D8C301B33A11C2F3F67C4084C82D32C986C23D0FA0920D4229B2
      E0A611B71EECB5008DBC7D10BE0B6E6EA804DC91DB058950135FA71311CF3C0B
      206208C980BD0CF8033DA9186C6E10AE82E32C300A780D7C9B81DE63562722AE
      88CBBB5E4409BE15D47066795CBA74C969DAB469DDA64E9D3A67E4C8910BFAF4
      E933B767CF9EC1FEFEFE33478C1C39754260E0F8D163C6F4E9D0A37BD9BAAD5A
      7D2CF243F3377F1DA067754376D46F0D4222222224E5CB97F7C5EBA8412F5FBE
      AC7BE4C8118BF3E7CF33BCF829A463C308633F93CAE552273757FB6A356AD8F9
      D6AD57D6DBCDB5B3542CDE5070D5CA15A123461AB481489FFBC1830765C58A15
      0BC05EC2A6AD5BB7361E316284E5A64D9B844F9F3E09C58B176755AB5665D5AA
      57675EA54B338548C4F69D3F2F4C5E1326FAFDFCF92278917FA287BDC3AF2357
      AE2CA6CF532760C78E1D18E285BE58719FB77AF56AD7356BD630BC0CCDEAD7AF
      CF4A0E1BC6ECBB7665B2D6AD99350439797BB3B21056A74E6D66E75A84EDBD1D
      23ECFAFB862C353BABB3AB4432A7FFDA3574C773393A0160562F3535F5E73D7B
      F6143876EC9880D78185B265CBF2C3C29C52731159586050155806D6B8B3ACAC
      59712F2F560274D19919C2556C8F492C649DBD0451BF80397C47443D5CEFDCB9
      D3014BCB13FFF9E71F77306778F79839BBBAB22C2C968B709259C39B5BD960AC
      B2B1611676764C62873117EF4F1572F760366E6EECB24A257C1089E436161601
      45ECECE9AEA61984313F3FBF56D8E41F7BE0C0018B0F1F3E082E2E2E4C666FCF
      AC89098E398B9F3E65D6091F983C3989492154692163E95209A751DAD8B204EC
      5F8AE472F65E2C61D0955839B99503AAE6797C9D5A9784B56BD7DA142A5468D5
      C3870F7BA3EE91616B46E7AFDDBCBDB9001118DAA25A14E841C438DBD6963108
      A7E228ECEDD85FBE4DC8C9917CFD3A93A6A4B061724B9555A6E2E4BE2771BD45
      D8872C8F6D926AF87C81806A6274489BDB0D1AB0AC4A955866F9F22C0D6F7965
      E3DD2601396410A8309AF039775CE4286D324A437A8F8D95BC842B6325B16121
      AE81356A67F479A445119118CBCA20FF6C14D9D93CCEC3CD95D9A0DA5424E873
      B4CE45375536D2A7223356169676F6628907788B3C5002795A5A9AAEC7205097
      881C4AC44990D0512A63852CE4AC0860ED5888D95A3BB0AAD13758C51BB799E7
      B59BCC3D358B15CF16312B95C02C6532B1086F040BDBB76FA73DB2E1CB972F97
      DFBC7953C01DCCF735D12E7CABCB8EEA1BCC2DD05B2CD17BD2C52246436A16AA
      4285864E44F52998C0923233980A3B299F3E7E64839B3451552D51F2FDDEA347
      4750234FC461A3A0CD9B373BFC79E488E0D1AD1B659AC3FADE3D56D0D1914951
      64AA269154CAB245229605815610F636319129109790960A5BC99253539918E3
      F8AC61C35585ECEC1EAEDFB1DD4F9833674E8BC68D1BAF8C8E8E2EB9E2B7DF04
      E7962D3973BA1478F48831F47B1BF28029ED1528610B5029D2323219357C0A8E
      0EBDFB98C8E8EB032FDFBE6555D029E64C9EAC7CFCF8D191C56BC3FA0ADDBA75
      730E080808436F6A3F3334549459A102B1E3B04109D0BA4C959CCCB079CD6897
      8A4E52512F5381026316FB802AC1EBF42C19DD331DD53576E44855C776ED147B
      F6EE9D326B41E8AF02188B424242FAA314CB0F4445C92376EF16AC517C1B74B9
      1430B644B558E33EA08657C24DD593E8E404F66A238E8D654A1CA67CF6EC191F
      B766CC98A17CFBE6CDDDB5EBD775DBB97DC71D523518C6797760331AB8E1AA55
      AB8473E7CE0974B361EB84518EA96AA8EB8A719365A19E55A806357BC6D26FDF
      661F9F3F675E254BB2B13F8D5595285E22EDF7DF7F0FDEB265CB2F7171710A2E
      C0D9D95984E1A27DAB56AD56A2F845B66DDBC630E1A00604660BA61808393F15
      8603AA0E11866B1E80CBFB8B1759E9A2451926241506C76C646EDFAE5DBB465F
      BE7CF925A23F9BD2A54BCB838282C6454646BEC398A40C0C0C54D6AA554BE5E1
      E1A172737353797A7AAA8A9728A1F2285E5CE58A3077D8D5108FD94E0586CA93
      274F662E5AB4E888AFAF6F05543BCF3871D74D99EFDFBFCF7AF5EAD56D042616
      2C58B09CB7B7B73D6636E6EEEE2ED0E84A430812B2C2850AB1322841FDBA7559
      8B66CD54952B57A6B74A9351E2BD515151D390F33BA805EA0360C5708770EBF3
      A540810296388DD0B07AF5EA43C0BCA15C2E2FA0542A45388245DBB8023536EE
      7C156C1526A7540C31FFE0BB2D5BAE5CB9F2FBE3C78F69BAD431FFCCD5D42502
      F3C26DF0FBF1C71F170407071F59B060C1CD850B17DE9D3F7FFE6DF8CF8E1E3D
      7A5D972E5DFAA36A4B49A518434C79E42984EA121DCABA18E6886A688F7A6E6E
      6EB51C1C1C68327100075D15C3FDFFA3E9DBB7AF0ACD5005659702F932A2DC52
      A157FDD5B265CB9EA0CB97905C054C9C3891610889C8AF905C05F8F8F8B06F11
      92AB00540DFB16217912F02D42F22C20BF42BE4A407E847CB580AF15922F01C6
      42E077060CEE938A152BD238A6D6AE119967B36FDF3E86C7280E8CAE3C1DEEF8
      78380C846008E7C336697B88FBB289898961C40CF3352B8AE991A8A14775859D
      0228804CC01A9001E487A536B9569196390EF7DDC62395FA31AA5A35865CFF0E
      166F807B1AD0644382E0FD6C72154039C783C9BC3FFEF8630425BB7FFF3E1B3C
      783039690829004732F006F80818E41EFEDCDBE0C18307817FFDF5D761103F46
      29A6CE9E3D9BBFBB3976EC5886F1E918C21D0129902FE38054A5017780984851
      352A3C66415552A9C8DDB061C340C4111D2C53935B1551233E4332AA5F056C45
      4242426B3C4BC3890D85B03086C7DEF9F0E4588ADC041053124236F83006D5E4
      3839D6AF5FCFAB8ADC4061807A112C43C36F06C3A0DC7D356BD62C056D22564B
      892EDB0CEE18801A1BD6B71B6A0F77B0A9053405AA0239B603E2F265480831A5
      EA219BFCF962F44D89F0DCCC878C6F62F2A5C4B9F5A22FA5CD53DCF710501692
      FC01B261191A89A1F7AB7D753F7EFC586EC6CC59FE78D3B148DDBA75AA83C366
      4067BE4500673E69F294D19F3E25F9AC0E5B5B115C4320A42F6C9D90FC561167
      1E08E6850A167A8DC3AEB7F1BC650F2141172E5E72850012028BF1B56BEEF88A
      0B673E3968CA683C85D21714E5580AB2D71C4CB65FB56A7510960A42F0365179
      F0FCE76B4BC0994F983869F4C78F89FC537FC9C9C95E10E0D4A041FD301C418C
      86DB1E4775DB817935E0AB4AC0998F1B1F38BAB093D36B3CB30989899F2A88F0
      D44F4F6257AE44B79C3265D2A22D9BB7C68D1D3BFA28985F07F22CA02EBE4A53
      6EC28449A3F1E93F1F3CA3C9F1EE993D0EE5F3151AD43FC3414CCF152B56775C
      307FDE563CF6EE01F32420F7190D447513123E96FBE9A771A89604548B882525
      7DF2C202A253A3460DC2703E383A353585D9DBD9C6CCF879DA4E303FCD18E3CC
      61E75A02304F283766ECF8D14E4E855ECBA416C2C7C4C40A021FE40576F1C2A5
      9633664E5F141EBE396EFCB8B1471D1D1DEE80691CD3FB7DE93ED03147F17D50
      D7E82D19F67806FE5C2D9F3E792E5DBABCE392C50B0DAA458F7F8E55C4998F1A
      357AF487840FC41CC7D33F79E1657FA7A64D1B871528E0180D37D606ED6266CF
      9A69522DFA02CC9580331F317214AAC5E935D681040C07A81601DD5BC5CE9D3B
      DF72CEEC598B70C2366ED2A489A81647936AF99200CE7CE4A81F47A3617D0441
      846A49E7BD4540C513505D9E4B7EF9A5E3B2A5BFE6582DFA02F46FB4BA1F3E24
      94C3B76547E3E39DA81611753DDE5BF0464A18D6F0E826620E8E8E31F3E6CEFD
      62B5E80BD0565179ACF896C3B764473B152EFC1AFD5C805F572D67CF9C6D191A
      1AB268DDFAF5715382261F451B7CB15ACC09A8F6E38F63FABF7DF7CE072BDFBA
      6AC18207A7C54DE6B960E1C28EAB57ADCC53B5F0449A8BB68AAE0F1C18102995
      4813133E2478A5A6A43AB56AD9320C4342347DBCAA50C182F876E6FC3C578B86
      37B7B402FE69D2A4F18BA0299342B0C09388974AD899B3675B4EFF795A78B5EA
      D576AF5D1BB6AC40810279AE16CE5973D10A20EF66BCD8F602751C82DE928805
      2ACFD0D0F91DE787CE3B04E63B417011F86AA32F80126F6EDAB4C98BA9538342
      8A16F538B764F1A27C550B31D242DB8BB47EB237376BDAB42CF0129E68208E7D
      C34F606BB036AC6530147793D6FD2DB61E4F43015AA65A415AC2AFF56BF9C036
      6C036244400437703BDB0B12E71DCC1AB0825B8D1DCC8AFC0CF11C9C1817633F
      82443A028A44400EC606E1958006407DA0144082F9CC00B7A1D1F2826D580243
      32AD4F098714280790BA5E03767180BDEE89432DE4F802D40220494B83A74909
      F6D2DCB7FEBEA5EEA61D1B7A84B20D13264A43826A481AB4715515AD609D65EB
      CA148235E8A90416A82AB205F80D8D86A7E8D4A95315F6EFDFDFFBD8B16393B0
      67B9C2D9D97987A7A7E7CA22C59D83EC4BDAF6B12C2A692871148A264B12A596
      16722B4FB957B1DACCB74D9D172D3A59265A970157578004C23235227C387B6C
      8757EDB754AE5CB93E1E8BF04C57CC02FA8D284BA2608FD2EFB12B4967D9CDF4
      689ED25EECC064121953DA67D9DA7BDA74AC296B38A5CAF306DDDD1F783B7102
      FD8BA6078AB07C4F75CCB7B7E89B095846A62564662DD83207A120CB5465B2A7
      8AC7CC4A64C3ACC4D64C2658B0D4EC14F692C533F7021E56CD3DDA34EA6E1B10
      8245F2215DBB762DA22F83DC222C887FCCE89F1985075FCE588E257CDA372862
      E3C6CADAF8B01296DE4C2AC89885C802BB803226C5F119126229C899A7E0CDDC
      C59EAC846B496B3C2FB7AD53A7CECAA143874EE8D0A183177AA78804883B77EE
      DC106BCFCE583F16A0F6F1AD94376FDED024CF842C119388242C313B81A52893
      595636DE0E1294CC4662CB8AB292ACB4AA2293E08B41181C49D310B0E42CF6F2
      F22A06BB3956EC0BDCB973E7A6082BE81FCA9E2DD3EE1EF66BA81498E0191A1A
      5B37EA4DBBC29222AC9675435644E28E9248994C69C15C95455903C766ACA04D
      21CC4F02074D4E9809196A44C0768B6877B35D2B51021711A6C62370D0C2067B
      8A4D51CA0D9580845055A172988BC48D39495C98ABB8282B2C726505544E8C7D
      10B1E44FC98C68A00033E4961ED2D9D5AB57D9EBD7AFB5DD5625C232FDBF335E
      CCEC8CFD01A5BBBB3B6DADF31C21278C1A1D9B154CCEAC59094969668937E39D
      A5AEACAC3D9E35B205869CB2C3870FB36BD7AEE9D25106916155E5D55516C04E
      A2E15A85FD804868CB85D10BDA1429524478F2E409C32443C5E539F474F764B6
      1F6DD88BF87896FE3293FDFD28866F1E215354F73C4360465FBF228B034AF22B
      38342F0EAE5129E6073AC8B1D3E4841E5093845075E11E6198D940C7F095A613
      8C65593049160EBD8914BCC75104A992240869A1E9D9332CF9305499EA61E083
      25E8492BA90444473A503A0E0A2C474966B668D1A2241A4C78F7EE1DDF852226
      D4889AE2F3DCA373F0BD4E6A037223C7BC04D41301EDC3773667AE7F69DEBC79
      11ACCDADC7AACA010C1D913B77EE8CBC70E142E4E2C58B2367CD9A15396FDEBC
      C8B973E746CE9E3D3B72FCF8F191A3468D8A1C34685024D68E0E74EEDCF900EE
      87F5D82999029E5500F3A65DBB7665B053BE153DE2C0993367220F1D3A14B974
      E9D2C865CB96712153A64C891C376E5C24EEDEC8FEFDFB1FE8DCB9F3EEEAD5AB
      FF52B060C1A1E0D804700774B5C30CA64DC49069DFBE7DDD8D1B37EE2601478F
      1E8D5CB87061E4CC993323274F9E1C89E5B403010101FBB179B10EE3D7CFB851
      BB208D0F60078800239E9A8189477CBE88B0CBD41E0B4FFBA89A8839724D8C77
      E1ACC52FD853180C525FC6981B20030C8D8E2739B43024219FB86DDBB63DB0D5
      B5A67BF7EEABB0CC3F0DB9ED8C880AC0E7DCC26360B4FC600BE6AA07DD4B7D27
      8280271C2A14876D07DA9BB099997881876BE9B9477DD16B0804686621B8D426
      BF7E3D4112D510A6964E2C8768BB2F79BE086A483E8FE440A5E36958821CA835
      C195615704CC9943087C079818CA8949A0518017FCFE7BF6ECED3E7ACC4FC306
      0E1A32B7A75F9FF0DE7DFAAD193C64F8F49F7F9ED9E9DEBD583FA2014C8CC424
      C430A0C7DBB7EFECE72F58E8F7F2E52B5FF4203EB86172222A4B4CB75E714F9E
      7ACD9A3DF7072C7E848F184EF719BB81C818809B2F09E878F56A74D1E52B564D
      C418E48427783EC0D1B8431313A5A6014E03F9850B1787FDFBEFBF95962FFB35
      0C71CF800420C7E7645BCC6C8556AC5C350644985DB0779F9EAE2C52C4E5B09F
      5F8FE04D1B7F1B80E7E3512D5A345B24168B1ED16028914830F27EA8F3F38CE0
      8E48D30EE046C2AFA69776BF2E5DDE42A1C872A3840A8522AD57AF9E0BF0070A
      8F407A0D882D59B2842D50134B69FF6049AD4B5CDC93B644FBEFBF77DBDDBEFD
      CF990A15CA3B83EE754E8D2CB97BF75E139148C42877F5EBD7DBA8611E814477
      802C20013882AAFB3364DE9CDDB6B63637D12634844B22B66C6D86B81F00B38B
      21C54F9F3EE3A650641614A0AE80E8FDC811C3AEC026E6B04CCC6B84C4B668D1
      3C6AC78E9D95A823BC78FEBC02C276036605B8DEBF7FBF3029603453E109F32E
      11E6826B78DBBDCC962D5B956874D25478BB511AF1CC993399D1CF0B3DC2E7D1
      A3C735186E72E8378FDAB46EF53768D4E3101C660C31AEB86DFBCEB6A8567146
      46A6086D7600743725B8189BF7CE2ECE09D950B2B084C31212125C8D09CCF85D
      AEFFF557412C0CCA289D54267DAFA5312720B645F366719B368567A2B8327457
      2A91336EA48648741630675AECDAB5BB1E4550B562667B446E823901E9D0AE33
      31A1DCC0A2482D549368D9F215FD2B55AAF8DEC6C6C61189F603FAC6FFF0E1A3
      EE7FFF7DAB1D75D3CCCC4CD6B06183F320A0DE66B69111C76EE1ADEB3D338367
      5543AF90A014E57AF5EE1B8C8F2EACC612833D1168111C3CCB178BB17E545AAA
      1E5B7C34326040FFDB88BF0698BD93CB22A2E2993367BDEBE35B149423159E74
      A18AB82F5CB8786EDB761DE6F6EB1F30A8A75FAFB1F8CEE3AF67CF9D1F007A4B
      AA1A2069C284F1EBE18F07B891F0EBE70B31AFD1A76FBF414F9F3E6D50A24489
      734D9B36598FA79FFE229158825C92EEE38E939BEE94046314D7EA14F8BF220B
      0B8BE753A7042DAD53A7F65BC49D02B8D117C099F7EEE33F080C1A507D3E78F0
      A081A767B1FBC1C133A7AC5A15D6E9C58B17D5904A468C61F32536546102FE80
      E728981F87BAF90EE1FB009DD1BF0F5AFBF5EA3D283EFE5903CA29DDF61EEEEE
      E7162F5EB89F4AD2B56B97E7AD5AB53CE6E6E6760DC3435CC58A3E17BA75EDB2
      6FD6AC99BB7F68D1E21E1E5C76816B0C6060F4B758BA4C0C9CD40937596FDC2C
      0CFFEA736EDBB62DEB411D0DFC0B90B1C6C51DF006928027C0632047A32F400A
      2A3F0869111F1F5F74C7F66DEBE18F06B4CCE1FC7AA32F80529390F6705803D1
      C03731477A93FB4081C03DC07733A2EFC62907461261ADDE7A83B1A29543A25C
      83F514AFBCAA8E0267AA4DA8CD484E7E4EACBE1856112524A8E3B80E8A35225A
      ECF86FBD4850D731AAE6BFF5225482DA687A9804A3A69242309EB3808080B6E4
      3685C00A311ADF3EC7A470A735B361D6DD6D98F3C2E25D2BF210BAE038A8AECA
      C5789A6CB0A98B6702662386539ADE38DC1D8563A1B1BEBEBEB13EB5CAC7AA2A
      A6C7DE2D732D565C45112BADAE8C55564C8D4DADF03E96F9A4C7FA56691A5BAB
      5CED589FF23EB138377C0F2B03779E3D7B56B654A9525BEF2DF21B4BC244980A
      331E3D7A848FC6F08290EAC7C2C3C3BD6FDFBE6DF3F6C97B9BF7E792BCBD2CCA
      30D1251BEFCC97D936CA77CCC6F65A61EF92E2B2CC59E5C66E44FFED7DFDFA75
      6FCC7A75B11E478325C3B70517E1A8EF7008909116A65B2F4200C304CF9A356B
      F6222222C217C7197CAB57ACF182D68B0A957178217E2077154739F816F62AF4
      42BB5E8459CFBB468D1AB148C73FD5453C500AF3EB451449D321D690C8C92161
      52BE5EE4202EC8D83D9977C1B6D6A78B14744DD6AE171111163FC8E2EB45E4D0
      5F2F92F0F5A219EFB15ED4AB3FC30FF331BB71E386EB8001034E430F62D1D1D1
      AEB59D6BC766C7E2EDC2F6854E17722E945C565E916D5BB6BD2DAA220A73F2FB
      C8C84852DDDF53E6C0820C5F2FFA9BB1241A8BF87A1116630F510C967218D61D
      6231F726237132565E6269BDA86FD7BEB1459C5C93B35F32F6F7F11886258428
      5405737575BD04444159BB949191412C38B0FAC2D78BD4DD097DD61EEB455869
      49E3B1DF7881667EE08F1FF6B46743050B2A0167870DB8F4D8D85807AC43FC4A
      CB36C3870F8FC24A4A149611A2B000128563BA5138D31255A54A95289C4E8E2A
      5EBC78145618A3D025A3702C8B4A108533C1515018A2D033559C29A357D5342E
      AD8552FCB75EA4AE76B4ABB6568C168F74C1FFAD17F1AAD0D7AEF924CF43B517
      7D0580C2F2EAD7AB7FF58D46898DA147841BC63C5D1E680C4B602C44EB274694
      7BB2294CDF4DFE2F40DDA5F409D6A84AEB7BC90DDDC8069B41EA3D347B81EFA1
      21CC9AE20C6026ADA1003581FAC99E7209801195D2068C2A010D80FA40294082
      38F53E1CE8E02773170B870619FC2CE033F33244A9079AEAA4F097036A013580
      E280B97D344A6B2284687530D68B36EED83071D19E90956D7F6FF04795ED4577
      96D862BBC279B3D01B555600C8791F4DC3518422A9B4C062F8F7D947A3CEA0C1
      E72A82444C2298B7BFD33E1AF8913110405B2A982CF8DADC77DB478B748D5A48
      92127B7E8A22E6781C6534177FD33EDAA18EA58827DDA05C2FFAB7E1DD48629E
      86D7C2E81999B65508E20C292B21F3665C2FC2DB7D998A4C86AF3233079923D3
      EA45627CB18E267BA41350BF52CCE10DA10C2CEA76BCFB480831D48B684D02FA
      4D8EFB687B5F9C68BBF7F9C9B6FF63FB68C81137FF63FB68933DC644CDF19C1E
      F5F5FB68AE33FFB0D81332EC7BEEA3FD3DFC46201AF967F58D86824F291814B6
      77EFDEAB589A5191F205F58F4105C16AEE7B46AAE4F1232718BB857DB4E7721E
      4E0D8B647C5D1569B8D20CED9AEF816E6DB8A51DC5E106E6DF14E46EBA7CAF7D
      34E2A5058D945A37BB75EB56E2C183071760B9663694A9C2D8B615485775C71E
      27756328543C87744382866BE2A42E52386C550ADEAAC3E6DE1B1DC39C1CEDBE
      FB3E1A49A2E15A3D40F1F11C4F3E5FBF8F36F15C35D43B0D9E9C07B135845688
      3AF4EBF522BD0CAA5998BB92100A5713536E545080CDEFA3E9D15012E4DE24E7
      028F3077A1C4FAE143854A60709307D114691A6F9697C170CD139BBB1043DC05
      B871040EA2518791EBDB806F247CD3378CF252023B647129047502E4707F95C9
      8B80FBE04813C82686570E20A42A20863B4F262F025E8053BEBF6194AB007CC9
      210502F2FD0DA35C05803999CBB8689F5133E17E07D08730FEFBAE23FBF2378C
      BEF6BB8EAA9933A9CF53F53EC485BA672BD8DFF45D47DD8403E60DC1EC0C6C7B
      27154BC481F059F00701DAEF3AB684FB39100D7801C6A630029E00F18013C033
      2B828381A905EC33C05061E6CC4F5FFA861168CA009680811133ECEA31ACB031
      3600119D817080079280A1F04C00F352B04D0CEEDCA608DC0D5801F10075D5AF
      FAAEE308240A05485815D87F4198005B6BF2FD5D4789864345D8570132C1B82C
      06E84692C22E0E54419B5C80DD18B0005CF1BEFF39D8B97EC388B70108C964D3
      05A80D1C47B59487FD23500638CD188B00D600E9409EBF612402319947B81043
      58EC322ECD800CE019F05DBEEBB81E8CC600BB8160E0DAEBC460DA2EDC01B7A5
      C018352A9CCC0A97A2C01680D29C66B97CC3485B823010D643776D89C6BD0E77
      6B603590062422DC0E36994FB8507FA76F187961A4BD05FF08E0CBDF7504D304
      10F505FE04B37AF0FF09C8E12766F6707F823B5FDF30D296808109157B18189D
      8790954029B849700ADCBC0432D93FC908DB08A4000DD01124B0E9434957600F
      07FE0588A71C3637E4E10EBA40C81AD8A50117201650005900AFA617A32A64C2
      FD1B100294060A015A73110EAAAEFBB0CD0B400495241682BAC02D053C002F40
      574DA8F70CF89701DFF40D232A193A0FD8E460F09DA21CBF61A49FC478AB513F
      EEBBB829A7DF85514E4C7215807F39F101E674EBEE7705E78ADE031FE1BED6B1
      53D7108457CB89B1363C4701482CEFD4B9DB1A9C25BA85279DA9F830464DB80B
      00F67057C3F3D864B8AF81662B686DB40C8D6DB302F0FFEEF61289E41C961486
      D0E3123D0CD2160C1E93583A56D8C94D61140761BDF0A6CB45A47132664E7E09
      5D8C21168B765B59C9ABC1C65364061D1488494FCFD8C418BB0A6403352D2C64
      7D21A01A32C2ACE4729FE4ECACDDEDDA776A8ABF2CA37890A88D89001C6BE885
      9C37A7C754CA71724A4A283678664445EEA79B4C9D0AFF1509BA15889F0CDA39
      D867A0C3AF8DB067D017049B98DECF4480582C9908E0331D5938379AB21A390A
      D2A3D73921301B1E9CB3E85800D5340EC722F0042A1D87B04D4CEF67D006AD5A
      B7A3135195293E2333230D753D8DDC5F02F6E166A02489F8B83F3D8CFB80070D
      E7BA2406371A4E7A3441914F5091F1CC7B32F2C0BEA63ACA2F38505DFB91AE3D
      3DF163C3A82DFE11F4A0965CFF5C058B88D852199FFFE8419178B8BED4A74FAF
      3FC89D1B22366F6D269148ABD2C37A4646E641FC01468C368D411B80E02D8ACC
      D7224040232AAC3C19175A6BA2B4C4433F854109368547248B45A20912895804
      4297F5BF6D58DDBF9F7F9A7E026337FE1BC406FF39B10A69A4386893ADC8528C
      D34F635002FC81C887A6CD5A9CC4F25873DC48720CA76BC0B01B90B351A97E26
      5AAA7F34F679E2A14F6CD08B2802844B0072322C7874C5FF7451D7E37EE30BE2
      466117974E12E286CC44D756CC32A6312801459E3D73EA70A3C64DCFE346AB8F
      9230AC422EC63F877582D0CD88278580FA7F358954DA0371CD8906DD99869033
      487B123406C6A00DB431EBD6ADBF02778048249202D4E8452512493B301B027B
      184AD61E1F0C2B894CF02340A81A547F6657BCB0F50AE90C8CC17DA01F83FF4D
      1B881CAEA77B8218E9C769DDD47328F77885F2C7CB972EACD086EBDB2655A48D
      4482DF6AD7AE571327EB29D7FC2494368E6C624EDD126BADDB406B9639D1E528
      8022952AE588D4B4541444DE17D5A313A2658E9BF17774677FA2CD0939569136
      01FEB816B786682386662E84C229E71AE63DF1171FD4E8146C16B90AA054F817
      381212812ED98BFC6854CA79CFEBD7AE7E9139D1E64900119210D88B0131301E
      CC3361E76AF22C20574E3910887208FF6EC15FEC451A292561D703CC99E3087C
      01E468BE54022FA4F23F75EA748F69D36774193478D88C3E7DFBAF0256907BEA
      D49FBBE108A81FD10045015BC0849F0481E64C6BEC23B8CE991BD2FBC58B97FC
      BC35DD07E8459C16DDD4F6C9D3A725424317340A0D991784756E3F4F4FCF8FAE
      9FFFFA2C8213E222028C4D0330F518373E70EAEBD76F7CD1FF692CC2FF8C6791
      FA42831ACB5428680C4A1A3E7C582804D92F5FB97AEAF80981615DBBF50C5FB8
      68491330EC007023E257C34BF1993383472914999E5ABD4810D8B3D2A5BDB777
      E9DC716EF76E5D6657AC507EF3C080FEA190C2D6AFDF30198FB8B6A00117153E
      BD0DC0A535C655D476E5CA55753E25255590C9643CC79EC53DA3A64D09DA0B35
      311989EE02D9F8E7B16A274F9E725FB7FEB7C918366CF19E780CAA6A19867411
      0EC2A683663FC08D71090A5CBE12DD5AAB17611FE164C8DC39BBADADAD0F827A
      17700BA876E2C449F7356BD74FC63F584125B2E3CC0F1EFCD37BDCB889E3116F
      60F40538E1448E636A6A2AF50886B93573F4E851BF83FA02F01A20E3CF99AF59
      07E6F8C2A3AD6DCC820521CBFEFCF388D71F7BF78D8116E87D19FF9D06420F80
      1B7D01456FC5C4144125E2459E2C666B63F3C0B358B114503D04C8F81F3F7EC2
      7D7558D864BC36666B676B13B368E1FC658E0E0E99070F1DEA80F69289D01831
      B76FBB82986712B6C1915CAB4F894972251621B2B2B219F4A34422D0C0FFD8B1
      13EEF8DBB8C959594A5B3B3BBB189C435DE6A87E6D9EDE367A4BE90878F5D812
      69E40037FA8D9C8813AF49E8E3BC5BA6A5A5DA730AC6FC8F1E3BEE8E13B39331
      F6A3411D627E59B208CCF9CBE71741E3959E966E87C646C9B3F18AB74312C23E
      02DCE80B7852A346F5E7E11111F4E143516262A633511C3D7ACC1DAF09A8993B
      3AC62CFD75B13E7357BCBB66F9F6DDDBB2D4EBB2B3B3940D1B34788C74F7016E
      8C4753FF1E3DFC26262527171B33FAC750A2C0996BCEBC0098AF58B16C198EF9
      DC4138E59C32D76BCC989F7A3C7CF4A835CDDB383C7377E78E6D21888F00B821
      22EED05C1E77EAD4713FDE644F552160D9B2E5BCB7142C5820468FF93B44B506
      0ACD9811DCEC5E6C6C6B520CA056309C01DE8FF0784067443A97DA81AF23F688
      A53BF4D7A5CB264363B0752CE018B362F9F2657891C47EFFFE03CDA2A20E769F
      33775E971E3D7B4DBC7CE50A9F4641879365B677F1775954BA336A56EAAB7109
      A4CD9AFFB0040D86770F047AAB2E66D58AE5CBB0F55562C3C64DE3A123C92819
      6CDE11A85AA85320F75923460CDB4A71401F804C045D8C05F0CD6A4A04264961
      AB572E438E4B6CDC143E1E0A17FAB988D2E8805EC50741FC8FDDF6766DDB3EED
      BD5F08A7C8AD1D54FD6013B1D2B891FD102105B8397020AAE8C2858BA6639836
      618E52F26E89E1E4D2D6AD116194A0E71F6A013B3A7301146472A8783B0F65FC
      0614B56CD9A24FC4E6CD31D8DDAB26C686355509C56B993B172E1CBD71E3FAB5
      DD76AB19531C41EBDFDD4DD5CFA48A88005012D0B76F6FDCB07E555FFFFE63B1
      61EA43930EC279CE9D9D0B476FDE1CBE0A34DA7F37A62813E424404B780DC3A5
      6C73C4A65FFBF50F18F1E6CD9B6A14E1E2E212BD75CB66627E137E9F3F7AAAAB
      A4FD1675490EF451FB11C7AB82EC2FE12284DCDAB17DEB8A52A5BC8E962C59F2
      384A15869C13F31B4818016C0150321C4BCB261707854508549FDC9BFBA52848
      7C99FA7716561C33FCF91B7AF93AAB49231BD118789FC21701E464CCC68972A2
      D60B2F0937E5CE18345C208A55C785E2C886D3D07C498017480DF4A2BEFE0356
      F5E8D97BA37FBF8060E84465291E5365DBE12346E19B2E1F2BC3DF0030301203
      DF678F59BD880635BACB2B55AA78AA5AD5AA6F8F1C39EA893FB50D443279E0A4
      2963F0D599A5585B82979DA30BC15C09CCEA45D018580A3EFF5ABB56CDF5F808
      CCE9C360BE6163782086E8E755AA54DE8A17822A4C0C0C1A0385AD34189707B8
      3127C0442F02E533E47AE3E851232643113877ECD8F1A2F4C7F05672CB97A121
      7316064E1C7F1482C313133F56809AD99F31C6EF17D826BDC8402FC228C93C3C
      DC0FFF3C7DDA6E6B6BEBF748607FF4D8B1A2EBD7FD16682997BF9E3B77F622E8
      41715838717FF0F041050CF349DDBA753908BAEB0037C625D0E94534F1DBD9D9
      5D9F1F1AB21DCC8F80DA9E5496B56BD707CA2C2CDF8786CC5D8083AC9CF9D89F
      268C78F3E6ADF7889123421A356CF81CB4FF00DCE80B70D2D78B321599CAB163
      46D3F07B07944D4F9F3EEB465A0546D6F773E60413F37894B0C8D89FC60FC360
      E83D027AAA6FA386CF133E7EA439C31269B8D11760A017E10F69E3CA94299D08
      AAB2A74E9F765BBA6C59904C264D983B67F602E84B4FC0BC30BA2772FEA6DCD0
      A18316346DDAE41984F5FEE38FBDD47DA9CB22A9E137A46CF1391B6BAA1A4293
      C6BE4741A1C2AB48AEBFFEBA7C129827CE9E1DBC002FFE50CE89F93068DFE546
      8E1C1EDAF2871F9EFE346E82DFFDFB0F5A14F7F47C8474360037FA25F8487A51
      1A0E31D5AF5F77FD98313F5E3A7BF69CEBC2858B83D0FF93F09FE0A1A5BCBCE8
      7F350B0D1B3E7208DED3AC386CE8E005AD5AFEF074DCB889DD1E3D7ADC12B3A0
      895EA42FE06ECB1F5A3C6AD1A2D9DA8913C69FBB78F192F3C2458B2761E92839
      64DEECD052A53873C76123460EC22B4B95070F1EB8A06DDB3671CFF01FB3F7EE
      C7B620B551A9CC36D18BF405D07B685B268C1F770E6F1339437308C20C968EAE
      38BF54A952CF3333331D870C1D3EE859FCB3AA43860C5ED0A96307AA0A366F5E
      685781315916FE7F19EB76B1C58B7BA630C6A8ED60A9A746EED05C3A4115E1CC
      51DCF49090B9213834FF02717643870D0F888F8FAF3678D0C0459D3B75E4CCA7
      4E9BDEF2D1A347CD9011BE5E847707F783361ED019FD123480EAED1A1C3C3B48
      24089973E6CC9A5FBE5C39626E8DD96CD0E3B827B5060D0C58DCAD5BD7078B97
      FC52AB6BB71E53AF5DBB4E2FBA71CDC2C63677BDA8385EB2ED8F358882AD5BB7
      0AAB5CA95202B241CC031E3F7E5C6BD8D0210BFDFC7AC6A281FD62EFDF6F89F5
      22AE1B65E38BEAA8BE1CF522FD123C9E3D2B782DA6C29751510703A00B950B08
      18340055508772DEAB975F2CAAC92F3636B625DE7C836C9A1EB379D5E8EB455A
      DD08049CB7FE707DAE4285F285172F5A18327EFCC4A08D1B364D52A95499FDFA
      F55D0A95F0EEE0C1C37A3F78F0A0854482242AF55F1853EE492F4247380E867C
      4E265B833E649B9B93BB60C8701D377EE20428B33B070F1E747BE0A0C160FE90
      33A70655E11D41624E7A11A92EBDF75B6C2466C620BDC89C00A26B8B4B0100EA
      D0079701018346C15189E71E819C39F4228DEAA2ECBC43ADAE20CAC0903A83F2
      1A84693D515A07EEEEDA5B36872F839061AF5EBDAA41E1DF5B2FBA0CBDE8C6B6
      AD9B5795FAFF522FCAA90DA8AAF551129E7A8039730581F700B386DF0C6663D4
      815EB04C7423AC19850D1A3C3478EAB49FBB1D3972B407D1003E80899198847C
      0E30AB1BE14EE7144AA5CAF3D9B3E79EE1115BDA1E3CF4E7A5E9D3A66E72722A
      54029134E0C1529B9C0470DD082AC864E8439EB4664437170DC9F4D8A4425AAC
      7332B158CC28EEE3C7C43A132705794C9F1A145AB26489C6883E057023E257D3
      8B896E84F5A06798A3B777D1AC19952D5B7A2B2699B84CFCB1026C969D95E51E
      12123A2A3353510CEC7419372740A71BD1B08045587C3CD8230A5F1098D1BA55
      CBE8E2C58BBFC75F4727E13DE5EBAB57AD08F6F22AB987689001D2FCCAAC5ABD
      9A3A431B08E1C69C009D6EA45064D18B3C27E761CD28F6FE037B7C4560099E9F
      C7AE5CB57A0C5EFE5F121B7BDF7EE68C9F0F7878781C265A9148CCAE5FBFD112
      9CED016E7445E13EC69C30D0F13523AA5B5A33FAF1C791BF632FA7CA3FFFDCA9
      2CC1480A46522CDD28A8FE57AD0EF3AB5CB9D2950913C6ED1D356A4C23FCB383
      1CEB4DEE376EFC5D10FAAA0B78BE322E81816EA45D333A7EFC78FBC78F1FFB16
      2B56F4F8D62DE193F1CFF4D34B972EB50F937F2D7C22AA2DBE1A976E6F6F7757
      FD34A8627FDDB8E10EE69E0C3FE312985D336ADDBAF59EDDBBF74C6CDDAAD529
      A4B900A475EAD4C92926664EC7D6AD5AEF819FC9A4B24FA44F29B1DE949C946C
      85301BC0E42150B76644DD52BB6674E4C8B196E4C727E84863A3466C8605DB72
      C460DFFEFD74A331548D03D1D050EEE0F879CDC8B804BA3523091422E8FA5EF8
      4B7B6B74CFBFFEFDF78E14DF5CEE862F6E78666567C9DEBD7DE78355993B65CB
      96B9F2E6CD9B1CD78CCC4D38FEF48FE6D893AC4039C4524EF4A68D1B56DCB973
      C71E9FF4586669699982EE9B050DD01E0ADAD41F7E68F16CCCD8713D1E3E7C68
      76CDC8B89189E7E3A64D1AA3DB29C88D6F18BDAE41FA0FF4A3C401FDFB85E01D
      FC45E3C7FD14426E62CED78CEEDDE3CCA15D3063DDC85C0988B17F97AEDDA7A6
      A6A67A8B44F8E712686D9874627D7C7C2E54AE54310E5D54F9F7AD5BC5636EC5
      D4443556A0EE4B438895B5F5DD3DBB7786804104C08D711BF0405C9E430FDA14
      3A7FC14CA9542A2321494949DE78C3C2FBECD9B388665C27A2705417B409AEBE
      6875A3784EA0B9E424E044F3E6CDDC2E5DBABC05CF060110C28811312468D272
      8B724E83A0563742207565586A9393008A8DF8F9E7690C8A5789274F9EFA5235
      90108AD042DB2D5D5C9C2F6974A36DDA38ADFD25014473119F5C14F51F1020C5
      927F3DD43D2F09456899936EB469D386B508BB016401062637010F30C158A09B
      B27EFD07B0E7CF5F7021C4816E28BD35A39B088B014C4C6E0228C13F10C2C237
      6D647DFAF61361FCA9438146BA11E59E824D203209311F4042AE43015B5BCABC
      6E643E154273BA0F1065D61445A82F53FFCEC28A63B9FCF25245FA2C9EC21301
      E4D9E455404970AC079833FFE9450A4F0CD1584851F1873D1A1A54A828310641
      BAF128EEBBE945A5F5F6D2CA96F98E7A914751B55E3475CAE47DD85FD8857DB4
      9D414193F6AECE835E64AE1719E84578C8E37B69A89523C06B406BE8E6B3835E
      84FF2F0AB28E7FF60C6B153ABDE89C96C8588093B15E8425B4DF414C9A843E73
      0471F309D71313268EB3FC26BD084CE81EF0875D1BD01A2F3828ACE997F42211
      88F48D59BD68DCB8893DFCFB0D1807426FA000600BD41D3172F4008A833B47BD
      4842917AD0E9453473A5A5A9F7D2BA76ED7C7AEEBCD05913032777C2568A358E
      FB28D7AC5DDF282E2EAE2E3EFF1D4CE9317FE75F2F82C63BA8886B91EBB1B1B1
      8D870D1BD191184A24927417E7C237D7AE5BDF0F8AC0E29CF6D28CAB28B138D6
      7BB0DA7847A15030FC64B366CF1E30786040F8FAB5616BB1FAF8A856CD9A5B09
      7676B6F7366C58BF82E2E6858476205A4A63BC66642C0074CC442F3A77EE7C05
      8AC02254D44F3F8D394DE8DCA9D3010AC3CA5899BB7773D68B8CDB80D29C1B34
      6860F13F0F1F8945BD7A535B445FBBE687F5A16A3E3E152EC864B22C0C11CA7B
      B1B16ED84BEBA4D58B48B3B0F9BC66748A1811CC09A0F01CF4A20BD08BD4F710
      092690A641F333B4BAFFF4A2975C65A12AA13AFC4F2FA25AD0070DCDFF67EA45
      869F17FA5E2FB1E9BD6D6728405B295A415AC2AFF56BF9C0361C8B88110111DC
      C08D57E5BFE9FB459C0F5E2654A91DDFF1AA29BD6109BE237F2D2B41EBD0DA73
      D6ECD987E5800E74A7D239176DB8CE362E2BD671E8EEC69AD1FE6943BBF0C948
      470B871830300D5B75DD3E7C404B56AB727156BB4A49568B50B904AB59B924AB
      51A9A4DAD6B8ABC1AE8AB0CA15BDD8B51BF7CB9C3EB49B16745FEA33549780EA
      0B0D4A11D3976D530DEBF703ABF4F363666F23C1A97019935962E94CAA864CAC
      62161211BE1EAD64164C6022958A4D6B6BCDC2779C60B347F7AA0E1ED701F567
      42C0D3A40D40CF323215CCD64ACA1CEC65CCC14E0D7B5B194EE148999DAD05FE
      0C55CA6CAC65F8D32309B3B094803E0B2FA018D71D17A3596D8124B517B5AE54
      B28CF42C269153CE65B0A54C6A29FD6C83A1C4029FEC9101289918767A061E2E
      914ECB83DB1A9EA625C087C95233144C6221669498830458400841061B4CC514
      2F010DAA8B04609988F335BEA805501B6862483D4F4B87003011492510849C12
      53628E5259009672099323CC92C2D03619D040542AA58683C6D2F0540BD08471
      0B8D900A0162ACCF4920400AA461D3F66DB6843D4A13B3AB1F193BF54AC50E3F
      55B07DF7D3D8DEBBF84BD74C5411D2F1F44617B5004D7D511C4EC9B2B48C6C26
      42F153F04DEB47C9028B4F13D85B7CFA3B516CC1B2A5160CFFE1CAF017ACA0C1
      57BDB1D248F4948ED2EBA0E1A9EEA64882086483B109F3D7AB2A55AFCAB6DDC7
      393BFC45AE18D5807EC9049CBB13501A96A5642A2CC6E2181B53A1AD14194A36
      A85C06FBFBDA0DB668D2A0CFDD140CC9A8D5167CEF5AFBDEB10A7D3B0589B60F
      F0642A6536E33F6DF1B98D3C095608269B3101CBCB3B8EDF45D7801FA13A436D
      80524874011A475AF2A73377FFFEABD1F4EBD7F05C868643F7E6C306D99C0D18
      91C11081BC3091206222FC9F6F665ACA190D0B034B30F0A93D25613900DCF41A
      39D5AF68998A7EB1CFDEBB766C5A95ED3BF117F3762FF8E2E9DD5BDBB7AD9CBB
      9D137DBED043CAB3CF5E9450DF63CE3D66DE5A55FB1F1A306B5B1B7EB7621B92
      A52425B30347CEB1A5538698CBA0011B3581A6BE0C62341EFF71B3C36D1D0AFA
      A1FAA50C55447942ED28923EBEDF1EB1647A3F0D99A9B50629CCB5813125984C
      45D852409D19386054C01B20576338274362AE29F24240B9D7D0190AD0046ABB
      AC6E2AD50AD626CCCDAFE5035B7D27C3C10D2524700F2EE426C0C90DB909DC83
      0BB9097072436E02F7A82F12B5F59DAF7A420C4BF09DE510BB5C4B800FF08841
      680950CF21C049F730A3AFFFA492E74B507F7F484D515F6D99BDDA20B412D000
      20BA52B04921D3EFBA083235DA2A7247D479F41A58260603129322B41C500BA0
      2DDFE2B0A904DA1291D72CB402DE21B63BE06AFCFDA27CFF0F08989151FF0F88
      CB81AE735CFEAE33C2396E835361E79D9EDFFB7F40EAD6ADDB73481D77EF55AF
      3D7F78695BCA09A762BFFD7F4028FB80EE7F402C2D2D54F73AA61CED7ED9A9C1
      F1379605E4CC56F86EFF0382B5094C2E2AE662999DF9A8DDFB3F93052BD9E5A4
      D2856D2535E4F9FE1F101A76510261F7EEDDB32B54A850098BDD4206DE58C452
      018B7FF152B8FA56643B38B64C43D0B06E45669DCE607FA58B85A42C26A898A5
      48CE5CB23C582D952F9328A88311951A5084554F9E3C515EBA74E92878AFE7DF
      750453E19EDEFF80B8B938ABAA3BA47FBA58F9DAA175DE7F9F76141A5B276674
      29ACFD7E91BDA282ACBE4333E14BFF03A216C7BEFC3F200EB6D6CA4AD68AE46E
      4E69AFEBDBDF7FDDC725F854CB02AB2F3F4F6B50A8CBBF5DDB5C7B2FB59558D9
      8BB12566F23F20D8406D0921EAFF01C157283760B544E9EE9EF3FF80949195C8
      76977E48A96FF7E29D7F918C97BF3A1E3F1994D0A8D1D2AB1F9D8FC43CB77A8E
      FF7347F568774854274F9EFC170292682CE2DF75C4E9B33CFF0F48CCA35B2AAC
      B0A44ECA3A1EF54A54D02ABC545093D60F575E28F4EA6A029872835DAB5770A4
      69EFE4ACF9F3E787E7E7FB4576C9F1A90131E30F1F2A39B25E56E13256586A63
      D825546179470E012A2A016C9C994A4C4CCFEFF78BA41919595DFE093EF5C0D2
      D3C13DF59F0C9C51CAE64C714248637FB6BEE5BB8E182C55953B0DDB8D069E02
      8E5500F3A65D3EBF5F440258F75FC7812B8DCEBADA81DFD4E4EBFB4583779666
      ABB37A9872331F929FEF1759F252AC51599867691AFAF5FF0342CC49BDD14030
      E56936C493316607DC027237C45C43F5E586D010C18A63F9FCFD2F4319B7E35F
      C3A1960000000049454E4400000000}
  end
  object TBXPopupPanels: TTBXPopupMenu
    AutoPopup = False
    LinkSubitems = NPanels
    Left = 224
    Top = 96
  end
  object MenusImageList: TTBXImageList
    Height = 18
    Width = 18
    Left = 48
    Top = 144
    PngDIB = {
      2C00000089504E470D0A1A0A0000000D49484452000000120000031808060000
      00C6BDB7AC00007356494441547801ECBD757C1457FB3E7CCFCCBAEFC636EE1E
      088104777768A1B8BB94529C9602C1A5A5A548B1BA60C5A1688B6B7177082440
      DC75B3BBF35E67216952E479BEDFDFEF9FF7FDBCF99C6BCE9923D7DCC7EE3D36
      138EDEF0577FC20E05BCA38108C00D108054E02170E9E492CED9B02B19BED21D
      6EEA7DBCC55514C5DE3C475F9B8DCAEFAB07392F88F4D1CF356AE46B059ED620
      CA283CC81B76255389A8FEF8ED26E2F9CFB44AC9E296D53D6AF56F1E42FD1AFB
      51A75A5ED4BAA61FB58DF30E321B14D3C030076441B0CB4D391148241C47E302
      DDF54306B78E30756F1840A13E4EA4D468C9643252355F1D35AFE242E33A85AB
      A27C743D20F14C90B930A6D3A74F4358E662E0B858BD5A3E6050CB5065EB1837
      B25B4B89974849A95293422E27A35A2057A39AC27D4DD4ADAE87DCCD20FB00C9
      DA002493C9E40E89C0CCC1A3B3AFABDA35CA5B4B89E985444A1D88E464B78B24
      2290E778F817D0E507A9E4A693528859298554DD9096CFCECE16596D904FDD1E
      3AC4ED5B3DC0185D4A025978152915ACE2442A2C2925B594233F370DB91935A4
      554A29392583D2B272B984744B91D5CEFD1EED5C50C22421B0BA83686DB706FE
      EDBDDDF42457A88813242497F06429B552A85941BE2E2A426D524E6E2E5DBEF9
      908E5C7E42A79FD26D8B5D683FA44AF6D332221323AA11ECD2A5511533D50C75
      A3C48C22BA955448A5361B99541C99353C7939AB48CE95D0A3C74FE997238FE8
      76A6E2BAC84B5AA15DBD908080991C5C929FA4E4D96C51EE82AB1E85AB91112F
      DAE9D88D647AF4AC84B2F38AA856A0966AF948E94E42322566DA4491B84CA44B
      07C8211173207B2D2402F773CD1017739F46BE14E8AEA5BB8999F4EBB1A79459
      60A5ECFC128A749750D3409E0E5F49A2934F652536897222A459C1D2572452C2
      63954A26F46D53CDC4B7AA62A45DE75FD0B98412B2586D9457504281463BD5F3
      B5D31FB74AC5348BFA04717C3F103D41BA7F246237902A04F65267B5D028DA5D
      545D7D5244BC20A382FC7CCAC9CB23A58C279D515F9A69919F15899F8EB82740
      64875D99887980CC07B5334846F6EEED225DC26A04B8D399BFAFD3E1DBF7A848
      EFFF9C93C87723DED720B80DBBDC381A64D9DDC89123B5550A0F540FB39C7DD0
      C053911D1EE06E95A84DA431798AB52382EDA1B2F407FAE4C397E457964B91A6
      52DA4A375E5E5E9E515151AB5B358C5D1AE52DA9EEE5E42AA86572AA13134A0D
      6342B9A6D1AEB15D9BC7CCF1F4F41C0D2267E0CDA67EFDFA818D1A35BADABC79
      8B07A3464DCB59B5EA44C9AA55274BBEF9E644C9BC79EB4BBA74E99DD2A04183
      BBFEFEFEDF80C103F8C7ECDEB343BA63E736E5B6ED5B5455AA543185840437F7
      F70FE8D8B163DF8F7AF61CBBA07BF7310BBB751BBDB04B97C10BAB558B9BE0EE
      6E7ECFDBC7BB4EB56AD14E2C0D4BCB38FE61FC3F701D397284AB5446FF5B2E15
      FE1CBD7FEFDEBDDCB061C338E447D9AD5B37FF2E5DBA5487DDAC4F9F3EEF75EE
      DCB956CD9A35F36D365BDAE3C78F994679ED7923468CD009274F9EECAC542A87
      2A148A917ABD7E829393D36093C9D44DA3D1B4855F23AD565B1FFEAD020202C2
      4158885ACDF3F3F32BBE79F36639211EA6E09168727878F858D444273737B76A
      9032401004779EE70D52A9540EEDA7361A8D21201A1E1212B235303070457070
      70D74183067974EDDA95636CB9B9B976BEA8A8A8100978894442B01DE0388ECA
      C0FC40CA741107290D66B3B99B9F9FDFD7205DEEE2E2D2F3BDF7DE33E5E7E75B
      04B0768614A160E6D035280F7DAAB8B89825841741D5DA1DA476BB9DE0CFC1E6
      E472B906D90D85A48D4B4B4B6B9E3A75EAAAC462B1DCCCC9C9E98027F3884028
      17B25AAD545252E22043213BDCCC8F498D6C13B2C2414F0BC9C9C94E2F5EBC88
      7DFAF4698824212161874EA76BEDEAEA5A1DD27088CC9E889F379E0A0A0A1CA4
      4C343C995253531DC8CACAA2CCCC4C42B130520B1E9ACF0D1E3C58121616D6B2
      56AD5AF30C064395C2C24281950F938C65F5F9F3E7F4ECD93307014BC8C891D0
      2125722342A2ABF7EEDD1B2AF9EEBBEFACC8EFFEB163C72A1B376EFCB95AADF6
      43E171884419191974F7EE5D626E944FB9744C4256012CBB7870367292CFFC1C
      78FFFDF7555F7CF1C5845DBB76651F3C78D0BE7FFF7EFBEFBFFF2E2E58B0409C
      3B77AE3873E64C71F2E4C9F68F3EFAC88E5CD8D0602DCD9A354B4073F812E566
      7690945DFAF7EF6F5AB468D1BC3D7BF664FEF9E79FB65F7FFD55FCFCF3CFC599
      3367DA274D9A64470B2EEEDEBD7B1A08FE46DB5BEEECECDC038DD91FE91D3D04
      F63FA67DFBF6AEF3E6CD5BBE75EBD6826FBFFD56FCECB3CFECA3468D2A40B7B9
      0F35B3D5C7C7672CB2551329D84F1807FBEDA64D9B3641C8C6AF4002082EC4C5
      C57DE3EDEDDD03AD3C0CA934C0BB0910C1619090439FF2029AB9BBBBD704810B
      0264C07F478088FFF78DEB57E2FFE8E902AAD9BD57AF5E21C3870F8F40AD351C
      3060406FFC4DE8EC74FF33DCCF422D8D402D79D6A953270759CD3E77EE5CE99B
      C4E6518523F0EBB104EAE43B602DF4D104A89396B043A08B4CB0FD50531FFAFA
      FA2E41810F9C32654A18C8CBC60CE59C3C227746A4861E1E1EBE68E1285719CF
      7A38BA077A0AC7FA1C87D6AE84FAA8079D350F2AE41BA8900903070E0C84E628
      57D53C7A721E681D89C042E8B40EB5010F87CDFCD072999B75662D246C1C1A1A
      FA594C4CCC0E28BBD920ACDEBA756B9904EA20272D2D4D442775142EEB98AC5F
      31223CC0D139211D313234440E60AA46831C44E22111E8979D804142CF9E3DC3
      140A457DA80946C4412FB1F48E0ECA08983E62E4ACE33237B2ED5021503FDC93
      274F9866B0262626FE2540CC17501FBED0760150113C23644F6784AC77330206
      3C959080A03288A916E482981F74532AB4C426C98C19331E7CF6D96733F1241D
      6AAF0102254C1248E950B3D0378E844C91E1210E65C61EE0101B17E8A634A892
      743889476DF1206B833675F7CC9933F643870ED94F9C38216ED9B24584361067
      CF9E2D7EFAE9A74C8D88E3C68D13D189EDF815B17FF0C1073674A7BDEE1EFE81
      8CC801744E397A7DEF0D1B363C662A64DFBE7DF6F5EBD73BF4D19C397344482E
      4E983081A9123B7E388BA12592A1558FA02F7EA837988D0E92B24B870E1D94D3
      A74F1FBE7DFBF6677FFDF597FDA79F7E7248346DDA343B34A81D2D3D1B557DB3
      468D1AEBA1E387205D554003BC6EDAB56B67C0D3E743B2BC55AB56D9A74E9DCA
      34625E8B162DCE60B4B204ADBE139A00CB8A0AA9594DC37A8B69D5AA95FBA851
      6317CE9C39F7D9AC59F3ECCD9BB779E0E313D00D8D952933C9BF9339F7DCF176
      C229537E0A3C70E0DAC103076E8BB366FD92D2B66DEF7E2028EF1270971BA7AE
      1BA442F91D1C1843BAD6AC59B365BD7A4D62222262078487FBD554289CF48585
      36C16C76F2C26FBEC1D3D305DDCE6C4783CC4412875106B6142B3D01ED28B26A
      D5AAABEBD4895D6134F27D89D45EC80EC5C444C82323236AD7AF5F6326D4F04A
      14F46030680187C9DC3DBCF27007CABD0E7EDBAEB76AD5E6D1B8710BF3D9F891
      818D2167CDFAA1A463C72ECFEBD6AD7B1B6A6511182A57393CCA0DBA8A0E6AA5
      9EB39B779BD66DFA8C7EFFFD51333A771E31A353A76133DAB5EB33C3D3D37730
      246C8E5A8B4422364486F50E131F1F5F59D40A71DF1656A98C2AC4A7A3478FBE
      95EC4D616F25AA48FADFB8FFAF11395AE4DBF2FDDF48C2E2203DE720623715F3
      0D37C5D34CE6FD1AE26916356EDC98FEFD2729F3406039299E2096F9BFCDAE18
      9FC5E1D9E5FF06FE3F4CE428E0FFA670DF558E48FFF6EA47ADBC312D6B1A6F0A
      7B67F5238143E28A8C78BAA369FC3BECFF25B506F15FCB5259F6DE155616E7FF
      C8E60E1EDAFF9552A96A8941041BFEB2A9159386C34041C030865506BB773C84
      E739BB449058304812317E72143AFC4A8B8B8BF6488C06E328A34FA4ECD2931C
      47E4B20B472205B92828CADB483C5AC9BDE45CBA9A984FB67F0D76C3DCD5A4CC
      B8EF2661925C799A478BF72650C53F7F27194D6BEF8721DF4B5F09CFD1D13B19
      F4F7E37C48FDD28F5D7BD771A786CE7629139DB0B0872530E6FD122C2F3DE39C
      29CAC74402FFB28504B869695803777A90F298D2F2AD2F23E22A3A3248F43216
      3CFE3122A924764A78968655ACA272EF82620B3D7E9E42465909610DAFDCBFCC
      E190A8EC46863BA560A360831D4F10E8C1B30C52CAA52493482835338F50F814
      855989DD6EA5E44209155418BA974BE46392D2F4B65EF4419492A23DE4E4EFA6
      23B5424A761B96E4F024AD5A41016613857BEAA97DA486E675F6A55AFE9AF232
      741005B92A685A077F6A5BDD8B7A3789A4DAE19E14E061222F17031636A5181A
      F3E4A45591BFBB89AA0599A9631DAC27457AD0A7ED0329D64F8BC7BC2AA3AA5E
      3A8AF57F59B0469D8AAA60392CC0C389742A4579610B024F7A8D92FCCC4E64D2
      A9210947DE4E2AAA1FC2864C442815B0A16A1DB4AF2E3289E0888846F7CAE7A5
      C5DA130AEFE5CDAB6B591C07D1FD6436491629D8AC75ACCD66E616905625279D
      5AE9688C2C8D1DF55C585442997985A457AB1026A7949C627A9A59EC58F17310
      5D7F5640BB2FBDA08F5BF8D0D317A98EDAF1331B50262FB3C762E6171653525A
      0E3D7A9E4525A5A8591F37FAED7C3A569215D4C099883B77EE8CF884F3A3F9BB
      1F92935A42AE0A0B8523200CB5532DC81D65824577749167E93974EDE10BBA9D
      9845D7526C945A2CA78C421BF5A8E54E8D5CD2F21C1211FE5825A717D828BB88
      A7423CD1DD584A4E287829CA0BC16444415BD105AE80E45EA640A5761BF32E87
      A3FACBEFE0C0FA343DCB17C8DFD34C465439BC1C4683320BF53113564541C239
      FC2A5ECA25AAE8596A275A7F3E8D42BD0CE46962C369A25414EC2FE7528949FD
      3A0DAA1FD5570A55201DDCD0B32297A3961351236544E9F916F27356D2C0062F
      89CB224763699FCB4DB7727BF7FDB1D1C9646A84EAFDCF63C2B2D4156C08624B
      4F4FFB93B018A9ABE0EF7062AACE03D58108C0A1391D01EFB8F0369BDDF88670
      560C81F05F08F400992BEC771A36A37EADE652C771AC6E9F2165143007980AB2
      CA4B3BF0AC685E23A9109804F74380D54223D806C0619CBF1405972F452DC8CB
      B35D8908010250562D59487503792C85CD54A51D61814030FC02E1D707A8877B
      296C472D33BB0C4CB9B4436055F4D152E03602D8BA80047613602AF0057ACC68
      6018DC238050C4E72A49044F0B1002CC036A01B745A29BB059B6C622724F8EA8
      35EEDB01CCAF29EC0F002D8F4B4553849BB30093AC1FEC2AC03DE019087C0035
      12C8607BC1AF18108038C0087F58AF0C6A0B02D019DC4E83E8D781A670370014
      08100018A80C2219FC98FB6BD8BF02D9125C2A19901522CF8CEC1ECA88ED29D5
      05214BF802295D10B9AC3298445B717F0F692CAF1121A0CC70487819D9A8098F
      48909EC33DEB055E204E83DFAFC0134602FBA5CE16E3E32370A3052EBBE967B2
      A7C7C06D832ABF087B1F100C48010B003EBA0DFB3C60051C86775C899AC2DE04
      74AA657DC21214C17D1B7800EC064E013780CB904603FB7D60253002C5A08120
      BC0437CCC871F105BEDA55F0E358D8BBB8F878D610091199AD841F23FE0B3693
      7810B21C231295E07E1B80DD385C2B184FB897003C9EB2136416944D21EEB590
      A439EC7DC0324009926EB0A50023B6F170FCDBF8C28391D581CD4C262E3B0067
      C0034820A205C04680D51C232AC06F1EAB1C3A07CFF915F003DCA500A58DE78A
      21CD2F70EF86744ED0FF1C6A2A818816013B013B2417E9B7F53F9B70F31F8D79
      D933B3C7F2EBE17ECB8E4AD68D8EE150769CDBD799CEE6AF5374B4E65FC3B857
      6C9CCBE2A248A7592F6A6B7B7C6B12DC2254F057BE820CF69B8A83FD8622E85F
      064FAB0BAF29C0618089FF14D9B1C3FD56F34676C47E0EB802D380794028F04E
      F3362256534CA939217523C00C380CB423FF4A3B2A1C1EAF2EC22B9B353C4EDD
      3A5E0D58E1C7B2E18946D714EE1CE077F8F380A7B33D5F0BF46A52FAA0F91F0D
      0262BF685C2F2CBE7163BE9C08916448C0129A51CDC9706334452D60DB80448C
      89DB4844DB800031B30586A3EFE3F7AC45983DB5999798530F0F0CAE48847BAA
      8144930026C513B49F60B85959554160138EE3426CC40562D8CF3FE29DB8E392
      80C372D1363D844F65C580A8AF0C6A2B0ACE45908865EF0F104543126F9EC406
      1CC71B10865B910D0D9E6048A7C57D1230E22BD7F5B724705434F770C3B4635D
      D8B148D5D8642F2C2AE4A44A0B49911672711C4BA346F84FC003E039565299FA
      85B38281541C6EB56A7BC900CC47E6AA458BDC467C7106AF46CE38D63839D4C4
      0BC419041C032C5FBAFC66E0E078CDB45C9CE81A614B1EFB42D08D3D2FF8488A
      38C9792B49F2103108D92D84BD0B588D46FA1C362D5BB64C5389286A49AE5221
      96C60E2839DFA3B1F561E7E392C0E7F314CD9C2D9C900A959F8444D540741DF6
      2AE00C900732B6EC2AE1A077987A2886A7FC94E05B054F9F186E4BADEA26E66F
      99236FFEE36A799D9118164A11FE10242C3B6E703F0398544B40848760A0058F
      798005D0D7B33D6900BB00980FFCBC5A5157015B89A9DB23D83F125129309127
      0AB5137583FB77A09C280F3731403EB01438055C838EB1387F399358DE214953
      F86D067E02342019099BB541396C8791E01A4F44ECC908A77C46807B87E1888A
      E0D80DB08461B07702DF0036E07DA09C08EE771B340725D00B1DB53F3AAC82C5
      C6BD11F808A8C7EE5F628D68808633014CBA977EFFBA9A9725E9DD97DDAAEAB9
      E28CB22C08247AED8A0237C59A6417A47DBD1D61859DC70E83110B972E58CF57
      A6A7A70B688964301AEDD8B52D4978919C7E3F2539736CF71EA565A4CCAE2405
      B673D4581DAD85C43D1F3C78D018BB9DEE205310E6E47A93A9D4C3C73BCD6834
      9D71D56A772CFEE5E77D93FBF6CB62240CE5445851D76173A0CF8D1B3786C01D
      7EF7DE3DB9D56E278954CAD9516DA53C2F170C7ACF90B0B0CE514141714A41E2
      3B6EE5CA555F8D1E9DCD887876C1EE9E023B367D21D127EBD6ADAB7AFBF66D39
      36FE39F37BEF716E6DDA90577434F9444592CEC383BB9C9E26D97AF3864F625E
      EE68A3543A68D2DA357AC621DC58B14292A254D6C7FAF44C6C64FA97582C828F
      9F1F27737121CEC3034763049260F397944AE2F4382EE3EACABD9049B92CAC51
      0728953E6AA27BB5716281CF532A3D90A5AE57AE5CF1C74609A731184850AB49
      82DD6379420229D3D258F68853A948301A4874762695A7273DD368F864411220
      972B3AC8950A3D9FAA568763FFA8C1C3870FE5A82912E572B221110A98643939
      C4E5E690C87398200B64C7039E55AF414535E2C8E6EA464F2512992093D52BC1
      E493B74AA5B1D885F1C2760F0705C571EEEE640F0D258B9313B1C256E12013B6
      B108EA954D7EC9AA501076AC482A9773D982C0F172998754B447B04DA8414F9F
      3EAD76E7CE1D549094537B79910224D8C8262976DA3DCC66E2513E2495112793
      93B4B8841499D990368FB4A516AAA252D92DB9B97F4BD0D8402EE7200E713A1D
      153D7F4E126C3D6B90C512547BD293042AC1112B0B669225020E39A1EC0A1940
      E86CD093A64E5D6B9AD59ACF6DDBB66D0176CD477FBF699346A8568DE3B17CA1
      494C241D1249F12B60E77962106117582C98AA1652016A31BFA8889AD7A92376
      6CDC24E1F28D6B0339EC0F75C526D4A2EF7FFFDDEFB95E8F1F0B9E532525110E
      0290140D9249CA4E1A95C26D83243928FC82A26292C864E2D811236CDE1E1EFB
      4E9F3B3B92C346B85F7474F4A2CBD7AF77FEFD8F3FA45848E158D54B912DB6B8
      822361548202B6A00D8990C8FAF02115E2785593264DEC7D7AF77EF1F8D1A339
      C74F9EF891EBD1A38704C7603AA1A32EC6AEBADFF1E3C73946866D41B4009E38
      14B45D83C514B41D1BB25372EB164504068AD8922D42FBDB70FAF4E939CB972F
      7F02DD453478F060436C6CEC789C441879F5EA55230279B6A387A6CF1A10D950
      6E02DA9693D128560349CDD8D85254D2D1F3E7CF7F76E1C285CB67CF9EB53175
      49972F5F2EC6D6E13DA552598ACD1333B676B4388121A0ECC8802CF9A0AB540D
      0EC6A99F186B5060602A34C241102CBA7FFFFE456CE8316D89675630CD9B37D7
      8F1A35AA350E0BAC82B81756AE5C9908A402CF717F73E1C2859BC78C1933A865
      CB96BEFEFEFE8E0E5F21796527F68F0494992BB6A16B0D1C38F0830183870EEE
      3774E4C05EC3C776EA326242D58E13179BFAAE3B5AAE7ECA52739B366F30A060
      55D8C074945759C08913270D5959D91E58372A75717649AA592B8EFD10940557
      B4459C6129ACE851EEC68E96124D6230F6187FC766E5CFB86F521EF816C7DBF2
      E98E828EC3BEA3884D4B2524AE0332F95B381CDE6F2352A3D6B46DDBB675C1BE
      632E76AF8C886D00D96B65037F87791B1141A5F08004246CE3D71BB1FB02AD40
      E60768000EF7E5E66D443616E35505886857129C13A985F300BD90CD495080A3
      101E0532473B84BBF2741D011C030B00D813D167053B0A3C19ED8B6595860E1D
      AAC33E6D2D847F000494C52F97081EEC57B406026B03CE00232248C3D7AB57CF
      8E96AF40BB92F7EBD7CF057BB916C4AF8AD3417D11CF097829113CA5B869AE56
      AB07A1B606A06CBA633D5B0F3F1E44C190800D525538F6E08DACA91A376EACC6
      76B4157DD38C382EC04B223858A2EAD8A3367CF3CD37EA860D1B62735D512495
      4AAD286C35887588C3A4C72DAFC4C39CB0AB8E67594BE09F07941315E3260D3F
      8C12FC646BB09F4D1F7EF86136D8D8500741E5866597A1183DBE105BFAC90849
      031CE32842D658A00FC4EE131111118C822D825A098024ACFD304958DC328818
      1B244E9C38310FBF853BAF5EBDBA8D053002669791614ECF75C1C67704F6F715
      E8BC8108FC778BB660B7E611A486A2CCFDFADAB56B3711E7A544CC510648C76A
      EC3D9C2BAABF74E95223F49313C264007BA808AD980B95F2005BF82FE0B71C44
      19B05F27629E207343360757AB562D047BFD323F3FBF00F8B3EE2142A9658C19
      33E6E1F5EBD7FF86DF4610B1027F331122B0AC46806C3C4E40C9B0358D9F174E
      80BF9892925282834DB9382BB109F78740F4EF0A81770503A9DC811FB1DBBE0D
      65F56B19D0383742C56C475873802F4BC261FCE7F9EA268B8673E50A0A9158B9
      04224C05FCDB58E1F1F476BF7D05A55A0F17B8455AB376252B44B8DF6D7CD76E
      E6BE5B3E48BA6355DFC0533FF68DD91ADF9495D9CB446B44677EF8B0D1E2CBBB
      775F9F0CEB26FAC88A7DA243CC73FC02BC967B077835DBB3A88DF4552A2CE4BF
      72BDCBDAB1E47DFEF0BA3E55C2C27C168A25C59D8B73B26B7BB83B7DEEE2E2DC
      69C7ECD68C2CABBCB0DE46B47F65774E6F50478784F92C36A8659D54325E692F
      2E122C3959915A8D7A86C1D9A5D10FB7EBD8FEC9E75B9870A2D71C12EA3D4BAF
      92B6B6E2279B17310DC42C27333D972F2ACE0CCD299675BA9F66BBF8D682DE8E
      ECE8F4AA605F5F8F099CB5A88F49A3509616634893934719E9D9949D55509A92
      5574E9C2C39C59B5BB7E7AE68D441BE777E0DCDC8C3121A15E73D532BEE9B3C7
      493205C6FF02D9E8C50B9C164FCD2E4DCBB7FC7DE852F2DC1B4F728FACFA650F
      6B2AAFE769DFCAEE81CF4E7CBA3BFFD24C31FBD4643165FF48F1FE865EE2C5D5
      1DC5FD731B97AE1B1B77BA6723EFB62E3A9982A5DEBE7DBB9AD9E560D9D9F1E5
      FB5E094726AFCE39FF5951F6A94962EAA10FC5842DFDC4CBEB3A8B87E637B1FC
      38BED6E95E4D7CDB7839E17CFAAB94BFFDF69BF29593E8B7B9EDB923DFF58B79
      76F2D33DC9C7271625ED1F6D4F39304A7CBCB59F7876793BF1C0DC46961F27D6
      3AD9BBA96FE3003715ABF2F2B4D0AAFFD4FEBE953D221CD9B93843CC3A39517C
      BE77B8780FD961921C9CDBD8F2DDB89AA7FB36F36BE761C428B59CA28263FDBC
      0EDC916FFB063F3D367533B253F2323BA3C5C75BFA39CA8465E7A789B54FF469
      8AEC98E46FEA772FD97E9DDFC9FDFEC1F13F3EDA37A630E18F1162EAC1518E32
      39FF4D07F14F94C9CF136A9D01491348F2E69A79494334E7A3A6FE87BFEFB7F9
      E9BED1457736F5136FFFDA43BCB4A6A3A34C7E40C1F66BE6D73ED0AC7A37C92B
      32D9B0F7AAD63DFD7DCF43F736F62E39B6A4152329FD7142ED533D1BFBB4F1AE
      503BAFE2BFDDD2ABA58A816D429BEC5DDCE6D05F0B9BE5FF34A1E6F95E8DBDDB
      38EB3067787BB27F42D688FF48AC5349E5DD1BF9D6593C3466F1F076811DAA05
      1AE4FFC4FC0FAE35A21B41430A15A2C9E4BD9606A8FB7C1D485F65B25F930A41
      70AE119D10DF13D0E1EE1F03C5C654ED25F83075B910F609A8DB6B88580D6E1B
      DCD75FBA6D365F71CBAD447A2FDACE61A376387713FE3144620BC4FB88887BC2
      5AA43B6EBC803A00238045D5898A63DC3612D44C712D2C89542F36F4E0EC9CBD
      0602AB02CCC482200E603ADFCC8896C0F757602C24F81936C1FEDE4DAFDC0437
      DEFA505E82FB3ADCCEB0BF43D806B8599C75B0C7005B81C53C2E4780838890B1
      75EB567EC396F5EA1F37FEE0F21EF5F5F1E742E254A2A6356F17EA939D5C108F
      C587F5CA0CE792E13A041CE3F7FB1E78BED77BDF2D4C665AE01057373777D741
      6A57D587B186BAE3AA2A62BBBA091ED19E9C4F6460514415B72C6F2FB7F5A445
      96FF211CCEAD811077F833F6A08FF60B35E7F9FAFA0EC6E0A1875AAF6E91ABC8
      ACFE48B8E39DC9A729D552B5DC49E16A366B3DDA052BC387D74C6FD2BCFAA346
      CE18BDBDACED35E260147C28AF94886EAE06B52B064F6A4C6A04B554C369781D
      65D93229DF9E471A890EBBE912BE4092A7F3D2FA443431B6EEDE58D1F6233CB4
      E9800103DC056B911C59E3F87ACAE4E4E101F94998A31146682411A5E42DF5A5
      00790869049060B550C6CBC8C09BC84F08E13D95BE6A7F2FFFAA38803A1C63A9
      31A3EF4E7DD27A7B9B67FCCF092A5A74C9EA89250DC7396CCCC348629751A83C
      8AC2E455482E2A4923EA285A1E47BEF200C2BA0404200EA33919066291E7DCDA
      7D7A3C70703541D2EC43AD59AFAC5E55955B525A5ACA86C30EC9A490CC6EC301
      785B31193827F2B505119F2F751CEF4CC29C17C3448ED90F052F3D363AFF1206
      C4981263B4050538C0E76FB158A438C1EA385DAF5440128986720AB349922327
      3E5D4A2F9292292121C171F814C73B1D0754B9FBC76E592F6CD92F99E73D3BCC
      9099E9B4ECD2BE139877348154729C5AE53036A2BCEC02121F4AA934D742C9F9
      2984306265C9F286D1BFE3A8ECE3AA83AB2736A862609DB63AADB6F71A3D7AB4
      DBCF3FFFFCC9912347B61D387060D7E1C387776FDEBC7937668DBB7166743746
      6EBB715E7B37CE8CEEC639E55D9818EEC4CC69A3FB985FF7D0C2C448465E8EF1
      E3C707AC59B3663E16A2B6E320EAAE5F7FFD75F7E2C58BCB8930EDDA3568D0A0
      1D38EFBE1EA3B82F30981F8C66130D020993A8061A545FDC10CE8CF2185F57C1
      40F32B48B5F3871F7ED88D83A7BB707E7D07DEDEF81D04AB31829BEAEFEFDF09
      E3F150A4D1D38AA251B4DA16CE882682E817C0090184573DF8214386D4F9FAEB
      AF572E58B06033087EC60316610437105388DA2A95CA154D846944A682CC48B7
      8556148C85483401046680051CDC359CFB5933FF76D5FD59E9D6E8F30B779F0E
      1BD13ADBC9A540DC5D7B0B2D2FE84932953FFAD606100C459A96401712A4F559
      E77B819B24E00C7005A00253D8B96786985F0E3DB46DCFF269F8A5E8576B1BFC
      F340721EF63580990BB8FC0D24112779C4A450E109E5835004380CAA9F736DD2
      4D1AD0E5435F1FA3B6AD9B5ADEDCA0903A61E9E956894DDC83F5FAE3F35BFA64
      3A22E3C2246280B3B271597A96F7E939B96A1D7FD759ED429C3E691B6C6CD6CC
      5F57BD818FA6474D0FF5970106D9E4F98793986675246424450E57858BF717C7
      381F852DBA81877256731F5DFB50A352EDAC94966AA542895129A10093DC3DCA
      5539CC5B2799B4E448A2274B2A41B66CCC51110A89A80DD3CBFAD6F7D4367452
      48AC36BB6815B1C266C1A29BD54EA24CC20B0685A0F435C8BBD9ACF6FB4B0E3E
      5A2BA94850E6362B0573B051D9D44323276C3D94B2D33D18DAD9B1F8674716B8
      E252BB5521E12D7AA5C4A49109ED0A65C2F6975AAE8CE195EDDF6160784D4FC3
      8000939A07890D83680E233F0E64100CD4882745A90B3C27C92B2AE53232728F
      BC91C8B75D7FEF2A66DD074138482211A0FAB12F08222C4CA129723CC7484491
      38ECFE09189316A5A5E6EC7F2391679BBE0A4FBDB24DB88BC649291120111306
      F2C040240E6A8A18302D923DCB2C7CF2EC69D2C63712F18D7A94A20C02FD0CCA
      EAEE5A851DD943660807BD0088818318D0E3BC3425CF624F4CCDFD233F3B77E7
      1B0B3B29F189E460666A81A5B890FBA87EB82AD049830D4D8E15B4237BBCC849
      53F32DFCDD67D967F2B2F37FD7CA14B910D6F1B07F2E833F575251EE200CEE3F
      91199D3D3AC6C5E475AAEA6B336B95723976D10A71D405AAB538292DFF5C6166
      EE5265A9E5E82703EA955426EAFCB19E4A2D3DF0C6E9144EA3F77136FBA6D4F4
      0FF8C5C7DD2549A5904728048913F4708EBDA4F4365F5C72586DB7DE0609DB53
      A93015ED38464F053903A9A4E0634EA5F374F30D4EAA1118FA6394B3CBDA2C51
      C8D00A9C0227EA646847562CF216E1854FCBB481F550F6FF6486A8F70C2535EF
      378A6A7548A2FA5DECAEBD3E49EC347FF327D3BE3FC1D44BC598EF707F30D540
      1D3E1C418DBA3FE41A76B7BAF498F8BC6DFCCFD387CCFAC1ED1DA95E0BE249A1
      EE01DF49BC5AEF83EC24C68545AF8930BBAFFB76E6C014F8FFD78697EA5CFC25
      7A27AD9B775052CDB0AAAB235D5CD67C31A2051BAEFCD7242C221FE617B9B96E
      58D40F8D422357557533FFF8F9E8D6954858A7606091DF05A621D35F45F81EF6
      1EA895E3D0C755E1D6C37D026EF673A385FB24DCD5E0AF79E5C61892EAE37E26
      908FDA2427381802618700CC78119584614025C586560010F1CAED8BC0208019
      1F5CC20096D6C4C33113D8098CC693BE85CDC6877BDDF48A1F89C8D34DAF4A06
      98C27783FD07E2FC48EC6F38C7D24C817305B08811FD09C77E44486663C88D5B
      36687EDAF4835B0F1A1610C1C5D4D491A195C42E6D8831A419F1587C58AFCC70
      2E1FAE43C0517E9FCFFE677F78EDBD78F4E8D1561843F6327B9A8768CDDAD131
      A6B8B15594D5BBB808EE553D78EFF0E0E2A86A3EE9C1FE6EBF7206645340E297
      6638B70B8EF3FC117BD8CC5FECF5BEC41AE3700C9CFA969414B74BCE7F56E776
      CEF5E0B4DC1493BA40AB752A76F772B37B76F4B5074D8C79D8B06795E30DC2F0
      AA900FE0860AE806A256BC516A33FA9B143ABC6812BD7BF7EE2A27FF3A1DFAF8
      645268DAD99CD0E2F314AABB620E555D710AB55F9755533E30B6F44B0B9B184C
      515BF0DBBF1FCDE2574969812D509A532CCC1ADEAD41BB603561D9DDBD69D3A6
      8FF1464A56685848729E262B2FC3F379AADDD5922D7A5AD2A4CE42769588AA49
      AE120FAB5AA9B66360968945298F2625E78E37E2EF5E94AC7AA4E5852C3ED01B
      AB0C580F14B1FCEE82C52573749D2A77234CD1963BB7EE98C44C5E17161BFC30
      5016CA9D797CCE0BA3DF428CEEA0C639DD797D838FAEE518E582BCE9288D8B5A
      1E664CB9684061E762454F89619DDCDBDD3B47ABD659521FA7B938051B9EBB70
      66D19641D2C4A789AE18B57178071203BB224DB2E09C995C2CFCCDB74BDFFA47
      838CBD4B21663693C8DFDF3F152FEA5C71377B14061882ECA131C14F9DB52E39
      3909F992C78F1E33299EE32DC33C944F0E0A995CEFEEBCEDB177E26D363E6AAA
      5991390BAFFE2CC7225C02964E9330BC4BC23AA303C33E1C96346068FF24ACD1
      26F5EDDB3709ABA749D81349C21B63CFF0C0A488B1DF9D12163EE92E50877837
      0BAF50BA5F5CBB0CA3B1BCB8B83815960CD3030202525C5C5C52ECA5F6149D5A
      978201568A52A94CC1F83A05D2A4207B29D84A7B9C24F5C8CDD2875C60D29503
      AFC2B8AD58B1E2B383070F6E3F8001E9A64D9B76634B880DFF76E305273610DD
      85CD989D18026EC04BCC5FE275AC61680631D470A4D6913534AAC98C0D433CF6
      A66F00962866A25D6D6783D1B973E7EEC268968D2137A301AEC186C2545F5FDF
      0E58960EC30A30C690C57D90BE1923FA128E83400023C31892C31BBBC18B162D
      5A8831E426368684DF22BC96D70FADBF965C2E67634809E232156442BA9F80CF
      395C447832C33ADF2174DECF259F3FEB61B4A4B6883B35EDE0D9B0112DB395EE
      C5F67971533182ED4C528533E27C85747D90A80DD00B709C197D0007337FE2B2
      1720ABCEE36C5A89EE26CAE9A1BDD697D748EF26C32278612E89A7118EB10EAE
      44CC5D0A574DA08049E483273CC5CD4BB3461C03C74CE02E3008D00250709406
      7B35C0B4E41748B310EE9706C786585E05C71D6E606B000500AD67F78496645A
      106EC779001991CC8B4860F72A648DC52D00A1C8C023D10B78B29FEE48B83F04
      36008D24FCB50FA0111F024F80DBC003817FD09D8501DF01C3811A48CB38B054
      3F9C2B86871268A220EBFE0D4E1BB37FD2FF786FB476557E2857B59696F4AD04
      BB84CDB25D9DB561B7F0F4E3C013C4DF033400585A10AD1127E2E6B3D16E8FF3
      0F459C8D72F7701BA231AB47471B633F8A52567BCF453057F5E47CC3838BA2A2
      3D33FD7DDD7E23BD43430EE7581932ED380D527DCAC41A09A24FCC4E86A1D045
      5D30CB6E9A27CFAA769FBFE995C6252B5512B5D4A4707675D1B9B50ED4848DAE
      97D5AA7D9D47ADCC83070F969228FA11D127C018DE53C84F7657DA0B429C6436
      F423C72CDB80F22CB417509E3DC731CB96F252AE50C853BB69DC031B9A9ABFD7
      40DD623C1EDACEB9E8A99AB7953C04D1457EB2F3D5A3F111D9579BB91667B359
      21869EE4267852A03C149BBED869E7049262966D149CC9970FE2DC65DE721F0F
      DFE0A0A0A07ED3687BA35A594777B9A5FC3D83BF9949C51753AC5ABCE4ED9865
      A3FF60CA2EA1607904452962E86AD6A7619733E7465695C752A02284249C94ED
      6EB1F7B7D93B024124DA6BE7E6E47A088AFAFDA56693BE460D1D36982DECED04
      9E30A1011912E020FCCD02573D3E7C213696156470B91262D37A36BBBE73E70E
      C75EB07C2A3A29D20BED273897AF13439B1AB2264F8B2139DE42D09562CACEB2
      C8C8F2ADB9742EF514E567E49326CB48F959050455EC981443CFB2C9B278535B
      5D7D47193E4B92A6F05AB6BDC8B5269DBFB6755234C75EA4502392E39DD1DCBC
      5C2ABD8721318608A9F9692CA163968D7D37C7DA40A236DC703368506C89443D
      9F7591200B2733ECC9F73BDDFCF20E15346353E86EEC895BB8FCBC7C2A65AF2B
      9472A89897063FA2ECAD7A11FB4694C5F35422D3291112C8F4D16034A8F1B4DA
      1A8C3D104F28B578BCE2E9D090D879DF8D17BC1DB36CA6213FFEF86336CBDEF9
      DE7BEFADC7B6C617DE2DFA4FC0547D1A7D631901329835A21A6413252BF21BE0
      6DF0E0B56BD7CE8386DC81D93653B365B3ECCD50B1AB6263632743A7B7930EDF
      D88396A4CEA7E957F460806124B040E40B4CE05659C22267FF356BD0EA3F0FCD
      9F3F7F13B60A7FC40EDF02ACCCF4910DFC7E1C37F9E468AC0856A355B6B15878
      08614919581771658E571D71B5C84B3BDC746B3AE3577B9DDAEB7223B3B7E8DE
      13F6C5CC96DDEA7F28DF52B3DF476260BD15A432BE8FB9C43A4CB8EE3BD2E2C2
      149B174892E07E69D68843E09802DCA1E43B1F91D2A02095E3304F0E49644B89
      E399625B81345F234EB9614406DC31C9721168859B9DFB631AB136DC85C08DD4
      71F8100B1CAF99974BAB1AF8DBFFA957DC95199C2D92C0DD1BE809FC0EEC0659
      2AECB71A26C99B026DF07C0A04037380E9207787FD56C3BF29044F17E1FF1478
      0CB802D1801A70181CB192E1A89513C8E50E0F5C5819695036F97097958D0CEE
      42400FCC43DE078944C7E0FE089002AC87F0B05B61827A09F6493CB89479943F
      099E2C7177D8B198FC5A801B70E701AC1B74863D1758018289C030B847039190
      8C6344C5B8219CB0E325A2AD086E3F2262096AC3BE0B69AEC376054621721BA0
      3EDC0D0196AD26B0BB001A1ED9CA0189013783EEE62E6681C7E16659E80EBB3A
      F01848E6885C003920053CE05704F040154023ECEFD3471664320DC58DA78C6C
      C7BE50344E20A2CB101DC320C751CF58DCB35A3483400060889D1E7F06FFCF81
      43C03D497D1F1F3F228A0256032E2939B3E438A37D19EE04945120ECEA20654F
      4E1489581635F0632617973F804414B65DA290483C716301528021C003E03EA0
      46C2EB78FC13B883417ACA4EC4B288610D25C1EF3B208591C026FE8ACCB1AB89
      386C359DFC2EF11E4F11D014306351E518A4D90E770920E1B0360F5B013C0718
      9994D518DCC43F15F4057048013590EC26E6556F5F7A2BC7C59E7F1B4F4B84DF
      01E034701638016215ECAEC05A6024E0C8AA2487975FC14D4B2012D8E329E68D
      FCAEF0F750B8AF507CFC3A3738608CC00BE008A003FA43BA1091A819DC1B803C
      DC43DEF8F87AB81906AC079201139001DC70D5CDF025E27E81244F703F1E5000
      1F03FD806BC06048FE8887839933B8FC007403DE07BC8128A08A8932D3606F04
      4C0013F029ECC5C0AF40092003FE598940A3E4E0E10B3426222F4002FC099C42
      736059EB899ABB2E129D4C1FCFB137350210D616D80F891EC07ECDB003A91210
      CB8E0F1ECCCF9D33CB73F682CF83DA2FDE55BBED975B5A7658F24BF0FC797302
      472CF82578D882DFAA0D5DB02162FEBC793ED4F09B6B7CFF55879593BE3D21AF
      4889DF2FB556A309C031A21A387615834FCDF8615CA0A818E73FBA315570C587
      4986F4EDD367FDEC59B34EE27DFFE3987BFCECE7E7D71564DA3711F0FFF69408
      82BE5A7474FF8FC68C99848F04741E317264CD51A346D5FEE4934FBA0C193C78
      4A604040278C0D34FF4E578988C3F93AA3C914D7BE4387EEEF77E9E28FAFC728
      B0622CC13C4E8291BFB25BF7EE512D5BB5EA83F94724E2B2CA29E7AB4404B165
      F8F046749DDAB57D305DC0B2A5C0E23B8032E3BC3C3DA5988B84224E28E2B25A
      7D331152483059D1E90D0619DCE591CA1C8244C2998C46254E926940544908A1
      2C12B391989D64A98A233175F05425A47078B30B20E2188C78F9D2A58C23478F
      1EC4218BEB184FD9E1EF3095F2091FC1C5D9B9D9F8091316E1373F0AD2E1C54C
      E800042091886F67947CB964C9C98D9B36CDC080EB2CBCD13E7185F93711A146
      8CF8DAC9A85123478E68DDBAB559A7D73BA4C69CD7B6FEB7DFEE61D8F3F98387
      0FB7610C958BF4EF36D859F06BDFAEDDCAFDFBF6A563F466C5A0CABA7DDBF634
      CC1AA76348C8BACBBB09CA42515682BBD9DC64C2F8F1C730E02CB97BF76EF187
      A347FF856650AD2CCE7F6D6367CB54B366CD78CCD9D2F18D9F6464770AA47963
      ABFE4FA4020ABBC5ACF8F81BD33EFDF4ACD9CDAD2924AD54E5FF89A03C1C12F8
      77EAD8717FC70E1D36607A11501EF03F75A01D19424342D68504052D60EEFF69
      FA8AF15532A97401301E9E4AE07F6D981AED8FD4EF0195FA16EE2B1B8C5C5F6B
      942CC61766B3305FAF5761A2EF172D93790F512894A3A55247E364E1AF638DC8
      26BA0CE54F4479B0AFE7A89D9D9CBC31A60EC7CA4304D649FC70CE4D8F96CF97
      93AC11790CA959DAD7B38D9A62242AE8A20868C5569F4C9DDA65FA679F75EDD7
      B76FDB986AD56A40BDB08398AFE5E21FF6578FC19285D2DFDF3F10C76423C77C
      F8A1D7C041838CFD070C306047D01DEEB0A8C8C87034050D7BE0AB240EAB1291
      52A9E4D1CF9CB09B17806D415D54952A3CD409876FD471E111117C9BD6AD952D
      5AB6F4414335A3E5572AAFF27261B4281BC1D5C5450FB5AAC19C1587A0958EED
      56160645462627673204D4702E8D16EB17E8E2E248E98C03DC8E8DF107952442
      17605F3893621D04E7B765C412C30FE7B489BB9458AC9DB12FB3C6EA1BA68E59
      C6D8B624D53626516C8D87F4051AFD5368C32E70EAAD9DA5213E2EC1D8A38EC6
      5A113B6DC873BC409BCE677A7C7B32A3764EA145292D4E4D48BA7E626F6AA9FA
      84CDA791155FA3348188FDACC36266D8055EEE1224E0C7D00B3B9F1DFFFEFBEF
      4198BF0E5B7F36754683C5B70ED49E7FFDCFCFD65F9D3F79DAEC5E28B7202C95
      552A16465109F8948A1253AAC8E9F1737B2CDA767D56FD85370F345C7473DF2F
      87EF4E5EB5E6DB7E8D1A36AC8DAC57AEB561172A15BC8310D5CFE95D3C359E1D
      E2BBF98D3D72B0D6DCAB07B79E4E98B26DEBB6011D3B746C0A85E78CEAAF54B6
      34EC02EB4A8EF4952E5CF7EDC15CBF3FBF721EBA77CF8C9F4F4DBF78F9EAD029
      932777C5D67C302466A3BB4AF1418471C3B00B8170340602005FA0230DF97B19
      0D3AF9A54FDB299DBEF87269BF8D1B370EC03A6E437C1F4A8FB2F9A782CAE886
      5D70107540E26F802DC076E05B1A7AFE63EAB3DF07A70CBC060E18D069F2A449
      3DD1E7C2D0D75E978691BD22D2207114D00A680DD400D42C1CC31A43C3060D9A
      E1EB399D51C0EEE81695CB864562701031C75B809EAEF2F1F6AEEDEFE7D7422A
      95EAD1405FCF164BFBB6C26661AF20C330A71A501FF7AFAB0A783A0C1AB3C37E
      C785B58F008447026F2E1F04FCB74681882AE0CDD942003302341C477B6631F7
      4BAC11255C6C37A5CCC94BE59C715D8B9AC35B30262D0A5A65AB3F42616D3E5B
      478D3E75A12A03BDA8C630032007FEB5E3072A24604739341884FA4657ADEA05
      25A7C6061D773FE145C9E1DC2045BAA1A633C9B546CCFF9DB08050822439C011
      1E974A06CD5F8565AFA0DEBD7A457E38668C67FF01038D355AF7F129AC3AA4A3
      D63FAEB7939AEAC605190D5D1A856ABA360A738AF0750E00019B59C27A65A021
      058C0F5D70D0D4AF5DFBF65A2838FE69A9C9B4E186105720750E691813C8776D
      10E212E5A3F772D62A3917AC75B14F402039A6A2B8961987867475D5E3D88706
      DB15DCCD14BBEE8733D9D119459C7B6C880717ECE7A5D769D4325EB4C945BB8D
      2F4BC76C1EADB8BC5A5983436B966AB53AFE5AB24DBFE258464C4ABEE8111BE6
      CE3BE9F1720E472460031B6FDA3112DC318A97A874A3D168A4E8E1E14D7B8D6F
      90A0A8563BB354EE1317EAC119B578FD45C41299CD2AE6E4E6D932F34BB20495
      53962095D92EDC7D41E7EFBEC8E520116BB106F0A6F10FF6A2C6E4F55DBD827B
      BAF90404D58F0E90B818D408621F9C14D9229D3DE9454ADED5C79937AD722701
      EFCE1424A5E74993330B648CA81562B2F99A85EC364ECEDB0CF88287B945753F
      D7201F378C1FD8EC9C23BC6C229EBF7A37FB724276415A91F43149F0F9060EEF
      0D721C536A570434A6FE26ADD2E067D66B9DF52A5D74B087101BE1C3E3BB0BD8
      E6B7C9A41229E5E417149DBA9620FC7D3B3129B7A0E4882851EEC14FCC21E285
      8B10E20E709D90B5B9D51625AE19F587E8C0C83DF635237695AE19BC29F39711
      EB13F78FFAEDE9AEFAF32EED310FDEFEA3A6F59C66B25A23589741DACA86957E
      251FF63BC661A58393288AEE3C4E4E3C72F9A13D373335CB90B477B3F4E6FA0B
      FCD51F596BAE94E6E5CDB00B952462920DDF6D5BD568D9F3595CDF83CBBC876C
      F9B1E798D943F01314F2560DE9607A0351ABB51933A5232ECEA13E076647B5FF
      68203E19D649AFD79BD10F5FCB8183C3711976E1D3B0398FD7F4D858E840BD65
      C95FC9465D5E4843FF1EC7B55B19E2E3EBDFD0DFD7B72934A48E3558479A375E
      865D884581CF065602AB8079400FFC003821BE1CDA3116A803376B6FB0DE6C38
      2462EDC08F5E2EA0C0A20C5C1ED1DAD812D8EC67D9FF95FD007629F0BF364C92
      FFA821B937D1E3671B6F93C914502BDA71E33EDE181C121287510726AA9851DA
      7128E25F89B09D98C0236B956A023583979F945A5F3FBF60BC3E545DADD13937
      6EDA42DDB8594B55BD86CD94751BB752D76DD24A5DAF696B75FD666D1CB6CE68
      523212267AF93398860C0F0F0FC6E0331CE7B53C940A85947DBC442197924A21
      25A6C854CC0D2865124228BAA88D586132320711B2C234A41B5EE0F26BDBAE1D
      D3DBDCBD7B77B1409EEBF88E3666DCF4F0FE3DB2DAAC50D72F4BC5C3C39D9061
      079183845DF0CB2AC12CC8000DA9C2DC0C2A851DCCE07082AE948AF1AA99C552
      8AEFD9E662B9FE25115B902C717626022793887138C01A1C1BB6006CBB021D9C
      2776F0A8081FC24D4F4F252D3EAD9298F48C6C561BCEA740D32195938B19570E
      87096B0C93D3C5B516DCB16D0C5E833D707CD6D205CB1712D41E77F3F69DBA21
      C1214ED89D20BD4E4F1CCEB06BB56A321AF4B8D7929B9B0B3D79FA348B35482D
      1A5F1E2362658491BD37F66163F1C1443DA60E024E834FC05C3684BD86862133
      E11D5264CDC6A263508BAF360505D1C913271E33222988CA5B2CB2A58E8C8C0C
      EBD5B367287E9254274E9C98846968C8FDFBF709AF7E105E374399591D248C0D
      EFDBD0B5AB571FF31549580036BB8B1EE16FF79E3D4FCF9C3E8D6F95DB44484A
      5E5EDEA43718C9D3D3833CF13E9B9FAF2FF9F8C04FCF96DCF0EBC21257047684
      4528372B08ED689C267CB1B4595818FBB60E915E6FA0ACAC74C22F12D60704F6
      D3444E980D24243CC946C555A4F9C78DF9863A243838AA63A74E8B506E4120C7
      6B90820536DEF87C998CD51BEE09AF8826BDF4F9277DB90B834E01DF2FF4E9D2
      A54B1C7E81B93D7FFC7159A9543EC1E744AD2C71794438D0FE18275C6F30F81D
      E240E4D4AC69D3762D5AB4680B0DE90AE556DE0BDE90E4ED5E48A8F1F3F1690C
      34825BFBF698FF398469C85AD0903511F58D3F43F0FFAF0CEB426188190A940F
      36E0FE1F1B56194CCD300DF9CEC43CB6E35F8E125E45DBFFF9E7C2B68103DDF6
      B66BD76867CB96C3B6B46FFFC92F6DDA4C5EDBA8D190251111B5261A0CC671D2
      D7A7EDAC16D8531D34EB172C50864546C6C4B6683138AC53A7F1517DFA0C891D
      38F0FD3A030776A9D3ABD7D0BA5DBA8C6BDDB265EF66356B46C68787B31F0D47
      3A76615F836536FDD2B9B3B68E46D3D248F4A1DAC5A58B292222D42D2A4AE11A
      1252EA1E1262F5AB5A55155CB366D5F066CD7AD768D366CCC0BE7DEBDD5AB346
      EE488C0B938866393949C224923A3ABBBD97BDA0205A2C2E56D98A8A128B39EE
      64B142718001EF1A9F84A67BC426BB72A532C6A4D1F4D6E7E757DDD0AE1DAB10
      921417167241987F1ADDDC5AE0ED5E1F106033C2C6152427E7E3DC7092292020
      1D3B9B642F2DE57378FE49E6FDFB41BA8C8C40BDDD1ECCE7E7B730592CCF20D0
      7389F4C50B99BFA76775854A155E9A9F5F58505A5AA87576F65048244139376E
      5873A5D28B4ED5AB67E1BD6C9BD46CCE782893E5A9718CA58A545AD39A9B5B5D
      53547471B18B4BAAC4989EAED3299591F8A2AD3A2F39F9E299BCBC47713131AD
      DD349A10BD2044165FB9C2156934E7E54141ECA48B3DB1B030E7E1FDFB0F03DD
      DD9D51C061128E8B2495EA2CAD6DD5AACAE5B66D973EEAD3E7D0959E3D87B489
      8AAAFAC39429939EAD5E7D2863E5CABBE9CB975FCFDEB1637DF6A347532E9E3F
      3F04EB931D63BCBD832F346F3EF141EBD6C7CFD6A9B37881BFBF99D6366C58E3
      5CFDFAABAFB76EBDEFAF264D3AFBA077B66ED4A8EED659B3A6257CF5D5DEE479
      F36EA72C5972FDD94F3FFDFEFDB4691FE223AD550D6AB5EE485CDCC0EBF5EA1D
      3B1113B362B6B7B717ADAC5327FA4854D4F2B37171070E56ABD66BA45AADC44F
      9253A7366D9AEDFAFCF3A90FE2E3F7DE1F36ECEEB53E7D6FEE68DC78D91467E7
      B02E12897A5F78F898B3D1D1C70E85862E996D36BBD392FAF583FEF0F38BFF2B
      2CECF0DEF0F04F977A7A3A436F4BA19F033E1E3DBACBB955AB665F1C30E0CF93
      4D9BDE3C5CA3C65F1BBCBD3F5CA1D355DF1310F0E55FC1C14777FBFA4E5DE0EC
      6CA4C55DBB3A6D7475EDB9D3D373DFDEA0A04D076BD468FD478B162A68456DDD
      3A75EAFCBC6EDDE04DE3C6CDD858ADDACF0723228EED0A08D8BFDEC363DD364F
      CF033B3D3C766E7675EDBCC209DFEE8B7176E6BED4EB437E3599BED9E2E979FC
      70FDFAEB6E4C9EDC64F5800126348B60ACB4F7183D6448F70FBDBDDB6EF6F2FA
      1A714E6D72733BBBD1D9F904D2CC5B6B34FAA31DBD3413B55AC51ABDBEDBF77A
      FDAEF5BEBE27FE6AD7EEFBB3DDBA0D5C101CDCA2578D1A433EC0D2FD28A5B2ED
      2A8361F18F46E3C91FF4FA73DF69B51B976934CD3E56ABCBBB89836DA9C9E4BC
      52A71BF08D5ABD69ADC974EC270F8F436B9C9C76CC35180ECCD2E9F67DA152ED
      58AE541E59A1541E07D62F57A9DE8F57AB5F5BFB7790C51B0CFAC56A75F32FE4
      F2855FC8643B17CB6427E6C864D7664BA557164AA527E74BA5BB66C964F3E72A
      954D676B3495D48F83A0E265AC46239BAC50042041FBCFA4D2B11F4A24CB470A
      C2F2D18230ADBF20F4EC2C08E15DE472A6EC2A267BB3BB2E3EE9D0552693B611
      045D1CC779C7709C6F0D8E73092252E317C0A131DE9CF295EF945D22C79C2E53
      8A058FE9E224D327E23876EF40C3A5EF24A814B8A82327B6C5467DB059DE70E9
      FB347D681DFA989AAD0B6544EDFB8CDDD26E8D78563349ACC5EE3B7F2B564A5B
      E9267A91A8DA3B82B35B6CA4C227FEB4BD6B904FB7DE83B6F6FE593C32A436BD
      D7AB06D52A7C7C740623DA31047335E6780561E95151D27E50BCE8DA3C3EBE43
      24ADF46C155FB59E3FF5097426B7F402B2477B716E41CEE48771686906361977
      1D387E71EBEF9B8E6CFEF3962F8574CFA667471D3FD7928F1B73561A5DECBD71
      B07C46CB50E29A8550706E31D1C30C12B5721264025959C1A5E793440D753F7E
      64BF8E871F52E3AED3B69AB7FCBEF96308B4BCFD1A9B2050AFCB92A12DBD2CE7
      12A8367B324E4D97DE4F232EC24C9C468EB78F4A4970D5128FC78A0516E2BD8C
      24F3339136D495B8DD89FE4129C3FFDA3074E8B002F63090C2345AD6EAF31963
      F6804030E0732EA9F9C40FDD4862466E4942A350B9764C037246D9894518DB01
      B6EFCE92E4D2893D8F685787D6487D5F42839E767AAFAEF7A07A01D428D69BF8
      943C12FD9D88E61E247BFA3721DF53F6FDAF6BEFB488BF5F91AEEA5885EAB389
      2E120A97FE3E9D497BBB2D80FB3E173918734E9B65E8FBD1D4B17315D2C3D3B1
      9C5A8831AE971E3B8E12D57EF8DD58D849761359B98FFFE7C28E9D8B28371ADE
      B5AE4E3232A327450E0E156F7E27727CD7A3BD27F66DB4061F1564B30332225B
      F841C6F08FF85FCED3835ABEF403EE03740A1A8033FEBC4242226A93C307084B
      B75F23D9CE6FC62FA3CB5F8D7594916468E2229BA0EEE4ED66F05CD195536715
      A12FA086900D2E2DDF31B0779C5F67B596594874E03651AB702210D1D105F5BF
      A2E7A7C64BA8D132C1BACE7B0A450CD81838F2FB831C476A258E1EB11AC253EC
      CC8D4C9204634F64594CCD236EEFE9FBF97B775EBA4185299920F903D947CD8C
      1C2369B642E434ED7EE839B836E79C857FDF83EAE7593994DA8807247925243C
      487364C9C6D65AF8D2EC44DAD7A33B1D1BDB9124CAC38C88739B6693A7CC134A
      F8C14F874FECE8BDFACF7B44971E6451AFBA46AA1FF0F23323DF9F250EB54953
      9BE3933B52B24DD945F6B425C61654927D0C241C8066E6518F39883E3829A5F7
      FFFA9E3AEC7A2A891E3E9562C6C54FDA29DA27EF14ADD4FD6C2EF9B4F88E1F9E
      7E62C06FC868E7033692E9DE03013395FA2BF360609E21CC414D569A43E78956
      FD14240CEFB75F2C2D52C25F43D5272E2543F03464C9D15CE0F70EE3DB9AA3D8
      A93C355DBD806A7E768C64FAAEEF88FD9620A52B93EACD81023BA481A9196B11
      12FC37A537C77AE9DBE76C350751CF53553E1E71AD415AE7636109717F18BEF6
      DEC8FBBC8CF1E6AB2351C5204FEC197DF2A40D6F767571F3F7F274AE1711E7DE
      36BCE5E09AE62AC7CC43144D58DC98EFD9C90FE6FA2F30EA568339F3D2DEB77F
      99D7B3F8CBB4C1B6667B0245A776F2892CA947BFCA1FA9627EE5187AB58E43BA
      21D76A8EF9E84ED3E3236ED64F98F5A293B828B38B35FE698FD2E8CD669B5333
      C50896C0A50DDED0618E37A1EFB918B6524CAD77F9CF9E91D8515C92DE57FC2C
      B1AD7D415A67FBE4075DAC6E3FA8EC866E92296F4ACBFC1C8999E3DAB7C9689D
      4481439CCEE4E5E675552A0467B420BB28E2A54E5EC1F9985C44FF38635D9701
      AA56C2704EA6182DB994B7C6C2923AC039AEAF2EED8F844BC552B2A63D2F6C5B
      A59AEB76B3DEC4E3ED204E8ACD70834A27EA1446FED0956BB475F1A9B30BA74F
      6F3131764EFEABA4E55625C263E74E7A74DB5FE7CEB8FB0D91ADE6D6F8C4F6F6
      E949ED6C75F6F85BA82EFD34B5F734CF5729CB7354EE4000F7F9A2CF9B4E9EFC
      E9748DA05A2ACF74F6B8C21F235F6717FE595E3A5DBA97CEF511C68BED4DED0B
      03A2FCD37CBD7D9F9CFEFB7431D2398C8308EFAB85E25CF691E62D9A4F4A4878
      14F3F7E5B3AA8C5BB97CBA35877BACBFC7E53D3572F1BE5F53F380667C60F540
      1FB956DE45A955F6C58242FE25FC6DDBB64D9030BA8888082FCCF5A3B0045D8A
      D7ED28E95992902E3EE18293AB53DEB960AAA9A94D8281A72C5D263E5FACB215
      17165B535352BDB0E3DE1BE9D76115DEEE289B59B36635C00ACD31A3D168C5D8
      51C01E08FFE0C17DF1DAD5EB64C9B5707A379D1810148857842474E1E205F64F
      70AC5860C14ACEBDC3F81C542B90393A20E13B3472CC9C87633746860302B6FC
      FC7C8E6D1786848690D1CD88C5032FEED1C3471CDE4824CC1E44994C2660BF5F
      C07F64BA8AC3049B4F9F3ECD0978AB909B3A756A064E189EBA7AF56A38D63FBC
      D9A238F6F96D32A90C2F50F1DC86F51B084B1AF87A114F0216B45353538BB1FF
      B60D044B20CD33FC1B1FB6F203E72BD3B97367369C1B8BB9FDA4060D1A18406A
      07B9B87EFD7AB6E5CAFE851CCBD605BCDEB812E7237E47B202A0B2419E1D7D8D
      F9A2F0AAC4C7C76FC34955F1D6AD5B224ED88BFDFBF77F161212B200E1414099
      719471D9CDBFED72C2BA75EBF6C51B99D7EAD5ABB713919A02EF4C88F0CA66F3
      E6CD3C7C1860911C978AC3E0327F78FF07933333C2D150D37A9BA3D306989F9F
      EDE8FC3D4B223E6DF15689DEC8CE4776B6A7CD8E93CAA3FDBF32F56A66F6F250
      775CDEC12396F339246E68667A23D96B44D93F7414B4DDE68BB2829C65AA20EF
      26BC019F4FAB11E6149E5D38FFE9B2704995A65EFF99A86056146F18B8CB96B7
      A0CA287574E408819797DA1333A49C8FB7B5664DFF162BA7DFEE1F35ED9AFD5E
      0797D7C8CA2512BFA9CDA967DEB05BBF8E6EA3F6F05B21C8B4563BFEAF165760
      E5EC79259C362250ECDFD07BD6BC20A957C8EE34871264E5568672E6C4793578
      EF6917ED4FE2C3E22572FD441793592D91C844C727D159328DD2567A3D49F2FB
      8EBFBFED73BB60E8ED79515CF8B41B2CC4C1552E112341A314B8EA31B3ADA1DD
      639EDEB9B78F8AB14C5864B751898DB8420B2F0BF2B2358F0D1B30C643D59A91
      1CACFF4F16CB881C92A1E3DA7C3A6EB06B922E66288CEE2C6B2496222833930A
      8E9CE5D21F3D15F5323B5F4B523C5CDCF1BD50B77D74597A9238E42262227238
      68D124B26ECB3E6754EA4E5117979A84943B76BB4485C19E88FFC868179F44F5
      1712849B85BFFA2A73AA36EB6BACDA7950FAABF42F899886843EDA8263535157
      2F5DA4AB771FD0E302BBD8C8CEF1A1B66451A62E248949CEB9C435123DDAF750
      55E9F7BCFFD5BD5B9B8E1A3664CE376BBF5DC734A443B48A1A322024B454AF55
      D96F156AE9375943F177B70674A6D497AE460E1139BCAB2D29C8C1A1BDB4D2F4
      8C4CEF948C2CA621B15EEBF6760DF9101AF2EAD56B945150CCB97A7889E121C1
      C45B0AE9E2C58B4C95FCF71AD2C3D393C3AB2EE4E16C225FB30BF7E0CE4DEEC8
      D163FF3B0D8925436C23F1DCFA0D1BE9FE8307FFBF862C6B59B0FF571AF2D0E1
      23E80344F367CFE67FF9E1FB114B977DFDC7E2A55F5D99BB68D1D9E99F4C5D37
      67E68C5870137ED21DF198FB4DE05B346D221EDCBBD7D5DDCBEB70B15AB7CCAB
      7AE31AFE75BBE83C6B7775D7D768DECB22551F1DD2AFF7474B962C11BB74E9FA
      5632FE976FD7495FA4A6FD2AF7F6ADD3A879EBC716D333DB99DC1F74C7B2BE93
      DA5D5C1F6BEA764E730DABFE7593DA357A6FDDBA05FFD86DDA1BC978855CD6B5
      40A16E1259B56642B04EEDF424F79681824F39DDB36F34E7A7E5CAD572B77C5D
      F55669C155E3A6B76B58D76DEEDC79AC83BF963BFE69466667B54B60EEC594E3
      EA2FFE5EAE7898734F70D5388B0A99CA7E3563ABE1AFFB8B5C2D3279814B6875
      DFF3972F36610C182370CCAE08A171CB961F69BC6A684E676E5196061ED3FBF8
      4905ABA5847377D672E69014F5F9C4BD6A776BC7020D27C89FDD3C7333392DF3
      047B49A52289C33D67D1A23F971FB8FFA2F7D6692F1A6D0FB736DD1369FDEC49
      7B7BDB3FC36DB57EF3B7067EE16399FEC7A567F16BB715057838CF74247AC385
      9B3665D297CA6A2D47599C5D1F956614CA2EE7AC37C6B5BD63DA7BFCAECDFDD9
      C26751FEFE82B3D95BF274DFF78A65F3670C01C716E035C37385D9DF173FBE62
      259B42A575092D91081ADB816377EC99CF457BA8AFAFE0EE5BD55A9AFC5873EB
      E481D490F0F022FF5A0D1571D5AB3BF458453647A10DEDD7EB637D50CC57F298
      B62F8A7849717E76BA548B7FC2EA6CC2D72B531E6B6D770E6BB03353929B53C8
      5FD9B571D6B77F1E9FF7D5EA9FF97123FAD9CBC8789C79E0D6FDBC7E29979130
      3C7DEF3716D99D13AEE6926CAD3AED9932F9CF9FE57F2E9FFCDCC9682868DAA1
      ABB48AF689F453E7BB9F1EF6A62E8CE4FC8AE0D7242B230E88F0F3185DBB7A95
      655141BE5F2AF042AC87BB77BD798B97661EF8F65331FF58778BB8B3AB983FC8
      AFF8742CDF8D25BAE6F972F8C8DC0EE037AD123BEE1DD94E0CF3E03EEDD868D2
      C5F6AEB9E2A68EA2F5EAC8127177575BC1B0A09253F80F198EC46FB87078D5C5
      41C0C2305C2E771F08A76619DDDCD3C5F5ED45EBB5511671CFFB62EAA830EB37
      B3FA2EFE6E429CFEFF69EF3BE0ABA8B6F5D7CC9C9ADE7B420284125A20489120
      09BD3721142108812055112F88A2820801690A02062902D215E589341123C815
      1490225D5A08105A800029A7CDFBD69C9C437AE17AFFEFBEF7F7FCF6B7EBDADF
      D97BCDCC3A736697B10B72C59270BDAD8B18B83BD3B2C395A2A35AB87FE5DDAB
      B18F1C1168B82877512DDDF247F6ECA4C96D4BAA5B24FFEE7B0D95AE9FEC5CEF
      F9B4AE6ED74F6F5B28A7FC7642EEDF37EE0C7E5F838B54282D2369CABBCA9DDC
      3B8943064C787372669F3E71BF878787CFC64FBD63E17A227E75DD0B67E64F3B
      467710E3FAF6F5425E0B209488F8790051010BB962F92B1F2F58F8DD9C8F17FE
      3EE3C3D9BF3C9385F48385CC75725FE055EFB9869E0D5B60FD58CB6097062DFB
      1B3415B0903760211D42C29A3EDFA2F995C86A615995BD9C4D7887C8239D5FE8
      25A7C65DEFF896CB42EA34BDB2B58EB195A39A5CF1F0F1F2C682785DA6A3BBC9
      E8EE4DD90ECEAA2C17CF27CE511DCAB690D7EF64741702AA643A1F4E71542F99
      A171583DCBD1FBF399EE416B3FF40C5D3BD7D37745928F2C6A9E78D680853C7A
      2416CAA5E22CA4307BF69C7D624C9FB00E3BE6E86B5ED8E9495A1CC9CC07840D
      6A807BF4C739337DB9E0B7EB42F64DB7FF9AF57AD2D1337F4E67B222983973D6
      9E69FB2EDC4C9932F1E69D36954DA91D6B182FC7869ACE37F1379EA9ED62FA36
      D4C330E39B6337DE59F6557615FF522CE46458486DFDB62325F7808BC2DD1B0E
      1691640C9E0B064432F0D7D7D341A3F1A85459BCB273B97EC1F4522C24653F58
      61B878CC942D681D1E7885E53E760FB16479049B733C022D3E95424497F0DAA6
      5C18B7D3FB2B602175911D6E1A344ED92AB2487A8D5AD2AA3592F1E63967CBD9
      1F9D30D25E7E0B7967C71203FDB1DB574E3DEE967DEE17E7B45DCB6021DFBCE1
      F1AC16B2E9DF16F27F97852C720915CE600BE9ECE3CB26B739CA42C9662111F9
      CF7282AD39ED3FD82E73FC8BB07E1C94890197D72B323B27775438544A0A9EC5
      8CF71B558E24A72AB8E4912ECBB95B6AD1BD4BC7EC620A1BA75ABFF3A5EC5925
      92E677AFC2C93231EE9B8B74EFE231DA33CD7ACB6C276A39699DEC89167DD231
      BD4C121618BDDD4F69D1DEA4FE0A87E27141CC3F56C91E61F5E8A33EF5385926
      5EDB789C322E1FA794D983140EC5E35A2F8C5F217B86D5A525DD1F73B24C8CF8
      C689EE5D3E41FBE60E5138148F6B351FB70C2DAA43F3FB3DC7C932316EFD6F68
      D149DA3F7FA8C2A1785C2BFAD56410D5E568B9918116FDFCF1708543F1B866B3
      314B405487A3F4F876AA1216F6028282EC5999065169D1818523140EC5E3D2E7
      477DA2103DB97B9DBE78BB0B67158B01D3BF55F299F4F29913F4CF45A3150EFB
      0989C7CD58866EBDDB355A2C342AF9286565DCA495933A29158BF3B88E2DDF4E
      8495ED982E6525CA05914D88E336E12221E46C794AB338D164F83CD92DB826E5
      3EBE4FF3DF684BE3E6ECE66C25AE44F23C5B3E77EDE2C9C37430F9758543F120
      E3D53871EE1884EF0234E5F592BB3365DE772C62C7A1A5E3BD91B86B2742821D
      AF6BE0B022C880F05DE0AF71B6161560C30D6930325A034D8110801D9F5CBF20
      B267D9B265D7101675A8A8100E1D9A20260C1DFA16700DB8393461C886E18307
      CC60709CF3002E7B8B6599C95697E30A20E09C3074D88E6109836F250E891F88
      CADA4189A3F96996028E731E97B10CCB721DA5323C018C3C2AAC43FC4BB539CB
      F3A13EA4C3258F98DC3FBDDA0D5391319E04A926CA70736F3E6322F5EAAA7777
      7D56392345EB9A9DBAC32839DC43195B3683AD4B6F8B06D3D03FFDDA344C77A9
      EB91EE5C679D48D410BBC051AE091C90D64A443C2E69213AECF7E8647FBFCC13
      1955D3BF3F6CD1A89661946CA16AE8D0A1A11891497CE2E79A90EADE28F3AE63
      F856B52C37CC31516EA89BA0EE17418207DABBE90CC9BFDD948D206C78C3A5CE
      4A83A48BF5A75F87E9EE652DC748EA175283060D7A6374B7DAEA854BDE32775F
      300493D347651985DCEA9E8226A53F89CD8388425D31445983C4945441BAF688
      0C3A410E7BA4F34AFD6D72C72FEB4745F5138832D0036A2E594C3FA0F5A43199
      FA623360FC2C092A6C11217C7E92E4E66B4948D84E823776E08AF022C1641154
      98E32F6B21CB75F2EAC6309197209B2E73A64C5403A34E02B6B515AE3C20F9B5
      9D244407137D184B841175E1EA4392258104684D6059AE83BA1711FAA9E01570
      E82F61CC9FB428D14137131A11F93961C87923D1C11B189A5683062C052A21C1
      2DBA2B0BAA30C4F9ABCEE208B0980C326C2F47F23B3F1325EE243A90864DEFF0
      2E4508C9D0A92C109DE53AA85B05613A13ED378BAA56489041A5DA40167E0988
      CC2F11E4E178A15508F414A4BCF502D3FBF1285696F15CDA2228B2A894573785
      8F5A068AC744B489FEE3BE5475EB63AD574B9CA161E82276DF13C4910D487044
      77D69E26592D62F48D486B92C403DE4F2E8CEB1025B45467195F82ECBB1206A3
      1E4445D577D43ECC9E6256E9564197BB1F697D71B10A2122125F9C2261FD6912
      30CE8F94A042B70EFB3F3A191FF8F0A8C52FFDF4360C287D0675FC54EA2522C9
      B844C4BC4BC4623E63164ABE44D04BAB4B183AF45FBA6815165C263808446C1A
      4058D88C4CC7553FBD2C33A210286CF93C10E3C2A096C86A0E8401EC2EC3DB0F
      EC5DB66C591AC2024E5520F53421202A01326002D8719CF3B88CD34581162885
      2574ED7FA785CCB175E95FB290CB962D9F2E413FB090C227D93ECE434FBBF738
      7FCBB9E6D72A929BC0C4E2152C82EAD7783CD68A245A7E82081760AE5AA09087
      3ADF064649BFD44775E2842ACBF47E545483AF44A8BE0D4EFBF4B51F2ED873C7
      297C9046B634CB32522EF21557C58D644F4C935112F0B26082D5164B3396E53A
      5C17D96D98A8580B8942C5F0DB2C24F683E4C38F5655D04262CABD86C93EC7FD
      045B488E334AB290DC222EB74380FA61D4882D2467F22F086C3547796EBF864D
      0A7E756079942CBBC744A55A48BB24229A522C245F226C2187424EB1906A93A5
      9520E0C7421644A52217006C9BD00CE809165296B5F92D241E5E249769213B56
      C10CB2FB446558C8C97F8985C409B9EDAFB29039E8BDD525FC9FB390D67EFD05
      BEC41C6DDFFC6553FD0E2326D56E933831A2F5B0D1B55A0F1F5BBBD5B0B1555B
      0C4B08681CAF098CE87EFCDA910D3693CB558A80CF6C727692AA6D7E2BCA79E1
      E8FAB46874034C108D94E68EA8277DFA5AA44BDB96D55FF30F09F924FA956F1D
      8AD4CE97A1108982E09091254B1730B27CF19E85CEDD36D3B1342339E1828BAE
      E1462F4407B774F4729D90AF5E91A84284F39EEF3C48AFE2FB4481B4B8C4751A
      916E3FC8A61A78043CB8892B05FB390E28523B5F8642C469BEEAD5D0980A57BF
      8A89B0B7EC8FE79E846DFAED6ED88A7DB7C2EA54F3AD326AE5F127A3971FCB8A
      5F74F87CEC9BFBDF6D96B855CF75190A115A84DB1E2CB011454CC513498D497E
      DC2AECCC4D2370CBD9B7A93FF503DE79B196C3C49E11FA5903EB86437753A1BB
      4DD01D6EC388AC4460B230AD4227631D11BA881699CD32DDCF92A914DD7586EE
      92B86A1E1136E8450A7595D6585B24C16A60890BBA5A9AEE1C31A50955AD2DC2
      FD18F6E83571BA088AD3DDC50C81F65FCAA587D932A9F9DB514B6991204B52AE
      A9E88303F458E92C76E628A03B3DEEDA9D71E785E9314AABC1636D91D190FB44
      2D3A93AB0EE78EC6486E3A13793AA285602A4E773A35F66CC30458585262FBCD
      443873F0C367966FC44F3FEC6D30912B6E7EB3712B8732999CB1B1C7E0170271
      7787A382E9A5FCA380338330710B4703CFC4501B233A9025429468C7CCA6ED94
      5421AFC7FB878D668B09327085CA38892DA3B188144703094547088B75D09DAA
      38DDD984B90C9B182B4CC57F559E6416DE4402DD79B0EE5817B826F1DB662DE4
      DA92E8827F092603E7709AC36211FBFACF3F61D55338CE263FE8EE894D772CCC
      F7CAE8DA6357478D7ECBFB8DA05BCEFD0B50528BBCC17D0728D3E1A83942C880
      EB1D4121872552B53EFB6CF93F060D7AB95D7CFCC076980DD51E61FBBE7DFBB5
      C7260BEDBB77EFD9BE53A7AE6D3B75EAD6017BDAF9BBB838FF5E2C11561ED69A
      3163F6A2F0F0EA4D0303839BFA070435F1F70F6AE2E717D4A452A53005A1A195
      9B8687576B82895F992FBF1CFF65A1B6E08AB0E674FAE18783598B162D330C1F
      3E3CF71F6F4C30BCFEFA78C3C4896F1AB0AEC0E0E2E26A0809A99475E8D01F86
      71E3DE5CC5558A6D110E73B5810312FA3F7A745F1511515D6A16DD4CAA59A3BA
      542FB2AEF4F9E72B25ACF7C3E52751DFBEF1EA3FFE3871E2E0C1035F173E2105
      66B788D8F51B6652ABD30BFEBEBE7270480805570AC16ABA60F2C3D290E0E060
      F2F0F05064ADA7042E21AE68C3C5BCC822B3A5AD7E45B294C3EB14311792BF4D
      C0B586350484F59198B9E243589D0869D8319C508858AF7E8E30AAE0ACE3B0BE
      5ADDB4F29D1B82CBEA9516155A25A9D53078125DBF7E1D6BB30DE4E5E54D5836
      812B5F21E22A243EC085FB032EE44A80BC7EBD4E9E3DDBC58035469AB050F979
      355E0EB874A9CCB7B822EC78FACD741A923004FFD605C25BE21422BE74984975
      8C68AA978BCBB8EFFCFDE9E2E4C9044553A5AA5535862B57047D6090249D3C29
      64BEFF3E797E301D93BD2DB467F76EAA5CB9323DB8FF90EBDBAF3DF10E9197D6
      DF5FEFDDB1A3DEB94D1BBD73BB767A9776ED247EB3209D3D2BE8B016D465FF3E
      7A3C750A615610D58C88A05A11B5B0DCD3192D622E993D12B6102DA9EEE13154
      5FB5AA206565918475A1AA9C1C5165360B82C120E3FD868206EDBF191848E2A6
      4DF4CFC387095B3ED0CF070E985AB6ECAAFAFAEB8D5FCC9B3773A00A3FE80ECE
      8F1EA974A74E911A4478C04230B2D8C15939A402CAE5CB6E6E746EE448AA8397
      03BA3A3B938FAF2F3939397353007E2801454347070E1A8D3E50B09B8495F778
      F860E94654275C105C416AB91912229D1C3B96829A47D3EDB454F206893FF489
      D99C58E1AAE6AD5640663D9BDCD0491F68CDB90E9E629E2472388CEE4611D53E
      ABD70B5FF6EE4DEE2D5B9A2BA1552E1EEE14101088D76A6ACCDBBEFD4EAC53A7
      A1FAABAF366E9A31E3FD41E028E2828FEAF5C7AF6AB5F20B92B40BA5E3809E40
      0F80C32E08DB00AF00EF036380EA4011270EF7F018F9A28BCB74948403A539A8
      90D425094055CA0BDAD55FE8747C3DF12F10DBF6FC601906972B10D81C94C458
      DEFCD4D4AB3EFC2DB46BD74EE53E2713AFB0C4BE50F6FA972E5D52CE627B46A1
      0866B252EFDE71F841254F8588CB75A3DA910E912BF4F423227A854AF9246DB6
      15BAD88938E761E2542C07AECC51056958F1ECE3C3BF034AB28097F94E4281B4
      5820F52F24FE32A2025D735DFA1EDDCDD72AD65966BE7469513BD1BD0FB7F066
      ADD4A85123BB3CE61B13DB677B46A1C8ED3367EC3976227B4EA1C8E325532977
      97FDE8D84BB5ED7A13C5C4D9D365EA884982BF3E4E21BBFE542A856C3B4D01CB
      F714212F93886B0B78D9ADA85253D0A6DFF0AE5A4752F906707601285DE33318
      F3B1F19BE507EB7AD62E8045DEC457656ABBAAC4AD5279FA90E9DE6D4A8B7B4E
      91B972E50AD93E4A8BD89863181E3658266C7B6507EE011439EE8EE4E2860500
      E9C464213BCE5BF3F1C3A944E0292D4258AA938D46CC00C8A5B4BE8D89494DB7
      6E14912F9348DBB6375DEB51CF5E31B5738412E77C11FF5D94043C8588AFFA9B
      376F12666412C731215ED96D80F7373677C063FF8E8394A5F8B86F22D61BEA29
      EE4E7ABA12B2A710F5B69A024E3F2B94FB7A27D40E25A25A409D0AA236E4B95E
      A80A16F23612FF92FBF758C89AF39FA055129ECA1D4198DF154E3F2D3B94608F
      17B490F1EDA3A863B8BD908EDF22AAE7FB349D3FD67761C12F50CEECFC02CF1A
      FFCB8894F3C8D68AD53B8FD0EA9DB654C5423BD1E144AC79C4BD51A30A5848BC
      3BCBFE6D76227B4EA1C8E83DCE686541C5B2081F9851811CB3A24C22EEAE55D4
      EA1F8E6AA8441AEE3C4CA39E1E7EAA90B2E5E4A22D5358E1292D2ACD42424671
      3692A8C4641286E3360CB915B6903612D4B593701CB3C7385050AEAE09C27045
      D8D6122551C82B93888F0E251E2ED012E6E0FC0A59C8F155CCF4C6684D3116F2
      0826C6DF614E058AB27BFF6D211565E4F38ABD87ACB9AFA655E49835288F7F28
      F2904DACA0851C955899EA526B5B219DA03D05D2F60244862F5D0AFFA92BF33C
      7A2A5A7AEC2F2352E5FF9E454B2F2159B0C94485D31029C6D9890E471DAEF03D
      64852CE424E748B2B6B46033F8C0C4D37A7B66993AB291244645919C2813875C
      DB96CF714699442C64ABCC7146E134E7293A2ACB4226437F2CCCB0C597E2F5BF
      F92DA442C4F7905869A8289BEF21B902E3FCF9F31CD0F0235683BF28641B8D4A
      EDACE4B157610BB9142B7BB8A2CADB8F03B2A595449E57A68EF8E8B02C571696
      0A7612CECF6F2195BFA29B376FD2DBEE21B11A0A43FE16E51E92B771E2FFF8FC
      4C160B28A9F03D241657D2C891A3B271A3D558D1D17F9485E4EE17019E535778
      7FF122CAC6F64DD885A75A3F2CF7FC2071D8B08F318EFB71A74E9DDEC7A1EE0C
      E53A17F9D6BC8C0244D830E999F61767AE0244BCBF78C74E9DFA56747FF12244
      F8DBF94CFB8B1721C233219767D95F9C8924F66CC043CAA8E6CD9B378562F538
      729CCD0F50382C757F7116B009721C8F04BDDABE366EDC87783E5B0BADC36354
      5129B7EC594BE2DEA7464C117EEA4D1192BE9BAA08DAF2700914BBBFF8C38BA7
      2DEE2BDF2CD07A5B1D84F34034BE001132B131996B68F3E8E87F8C1E3DBA4F8B
      9818376C6246DFEFFEFEFE4F2B3F5E37BFB6E35896C907FE39ED01A22B458870
      814AD824E8056CEE3605DD6C82D783C90B172C38B069F3E6F1B7129ECB06C959
      C0E68E81A43E278A107126FE497AD4AF5F7FECB469D346E31FA569DEBC79F37F
      F9E597595C264FEA541DA18D8C5BF30DD225BA22FB8BE7976432E0BDFC7925C6
      B188FBEFFDC54BD40ED11CEC1551B1FDC54B21FB1F2B2AF684CCDF9A15CB56BF
      816BB00E1592C400453A1E7D4C4E1836C8C8F2CACF11474A026C78AB98D8E6ED
      B53A8D5D8487367E4AD97FFEEE9D7B7C525A89BEF97ADB19BB4409112681E1B7
      9762974692B0ED2B46B2EC79DC226C0A5743D9F3D19E8B888C8153349D743A2D
      998C26E43C753CA662329B31DA27DB3355F8E5C078BF13E5E4E6D83339C2C236
      BDF06333CE535A80BA26AC95C1A082F2D3CEF90C61F3C62D72B56A55955B9A87
      260D5DCA7141B331182089CA1809467AC0C7D3FC48797466B16050010FA0F6DC
      709CCC0436482FF6EC3585876FB2720C7436D78D2828901C7DF12A30576792EC
      7042DC894417EC5FE486DF4817270A0BD4B4AC5A49D3D2CB4568792BC3545FEA
      D9A3D7144F4F0FBA974D7449F0C5D3631752618A910ACA54637418DB552BE346
      1C6735F08F82846D75B0C30369F100FE5A7A0EA5DFC91D24AC5DB3410EAF164E
      FB6EA8490A0B237757273219D07ED9822E62E227CE1FD617020CF9C80061CC04
      FBF36030FC3176FA3C7EE6414AC643537F61CDEAF5B257A51AF4D34357720F0B
      C21EA2984D6B30421F468C7058C73B6C7AE010B3013040AE218D5AA62BA91959
      57AE3E7ECDDD51BD4CF86CC55A39D7A71E5DD4FA93B3871B65A18B262C67E0EF
      E58A8581F7FF9256A322B3319B4E9EB8B1353BC73C38657C8DFBC2D48FD6C8F7
      039B902E24085FAFC369000AF4AC30812D0DB5915E4774F9E28D8CCB9732C61C
      9C54671D97098392B6C886B0280AACE445B906012F22E3ECE221405138F264C4
      3977EAE4F55F321FE6B43DF9C1738F595A887E6BF7AA0CC195785C8D3370BE71
      502CC0A3E463708B4FC6C567929A1C5232FE6F7B4AB70FE332524E9A67E92B14
      D610C79BCD080281A25252E8593E476262D00C0CB3123EDCA2A8BD7B892A4A06
      92232D5BD2D316810CC7934A3D8958A630D8A6E4E559BBC609583C3B51DDBA44
      0307127DFB2DD1BE7D5C4AF4C20B8437BC13AD594374E284358FEB586324E685
      050326E19C2E5DD8B7C216B7955973EDBEFDA8456DDF4EB473A7B5A0756BEBB7
      5B53057D6EE59E3DD6BCF6EDE948C78E8A8E9E126DDB46F4DD7756019BBF78B1
      2D660D31566B8DE4F99D3AD191CE9D1522555E164668CD652BDBA8FC84D9AB60
      735F7BFC698BBEFE9AE89B6FAC052FBE587AD7BEFACA2AD7BD3B1DE9D1A39416
      D914CBE22FBDC43ED1DAB5D690CB366CB0C6CB3C6A7C88599415CB21C316B795
      715E3E3CED1A46CE69F3E67C45E588F6EE4D47E2E24AE95A393814917C5D7B7A
      D4F874EFD54B292FB7C775F284ED44476C4ACD2BF8FF3038DDADC3B9FCDDB6A5
      A5FC9965C5B99205B34606D7AA33F1B3D74E37FBDEE9CAEA1B26593F203CFC75
      E53C2A8BC05E9E2C57DA91927049E6D928C8CCC8CD854F34A0CD2A2A3F51B21C
      815AA700FA616F7C968BF1C903B5C5981BD9E1BF8622AF46F98892652D84D300
      AF7D7BFA3F9033D36F7BF3621264B08BD8BAA37AF116924B0BE25324BDF6EC1D
      94C524881357E6F00E06657E6A177BAE6CA26499FF6ABE4CF8B46EB92A1001B5
      D8F563750E6D649C2EBB6BC9F25954E28A8368B8B01AF1625DE944C9723C6AAD
      02CE81A406C2129D586249B2EC8632264140FDD82B0D2513116DCAABF8395AF3
      7B5EBCC4A078A264792E6AB401EE02AF00653A7EF9E64848B1422F21AC058C02
      3A00EC6AA135A739521654105804147619C8680092AB08CBE5B86B6321B90238
      066C01B835011521419DBF5D3934209443A6A048B2AC464623A02710049C01CE
      558C2859F647A58D802B50C0E527D2A2C409E05302F738CAA62D5CCE7191FA2F
      0EA306BD56A21C6F087F7C88EE5DDD421AFD290A6BEC8DBC50CAFBF08FC00788
      8F000602C3010EFB221C462ADD60EABFE434F59A9B46D1C3BE421E97F115C175
      B82EB2ACCE014122C02E823DA01660FD0D1FB3E3531AB6F132DE44F429F2D8D5
      630FE02F744068778E880D0222812480BBD71C613405D69D46E37EF893067E96
      4A6E414C5C0DF9CB0076DC033D476CE0C488BC447C5EC841479A77EF3BB4E438
      351DF421327A03EF039F014C381821370281D539201806F80033014EBF4D1DDE
      9E44D32F9FA631DBF723EF396008C03273107A005C876511B53A6665764F2407
      02A47463C0D2549A76E10CB59DD01179BD019B0B25EB67380207C0EE24C46600
      0900EB21817ACCDC43F12BAE52BB895B9137008803B8058908F9CB5815331097
      80024E8F942FE047EF9E9C82D7579FA6A4ABFFA4A0C840E4F9027E800FE00D70
      C8E03A48E20F16141987D815224A03AA007D8168805D0FD8A54B1C290B2A08BC
      0D14760F91D10724371196CB31111FA5EA90AE09A4016C257F058911F1BFDD7F
      82065A6D6C15034CA1BC8F9817562800410C11FD08BC07284E50FC0A7831EB62
      620824EDDAB7A35D3B77514AFF148543F150502E17BD3A3A8640D2A97327658E
      0413FD1CFFB3C2A17828A4C6CB1B4F213C4238947028858AF9A03C8640D2BD47
      77BCB0211B51ECA38D16415EE1503CCE6DF06903994320F6E82B475328DF0765
      310492DE71BDC9B617DBEE5DBB914576593B51ED05B5E59706BC446BBF58AB08
      FC31F68F14C207F93104122EC39C7E44B1AFD4EEEF398CB5C970C24E547D6E75
      B955EB56CA8CCCCF577E4EF8C402EC7E7C79F0CB74EDDA358ED30F7B7EE030F6
      DCF8732994EF23D8E261496172CB562D9564D5AA556969F252259E383C5179ED
      0E27F6FEB09783D8CB932EA750A18F9D28E8FDA0184217B0732502A2885A114A
      78FAD46925FCF1C71F398C4D7B372D858AF9D889B8CCF71DDF18CA4786B8E26C
      24B7A6DD4AA1123E058858C67392670CE523B391DC4BBA974215FDB84F708F01
      E43CC4D0DF9F7269C02C48AA2C121DF3098F41BC0EC0AEC841E3CC52E149A4DD
      A37778775860D04D087606D8E1793C07E540B6461730D6227718A3522D0EAFDF
      20FE4F7777A7EF9FFF47188DDD1E4C1DDF0E10CBC1A188E8DC3D06F8B8B96DFC
      CD646C76DC2F40D660C22EB97B07A2B00789D2C472133DFFE69E5D0D961C4BCB
      EC3AE6616D91043727079881B9D341340C2F17E39B0944CB727C4B9C2CA7E03E
      E138307E42ED3AEDBB56AE22A35A1CC0AE9C0D4A96DF1792E51334F7EE72D4D2
      00EC98A83F1E164B48608D1DFC525DB25C03E593012C7E7388C36B31B325BC96
      5020E12D307D89FC9380592CC7F9310B82EC26E3C6A239228BCDB29C62C12616
      88CF06D6018300ABF32CEEFC5894DB0D3A61BD6CB54AF1E334EA86780BA001D0
      04E808C4887C7EDC12A4B8F392EAFBE8BAF5A68657AAE487023D083050A49A86
      38BB89EC01870126FD09E151E020B01D48116DE7C721B3A9F9796FAF273A1717
      33F9FAA08C6693C0016D4597CE2AB1523CF1C7C1F19F4EFC72F3E0A691F53787
      ABD57A172727899A8CEC823A4D81FB80AD558896ECC4966149ED3FFC33F6B4FB
      88A3AFF554454CDC72142D760D611D605AEBE304B4C65872F5A72592D065CA7A
      CACEEC8EAC017F86B7A97B6ECDABBE14D1369D5C03DAD31B3E7C619A5156A653
      C94449A477A90EC99A64C8BA8911CFDAF2976F4CA3F5A3F87E12D9E573567512
      F121EC8C2A9100EBE61AC29F815DC02AA0DCAE2124BB012D8002E707D27F9913
      C0C440F00C6EEA4FF78AADDCE54DEBFD50C5296559940F8F549555512A4E0003
      BB0EAED17D9ABF38625ACFE8A3197DBEBCD0A073C40B839B04D46CE5DFA64BFC
      FD737BD73FCEC5408FADEED978E2B549D6648F69C9C2F5B3C7A52A2DBA3CE7EC
      1E345A92B5CF5B0CAAD0079984E1530365DCBC4D2AB3E191A3563A2B183357FD
      FAC5CB6B3A778B335469F862C76BB732AD7367AC54445DDF5EEC2C6A74EDEFDD
      7F52CD952C590FB36437D1D1BF5E96DA2FEA518E26D09C9D83DD118C844D2972
      DC75E62561C1DEF71E644BAF981F5E5824C44E5DD8CB2DF4F9D60E8E6E591E8E
      74CA70EFC6C57A81CED746C646DEA1162DB2E8A79F5C029F4F682E7B450ED407
      54EF20AAF47A390B7B37CA26835AA59525512D7ACAE7460A0113564ED7B9D619
      A5767073D53B89B92AADF440EF20DEC25B73AE3B3FB9F5F3A6BE0DD7A0C5D79E
      1FF355E0DDDBB7502162AC852407392B9764A320BAEB354FBCE53FC70841A357
      CD911C6A0C94D58E5E162D2CB023DE01E9A82695293DFDE2BE5D0B46F5EDFBC9
      A29ED51EF5F9345DD48917137FBFAF9BF6384BF610B3B3F19E59597455AB727C
      0CE78748AEF5E27A8B16CF48015D174C260B6C2FA98C0F6FCA57FEF9CEC5E5E3
      17F78AF0CE19367B83F74317E3C48B2E95DEC99434EEA45509A496B092589275
      3A315BCCBCB859081AB676B99AC25F942D6A1759058BA155C96A27213BC0C332
      7BE3F8C88FFD05E941AD4133AB881EEE43EFA81C6B493AE74049EFE2270A1ABC
      1C52EBE8AE121F06E45CEA2D840CF96A9D2456EA66CEB923F136F092D647475A
      B5ECE02AE438E8851DB503E4659F0FADB7C3DF2F4A7C947E04FB8437F2147D02
      03258FC060C9D5AF8EABBB87C6CB74F353A1D2908DDB31572128FBD6AF6B8C46
      7388A34FAB812A7DA0AB455493B3A703D6F5EBD3433CE9B8B7ABFAB85E2BA465
      DEBFA64DBB74F6D2BAC9F15B7444EA1C2C489E7459CE15027A8D1A6DBC77E3EA
      C0BD5BB6CFD30B4EAE91038638FBB6ECA57208AC23E95C1DBCFDDDA5607F3579
      389B8DB225F346E69D8BDFE7DEBBBD7CCBACD1BF63481A27388E699E03715E2C
      2F700EAC131ED4E49581D53BCE4A6A3AE093059D4727CFEE392A69FC73ED7B47
      43C40DC030BE2C715811A82B22FCEF91750B6F2D369E70197718D082F2150831
      0BAC79CF21A287974FB1764A11FBB7782B89027E95D4AF0073199C2ECF17A1F9
      4FC550F1150F6C9BA56A19FBB1B649A357459DFAD5484F8FEB5FA8A4AD6D892A
      3D952C1AB313314986D9D8AA4A6CEC4B55BB7713028382E490D0500A3619E586
      4E4E5DFB0AC2077D893C7173A1562D3646239C0B6C04A600FD94736125BA83B3
      ED954ACD9BF70E1B354ABEF5C134E9D2FE9F454DCD9AA273C38666976BD7043F
      477D3DBCFCB0DADE6643C61875CEDD71225581ADF042DB6A02D18A51AF25A9BB
      0A8D1A760F1931C2903E6F9EFAF8C54B17AA88E2CA8769699DBCC68C69A67175
      35796DDA28C43969BA9FDEF43A7DD77CF8FAEB16F33A2C8E3E6BAC1C036D9075
      9B6630866B6BD59224070731E3D429E19C286E8AB458925E7EF224FED7DEBD7F
      CD69D254A56EDB5E727376A5E1CDDCA9096DA06FE6C71E34CE8AFD133719FC88
      68B1D2B561A2D44E1551B3A96BB56AF478DB3651CACE76EF2C08673E7DFCF8F7
      4F2E5CD8BEEBBDF983823BB776B8DCB59D9CD3B1BDA58E8F4FDD2D75AA56D17A
      3B1D483B79E1111A621D5D3F6536CE353D782062CAA2A4F770375517A88E5EAD
      FE249928DA756F55DFD19D9778ADC6D297EBFEDE82DE6C313B393951506666B5
      83EBB63B3009436911EEE51EF53E7F3E58E3EFDFC03D3656C84D4931FB6B357E
      590663E3EFAB367FF96E703DD713BE61AF4764EC73960421F49743FF3C6C3088
      09FBF71D38C52445B053A79F7775EA54F9EE94778D5745D174D1C75BFEA8550F
      593FE5502A0B4F4B7AB3EA5BEFBE96DCA94B9B869CEE11172B7058001B31096A
      2E8CD4D7CE4EF35267CE926FBF31DEF8DBC7B3E59423FBE4E90BE65C189CD82F
      B640854209D19676074955FC0C6B1F3D9E90FEF6E4799782FD548F3BB4233707
      57B9719D9A55B52AF598191FBEAD8FEBD74DD5B35767C156AFD8701F9E4B2E1C
      112FA917677FD4E9ED55F237DB36CA3FEEDF25CF9C33E5D731AF0D698C4A2250
      AC53E1F41E8992B3C0A51794E579F268F0B5DF11F212D53D336FB393E564D525
      8B3F4B4CBB7AEB18644A742A942C02F239FCCB902DF72D82543FE91F13AEA280
      8FAC390E2F27DCB4FE9B126F4C79106A0C84238106C025E033E0479CB1B908FF
      DF3BA1F0576215A29C97979D179618A4A65E75B015B28E6C7125E485A66BD6AC
      E695BE7A5E91C08B09940278BC8606EF42428CA85DBBF605BEA808514E4E0E0B
      E885E147940A44B69093ACF72774669C23270AA008114AF580E2368C8952429B
      B7FD02E55F146797E3723B517EDD60DE3F85A074C24E78851CE7D338EB12F5FC
      4576A267D00DABC0CE65277A06DD14DF3550DB0BCAD20D664843BCA0E3D5ABF6
      F3A6BCBA193060604116A454CFA29B0B17CEDB4F4470280E7B4B54F8BCB1AB40
      61C8F358D9F682D27413F3D1157DCDF93839DB7FC5556DEA205EA2B7BAA78039
      9D9C9D87C24B5BF3B295A0B485952CA0E888239973AA29CF63F1EA014E2AE06B
      8B75A824F279BC9CF1C8D27C19884A8E8E4E53E2F078196FC815468E1C8989A8
      9EC8B63A5E4E8D79C7FC3E765A74D0A86432094702A2BA50E0D229A47C8E2C9D
      2A60CB1879FEFC79CA53E39B589B8DF74210CFF0E529C2870E1DC2D43623F963
      EF8CC6CBB1D553F211A51E7B05D6442E6D28F0E21C9BE2B231A7588F3B3C9653
      C084B6F485D8755838D61037214F97782A42EC311187E541FC16592EAC70AE67
      3F6AF8DBC9698AEBDB8BADE570247A006CBF39CD7DF91AE9E4553D4858D5230A
      D182CED662E5770A248128DE1B1C129230EAD5490FA7CFFEF4C0F4399FEE1BF9
      EAA447C1952A8FC00D554A9E0CC48A77785984D292B5EDDA77BE95F8EAD46B77
      64DF3FBE397277EBD62377376769037E183761CAE136EDBBE2959BC27A90712B
      8B65E2162560098CD30B6D7B4A4BBFBFD8E397F377A74686E8EAB78A70EE98B4
      E1ECE27EB30E0DE9D8E5C59BC1C1A11E60180A14EB9828AE47CF3E7F1C3C9F71
      EBD7D3F7A9633DB72FF46AC91DB386CD1F25545BC1B5CEA63D3ED0B95BAFD324
      887D395D1C54C8AC1F105C3923E5977B3787B40DFC2433C7ACD2488251122D26
      AD5A30BD1D1736F174DA839B9DEA869971ECEB41BE58C744989CCB939C6529E3
      B1F189098FF33492D9A09244834AA49C0BE939175C1CB5AC1B46B1249CC94447
      AFA75ECA0DF1F67359B6FDF2F0D806DEE325B2182495F0242B577EB8FFF8BDAD
      EFBE1431046F1C54E3E81DE34AC541E8DDE7C521C141C163C74EFCE0F880D9BC
      D12551A3088F21824C5987CE646CE04A1B26365E3E6FD6BBCF5F4BBD3267D386
      2F15BD713EC37E1E71C1B5B46BF7BEFF769D7EF5F8E7D68FEE5665868F9B46EB
      EBAED14F8CABFEDABA098D566FDBBA29E85AEAD57496E5CAC5818F1AE70FDCBD
      7B97E727B3DFC10B736F84BE18E5D1AA5B7DF7AE0E39A9CD3F9AF55EC31F766F
      533709C991578E0DBDF349BCEE55AE5018AC23C237DD40412B9C7043162D9CD3
      07F106003B3C02A4B9285FB17C74F09D669D13BD4E1F4D99B970A04E1CB32667
      3E0BD820D8AE355B4649E1C2819A5703AB35496AD4A287FEF0CFDFE55C3BFBF3
      9B6356E77C6CD75149150BE78F5963F8F8FAF983530EFDB425BB6174279D6760
      8D990B066886D9E46C3AB2A54B0D41F6E18DF387A6EDDBBE3CAB55FF293A4952
      CDB3555074644B94270459D2C2815AF3A60FFBBE274AC2B2F2D4F99F91F96FC0
      9EF700BA4129DA0000000049454E4400000000}
  end
  object ScalesImageList: TTBXImageList
    Left = 48
    Top = 240
    PngDIB = {
      2300000089504E470D0A1A0A0000000D49484452000000100000023002030000
      00F2AE03ED0000000C504C5445000000000000FF0000808080E6239CB1000000
      0174524E530040E6D8660000026F494441547801AD966B6EE3300C848780F45F
      01E2FB30400EE002D621774FD19B75BF911B24C56E1772525519EB418F872269
      57FAAE45EF52FCFEF5FE09BDBF4B1F1F800EB562EBB314DB9551294033241032
      6C00AB937DC32E42AA2BA0664808424D2A4A36DE10AEE9B6D832137D4A18AA04
      538A096B4A601B269A6C162458623B714731B40BA3EC5029C483001626FB92B2
      226E4BA1AF034DF9BCBE45B4CC2F7C4CF19435908D6182D54C8F6E2B02505740
      8D28288150A2D65057469A6E8B2D132D1BA0E269F3280148BDE1355B4DFD86B1
      EFDDF918A9F94C5350298163F1F543436461250BFB09089A8AA119D2D087896D
      677EA30EAACEEA275D0565818F6C26BB4969EFC2E7E39D21C3663C7CD747C6C1
      0705DDD542EE708AEAB1070EE389FE17DFD9FA28B9803E7FC0DF2B7C51D087AA
      B4D2697F439D9257727494155236A8F48AB47B68A54F3E4EF0E9D0E211AF8382
      3E026A22EB6BB88AE6DC43C1682208BBC9432820A3DB5F2E4FEBFB572A73A6A4
      1EE2F09F54B1C9FEF449ACC921B69363038480A28B831290522AEACB8AC9249D
      6A92FCAD511F6AD4875A1175E17CD9AF9CEEB26232CFC7C3777DC34FE0C6477D
      3044DF91AFC883BF50E1E7D9BAB88EFAF8017FAFF05DEC2FD9944547FCAD29BE
      1FBC919BDFCD86E8B711AF6B36385E4C663B55E68283737CDA9806C5CA9491B3
      1A1E8FB8CCF5C5669952F2474FD5CD04E978FA4B22757635DBEEFE0ADF5AA438
      338FC6D49E1FF31749122C245DE26549F89A47CBCA74ECEEA0B956B94DF66D48
      F1A7E8357D777F1B02763E533BD2CFC4771CF608C00094D6F10F5A9A7E803CE2
      5973FD411FF735A6AFE51F049CBBA96E5E46F799F24AF53A1BFF8BEF1FBADD59
      9D6BF277E30000000049454E4400000000}
  end
  object MainPopupMenu: TTBXPopupMenu
    AutoPopup = False
    Images = MenusImageList
    OnPopup = MainPopupMenuPopup
    Left = 208
    Top = 168
    object tbitmProperties: TTBXItem
      ImageIndex = 31
      OnClick = tbitmPropertiesClick
      Caption = 'Properties'
      Hint = ''
    end
    object NMarkEdit: TTBXItem
      ImageIndex = 31
      OnClick = NMarkEditClick
      Caption = 'Edit'
      Hint = ''
    end
    object NMarkDel: TTBXItem
      ImageIndex = 30
      OnClick = NMarkDelClick
      Caption = 'Delete'
      Hint = ''
    end
    object NMarkOper: TTBXItem
      ImageIndex = 10
      OnClick = NMarkOperClick
      Caption = 'Selection Manager'
      Hint = ''
    end
    object NMarkNav: TTBXItem
      ImageIndex = 33
      OnClick = NMarkNavClick
      Caption = 'Navigate to Placemark'
      Hint = ''
    end
    object NMarkExport: TTBXItem
      ImageIndex = 25
      OnClick = NMarkExportClick
      Caption = 'Placemark Export'
      Hint = ''
    end
    object NMarkPlay: TTBXItem
      OnClick = NMarkPlayClick
      Caption = 'Play'
      Hint = ''
    end
    object tbitmMarkInfo: TTBXItem
      ImageIndex = 27
      OnClick = tbitmMarkInfoClick
      Caption = 'Placemark Info'
      Hint = ''
    end
    object tbitmFitMarkToScreen: TTBXItem
      ImageIndex = 43
      OnClick = tbitmFitMarkToScreenClick
      Caption = 'Fit to Screen'
      Hint = ''
    end
    object tbitmHideThisMark: TTBXItem
      ImageIndex = 19
      OnClick = tbitmHideThisMarkClick
      Caption = 'Hide'
      Hint = ''
    end
    object tbsprtMainPopUp0: TTBXSeparatorItem
      Caption = ''
      Hint = ''
    end
    object NaddPoint: TTBXItem
      ImageIndex = 15
      OnClick = NaddPointClick
      Caption = 'Add Placemark'
      Hint = ''
    end
    object tbsprtMainPopUp1: TTBXSeparatorItem
      Caption = ''
      Hint = ''
    end
    object tbitmCenterWithZoom: TTBXSubmenuItem
      Caption = 'Center With Zoom'
      Hint = ''
      object tbtpltCenterWithZoom: TTBXToolPalette
        ColCount = 5
        Images = ScalesImageList
        PaletteOptions = []
        RowCount = 5
        OnCellClick = tbtpltCenterWithZoomCellClick
        Caption = ''
        Hint = ''
      end
    end
    object tbsprtMainPopUp2: TTBXSeparatorItem
      Caption = ''
      Hint = ''
    end
    object tbitmCopyToClipboard: TTBXSubmenuItem
      ImageIndex = 28
      Caption = 'Copy to Clipboard'
      Hint = ''
      object Google1: TTBXItem
        OnClick = Google1Click
        Caption = 'URL to Google Maps'
        Hint = ''
      end
      object YaLink: TTBXItem
        OnClick = YaLinkClick
        Caption = 'URL to Yandex.Maps'
        Hint = ''
      end
      object kosmosnimkiru1: TTBXItem
        OnClick = kosmosnimkiru1Click
        Caption = 'URL to kosmosnimki.ru'
        Hint = ''
      end
      object livecom1: TTBXItem
        OnClick = livecom1Click
        Caption = 'URL to Bing Maps'
        Hint = ''
      end
      object osmorg1: TTBXItem
        OnClick = osmorg1Click
        Caption = 'URL to osm.org'
        Hint = ''
      end
      object nokiamapcreator1: TTBXItem
        OnClick = nokiamapcreator1Click
        Caption = 'URL to Nokia Map Creator'
        Hint = ''
      end
      object terraserver1: TTBXItem
        OnClick = terraserver1Click
        Caption = 'URL to Terraserver'
        Hint = ''
      end
      object Rosreestr: TTBXItem
        OnClick = RosreestrClick
        Caption = 'URL to Rosreestr'
        Hint = ''
      end
      object tbsprtCopyToClipboard0: TTBXSeparatorItem
        Caption = ''
        Hint = ''
      end
      object tbitmCopyToClipboardMainMapUrl: TTBXItem
        OnClick = tbitmCopyToClipboardMainMapUrlClick
        Caption = 'URL to Primary Map Tile'
        Hint = ''
      end
      object TBCopyLinkLayer: TTBXSubmenuItem
        Caption = 'URL to Layer Tile'
        Hint = ''
      end
      object tbitmCopyToClipboardCoordinates: TTBXItem
        OnClick = tbitmCopyToClipboardCoordinatesClick
        Caption = 'Coordinates'
        Hint = ''
      end
      object tbitmCopyToClipboardMainMapTile: TTBXItem
        OnClick = tbitmCopyToClipboardMainMapTileClick
        Caption = 'Primary Map Tile'
        Hint = ''
      end
      object tbitmCopyToClipboardMainMapTileFileName: TTBXItem
        OnClick = tbitmCopyToClipboardMainMapTileFileNameClick
        Caption = 'Pathname to Tile in Cache'
        Hint = ''
      end
      object tbitmCopyToClipboardGenshtabName: TTBXItem
        OnClick = tbitmCopyToClipboardGenshtabNameClick
        Caption = 'Genshtab boundary name'
        Hint = ''
      end
    end
    object Nopendir: TTBXItem
      OnClick = NopendirClick
      Caption = 'Show Primary Map Tile'
      Hint = ''
    end
    object tbitmOpenFolderMainMapTile: TTBXItem
      ImageIndex = 34
      OnClick = tbitmOpenFolderMainMapTileClick
      Caption = 'Open Primary Map Tile Folder'
      Hint = ''
    end
    object TBOpenDirLayer: TTBXSubmenuItem
      ImageIndex = 34
      Caption = 'Open Overlay Layer Tile Folder'
      Hint = ''
    end
    object tbsprtMainPopUp3: TTBXSeparatorItem
      Caption = ''
      Hint = ''
    end
    object tbitmAdditionalOperations: TTBXSubmenuItem
      Caption = 'Additional Operations'
      Hint = ''
      object NGTOPO30: TTBXItem
        OnClick = NGTOPO30Click
        Caption = 'Current Altitude by GTOPO30 (~1 km accuracy)'
        Hint = ''
      end
      object NSRTM3: TTBXItem
        OnClick = NSRTM3Click
        Caption = 'Current Altitude by SRTM3 (~90 m accuracy)'
        Hint = ''
      end
      object tbsprtAdditionalOperations1: TTBXSeparatorItem
        Caption = ''
        Hint = ''
      end
      object DigitalGlobe1: TTBXItem
        OnClick = DigitalGlobe1Click
        Caption = 'Images available'
        Hint = ''
      end
      object TBXMakeRosreestrPolygon: TTBXItem
        OnClick = TBXMakeRosreestrPolygonClick
        Caption = 'Make Polygon by RosReestr'
        Hint = ''
      end
      object tbsprtAdditionalOperations0: TTBXSeparatorItem
        Caption = ''
        Hint = ''
      end
      object NoaaForecastMeteorology1: TTBXItem
        OnClick = NoaaForecastMeteorology1Click
        Caption = 'Current and Forecast Meteorology'
        Hint = ''
      end
    end
    object tbpmiVersions: TTBXSubmenuItem
      OnPopup = tbpmiVersionsPopup
      Caption = 'Version'
      Hint = ''
      object tbitmSelectVersionByMark: TTBXItem
        OnClick = tbitmSelectVersionByMarkClick
        Caption = 'Select by Placemark'
        Hint = ''
      end
      object tbitmMakeVersionByMark: TTBXItem
        OnClick = tbitmMakeVersionByMarkClick
        Caption = 'Make by Placemark'
        Hint = ''
      end
      object tbpmiShowPrevVersion: TTBXItem
        AutoCheck = True
        OnClick = tbpmiShowPrevVersionClick
        Caption = 'Show previous Version'
        Hint = ''
      end
      object tbpmiClearVersion: TTBXItem
        OnClick = tbpmiClearVersionClick
        Caption = 'Reset'
        Hint = ''
      end
    end
    object tbsprtMainPopUp4: TTBXSeparatorItem
      Caption = ''
      Hint = ''
    end
    object tbitmDownloadMainMapTile: TTBXItem
      ImageIndex = 21
      OnClick = tbitmDownloadMainMapTileClick
      Caption = 'Download Primary Map Tile to Cache (Ins+MLeft)'
      Hint = ''
    end
    object NDel: TTBXItem
      ImageIndex = 22
      OnClick = NDelClick
      Caption = 'Delete Primary Map Tile from Cache (Del+MLeft)'
      Hint = ''
    end
    object ldm: TTBXSubmenuItem
      ImageIndex = 21
      Caption = 'Download Overlay Layer Tile to Cache'
      Hint = ''
    end
    object dlm: TTBXSubmenuItem
      ImageIndex = 22
      Caption = 'Delete Overlay Layer Tile from Cache'
      Hint = ''
    end
    object tbsprtMainPopUp5: TTBXSeparatorItem
      Caption = ''
      Hint = ''
    end
    object NMapInfo: TTBXItem
      ImageIndex = 27
      OnClick = NMapInfoClick
      Caption = 'Map Info'
      Hint = ''
    end
    object NMapStorageInfo: TTBXItem
      ImageIndex = 27
      OnClick = NMapStorageInfoClick
      Caption = 'Map Storage Info'
      Hint = ''
    end
    object TBLayerInfo: TTBXSubmenuItem
      ImageIndex = 27
      Caption = 'Layer Info'
      Hint = ''
    end
  end
  object TrayIcon: TTrayIcon
    PopupMenu = TrayPopupMenu
    OnClick = TrayItemRestoreClick
    OnDblClick = TrayItemRestoreClick
    Left = 296
    Top = 80
  end
  object TrayPopupMenu: TTBXPopupMenu
    Left = 304
    Top = 128
    object TrayItemRestore: TTBItem
      OnClick = TrayItemRestoreClick
      Caption = 'Restore'
      Hint = ''
    end
    object TBSeparatorItem1: TTBSeparatorItem
      Caption = ''
      Hint = ''
    end
    object TrayItemQuit: TTBItem
      OnClick = TrayItemQuitClick
      Caption = 'Quit'
      Hint = ''
    end
  end
  object tbxpmnSearchResult: TTBXPopupMenu
    Left = 160
    Top = 264
    object tbitmCopySearchResultCoordinates: TTBXItem
      OnClick = tbitmCopySearchResultCoordinatesClick
      Caption = 'Copy coordinates'
      Hint = ''
    end
    object tbitmCopySearchResultDescription: TTBXItem
      OnClick = tbitmCopySearchResultDescriptionClick
      Caption = 'Copy description'
      Hint = ''
    end
    object tbitmCreatePlaceMarkBySearchResult: TTBXItem
      OnClick = tbitmCreatePlaceMarkBySearchResultClick
      Caption = 'Create placemark'
      Hint = ''
    end
  end
end
