unit fr_DvrButtons;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ButtonGroup,
  Vcl.ActnList, Vcl.ImgList, Vcl.Buttons,
  gnugettext,
  TB2Item, TBX, TB2Dock, TB2Toolbar,
  TBXGraphics;

type
  TfrDVRButtons = class(TFrame)
    aclDVR: TActionList;
    staAddFilesToPlayList: TAction;
    staClearPlayList: TAction;
    staPlayPause: TAction;
    staStepForward: TAction;
    staStepBackward: TAction;
    staNextTrack: TAction;
    staPrevTrack: TAction;
    staCenterCursorOnMap: TAction;
    staFullScreenVideo: TAction;
    staScreenShot: TAction;
    staRemoveFileFromPlayList: TAction;
    staSpeed025: TAction;
    staSpeed05: TAction;
    staSpeedNormal: TAction;
    staSpeedx2: TAction;
    staSpeedx4: TAction;
    staRestoreDefaults: TAction;
    staMute: TAction;
    imlElemColor: TTBXImageList;
    TBXDock1: TTBXDock;
    TBXToolbar1: TTBXToolbar;
    TBXItem11: TTBXItem;
    TBXItem10: TTBXItem;
    TBXItem9: TTBXItem;
    TBXItem3: TTBXItem;
    TBXItem1: TTBXItem;
    TBXItem15: TTBXItem;
    TBXItem14: TTBXItem;
    TBXItem13: TTBXItem;
    TBXItem17: TTBXItem;
    TBXItem16: TTBXItem;
    TBXToolbar2: TTBXToolbar;
    TBXItem6: TTBXItem;
    TBXItem5: TTBXItem;
    TBXItem4: TTBXItem;
    TBXItem2: TTBXItem;
    TBXToolbar3: TTBXToolbar;
    TBXItem12: TTBXItem;
    TBXItem8: TTBXItem;
    TBXItem7: TTBXItem;
    TBXToolbar4: TTBXToolbar;
    TBXItem19: TTBXItem;
    procedure staCenterCursorOnMapExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TfrDVRButtons.staCenterCursorOnMapExecute(Sender: TObject);
begin
//
end;

end.
