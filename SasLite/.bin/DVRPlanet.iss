[Setup]
AppName=Akenori.DVR.Planet
AppVersion=1.0
AppPublisher=Nexus Group
AppPublisherURL=http://www.akenori.ru
AppSupportURL=http://www.akenori.ru
AppUpdatesURL=http://www.akenori.ru
DefaultDirName={localappdata}\Akenori\DVRPlanet
DefaultGroupName=Akenori
OutputBaseFilename=setup
Compression=lzma2
SolidCompression=yes
PrivilegesRequired=lowest

; Uncomment the following line to disable the "Select Setup Language"
; dialog and have it rely solely on auto-detection.
;ShowLanguageDialog=no
; If you want all languages to be listed in the "Select Setup Language"
; dialog, even those that can't be displayed in the active code page,
; uncomment the following line. Note: Unicode Inno Setup always displays
; all languages.
;ShowUndisplayableLanguages=yes

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: ru; MessagesFile: "compiler:Languages\Russian.isl"
Name: uk; MessagesFile: "compiler:Languages\Ukrainian.isl"
Name: ch; MessagesFile: "compiler:Languages\ChineseTraditional.isl"
Name: jp; MessagesFile: "compiler:Languages\Japanese.isl"

[Messages]
en.BeveledLabel=English
ru.BeveledLabel=Russian
uk.BeveledLabel=Ukrainean
ch.BeveledLabel=Chinese Traditional
jp.BeveledLabel=Japanese

[CustomMessages]
en.MyResInfo=Initial screen layout will be optimized%nfor the current main monitor resolution (
ru.MyResInfo=��������� ���������� ������ ���������%n����� �������������� ��� �������� ����������%n��������� �������� (


en.SSMsg0=Select snapshot files directory
en.SSMsg1=Where should snapshot files be placed?
en.SSMsg2=Select the folder in which program should place snapshot files, then click Next.
ru.SSMsg0=������� ����� ��� ���������� ������� (������ �����������)
ru.SSMsg1=��� ����� ����������� ����� �������?
ru.SSMsg2=������� �����, ��� ��������� ����� ��������� ������, ����� ������� "�����".

en.VAMsg0=Installing on virtual machine
en.VAMsg1=Do you want disable video acceleration (may be needed for using on virtual machines)?
en.VAMsg2=Please specify how you would like to use this program, then click Next.
en.VAMsg3=Video acceleration disabled

ru.VAMsg0=���� ��������� ��������������� �� ����������� ������
ru.VAMsg1=��������� �����-��������� (����� ���� ���������� ��� ������ �� ����������� ������)?
ru.VAMsg2=�������� ����� ������, ���� ��� ����������, ����� ������� "�����".
ru.VAMsg3=��������� �����-���������

en.SlowAMDMsg0=Slow AMD CPU
ru.SlowAMDMsg0=������ ��������� AMD

[Files]
Source: ini\1920\DVRPlanet.ini;        DestDir: "{app}"; Check: IsRes1920; Flags: replacesameversion
Source: ini\1600\DVRPlanet.ini;        DestDir: "{app}"; Check: IsRes1600; Flags: replacesameversion
Source: ini\1366\DVRPlanet.ini;        DestDir: "{app}"; Check: IsRes1366; Flags: replacesameversion
Source: ini\1280\DVRPlanet.ini;        DestDir: "{app}"; Check: IsRes1280; Flags: replacesameversion
Source: AkenLogo.jpg      ;            DestDir: "{app}"
Source: Categorymarks.sml ;            DestDir: "{app}"
Source: CityHash.dll      ;            DestDir: "{app}"
Source: db_dump.exe       ;            DestDir: "{app}"
Source: db_load.exe       ;            DestDir: "{app}"
Source: db_recover.exe    ;            DestDir: "{app}"
Source: db_verify.exe     ;            DestDir: "{app}"
Source: DVRPlanet.exe     ;            DestDir: "{app}"; Flags: replacesameversion
Source: f1ct.dll          ;            DestDir: "{app}"
Source: FreeImage.dll     ;            DestDir: "{app}"
Source: jpeg62.dll        ;            DestDir: "{app}"
Source: leveldb.dll       ;            DestDir: "{app}"
Source: libdb51.dll       ;            DestDir: "{app}"
Source: libge.dll         ;            DestDir: "{app}"
Source: libpng15.dll      ;            DestDir: "{app}"
Source: msvcm90.dll       ;            DestDir: "{app}"
Source: msvcp71.dll       ;            DestDir: "{app}"
Source: msvcp90.dll       ;            DestDir: "{app}"
Source: msvcr71.dll       ;            DestDir: "{app}"
Source: msvcr90.dll       ;            DestDir: "{app}"
Source: NCScnet.dll       ;            DestDir: "{app}"
Source: NCSEcw.dll        ;            DestDir: "{app}"
Source: NCSEcwC.dll       ;            DestDir: "{app}"
Source: NCSUtil.dll       ;            DestDir: "{app}"
Source: proj480.dll       ;            DestDir: "{app}"
Source: sdb_util.exe      ;            DestDir: "{app}"
Source: sqlite3.dll       ;            DestDir: "{app}"
Source: TileStorage_GC.dll;            DestDir: "{app}"
Source: TimeZone.dll      ;            DestDir: "{app}"
Source: xercesLib.dll     ;            DestDir: "{app}"
Source: xercesxmldom.dll  ;            DestDir: "{app}"
Source: zlib1.dll         ;            DestDir: "{app}"
Source: lang\*            ;            DestDir: "{app}\lang\"
Source: maps\*            ;            DestDir: "{app}\maps\"

;Source: maps\sas.maps\genshtab\karta_gsh_250m.zmp\*;     DestDir: "{app}\maps\sas.maps\genshtab\karta_gsh_250m.zmp\"
;Source: maps\sas.maps\genshtab\karta_gsh_500m.zmp\*;     DestDir: "{app}\maps\sas.maps\genshtab\karta_gsh_500m.zmp\"
;Source: maps\sas.maps\genshtab\karta_gsh_1km.zmp\*;      DestDir: "{app}\maps\sas.maps\genshtab\karta_gsh_1km.zmp\"
;Source: maps\sas.maps\genshtab\karta_gsh_2km.zmp\*;      DestDir: "{app}\maps\sas.maps\genshtab\karta_gsh_2km.zmp\"
;Source: maps\sas.maps\genshtab\karta_gsh_5km.zmp\*;      DestDir: "{app}\maps\sas.maps\genshtab\karta_gsh_5km.zmp\"
;Source: maps\sas.maps\genshtab\karta_gsh_10km.zmp\*;     DestDir: "{app}\maps\sas.maps\genshtab\karta_gsh_10km.zmp\"

Source: maps\sas.maps\google\ghyb.zmp\*;                 DestDir: "{app}\maps\sas.maps\google\ghyb.zmp\"
Source: maps\sas.maps\google\googleland.zmp\*;           DestDir: "{app}\maps\sas.maps\google\googleland.zmp\"
Source: maps\sas.maps\google\googlemap.zmp\*;            DestDir: "{app}\maps\sas.maps\google\googlemap.zmp\"
Source: maps\sas.maps\google\googlesat.zmp\*;            DestDir: "{app}\maps\sas.maps\google\googlesat.zmp\"
Source: maps\sas.maps\google\googletraf.zmp\*;           DestDir: "{app}\maps\sas.maps\google\googletraf.zmp\"
Source: maps\sas.maps\google\panoramio_kml.zmp\*;        DestDir: "{app}\maps\sas.maps\google\panoramio_kml.zmp\"

Source: maps\sas.maps\osm\osm_mapcm.zmp\*;               DestDir: "{app}\maps\sas.maps\osm\osm_mapcm.zmp\"
Source: maps\sas.maps\osm\osm_mapnik.zmp\*;              DestDir: "{app}\maps\sas.maps\osm\osm_mapnik.zmp\"
Source: maps\sas.maps\osm\osm_mapsurfer.zmp\*;           DestDir: "{app}\maps\sas.maps\osm\osm_mapsurfer.zmp\"
Source: maps\sas.maps\osm\osm_mapsurfer_hillshade.zmp\*; DestDir: "{app}\maps\sas.maps\osm\osm_mapsurfer_hillshade.zmp\"
Source: maps\sas.maps\osm\osm_mapsurfer_roads.zmp\*;     DestDir: "{app}\maps\sas.maps\osm\osm_mapsurfer_roads.zmp\"

Source: maps\sas.maps\yandex\YaHyb.zmp\*;                DestDir: "{app}\maps\sas.maps\yandex\YaHyb.zmp\"
Source: maps\sas.maps\yandex\YaMapNew.zmp\*;             DestDir: "{app}\maps\sas.maps\yandex\YaMapNew.zmp\"
Source: maps\sas.maps\yandex\YaSat.zmp\*;                DestDir: "{app}\maps\sas.maps\yandex\YaSat.zmp\"

;Source: maps\sas.plus.maps\_esri\ArcGIS.Hybrid.zmp\*;          DestDir: "{app}\maps\sas.plus.maps\_esri\ArcGIS.Hybrid.zmp\"
;Source: maps\sas.plus.maps\_esri\ArcGIS.Imagery.zmp\*;         DestDir: "{app}\maps\sas.plus.maps\_esri\ArcGIS.Imagery.zmp\"
;Source: maps\sas.plus.maps\_esri\ArcGIS.Landsat.zmp\*;         DestDir: "{app}\maps\sas.plus.maps\_esri\ArcGIS.Landsat.zmp\"
;Source: maps\sas.plus.maps\_esri\ArcGIS.LightGrayCanvas.zmp\*; DestDir: "{app}\maps\sas.plus.maps\_esri\ArcGIS.LightGrayCanvas.zmp\"
;Source: maps\sas.plus.maps\_esri\ArcGIS.NatGeo.zmp\*;          DestDir: "{app}\maps\sas.plus.maps\_esri\ArcGIS.NatGeo.zmp\"
;Source: maps\sas.plus.maps\_esri\ArcGIS.Streets.zmp\*;         DestDir: "{app}\maps\sas.plus.maps\_esri\ArcGIS.Streets.zmp\"
;Source: maps\sas.plus.maps\_esri\ArcGIS.Terrain.zmp\*;         DestDir: "{app}\maps\sas.plus.maps\_esri\ArcGIS.Terrain.zmp\"
;Source: maps\sas.plus.maps\_esri\Esri_Topo_maps.zmp\*;         DestDir: "{app}\maps\sas.plus.maps\_esri\Esri_Topo_maps.zmp\"

Source: maps\sas.plus.maps\_osm\4umaps.eu.zmp\*;               DestDir: "{app}\maps\sas.plus.maps\_osm\4umaps.eu.zmp\"
Source: maps\sas.plus.maps\_osm\osm_gps_tile.zmp\*;            DestDir: "{app}\maps\sas.plus.maps\_osm\osm_gps_tile.zmp\"
Source: maps\sas.plus.maps\_osm\osm_mapsurfer_boundary.zmp\*;  DestDir: "{app}\maps\sas.plus.maps\_osm\osm_mapsurfer_boundary.zmp\"
Source: maps\sas.plus.maps\_osm\osm_mapsurfer_grayscale.zmp\*; DestDir: "{app}\maps\sas.plus.maps\_osm\osm_mapsurfer_grayscale.zmp\"
Source: maps\sas.plus.maps\_osm\toolserver.org.zmp\*;          DestDir: "{app}\maps\sas.plus.maps\_osm\toolserver.org.zmp\"

;Source: maps\sas.plus.maps\_rosreestr\rosreestr_cadastr.zmp\*; DestDir: "{app}\maps\sas.plus.maps\_rosreestr\rosreestr_cadastr.zmp\"
;Source: maps\sas.plus.maps\_rosreestr\rosreestr_hyb.zmp\*;     DestDir: "{app}\maps\sas.plus.maps\_rosreestr\rosreestr_hyb.zmp\"
;Source: maps\sas.plus.maps\_rosreestr\rosreestr_landuse.zmp\*; DestDir: "{app}\maps\sas.plus.maps\_rosreestr\rosreestr_landuse.zmp\"

Source: maps\sas.plus.maps\_yandex\YaNarodMap.zmp\*;           DestDir: "{app}\maps\sas.plus.maps\_yandex\YaNarodMap.zmp\"
Source: maps\sas.plus.maps\_yandex\YaNarodnaya.zmp\*;          DestDir: "{app}\maps\sas.plus.maps\_yandex\YaNarodnaya.zmp\"

Source: marksicons\*;         DestDir: "{app}\marksicons\"

Source: mplayer\dsnative.dll;         DestDir: "{app}\mplayer\"
Source: mplayer\license.txt;          DestDir: "{app}\mplayer\"
Source: mplayer\mplayer\*;            DestDir: "{app}\mplayer\mplayer\"
Source: mplayer\_amd\mplayer.exe;     DestDir: "{app}\mplayer\"; Check: IsSlowAMD; Flags: ignoreversion
Source: mplayer\_generic\mplayer.exe; DestDir: "{app}\mplayer\"; Check: not IsSlowAMD; Flags: ignoreversion

;Source: _demo\*;            DestDir: "{app}\_demo\"
Source: cache\osm_mapsurfer\z16\19\x19834\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19834\9\"
Source: cache\osm_mapsurfer\z16\19\x19835\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19835\9\"
Source: cache\osm_mapsurfer\z16\19\x19836\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19836\9\"
Source: cache\osm_mapsurfer\z16\19\x19837\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19837\9\"
Source: cache\osm_mapsurfer\z16\19\x19838\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19838\9\"
Source: cache\osm_mapsurfer\z16\19\x19839\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19839\9\"
Source: cache\osm_mapsurfer\z16\19\x19840\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19840\9\"
Source: cache\osm_mapsurfer\z16\19\x19841\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19841\9\"
Source: cache\osm_mapsurfer\z16\19\x19842\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19842\9\"
Source: cache\osm_mapsurfer\z16\19\x19843\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19843\9\"
Source: cache\osm_mapsurfer\z16\19\x19844\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19844\9\"
Source: cache\osm_mapsurfer\z16\19\x19845\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19845\9\"
Source: cache\osm_mapsurfer\z16\19\x19846\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19846\9\"
Source: cache\osm_mapsurfer\z16\19\x19847\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19847\9\"
Source: cache\osm_mapsurfer\z16\19\x19848\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19848\9\"
Source: cache\osm_mapsurfer\z16\19\x19849\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19849\9\"
Source: cache\osm_mapsurfer\z16\19\x19850\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19850\9\"
Source: cache\osm_mapsurfer\z16\19\x19851\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19851\9\"
Source: cache\osm_mapsurfer\z16\19\x19852\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19852\9\"
Source: cache\osm_mapsurfer\z16\19\x19853\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19853\9\"
Source: cache\osm_mapsurfer\z16\19\x19854\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19854\9\"
Source: cache\osm_mapsurfer\z16\19\x19855\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19855\9\"
Source: cache\osm_mapsurfer\z16\19\x19856\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19856\9\"
Source: cache\osm_mapsurfer\z16\19\x19857\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19857\9\"
Source: cache\osm_mapsurfer\z16\19\x19858\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19858\9\"
Source: cache\osm_mapsurfer\z16\19\x19859\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19859\9\"
Source: cache\osm_mapsurfer\z16\19\x19860\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19860\9\"
Source: cache\osm_mapsurfer\z16\19\x19861\9\*;    DestDir: "{app}\cache\osm_mapsurfer\z16\19\x19861\9\"


[Icons]
Name: "{group}\Akenori DVR Planet GIS"; Filename: "{app}\DVRPlanet.exe"; WorkingDir: "{app}"
;Name: "{commonprograms}\Akenori DVR Planet GIS"; Filename: "{app}\DVRPlanet.exe"; WorkingDir: "{app}"
Name: "{commondesktop}\Akenori DVR Planet GIS"; Filename: "{app}\DVRPlanet.exe"; WorkingDir: "{app}"

[INI]
Filename: "{app}\DVRPlanet.ini"; Section: "View"; Key: "Lang";           String: "{language}"
Filename: "{app}\DVRPlanet.ini"; Section: "DVR";  Key: "SnapShotFolder"; String: "{code:GetSnapshotDir}"
Filename: "{app}\DVRPlanet.ini"; Section: "DVR";  Key: "UseVideoAccel";  String: "{code:GetUseVideoAccel}"


[Dirs]
Name: {code:GetSnapshotDir}; Flags: uninsneveruninstall

[Code]
var
  xres: Integer;
  yres: Integer;
  ResMsg: string;
  SnapshotDirPage: TInputDirWizardPage;
  VideoAccelPage: TInputOptionWizardPage;
  
function IsRes1920: boolean;
begin
  Result := (xres>=1920);
end;
  
function IsRes1600: boolean;
begin
  Result := (xres>=1600) and (xres<1920);
end;
  
function IsRes1366: boolean;
begin
  Result := (xres>=1366) and (xres<1600);
end;
  
function IsRes1280: boolean;
begin
  Result := (xres<1366);
end;

function GetSystemMetrics (nIndex: Integer): Integer;
  external 'GetSystemMetrics@User32.dll stdcall setuponly';

Const
    SM_CXSCREEN = 0; // The enum-value for getting the width of the cient area for a full-screen window on the primary display monitor, in pixels.
    SM_CYSCREEN = 1; // The enum-value for getting the height of the client area for a full-screen window on the primary display monitor, in pixels.

function InitializeSetup(): Boolean;
  var 
      hDC: Integer;
begin
    xres := GetSystemMetrics(SM_CXSCREEN);
    yres := GetSystemMetrics(SM_CYSCREEN); //vertical resolution
    ResMsg := ExpandConstant('{cm:MyResInfo}') + IntToStr(xres) + 'x' + IntToStr(yres)+').';
    //MsgBox( ExpandConstant('{cm:MyResInfo}') + IntToStr(xres) + 'x' + IntToStr(yres)+').', mbInformation, MB_OK );

    Result := true;
end;

procedure InitializeWizard;
begin
  { Create the pages }
  SnapshotDirPage := CreateInputDirPage(wpSelectDir
    , ExpandConstant('{cm:SSMsg0}')
    , ExpandConstant('{cm:SSMsg1}')
    , ExpandConstant('{cm:SSMsg2}')
    , False, '');
  SnapshotDirPage.Add('');

  VideoAccelPage := CreateInputOptionPage(SnapshotDirPage.Id
    , ExpandConstant('{cm:VAMsg0}')
    , ExpandConstant('{cm:VAMsg1}')
    , ExpandConstant('{cm:VAMsg2}')
    , False, False);
  VideoAccelPage.Add(ExpandConstant('{cm:VAMsg3}'));
  VideoAccelPage.Add(ExpandConstant('{cm:SlowAMDMsg0}'));

  { Set default values, using settings that were stored last time if possible }
  SnapshotDirPage.Values[0] := GetPreviousData('SnapshotDir', ExpandConstant('{localappdata}\Akenori\DVRPlanet\Snapshots'));

  case GetPreviousData('VideoAccel', 'No') of
    'Yes': VideoAccelPage.SelectedValueIndex := 0;
  else
    VideoAccelPage.SelectedValueIndex := -1;
  end;
end;

function UpdateReadyMemo(Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo, MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: String): String;
begin
  Result := MemoDirInfo 
          + NewLine + NewLine + MemoGroupInfo 
          + NewLine + NewLine + 'Snapshot directory:' + NewLine + Space + SnapshotDirPage.Values[0] 
          + NewLine + NewLine + ResMsg;
end;

function GetSnapshotDir(Param: String): String;
begin
  { Return the selected DataDir }
  Result := SnapshotDirPage.Values[0];
end;

function GetUseVideoAccel(Param: String): String;
begin
  if not VideoAccelPage.Values[0] then
    Result := '1' //va = true 
  else
    Result := '0' //va = false
  ;
end;

function IsSlowAMD: Boolean;
begin
  Result := VideoAccelPage.Values[1];
end;
