unit u_MapLayerGPSMarkerRings;

interface

uses
  SysUtils,
  GR32,
  GR32_Image,
  t_GeoTypes,
  i_Notifier,
  i_NotifierTime,
  i_NotifierOperation,
  i_Datum,
  i_ProjectionInfo,
  i_LocalCoordConverter,
  i_LocalCoordConverterChangeable,
  i_InternalPerformanceCounter,
  i_SimpleFlag,
  i_ProjectedDrawableElement,
  i_MarkerRingsConfig,
  i_GeometryLonLat,
  i_GeometryProjected,
  i_GeometryProjectedFactory,
  i_GeometryLonLatFactory,
  i_GPSRecorder,
  u_MapLayerBasic,
  i_DVRGPSLib; //sokolov

type
  TMapLayerGPSMarkerRings = class(TMapLayerBasicNoBitmap, IDVRCursorDrawer)
  private
    FConfig: IMarkerRingsConfig;
    FGPSRecorder: IGPSRecorder;
    FVectorGeometryProjectedFactory: IGeometryProjectedFactory;
    FVectorGeometryLonLatFactory: IGeometryLonLatFactory;

    FGpsPosChangeFlag: ISimpleFlag;

    FGPSPosCS: IReadWriteSync;
    FGPSPosLonLat: TDoublePoint;
    //sokolov
    //  TDoublePoint = packed record
    //    X, Y: Double;
    //  end;
    FDVRGPSAccuracy: Real;

    FCirclesLonLat: IGeometryLonLatMultiPolygon;
    FCirclesProjected: IProjectedDrawableElement;

    function GetLonLatCirclesByPoint(
      const APos: TDoublePoint;
      const ADatum: IDatum;
      const AConfig: IMarkerRingsConfigStatic;
      const aAccuracy: Real = 0.0
    ): IGeometryLonLatMultiPolygon;
    function GetProjectedCirclesByLonLat(
      const ASource: IGeometryLonLatMultiPolygon;
      const AProjectionInfo: IProjectionInfo
    ): IGeometryProjectedMultiPolygon;
    procedure GPSReceiverReceive;
    procedure OnConfigChange;
    procedure OnTimer;
    procedure DrawDVRMark(aTrackPoint: TTrackPoint);
  protected
    procedure PaintLayer(
      ABuffer: TBitmap32;
      const ALocalConverter: ILocalCoordConverter
    ); override;
  public
    constructor Create(
      const APerfList: IInternalPerformanceCounterList;
      const AAppStartedNotifier: INotifierOneOperation;
      const AAppClosingNotifier: INotifierOneOperation;
      AParentMap: TImage32;
      const AView: ILocalCoordConverterChangeable;
      const ATimerNoifier: INotifierTime;
      const AVectorGeometryProjectedFactory: IGeometryProjectedFactory;
      const AVectorGeometryLonLatFactory: IGeometryLonLatFactory;
      const AConfig: IMarkerRingsConfig;
      const AGPSRecorder: IGPSRecorder
    );
  end;

implementation

uses
  GR32_Polygons,
  i_GPS,
  i_DoublePointsAggregator,
  u_GeoFunc,
  u_Synchronizer,
  u_SimpleFlagWithInterlock,
  u_DoublePointsAggregator,
  u_ListenerTime,
  u_ListenerByEvent,
  u_ProjectedDrawableElementByPolygon;

{ TMapLayerGPSMarkerRings }

constructor TMapLayerGPSMarkerRings.Create(
  const APerfList: IInternalPerformanceCounterList;
  const AAppStartedNotifier, AAppClosingNotifier: INotifierOneOperation;
  AParentMap: TImage32;
  const AView: ILocalCoordConverterChangeable;
  const ATimerNoifier: INotifierTime;
  const AVectorGeometryProjectedFactory: IGeometryProjectedFactory;
  const AVectorGeometryLonLatFactory: IGeometryLonLatFactory;
  const AConfig: IMarkerRingsConfig;
  const AGPSRecorder: IGPSRecorder
);
begin
  inherited Create(
    APerfList,
    AAppStartedNotifier,
    AAppClosingNotifier,
    AParentMap,
    AView
  );
  FConfig := AConfig;
  FGPSRecorder := AGPSRecorder;
  FVectorGeometryProjectedFactory := AVectorGeometryProjectedFactory;
  FVectorGeometryLonLatFactory := AVectorGeometryLonLatFactory;

  FGpsPosChangeFlag := TSimpleFlagWithInterlock.Create;
  FGPSPosCS := MakeSyncRW_Var(Self, False);

  FDVRGPSAccuracy := 0.0;

  LinksList.Add(
    TListenerTimeCheck.Create(Self.OnTimer, 500),
    ATimerNoifier
  );
  LinksList.Add(
    TNotifyNoMmgEventListener.Create(Self.OnConfigChange),
    FConfig.GetChangeNotifier
  );
  LinksList.Add(
    TNotifyNoMmgEventListener.Create(Self.GPSReceiverReceive),
    FGPSRecorder.GetChangeNotifier
  );
end;

procedure TMapLayerGPSMarkerRings.DrawDVRMark(aTrackPoint: TTrackPoint);
var
  VLonLat: TDoublePoint;
  VAccuracy: Real;
begin
    ViewUpdateLock;
    try
      if false then begin //(not VpPos^.PositionOK)
        // no position
        Hide;
      end else begin
        // ok
        VLonLat.X := aTrackPoint.Longitude;//VpPos^.PositionLon;
        VLonLat.Y := aTrackPoint.Latitude;//VpPos^.PositionLat;
        VAccuracy := aTrackPoint.Accuracy;

        FGPSPosCS.BeginWrite;
        try
          if not (DoublePointsEqual(FGPSPosLonLat, VLonLat) and (FDVRGPSAccuracy = VAccuracy)) then begin
            FGPSPosLonLat := VLonLat;
            FDVRGPSAccuracy := VAccuracy;

            FCirclesLonLat := nil;
            FCirclesProjected := nil;
            SetNeedRedraw;
          end;
        finally
          FGPSPosCS.EndWrite;
        end;
        Show;
      end;
    finally
      ViewUpdateUnlock;
    end;
end;

function TMapLayerGPSMarkerRings.GetLonLatCirclesByPoint(
  const APos: TDoublePoint;
  const ADatum: IDatum;
  const AConfig: IMarkerRingsConfigStatic;
  const aAccuracy: Real = 0.0
): IGeometryLonLatMultiPolygon;
const
  c_SegQty = 64;
var
  VAggreagator: IDoublePointsAggregator;
  i, j: Integer;
  VDist: Double;
  VAngle: Double;
  VPoint: TDoublePoint;
begin
  VAggreagator := TDoublePointsAggregator.Create;
  for i := 1 to AConfig.Count do begin
    if (aAccuracy = 0.0) then
      VDist := AConfig.StepDistance * i
    else
      VDist := aAccuracy * i
    ;
    for j := 0 to c_SegQty do begin
      VAngle := j * 360 / c_SegQty;
      VPoint := ADatum.CalcFinishPosition(APos, VAngle, VDist);
      VAggreagator.Add(VPoint);
    end;
    VAggreagator.Add(CEmptyDoublePoint);
  end;
  Result := FVectorGeometryLonLatFactory.CreateLonLatMultiPolygon(VAggreagator.Points, VAggreagator.Count);
end;

function TMapLayerGPSMarkerRings.GetProjectedCirclesByLonLat(
  const ASource: IGeometryLonLatMultiPolygon;
  const AProjectionInfo: IProjectionInfo): IGeometryProjectedMultiPolygon;
begin
  Result := FVectorGeometryProjectedFactory.CreateProjectedPolygonByLonLatPolygon(AProjectionInfo, ASource);
end;

procedure TMapLayerGPSMarkerRings.GPSReceiverReceive;
begin
  FGpsPosChangeFlag.SetFlag;
end;

procedure TMapLayerGPSMarkerRings.OnConfigChange;
begin
  ViewUpdateLock;
  try
    FGPSPosCS.BeginWrite;
    try
      FCirclesLonLat := nil;
      FCirclesProjected := nil;
    finally
      FGPSPosCS.EndWrite;
    end;
    SetNeedRedraw;
  finally
    ViewUpdateUnlock;
  end;
end;

procedure TMapLayerGPSMarkerRings.OnTimer;
var
  VGPSPosition: IGPSPosition;
  VLonLat: TDoublePoint;
begin
  if FGpsPosChangeFlag.CheckFlagAndReset then begin
    ViewUpdateLock;
    try
      VGPSPosition := FGPSRecorder.CurrentPosition;
      if (not VGPSPosition.PositionOK) then begin
        // no position
        Hide;
      end else begin
        // ok
        VLonLat := VGPSPosition.LonLat;
        FGPSPosCS.BeginWrite;
        try
          if not DoublePointsEqual(FGPSPosLonLat, VLonLat) then begin
            FGPSPosLonLat := VLonLat;
            FCirclesLonLat := nil;
            FCirclesProjected := nil;
            SetNeedRedraw;
          end;
        finally
          FGPSPosCS.EndWrite;
        end;
        Show;
      end;
    finally
      ViewUpdateUnlock;
    end;
  end;
end;

procedure TMapLayerGPSMarkerRings.PaintLayer(ABuffer: TBitmap32;
  const ALocalConverter: ILocalCoordConverter);
var
  VLonLat: TDoublePoint;
  VConfig: IMarkerRingsConfigStatic;
  VAccuracy: Real;
  VCirclesLonLat: IGeometryLonLatMultiPolygon;
  VCirclesProjected: IGeometryProjectedMultiPolygon;
  VDrawable: IProjectedDrawableElement;
begin
  inherited;
  VConfig := FConfig.GetStatic;
  if VConfig.Count <= 0 then begin
    Exit;
  end;

  FGPSPosCS.BeginRead;
  try
    VLonLat := FGPSPosLonLat;
    VAccuracy := FDVRGPSAccuracy;
    VCirclesLonLat := FCirclesLonLat;
    VDrawable := FCirclesProjected;
  finally
    FGPSPosCS.EndRead;
  end;
  if VDrawable <> nil then begin
    if not VDrawable.ProjectionInfo.GetIsSameProjectionInfo(ALocalConverter.ProjectionInfo) then  begin //���� ���������� ��������, ���...
      VDrawable := nil;
    end;
  end;
  if VCirclesLonLat = nil then begin
    VCirclesLonLat := GetLonLatCirclesByPoint(VLonLat, ALocalConverter.ProjectionInfo.GeoConverter.Datum, VConfig, VAccuracy);
  end;
  if VCirclesLonLat = nil then begin
    Exit;
  end;
  FGPSPosCS.BeginWrite;
  try
    if DoublePointsEqual(VLonLat, FGPSPosLonLat) then begin //���� ���������� �� �������� - ����� �����-��, �.�. ���� ���� VLonLat := FGPSPosLonLat; sokolov
      FCirclesLonLat := VCirclesLonLat;
    end;
  finally
    FGPSPosCS.EndWrite
  end;
  if VDrawable = nil then begin
    VCirclesProjected := GetProjectedCirclesByLonLat(VCirclesLonLat, ALocalConverter.ProjectionInfo);
    VDrawable := TProjectedDrawableElementByPolygonSimpleEdge.Create(VCirclesProjected, amNone, clRed32);
  end;
  if VDrawable = nil then begin
    Exit;
  end;
  FGPSPosCS.BeginWrite;
  try
    if DoublePointsEqual(VLonLat, FGPSPosLonLat) then begin
      FCirclesProjected := VDrawable;
    end;
  finally
    FGPSPosCS.EndWrite
  end;
  VDrawable.Draw(ABuffer, ALocalConverter);
end;

end.
