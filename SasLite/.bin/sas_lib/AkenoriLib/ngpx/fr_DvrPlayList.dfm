object frDvrPlayList: TfrDvrPlayList
  Left = 0
  Top = 0
  Width = 764
  Height = 240
  TabOrder = 0
  object lvDVRFileList: TListView
    Left = 0
    Top = 0
    Width = 764
    Height = 240
    Align = alClient
    Columns = <
      item
        Caption = 'Name'
        Width = 170
      end
      item
        Caption = 'Duration, s'
      end
      item
        Caption = 'Date/Time'
        Width = 120
      end
      item
        Caption = 'Points'
        Width = 45
      end
      item
        Caption = 'Dist.,m'
        Width = 45
      end
      item
        Caption = 'Max'
        Width = 45
      end
      item
        Caption = 'Avg'
        Width = 45
      end
      item
        Caption = 'Shock'
        Width = 40
      end
      item
        Caption = 'Full file path'
        Width = 150
      end>
    ColumnClick = False
    FlatScrollBars = True
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnResize = lvDVRFileListResize
  end
  object odDVR: TOpenDialog
    Left = 40
    Top = 8
  end
end
