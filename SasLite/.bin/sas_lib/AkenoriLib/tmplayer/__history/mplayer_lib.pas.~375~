﻿unit mplayer_lib;
////////////////////////////////////////////////////////////////////////////////
// MPlayer VCL shell
////////////////////////////////////////////////////////////////////////////////
// VCL-оболочка MPlayer-а
////////////////////////////////////////////////////////////////////////////////
{
    Copyright (C) 2014 Dmitry Sokolov <sokohigh@gmail.com>
    Based on works by Martin J. Fiedler <martin.fiedler@gmx.net>,
    Huang Chen Bin <hcb428@foxmail.com> and Zozito Pelegrin.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
}

interface

uses Classes, Windows, Forms, Controls, ExtCtrls, SysUtils, Math,
  LogForm, PropListForm, VCL.Imaging.jpeg, VCL.Graphics, System.SyncObjs;

const
  CMD_AUDIODELAY = 'audio_delay';
  CMD_FRAMEDROP = 'frame_drop';
  CMD_FRAMESTEP = 'frame_step';
  CMD_GETPERCENTPOS = 'get_percent_pos';
  CMD_GetTimePos = 'get_time_pos';
  CMD_GRABFRAMES = 'grab_frames';
  CMD_MUTE = 'mute';
  CMD_OSD = 'osd';
  CMD_PAUSE = 'pause';
  CMD_QUIT = 'quit';
  CMD_SEEK = 'seek';
  CMD_SPEEDINCR = 'speed_incr';
  CMD_SPEEDMULT = 'speed_mult';
  CMD_SPEEDSET = 'speed_set';
  CMD_SCREENSHOTONCE = 'screenshot 0';

  CTimerInterval = 200; //100; //200; //300; // 1000
  CSliderDelay = 2;
  CDecimalSeparator = '.';
  CMPlayerExeName = 'mplayer.exe';
  COSDFont = 'Arial.ttf';

  CPref_TimePos    = 'ANS_TIME_POSITION=';
  CPref_StartingPlayback = 'STARTING PLAYBACK';

  CPref_AudioDelay = 'ANS_AUDIO_DELAY=';
  CProp_AudioDelay = 'audio_delay';

  CPref_FrameDrop  = 'ANS_FRAMEDROPPING=';
  CProp_FrameDrop  = 'framedropping';

  CPref_Mute       = 'ANS_mute=';
  CProp_Mute       = 'mute';

  CPref_Speed      = 'ANS_speed=';
  CProp_Speed      = 'speed';

  CPref_Volume     = 'ANS_VOLUME=';
  CProp_Volume      = 'volume';

  CPN_Length = 'LENGTH';
  CPN_StartTime = 'START_TIME';

type
  TMPlayer = class;

  TFetcherThread = class(TThread)
  // Accessory class for the MPlayer VCL shell:
  // The thread for infinite reading mplayer output and synchronously sending it to main thread;
  // if there is nothing to read, makes cleanup in main thread and shoots himself.
  // Вспомогательный класс для VCL-оболочки MPlayer-а:
  // Читает в цикле консольный вывод плеера и отдает главному потоку,
  // если ничего не читается, то выполняет чистку в главном потоке и самоубивается.
  private
    FhReadPipe: cardinal;
    FOwner: TMPlayer;
  public
    constructor CreateFetcherThread(const hReadPipe: cardinal; Owner: TMPlayer);
    procedure Execute; override;
  end;

  EMPlayerExeption = class(Exception);

  TMPlayerAspect = (mpaAutoDetect, mpa40_30, mpa160_90, mpa235_10);
  TMPlayerAudioOut = (maoNoSound, maoNull, maoWin32, maoDsSound);
  TMPlayerFrameDrop = (mfdEnabled, mfdHard, mfdDisabled);

  TChangePositionEvent = procedure(const PosMS: cardinal; const PosPC: real)
    of object;
  TSliderUpdateEvent = procedure(const NewPos: real) of object;
  TChangeSpeedEvent = procedure(const NewSpeed: real) of object;

  TMPlayer = class(TCustomPanel)
  // MPlayer VCL shell
  // Runs MPlayer as a process and communicates with it:
  // 1) translates user activities to mplayer text commands and sends it to mplayer process
  //   (also repeatedly sends to mplayer GetTimePosition commands)
  // 2) parses mplayer console output to own properties on timer event basis
  // VCL-оболочка MPlayer-а
  // Запускает процесс м-плеера и коммуницирует с ним:
  // 1) в ответ на действия пользователя отправляет команды м-плееру
  // 2) обрабатывает по таймеру его консольный вывод и обновляет значения
  //    своих свойств
  private
    FBrandNewInstance: boolean;
    FAudioDelay: real;
    FAudioOut: TMPlayerAudioOut;
    FAspect: TMPlayerAspect;
    FPlayerProcess: cardinal;
    FFetcherThread: TFetcherThread;
    FFrameDrop: TMPlayerFrameDrop;
    FLogForm: TfmLog;
    FFullScreen: boolean;
    FHWNDCur: HWND;
    FMediaAddr: string;
    FMPlayerPath: string;
    FMute: boolean;
    FOnChangePosition: TChangePositionEvent;
    FOnChangeSpeed: TChangeSpeedEvent;
    FOnPlayEnd: TNotifyEvent;
    FOnStartPlay: TNotifyEvent;
    FOsdLevel: integer;
    FPaused: boolean;
    FPausePostponed: boolean; //semantics: if true, pause state (if exist) must be restored
    FParams: string;
    FPlayerOutputBuff: string;
    FPropListForm: TfmPropList;
    FReadPipe: NativeUInt;
    FReIndex: boolean;
    FSliderDelay: Byte;                  // some magic
    FSliderUpdating: boolean;            // for use
    FOnSliderUpdate: TSliderUpdateEvent; // with a slider control
    FSpeed: real;
    FPositionRequestTime: cardinal;
    FTimer: TTimer;
    FTimerScreenShotWaiter: TTimer;
    FVolume: real;
    FWritePipe: NativeUInt;
    FLogoImage: TImage;
    FUseAccel: boolean;
    FSnapshotFolder: string;
    procedure CMDialogChar(var Message: TCMDialogChar); message CM_DIALOGCHAR;
    procedure SetActive(const NewVal: boolean);
    procedure SetAspect(const AAspect: TMPlayerAspect);
    procedure SetAudioDelay(const NewVal: real);
    procedure SetAudioOut(const AAudioOut: TMPlayerAudioOut);
    procedure SetFrameDrop(const NewVal: TMPlayerFrameDrop);
    procedure SetFullScreen(const AFullScreen: boolean);
    procedure SetLogVisible(const AVisible: boolean);
    procedure SetMediaAddr(const AMediaAddr: string);
    procedure SetMPlayerPath(const AMPlayerPath: string);
    procedure SetMute(const AValue: boolean);
    procedure SetOsdLevel(const NewVal: integer);
    procedure SetParams(const AParams: string);
    procedure SetPaused(const NewVal: boolean);
    procedure SetPropFormVisible(const Value: boolean);
    procedure SetReIndex(const AReIndex: boolean);
    procedure SetSpeed(const NewVal: real);
    procedure SetVolume(const AVolume: real);
    function GetActive: boolean;
    function GetDuration: cardinal;
    function GetLogVisible: boolean;
    function GetPropFormVisible: boolean;
    function GetPaused: boolean;

    procedure DoTimer(ASender: TObject);
    procedure DoTimerScreenShotWaiter(ASender: TObject);
    procedure DoFullScreen(const AFullScreen: boolean); virtual;
    procedure Init;
    procedure ProcessInputBuffer;
    procedure SendCommand(const ACommand: String);
    procedure SetMPProperty(const APropName, APropValue: string);
    procedure GetMPProperty(const APropName: string);
    procedure TerminateMPlayer;
    procedure RaiseIfOpen;
    function GetNamedValue(const Pref, Src: string; out Val: string): boolean;

    procedure Req_TimePos;
    function Check_TimePos(const MPOLine: string): boolean;
    function CheckAudioDelay(const MPOLine: string): boolean;
    function CheckFrameDrop(const MPOLine: string): boolean;
    function CheckMute(const MPOLine: string): boolean;
    function CheckSpeed(const MPOLine: string): boolean;
    function CheckStartingPlayback(const MPOLine: string): boolean;
    function CheckVolume(const MPOLine: string): boolean;

    function GetRealFromStrDef(const StrVal: string; const DefVal: real = 0.0): real;
    function GetStrFromReal(const RealVal: real): String;
    function DblQuotedStr(const S: string): string;
    function GetStartTime: cardinal;
    procedure SetUseAccel(const Value: boolean);
    procedure SetSnapshotFolder(const Value: string);
  protected
    procedure AddToInputBuffer(SNewPortion: string);
    procedure DoPlayEnd;
    procedure DoPlayEndConditional;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BruteTerminate;
    function MPlayerExists: boolean;
    procedure CloseMedia;
    procedure OpenMedia;
    //procedure SeekBack;
    procedure SeekBy(TimeShift: Integer);
    procedure SendFrameDropToogle;
    procedure Send_FrameStep;
    procedure SendSpeedIncr(const AValue: real);
    procedure SendSpeedMult(const AValue: real);
    procedure SetPercentPositionFromASlider(const PP: real); // some magic for use with a slider control
    procedure DoScreenShot;
    // inherited TCustomPanel properties - listed here just to change visibility:
    property DockManager;
    // new public properties:
    property Active: boolean read GetActive write SetActive default false;
    property Aspect: TMPlayerAspect read FAspect write SetAspect
      default mpaAutoDetect;
    property AudioDelay: real read FAudioDelay write SetAudioDelay;
    property AudioOut: TMPlayerAudioOut read FAudioOut write SetAudioOut
      default maoWin32;
    property Duration: cardinal read GetDuration;
    property FrameDrop: TMPlayerFrameDrop read FFrameDrop write SetFrameDrop;
    property FullScreen: boolean read FFullScreen write SetFullScreen;
    property MediaAddr: string read FMediaAddr write SetMediaAddr;
    property MPlayerPath: string read FMPlayerPath write SetMPlayerPath;
    property Mute: boolean read FMute write SetMute default false;
    property OnChangePosition: TChangePositionEvent read FOnChangePosition
      write FOnChangePosition;
    property OnChangeSpeed: TChangeSpeedEvent read FOnChangeSpeed write FOnChangeSpeed;
    property OnPlayEnd: TNotifyEvent read FOnPlayEnd write FOnPlayEnd;
    property OnSliderUpdate: TSliderUpdateEvent read FOnSliderUpdate
      write FOnSliderUpdate; // some magic for use with a slider control
    property OnStartPlay: TNotifyEvent read FOnStartPlay write FOnStartPlay;
    property OsdLevel: integer read FOsdLevel write SetOsdLevel;
    property Params: string read FParams write SetParams;
    property Paused: boolean read GetPaused write SetPaused default false;
    property ReIndex: boolean read FReIndex write SetReIndex default false;
    property Speed: real read FSpeed write SetSpeed;
    property StartTime: cardinal read GetStartTime;
    property Volume: real read FVolume write SetVolume;
    property UseAccel: boolean read FUseAccel write SetUseAccel;
    property SnapshotFolder: string read FSnapshotFolder write SetSnapshotFolder;
  published
    // inherited TCustomPanel properties - listed here just to publish:
    property Align;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BorderWidth;
    property BorderStyle;
    property Color;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property ParentBackground;
    property ParentColor;
    property ParentCtl3D;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop default True;
    property UseDockManager default True;
    property Visible;
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDockDrop;
    property OnDockOver;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetSiteInfo;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;

    property LogVisible: boolean read GetLogVisible write SetLogVisible
      default false;
    property PropFormVisible: boolean read GetPropFormVisible
      write SetPropFormVisible;
  end;

var DoneFlag: TEvent;

implementation

uses ShellApi, System.IOUtils;//, Dialogs;

{$R 'tmplayer.res'}

function StrToBoolMy(S: string): boolean;
begin
  try
    Result := StrToBool(S);
  except
    S := Trim(AnsiUpperCase(S));
    if (S = 'ДА') or (S = 'YES') or (S = '1') then
      Result := true
    else if (S = 'НЕТ') or (S = 'NO') or (S = '0') {or (S = '')} then
      Result := false
    else
      raise EConvertError.Create('String to boolean conversion error!')
    ;
  end
  ;
end;

{ TFetcherThread }

constructor TFetcherThread.CreateFetcherThread(const hReadPipe: cardinal;
  Owner: TMPlayer);
begin
  inherited Create;

  FhReadPipe := hReadPipe;
  FOwner := Owner;
  FreeOnTerminate := True;

  DoneFlag.ResetEvent;
end;

procedure TFetcherThread.Execute;
const
  cBuffSize = 1024;
var
  Buff: array [0 .. cBuffSize] of AnsiChar;
  BytesRead: cardinal;
  aErrLog: TextFile;
begin
  try
    while ReadFile(FhReadPipe, Buff[0], cBuffSize, BytesRead, nil) do begin
      Buff[BytesRead] := #0;
      Synchronize(
        procedure
        begin
          FOwner.AddToInputBuffer(String(Buff));
        end);
    end;

    Synchronize(FOwner.DoPlayEndConditional);

    DoneFlag.SetEvent;
  except
    on E: Exception do begin
      AssignFile(aErrLog, 'err2.txt');
      Rewrite(aErrLog);
      Writeln(aErrLog, E.Message);
      CloseFile(aErrLog);
    end;
  end;
end;

{ TMPlayer }

constructor TMPlayer.Create(AOwner: TComponent);
var
  jpgLogo: TJpegImage;
  RStream: TResourceStream;
begin
  inherited;

  FBrandNewInstance := true;

  Align := alClient;
  TabStop := true;

  BevelInner := bvNone;
  BevelOuter := bvNone;

  Color := clWhite;

  FAudioOut := maoWin32;
  FMute := false;

  FMPlayerPath := '';

  FUseAccel := True;

  FLogForm := TfmLog.Create(Self);
  FPropListForm := TfmPropList.Create(Self);

  FTimer := TTimer.Create(nil);
  with FTimer do begin
    Enabled := false;
    Interval := CTimerInterval; // 1000;
    OnTimer := DoTimer;
    Enabled := True;
  end;

  FTimerScreenShotWaiter := TTimer.Create(nil);
  with FTimerScreenShotWaiter do begin
    Enabled := false;
    Interval := 500;
    OnTimer := DoTimerScreenShotWaiter;
  end;

  FLogoImage := TImage.Create(Self);
  with FLogoImage do begin
    Parent := Self;
    Align := alClient;
    //AutoSize := true;
    Center := true;
    Proportional := true;
    Stretch := true;

    RStream := TResourceStream.Create(HInstance, 'PLAYERLOGO', RT_RCDATA);
    try
      jpgLogo := TJpegImage.Create;
      try
        jpgLogo.LoadFromStream(RStream);
        Picture.Graphic := jpgLogo;
       finally
         jpgLogo.Free;
       end;
    finally
      RStream.Free;
    end;

  end;
end;

destructor TMPlayer.Destroy;
begin
  FTimer.Free;

  FreeAndNil(FPropListForm);
  FreeAndNil(FLogForm);

  TerminateMPlayer;

  inherited;
end;

procedure TMPlayer.DoTimer(ASender: TObject);
begin
  ProcessInputBuffer; // parse buffered mplayer output

  if Active and not Paused then begin
    Req_TimePos; //note: sending GetTime to mplayer discards its pause mode (true for every command?)
    // mplayer is in running state here (always) due to Req_TimePos
    //GetMPProperty(CProp_Mute);
    if FPausePostponed then begin
      FPausePostponed := false;
      if FPaused then SendCommand(CMD_PAUSE); // we need to maintain conformity with mplayer pause state and FPaused value, so stop mplayer
    end;
  end;
end;

procedure TMPlayer.DoTimerScreenShotWaiter(ASender: TObject);
var
  sr: TSearchRec;
  FileAttrs: Integer;
  i: integer;
  afiles: array of string;
  fn, fnb: string;
begin
  FTimerScreenShotWaiter.Enabled := false;


  i := 0;
  FileAttrs := 0;
  if FindFirst(IncludeTrailingPathDelimiter(MPlayerPath)+'*.png', FileAttrs, sr) = 0 then begin
    repeat
      inc(i);
      SetLength(afiles, i);
      afiles[i-1] := IncludeTrailingPathDelimiter(MPlayerPath)+sr.Name;
    until FindNext(sr) <> 0
    ;
    FindClose(sr);
  end
  ;

  if i > 0 then begin
    fn :=  ChangeFilePath(afiles[i-1], SnapshotFolder);
    try
      TFile.Move(afiles[i-1], fn);
    except
      TFile.Delete(fn);
      TFile.Move(afiles[i-1], fn);
    end;
    //ShellExecute(0, 'open', PWideChar(afiles[i-1]), '','',SW_SHOWNORMAL);
    ShellExecute(0, 'open', PWideChar(fn), '','',SW_SHOWNORMAL);
  end
  ;

end;



procedure TMPlayer.SetPercentPositionFromASlider(const PP: real);
// some magic for use with a slider control
begin
  if (not Active) or FSliderUpdating or (PP>100.0) or (PP<0.0) then Exit;
  FSliderDelay := CSliderDelay;
  SendCommand(CMD_SEEK + ' ' + GetStrFromReal(PP) + ' 1');
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

procedure TMPlayer.SetPaused(const NewVal: boolean);
begin
  if Active and (NewVal <> FPaused) then begin // we need to maintain conformity with mplayer pause state and FPaused value!
    SendCommand(CMD_PAUSE); //this command toggles pause mode, so we need (NewVal <> FPaused) check
    FPaused := NewVal;
  end;
end;

function TMPlayer.GetPaused: boolean;
begin
  Result := FPaused and not FPausePostponed;
end;

procedure TMPlayer.Req_TimePos;
begin
  SendCommand(CMD_GetTimePos);
  FPositionRequestTime := GetTickCount;
end;

function TMPlayer.Check_TimePos(const MPOLine: string): boolean;

  procedure UpdateSlider(PP: real);
  // some magic for use with a slider control
  begin
    if FSliderDelay > 0 then begin
      FSliderDelay := FSliderDelay - 1;
    end else begin
      FSliderUpdating := True;
      try
        if Assigned(FOnSliderUpdate) then FOnSliderUpdate(PP);
      finally
        FSliderUpdating := false;
      end;
    end;
  end;

var
  s: string;
  TimePos, TimeLagMSec: cardinal;
  PercentPos: real;
begin
  Result := GetNamedValue(CPref_TimePos, MPOLine, s);

  if Result then begin
    TimePos := Round(GetRealFromStrDef(s) * 1000);
    if FPositionRequestTime = 0 then
      TimeLagMSec := 0
    else
      TimeLagMSec := GetTickCount - FPositionRequestTime;
    // to do: make correction according to playback speed
    ;
    TimePos := TimePos + TimeLagMSec - StartTime;

    {try
      PercentPos := RoundTo((TimePos / Duration) * 100, -1);
    except
      PercentPos := 0.0;
    end;}
    PercentPos := Duration;
    if (PercentPos > 0) then
      PercentPos := RoundTo((TimePos / PercentPos) * 100, -1)
    ;

    UpdateSlider(PercentPos);

    if Assigned(FOnChangePosition) then FOnChangePosition(TimePos, PercentPos);
  end;
end;

function TMPlayer.GetDuration: cardinal;
begin
  if Active then
    Result := Round(GetRealFromStrDef(FPropListForm.ValByName(CPN_Length)) * 1000)
  else
    Result := 0
  ;
end;

function TMPlayer.GetStartTime: cardinal;
begin
  if Active then
    Result := Round(GetRealFromStrDef(FPropListForm.ValByName(CPN_StartTime)) * 1000)
  else
    Result := 0
  ;
end;

function TMPlayer.GetNamedValue(const Pref, Src: string;
out Val: string): boolean;
var
  i: integer;
  s, spref: string;
begin
  s      := UpperCase(Trim(Src));
  spref  := UpperCase(Pref);
  i := Pos(spref, s);
  if i = 1 then begin
    Result := True;
    Val := Trim(Copy(s, i + Length(Pref), Length(s) - Length(Pref)));
  end else begin
    Result := false;
    Val := '';
  end;
end;

procedure TMPlayer.ProcessInputBuffer;

  function ExtractMPOutputLine(var MPOLine: string): boolean;
  var
    iEOL: integer;
  begin
    Result := false;
    iEOL := Pos(#10, FPlayerOutputBuff);
    if iEOL > 0 then begin
      Result := True;
      MPOLine := Copy(FPlayerOutputBuff, 1, iEOL - 1);
      Delete(FPlayerOutputBuff, 1, iEOL);
    end;
  end;

  procedure ProcessMPOutputLine(const MPOLine: string);
  begin
    FLogForm.Add(MPOLine);

    // ловля ответов на запросы типа get_time_pos:
    // commands (like get_time_pos) response catching
    if Check_TimePos(MPOLine) then Exit;
    if CheckSpeed(MPOLine) then Exit;
    if CheckMute(MPOLine) then Exit;
    if CheckFrameDrop(MPOLine) then Exit;
    if CheckVolume(MPOLine) then
      Exit;
    if CheckStartingPlayback(MPOLine) then Exit;
    if CheckAudioDelay(MPOLine) then Exit;

    // разбор и логгирование вывода плеера, не являющегося ответом на запросы - свойств медиа-файла
    // other  mplayer output (i.e. media properies values) parsing and logging
    FPropListForm.ParseLine(MPOLine);
    // FLogForm.Add(MPOLine);

  end;

var
  MPOutputLine: string;
begin
  while ExtractMPOutputLine(MPOutputLine) do
    ProcessMPOutputLine(MPOutputLine)
  ;
end;

procedure TMPlayer.AddToInputBuffer(SNewPortion: string);
begin
  FPlayerOutputBuff := FPlayerOutputBuff + SNewPortion;
end;

procedure TMPlayer.OpenMedia;

  function GetFontKey: string;
  var
    WinDir: array [0 .. MAX_PATH] of char;
  begin
    GetWindowsDirectory(@WinDir[0], MAX_PATH);
    Result := IncludeTrailingPathDelimiter(WinDir) + 'Fonts\' + COSDFont;
    if FileExists(Result) then
      Result := ' -font ' + DblQuotedStr(Result)
    else
      Result := ''
    ;
  end;

var
  CmdLine: string;
  sattr: TSecurityAttributes;
  hDummy1, hDummy2: THandle;
  sinf: TStartupInfo;
  pinf: TProcessInformation;

begin
  RaiseIfOpen;
  if not MPlayerExists then raise EMPlayerExeption.Create('mplayer not found');
  if Trim(FMediaAddr) = '' then raise EMPlayerExeption.Create('media data not found');

  Init;

  CmdLine :=
    DblQuotedStr(IncludeTrailingPathDelimiter(MPlayerPath) + CMPlayerExeName)
  + ' -quiet -slave -identify' + ' -wid ' + IntToStr(Self.Handle)
  + ' -colorkey 0x101010'
  + ' -nofontconfig'
  //+ GetFontKey
  //+ ' -softvol' //key to prevent volume state storing in driver
  ;


  //CmdLine := CmdLine + ' -demuxer 35';
  //14.05.14
  //if FReIndex then CmdLine := CmdLine + ' -idx';
  // -autosync 100'
  //+ ' -colorkey 0x101010' + ' -framedrop -autosync 100'

  CmdLine := CmdLine + ' -demuxer lavf';
  CmdLine := CmdLine + ' -lavdopts wait_keyframe';
  if UseAccel then
    CmdLine := CmdLine + ' -vo direct3d' //win 7 blinks and turns off aero without this key
  else
    CmdLine := CmdLine + ' -vo directx'  //to run on virtual machine
  ;
  //CmdLine := CmdLine + ' -hardframedrop';

  CmdLine := CmdLine + ' -vf screenshot';
  case FAudioOut of
    maoNoSound:
      CmdLine := CmdLine + ' -nosound';
    maoNull:
      CmdLine := CmdLine + ' -ao null';
    maoWin32:
      CmdLine := CmdLine + ' -ao win32';
    maoDsSound:
      CmdLine := CmdLine + ' -ao dsound';
  end;

  //CmdLine := CmdLine + ' -nosound';

  case FAspect of
    mpaAutoDetect:
      begin
      end;
    mpa40_30:
      CmdLine := CmdLine + ' -aspect 4:3';
    mpa160_90:
      CmdLine := CmdLine + ' -aspect 16:9';
    mpa235_10:
      CmdLine := CmdLine + ' -aspect 2.35:1';
  end;

  if not(FParams = '') then
    CmdLine := CmdLine + ' ' + FParams;

  CmdLine := CmdLine + ' ' + DblQuotedStr(FMediaAddr);

  FLogForm.Add('command line:');
  FLogForm.Add(CmdLine);
  FLogForm.Add('');

  with sattr do begin
    nLength := SizeOf(sattr);
    lpSecurityDescriptor := nil;
    bInheritHandle := True;
  end;
  CreatePipe(FReadPipe, hDummy1, @sattr, 0);

  with sattr do begin
    nLength := SizeOf(sattr);
    lpSecurityDescriptor := nil;
    bInheritHandle := True;
  end;
  CreatePipe(hDummy2, FWritePipe, @sattr, 0);

  FillChar(sinf, SizeOf(sinf), 0);
  with sinf do begin
    cb := SizeOf(sinf);
    dwFlags := STARTF_USESTDHANDLES;
    hStdInput := hDummy2;
    hStdOutput := hDummy1;
    hStdError := hDummy1;
  end;

  CreateProcess(nil, PChar(CmdLine), nil, nil, True, DETACHED_PROCESS, nil,
    PChar(IncludeTrailingPathDelimiter(MPlayerPath)), sinf, pinf);

  CloseHandle(hDummy1);
  CloseHandle(hDummy2);

  FPlayerProcess := pinf.hProcess;

  FFetcherThread := TFetcherThread.CreateFetcherThread(FReadPipe, Self);

  if FFullScreen then DoFullScreen(True);

  if FBrandNewInstance then begin //for a first clip mute state must be always off

    FBrandNewInstance := false;

    FPaused := false;
    FPausePostponed := false;

    SendCommand(CMD_MUTE + ' 1');
    SendCommand(CMD_MUTE + ' 0');
    FMute := false;

    FSpeed := 1;
  end else begin //synchronize FMute and MPlayer mute state on a new clip run
    if FPaused then
      FPausePostponed := true
    ;

    if FMute then
      SendCommand(CMD_MUTE + ' 1')
    else
      SendCommand(CMD_MUTE + ' 0')
    ;

    if FSpeed <> 1 then
      SendCommand(CMD_SPEEDSET + ' ' + GetStrFromReal(FSpeed))
    ;
  end
  ;
end;

procedure TMPlayer.Init;
begin
  FAudioDelay := 0;

  FFrameDrop := mfdEnabled;
  FOsdLevel := 1;
  FVolume := 10;

  FPropListForm.Clear;

  FPlayerOutputBuff := '';
end;

procedure TMPlayer.DoPlayEnd;
begin
  FPlayerProcess := 0;

  CloseHandle(FReadPipe);
  FReadPipe := 0;
  CloseHandle(FWritePipe);
  FWritePipe := 0;

  //Init;

  if FFullScreen then DoFullScreen(false);
  //Visible := false;
  //Visible := True;

  if Assigned(FOnPlayEnd) then FOnPlayEnd(Self);
end;

procedure TMPlayer.DoPlayEndConditional;
begin
  if (FPlayerProcess <> 0) then
    DoPlayEnd
  ;
end;

procedure TMPlayer.DoScreenShot;
begin
  SendCommand(CMD_SCREENSHOTONCE);
  if not Paused then
    Paused := true
  else
    FPausePostponed := true  // pospones pause state (if it's active) for one timer interval
  ;

  FTimerScreenShotWaiter.Enabled := true;
end;

procedure TMPlayer.CloseMedia;
//var i: integer;
begin
  // просигналить FetcherThread, чтобы тихо (без выполнения очистки) вырубилась
  // CheckSynchronize (на случай, если она уже зарядила код очистки на ожидание)
  //Как работает Synchronize и CheckSynchronize? Возможен ли вызов Synchronize, пока не завершилась процедура в главном потоке?

    SendCommand(CMD_QUIT);

    if (WaitForSingleObject(FPlayerProcess, 1000) = WAIT_TIMEOUT) then begin
      TerminateMPlayer;
      //raise Exception.Create('mplayer not responding');
    end
    ;

    while DoneFlag.WaitFor(0) = wrTimeout do CheckSynchronize;

    {i:=0;
    while DoneFlag.WaitFor(0) = wrTimeout do begin
      Inc(i);
      CheckSynchronize;
    end;
    ShowMessage(IntToStr(i));
    }

    {CheckSynchronize(1000);
    if DoneFlag.WaitFor(0) <> wrSignaled then raise Exception.Create('qqqqq');
    }
    // проверить, выполнен ли код очистки (Acyive), - если нет, то
    // явно (синхронно) выполнить очистку.
    //DoPlayEndConditional;
end;

procedure TMPlayer.TerminateMPlayer;
begin
  if Active then
    TerminateProcess(FPlayerProcess, cardinal(-1))
  ;
end;

procedure TMPlayer.SetActive(const NewVal: boolean);
begin
  if NewVal = Active then Exit;

  if NewVal then OpenMedia
  else CloseMedia
  ;
end;

function TMPlayer.GetActive: boolean;
begin
  Result := not(FPlayerProcess = 0);
end;

procedure TMPlayer.BruteTerminate;
begin
  TerminateMPlayer;
end;

function TMPlayer.CheckAudioDelay(const MPOLine: string): boolean;
var
  s: string;
begin
  Result := GetNamedValue(CPref_AudioDelay, MPOLine, s);
  if Result then begin
    FAudioDelay := GetRealFromStrDef(s);

    if FOsdLevel > 0 then // чтобы новое значение отобразилось в окне проигрывателя?
      SendCommand('osd_show_property_text AudioDelay:' + GetStrFromReal(FAudioDelay))
    ;
  end;
end;

procedure TMPlayer.SetAudioDelay(const NewVal: real);
var
  r: real;
begin
  if      NewVal < -10 then r := -10
  else if NewVal >  10 then r :=  10
  else                      r := NewVal
  ;
  if r = FAudioDelay then Exit;

  //SendCommand(CMD_AUDIODELAY + ' ' + GetStrFromReal(AValue)); можно двумя способами?
  SetMPProperty(CProp_AudioDelay, GetStrFromReal(r));
  GetMPProperty(CProp_AudioDelay); // to catch new value by CheckAudioDelay and to be sure the property was set

  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

function TMPlayer.CheckFrameDrop(const MPOLine: string): boolean;
var
  s: string;
begin
  Result := GetNamedValue(CPref_FrameDrop, MPOLine, s);
  if Result then begin
    case s[1] of
      '0':
        FFrameDrop := mfdDisabled;
      '1':
        FFrameDrop := mfdEnabled;
      '2':
        FFrameDrop := mfdHard;
    end;
  end;
end;

procedure TMPlayer.SetFrameDrop(const NewVal: TMPlayerFrameDrop);
begin
  if NewVal = FFrameDrop then Exit;
  case NewVal of
    mfdEnabled:  SendCommand(CMD_FRAMEDROP + ' 1');
    mfdHard:     SendCommand(CMD_FRAMEDROP + ' 2');
    mfdDisabled: SendCommand(CMD_FRAMEDROP + ' 0');
  end;
  GetMPProperty(CProp_FrameDrop);
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

procedure TMPlayer.SendFrameDropToogle;
begin
  SendCommand(CMD_FRAMEDROP);
  GetMPProperty(CProp_FrameDrop);
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

function TMPlayer.CheckMute(const MPOLine: string): boolean;
var
  s: string;
begin
  Result := GetNamedValue(CPref_Mute, MPOLine, s);
  if Result then begin
    FMute := StrToBoolMy(s);
  end;
end;

procedure TMPlayer.SetMute(const AValue: boolean);
begin
  if AValue = FMute then Exit;

  SendCommand(CMD_MUTE + ' ' + IntToStr(Ord(AValue)));
  GetMPProperty(CProp_Mute);
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

function TMPlayer.CheckSpeed(const MPOLine: string): boolean;
var
  s: string;
begin
  Result := GetNamedValue(CPref_Speed, MPOLine, s);
  if Result then begin
    FSpeed := GetRealFromStrDef(s);
    if Assigned(FOnChangeSpeed) then FOnChangeSpeed(FSpeed);
  end;
end;

procedure TMPlayer.SetSnapshotFolder(const Value: string);
begin
  FSnapshotFolder := Value;
end;

procedure TMPlayer.SetSpeed(const NewVal: real);
begin
  SendCommand(CMD_SPEEDSET + ' ' + GetStrFromReal(NewVal));
  FSpeed := NewVal;

  {SetMPProperty(CProp_Speed, GetStrFromReal(NewVal));
  GetMPProperty(CProp_Speed);}

  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

procedure TMPlayer.SetUseAccel(const Value: boolean);
begin
  FUseAccel := Value;
end;

procedure TMPlayer.SendSpeedIncr(const AValue: real);
begin
  SendCommand(CMD_SPEEDINCR + ' ' + GetStrFromReal(AValue));
  GetMPProperty(CProp_Speed);
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

procedure TMPlayer.SendSpeedMult(const AValue: real);
begin
  SendCommand(CMD_SPEEDMULT + ' ' + GetStrFromReal(AValue));
  GetMPProperty(CProp_Speed);
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

function TMPlayer.CheckStartingPlayback(const MPOLine: string): boolean;
var
  s: string;
begin
  Result := GetNamedValue(CPref_StartingPlayback, MPOLine, s);
  if Result and Assigned(FOnStartPlay) then FOnStartPlay(Self);
end;

function TMPlayer.CheckVolume(const MPOLine: string): boolean;
var
  s: string;
begin
  Result := GetNamedValue(CPref_Volume, MPOLine, s);
  if Result then begin
    FVolume := GetRealFromStrDef(s);
    if FOsdLevel > 0 then
      SendCommand('osd_show_property_text volume:' + GetStrFromReal(FVolume))
    ;
  end;
end;

procedure TMPlayer.SetVolume(const AVolume: real);
begin
  if AVolume = FVolume then
    Exit;
  SetMPProperty('volume', GetStrFromReal(AVolume));
  GetMPProperty('volume');
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

procedure TMPlayer.SetOsdLevel(const NewVal: integer);
begin
  if NewVal = FOsdLevel then Exit;

  if      NewVal < 0 then
    FOsdLevel := 0
  else if NewVal > 3 then
    FOsdLevel := 3
  else
    FOsdLevel := NewVal
  ;
  SendCommand(CMD_OSD + ' ' + IntToStr(FOsdLevel));
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

procedure TMPlayer.SetAspect(const AAspect: TMPlayerAspect);
begin
  if AAspect = FAspect then Exit;
  RaiseIfOpen;
  FAspect := AAspect;
end;

procedure TMPlayer.SetAudioOut(const AAudioOut: TMPlayerAudioOut);
begin
  if AAudioOut = FAudioOut then Exit;
  RaiseIfOpen;
  FAudioOut := AAudioOut;
end;

procedure TMPlayer.Send_FrameStep;
begin
  SendCommand(CMD_FRAMESTEP);
end;

{procedure TMPlayer.SeekBack;
begin
  SendCommand(CMD_SEEK + ' -1 0');
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;}

procedure TMPlayer.SeekBy(TimeShift: Integer);
var sval: string;
begin
  sval := IntToStr(TimeShift);
  SendCommand(CMD_SEEK + ' ' + sval + ' 0');
  FPausePostponed := true;  // pospones pause state (if it's active) for one timer interval
end;

procedure TMPlayer.SendCommand(const ACommand: String);
var
  Dummy: cardinal;
  Cmd: AnsiString;
begin
  if not Active then
    raise EMPlayerExeption.Create('mplayer not active, can''t send command' + sLineBreak + ACommand)
  ;
  Cmd := AnsiString(ACommand) + #10;
  WriteFile(FWritePipe, Cmd[1], Length(Cmd), Dummy, nil);
  //FLogForm.Add('-->'+ACommand);
end;


procedure TMPlayer.SetMPProperty(const APropName, APropValue: string);
begin
  if not Active then
    raise EMPlayerExeption.Create('mplayer not active, can''t set property' + sLineBreak + APropName)
  ;
  SendCommand('set_property' + ' ' + APropName + ' ' + APropValue);
end;

procedure TMPlayer.GetMPProperty(const APropName: string);
begin
  if not Active then
    raise EMPlayerExeption.Create('mplayer not active, can''t get property' + sLineBreak + APropName)
  ;
  SendCommand('get_property' + ' ' + APropName);
end;

function TMPlayer.GetLogVisible: boolean;
begin
  Result := FLogForm.Visible;
end;

function TMPlayer.GetPropFormVisible: boolean;
begin
  Result := FPropListForm.Visible;
end;

function TMPlayer.MPlayerExists: boolean;
var sPath: string;
begin
  sPath := IncludeTrailingPathDelimiter(MPlayerPath) + CMPlayerExeName;
  Result := FileExists(sPath);
end;

procedure TMPlayer.RaiseIfOpen;
begin
  if Active then
    raise EMPlayerExeption.Create('media open, close first')
  ;
end;

procedure TMPlayer.SetFullScreen(const AFullScreen: boolean);
begin
  if AFullScreen = FFullScreen then
    Exit;
  FFullScreen := AFullScreen;
  if not Active then
    Exit;
  DoFullScreen(FFullScreen);
end;

procedure TMPlayer.SetLogVisible(const AVisible: boolean);
begin
  FLogForm.Visible := AVisible;
end;

procedure TMPlayer.SetMediaAddr(const AMediaAddr: string);
begin
  if AMediaAddr = FMediaAddr then Exit;
  RaiseIfOpen;
  FMediaAddr := AMediaAddr;
end;

procedure TMPlayer.SetParams(const AParams: string);
begin
  if AParams = FParams then Exit;
  RaiseIfOpen;
  FParams := AParams;
end;

procedure TMPlayer.SetMPlayerPath(const AMPlayerPath: string);
begin
  if AMPlayerPath = FMPlayerPath then Exit;
  RaiseIfOpen;
  //ShowMessage(AMPlayerPath);
  FMPlayerPath := AMPlayerPath;
end;

procedure TMPlayer.SetPropFormVisible(const Value: boolean);
begin
  FPropListForm.Visible := Value;
end;

procedure TMPlayer.SetReIndex(const AReIndex: boolean);
begin
  if AReIndex = FReIndex then Exit;
  RaiseIfOpen;
  FReIndex := AReIndex;
end;

procedure TMPlayer.DoFullScreen(const AFullScreen: boolean);
var
  HWNDDesk: HWND;
begin
  SetFocus; //to prevent intercept cm_dialogchar by some stuped components like TListView
  if AFullScreen then begin
    FHWNDCur := GetParent(Handle);
    HWNDDesk := GetDesktopWindow;
    Windows.SetParent(Handle, HWNDDesk);
    Align := alNone;
    SetWindowPos(Handle, HWND_TOPMOST, 0, 0, Screen.Width, Screen.Height,
      SWP_SHOWWINDOW);
  end else begin
    SetWindowPos(Handle, HWND_NOTOPMOST, 0, 0, Screen.Width, Screen.Height,
      SWP_SHOWWINDOW);
    Windows.SetParent(Handle, FHWNDCur);
    Align := alClient;
  end;
end;

procedure TMPlayer.CMDialogChar(var Message: TCMDialogChar);
begin
  if (Message.CharCode = VK_ESCAPE) then begin
    if FFullScreen and Active then SetFullScreen(false);
    Exit;
  end;

  inherited;
end;

function TMPlayer.GetRealFromStrDef(const StrVal: string; const DefVal: real): real;
var
  fs: TFormatSettings;
begin
  fs := TFormatSettings.Create; // record, so no need to free
  fs.DecimalSeparator := CDecimalSeparator;

  Result := StrToFloatDef(StrVal, DefVal, fs);
end;

function TMPlayer.GetStrFromReal(const RealVal: real): String;
var
  fs: TFormatSettings;
begin
  fs := TFormatSettings.Create; // record, so no need to free
  fs.DecimalSeparator := CDecimalSeparator;

  Result := FormatFloat('0.00', RealVal, fs);
end;

function TMPlayer.DblQuotedStr(const S: string): string;
begin
  Result := '"' + S + '"';
end;

initialization
  DoneFlag := TEvent.Create(nil, true, false, '');

end.
