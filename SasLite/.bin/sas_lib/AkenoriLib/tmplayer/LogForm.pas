unit LogForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type

  TfmLog = class(TForm)
    pnl: TPanel;
    btnClear: TButton;
    btnClose: TButton;
    mm: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnClearClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Add(const ALogLine: string);
  end;


implementation

{$R *.dfm}

{ TfmLog }

procedure TfmLog.Add(const ALogLine: string);
begin
  mm.Lines.Add(ALogLine);
end;

procedure TfmLog.btnClearClick(Sender: TObject);
begin
  mm.Clear;
end;

procedure TfmLog.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmLog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

end.
