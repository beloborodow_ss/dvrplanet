unit TimeSlider;

interface

uses
   Windows, Messages, Classes, Controls, Forms,
   Vcl.Graphics,
   GR32, GR32_Image, GR32_Blend, GR32_Polygons, GR32_PolygonsEx, GR32_Lines, GR32_Text,
   TimeSliderLib;

type
  TTSZone = (zNone, zBtnPrev, zTrackPrev, zHandle, zTrackNext, zBtnNext);

  TTimeSliderChangeEvent = procedure(Sender: TObject; const NewPosition: Real) of object;
  TTimeSliderButtonClick = procedure(Sender: TObject; const Right: Boolean) of object;

  TTimeSlider = class(TCustomPaintBox32)
  private
    FFramed: boolean;
    FBackColor: TColor;

    FLeftMargin:        UInt16;
    FRightMargin:       UInt16;
    fCaptionHeight:     UInt16;
    fBarHeight:         UInt16;
    FButtonWidth:       UInt16;
    FButtonGap:         UInt16;
    FSliderHandleWidth: UInt16;
    fLabelFontHeight:   UInt16;
    fMinLabelWidth:     UInt16;
    fCaptFontHeight:    UInt16;
    fTicksGap:          UInt16;
    fTicksLen:          UInt16;

    FCaption: string;
    FttfSegoeLarge, FttfSegoeSmall: TTrueTypeFont;
    FPosition:    Real;
    FLargeChange: Real;
    FSmallChange: Real;
    FOnUserChange: TTimeSliderChangeEvent;

    fDragZone: TTSZone;
    fHotZone:  TTSZone;
    fXBeforeDrag, fYBeforeDrag: Integer;
    fPosBeforeDrag: Real;
    fGenChange: Boolean;
    FOnButtonClick: TTimeSliderButtonClick;

    fChartData: TTSChartData;
    fText32: TText32;
    fLine32: TLine32;
    FCaptSpeed: string;
    FCaptAccel: string;

    function GetZeroXOffset: UInt16;
    function GetZeroYOffset: UInt16;
    function GetVisorShift:  UInt16;
    function GetZone(X, Y: Integer): TTSZone;
    function ClientXToPosition(X: Integer): Real;
    function GetChartRect:   TRect;
    function GetCaptionBar:  TRect;
    function GetBottomBar:   TRect;
    function GetSliderTrack: TRect;
    function GetSliderDiap:  UInt16;
    function GetZoneRect(Zone: TTSZone): TRect;
    function GetPlotRect: TRect;
    function GetVertScales: TTSVertScale;
    function GetHorzScale: TTSHorzScale;
    function PointsPerSecond: Real;
    function GetLeftMargin:  UInt16;
    function GetRightMargin: UInt16;
    function GetDelta: integer;

    procedure StopHotTracking;

    procedure SetFramed(const Value: boolean);
    procedure SetBackColor(const Value: TColor);
    procedure SetCaption(const Value: string);
    procedure SetOnUserChange(const Value: TTimeSliderChangeEvent);
    procedure SetPosition(Value: Real);
    procedure SetLargeChange(const Value: Real);
    procedure SetSmallChange(const Value: Real);

    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure SetOnButtonClick(const Value: TTimeSliderButtonClick);
    function GetMaxTime: Real;
    function GetTimeStep: Real;
    procedure SetCaptAccel(const Value: string);
    procedure SetCaptSpeed(const Value: string);
  protected
    procedure DoChange;
    procedure DoPaintBuffer; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure GenerateTestData(TestAmpl: Real = 176.0; SampleCount: UInt16 = 1000);
    procedure SetData(aData: array of TTSDataSample; aTimeStep: Real);
  published
    property Align;
    property BackColor: TColor read FBackColor write SetBackColor default clWhite;
    property Caption: string read FCaption write SetCaption;
    property CaptSpeed: string read FCaptSpeed write SetCaptSpeed;
    property CaptAccel: string read FCaptAccel write SetCaptAccel;
    property Framed: boolean read FFramed write SetFramed default False;
    property LargeChange: Real read FLargeChange write SetLargeChange;
    property SmallChange: Real read FSmallChange write SetSmallChange;
    property TimeStep:    Real read GetTimeStep;
    property MaxTime:     Real read GetMaxTime;
    property Position:    Real read FPosition write SetPosition;
    property OnButtonClick: TTimeSliderButtonClick read FOnButtonClick write SetOnButtonClick;
    property OnUserChange: TTimeSliderChangeEvent read FOnUserChange write SetOnUserChange;
  end;

procedure Register;

implementation

uses Dialogs, Math, SysUtils;

{$I ..\AkenIndi.inc}


procedure Register;
begin
  RegisterComponents('Graphics32', [TTimeSlider]);
end;

{ TTimeSlider }

constructor TTimeSlider.Create(AOwner: TComponent);
//var
//  fontSmoothingEnabled: longBool;
begin
  inherited;
  FBackColor := clWhite;

  //enabling LCDDraw usually improves the sharpness and clarity of the text,
  //especially on LCD displays.
  //Here we get the OS to decide whether to use LCD font smoothing ...
  //(However, try setting this variable to false and see the difference!)

  //SystemParametersInfo(SPI_GETFONTSMOOTHING, 0, @fontSmoothingEnabled, 0);
  //GR32_Text.Text32LCDDrawDefault := fontSmoothingEnabled;

  fLabelFontHeight := 18;
  fCaptFontHeight  := 21;
  fMinLabelWidth   := 40;

  FttfSegoeSmall := TrueTypeFontClass.Create('Segoe UI', fLabelFontHeight);
  FttfSegoeLarge := TrueTypeFontClass.Create('Segoe UI', fCaptFontHeight);

  FLeftMargin    := 18;
  FRightMargin   := 18;
  fCaptionHeight := 30;
  fBarHeight     := 30;

  FButtonWidth := 26;
  FButtonGap   := 4;
  FSliderHandleWidth := 21;

  FPosition    := 0;
  FSmallChange := 1;
  FLargeChange := 10;

  fTicksGap := 2;
  fTicksLen := 4;

  fDragZone    := zNone;
  fHotZone     := zNone;

  fGenChange := false;
  //MinClientWidth

  fLine32    := TLine32.Create;
  fText32    := TText32.Create;
  fChartData := TTSChartData.CreateChartData;

  FCaption   := '';
  FCaptSpeed := 'Speed, km/h';
  FCaptAccel := 'Acceleration, m/s^2';
end;

destructor TTimeSlider.Destroy;
begin
  fChartData.Free;
  fText32.Free;
  fLine32.Free;

  inherited;
end;

function TTimeSlider.ClientXToPosition(X: Integer): Real;
begin
  //Result := Round((X - GetZeroXOffset) / PointsPerSecond);
  Result := (X - GetZeroXOffset) / PointsPerSecond;
end;

procedure TTimeSlider.CMMouseLeave(var Message: TMessage);
begin
  StopHotTracking;
  inherited;
end;

procedure TTimeSlider.DoChange;
begin
  //if Assigned(FOnChange) then FOnChange(Self);
  if fGenChange and Assigned(FOnUserChange) then FOnUserChange(Self, Position);
end;

procedure TTimeSlider.DoPaintBuffer;

  procedure PaintBackground;
  var
    lCol32: TColor32;
    lRect, lRect1:  TRect;
    xr, yr: Real;
    s: string;
  begin
    lRect  := ClientRect;
    lRect1 := ClientRect;

    { Paint frame border }
    if Framed then
    begin
      lCol32 := TColor32(clWindowFrame);
      Buffer.FrameRectS(lRect, lCol32);
      InflateRect(lRect, -1, -1);
    end;

    { Fill the background }
    lCol32 := Color32(BackColor);
    Buffer.FillRectS(lRect, lCol32);

    { Paint shadow on top of background}
    with lRect do begin
      Dec(Right);
      Dec(Bottom);
      lCol32 := Lighten(lCol32, -32);
      Buffer.HorzLineS(Left, Top, Right, lCol32);
      Buffer.VertLineS(Left, Top + 1, Bottom, lCol32);
      if not Framed then begin
        lCol32 := Lighten(lCol32, 64);
        Buffer.HorzLineS(Left, Bottom, Right, lCol32);
        Buffer.VertLineS(Right, Top + 1, Bottom, lCol32);
      end;
    end;

    lRect1 := GetCaptionBar;
    with fText32 do begin
      {Example: first, apply text transformations according to scrollbar sliders ...
      Scale(sbScaleX.Position/50, sbScaleY.Position/50);
      Skew(sbSkewX.Position/10, sbSkewY.Position/40);
      Angle := sbRotation.Position;
      }
      //Draw(Buffer, lRect1.Left,        fCaptionHeight-10, '������� �������� � ���������', FttfSegoeLarge, clAkGrayUltraDark);
      yr := fCaptionHeight/2 + GetTextHeight(FCaptAccel, FttfSegoeSmall)/2;
      xr := lRect1.Right - GetTextWidth(FCaptAccel, FttfSegoeSmall);
      Draw(Buffer, xr,  yr, FCaptAccel, FttfSegoeSmall, clAkGreenDark);

      //xr := xr - GetTextWidth(FCaptSpeed, FttfSegoeSmall) - 50;
      xr := lRect1.Left;
      Draw(Buffer, xr, yr, FCaptSpeed, FttfSegoeSmall, clAkRed);

      //s :=
    end;
  end;

  procedure PaintChartLayer1;
  var
    YScales: TTSVertScale;
    XScale:  TTSHorzScale;
    PlotRect: TRect;
    i, hstep, spstep, acstep, tmstep: integer;
    XZoom, SpeedZoom, AccelZoom: Real;

    procedure DoDrawYLabel(aSpeed, aAccel, aHeight: Integer);
    var
      xi,yi: Integer;
      xr,yr: Single;
      s: string;
    begin
      Buffer.HorzLineS(PlotRect.Left, PlotRect.Bottom - aHeight, PlotRect.Right, clAkGrayLight);

      yi := PlotRect.Bottom-aHeight;
      xi := PlotRect.Left;
      Buffer.HorzLineS(xi-fTicksGap-fTicksLen, yi, xi-fTicksGap, clAkGrayDark);
      xi := PlotRect.Right;
      Buffer.HorzLineS(xi+fTicksGap,           yi, xi+fTicksGap+fTicksLen, clAkGrayDark);

      with fText32 do begin
        s := IntToStr(aSpeed);
        yr := yi + GetTextHeight(s, FttfSegoeSmall) / 2;
        xr := PlotRect.Left - fTicksGap*4 - fTicksLen - GetTextWidth(s, FttfSegoeSmall);
        Draw(Buffer, xr, yr, s, FttfSegoeSmall, clAkRed);

        s := IntToStr(aAccel);
        xr := PlotRect.Right + fTicksGap*4 + fTicksLen;
        Draw(Buffer, xr, yr, s, FttfSegoeSmall, clAkGreenDark);
      end;
    end;

    procedure DoDrawXLabel(aTime, aPos: Integer);
    var
      xi,yi: Integer;
      xr,yr: Single;
      s: string;
    begin
      yi := PlotRect.Bottom + fTicksGap;
      xi := GetZeroXOffset + aPos;
      Buffer.VertLineS(xi, yi, yi+fTicksLen, clAkGrayLight);

      with fText32 do begin
        s := IntToStr(aTime);
        yr := PlotRect.Bottom + 2* fBarHeight div 3;
        xr := GetZeroXOffset + aPos - GetTextWidth(s, FttfSegoeSmall)/2;
        Draw(Buffer, xr, yr, s, FttfSegoeSmall, clAkGrayLight);
      end;
    end;

    procedure DoDrawCharts;
    var
      aofp: TArrayOfFloatPoint;
      i: integer;
    begin
      SetLength(aofp, fChartData.Count);
      for i := 0 to fChartData.Count-1 do begin
        aofp[i].X := GetZeroXOffset + i*fChartData.TimeStep*XZoom;
        aofp[i].Y := GetZeroYOffset - fChartData.DataSample[i].Speed * SpeedZoom;
      end
      ;
      PolyLineFS(Buffer, aofp, clAkRed, False, 1);

      for i := 0 to fChartData.Count-1 do begin
        aofp[i].Y := GetZeroYOffset - fChartData.DataSample[i].Accel * AccelZoom;
      end
      ;
      PolyLineFS(Buffer, aofp, clAkGreenDark, False, 1);
    end;

  begin
  //Draw these chart parts:
  //frame
  //bottom bar
  //horizontal grid and vertical labels and tics
    Buffer.FrameRectS(GetChartRect, clAkGrayLight);
    Buffer.FillRectS(GetBottomBar,  clAkGrayDark);

    PlotRect := GetPlotRect;
    YScales  := GetVertScales;
    XScale   := GetHorzScale;

    //XZoom := PlotRect.Width  / fChartData.Count;
    XZoom := GetSliderDiap  / XScale.Duration;

    SpeedZoom := PlotRect.Height / YScales.MaxSpeed;
    AccelZoom := PlotRect.Height / YScales.MaxAccel;

    hstep    := PlotRect.Height  div YScales.Steps;
    spstep   := YScales.MaxSpeed div YScales.Steps;
    acstep   := YScales.MaxAccel div YScales.Steps;
    for i := 1 to Max(YScales.Steps - 1, 1) do
      DoDrawYLabel(i*spstep, i*acstep, i*hstep)
    ;

    hstep    := Round(PlotRect.Width / XScale.Steps);

    tmstep   := XScale.Duration div XScale.Steps;

    for i := 0 to Max(XScale.Steps, 1) do
      DoDrawXLabel(i*tmstep, i*hstep)
    ;

    DoDrawCharts;
  end;

  procedure PaintChartLayer2;
  begin
  end;

  procedure PaintChartLayer3;
  begin
    //axes:
    Buffer.VertLineS(GetZeroXOffset, GetBottomBar.Top-1, GetChartRect.Top+1, clAkGrayDark);
    Buffer.VertLineS(GetZeroXOffset+GetSliderDiap, GetBottomBar.Top-1, GetChartRect.Top+1, clAkGrayDark);
  end;

  procedure PaintSliderLayer;

    procedure DrawArrow(R: TRect; RightDir, Big: boolean; Color: TColor32);
    var
      X, Y, Sz, Shift: Integer;
      AFP : TArrayOfFixedPoint;
    begin
      X := (R.Left + R.Right - 1) div 2;
      Y := (R.Top + R.Bottom - 1) div 2;

      if Big then
        Sz := (Min(X - R.Left, Y - R.Top)) * 3 div 4 - 1
      else
        Sz := (Min(X - R.Left, Y - R.Top)) * 3 div 5 - 1
      ;
      if Sz = 0 then Sz := 1;

      SetLength(AFP, 3);

      if RightDir then begin
        Shift := - Sz * 1 div 3;
        Inc(X, Shift);
        AFP[0] := FixedPoint(X, Y + Sz);
        AFP[1] := FixedPoint(X + Sz, Y);
        AFP[2] := FixedPoint(X, Y - Sz);
      end else begin
        Shift := (Sz + 1) * 1 div 3;
        Inc(X, Shift);
        AFP[0] := FixedPoint(X, Y + Sz);
        AFP[1] := FixedPoint(X - Sz, Y);
        AFP[2] := FixedPoint(X, Y - Sz);
      end;

      PolygonXS(Buffer, AFP, Color);

    end;

    procedure DoDrawButton(RightDir, Pushed, Hot: Boolean);
    var
      lRect: TRect;
      btColor, arColor: TColor32;
    begin
      if RightDir then
        lRect := GetZoneRect(zBtnNext)
      else
        lRect := GetZoneRect(zBtnPrev)
      ;
      if GR32.IsRectEmpty(lRect) then Exit;

      if Pushed then begin
        btColor := clBlack32;
        arColor := clWhite32;
      end else if Hot then begin
        btColor := clBlack32;
        arColor := clAkGrayLight;
      end else begin
        btColor := clAkGrayUltraDark;
        arColor := clAkGrayLight;
      end;

      Buffer.FillRectS(lRect, btColor);
      DrawArrow(lRect, RightDir, Pushed, arColor);
    end;

    procedure DoDrawTrack(RightDir, Pushed, Hot: Boolean);
    begin
    end;

    procedure DoDrawSliderHandle(Pushed, Hot: Boolean);
    var
      lRect: TRect;
      slColor, lnColor: TColor32;
    begin
      lRect := GetZoneRect(zHandle);
      if GR32.IsRectEmpty(lRect) then Exit;

      //Buffer.FillRectS(lRect, clAkGrayLight);

      if Pushed then begin
        slColor := clAkGreenLight;
        lnColor := clAkGreenDark;
      end else if Hot then begin
        slColor := clWhite32;
        lnColor := clAkGrayDark;
      end else begin
        slColor := clAkGrayLight;
        lnColor := clAkGrayLight;
      end
      ;


      Buffer.VertLineS(lrect.Left + GetVisorShift, lrect.Top-1, GetChartRect.Top+1, lnColor);

      Buffer.VertLineS(lrect.Left + GetVisorShift, lrect.Bottom, lrect.Top, slColor);
      Buffer.FrameRectS(lRect, slColor);
      InflateRect(lRect,-1,-1);
      Buffer.FrameRectS(lRect, slColor);
    end;

  begin
    DoDrawButton(False, fDragZone = zBtnPrev, fHotZone = zBtnPrev);
    DoDrawButton(True,fDragZone = zBtnNext, fHotZone = zBtnNext);

    DoDrawTrack(False, fDragZone = zTrackPrev, fHotZone = zTrackPrev);
    DoDrawTrack(True,fDragZone = zTrackNext, fHotZone = zTrackNext);

    DoDrawSliderHandle(fDragZone = zHandle, fHotZone = zHandle);
  end;

begin
  inherited;

  PaintBackground;
  PaintChartLayer1;
  PaintChartLayer2;
  PaintSliderLayer;
  PaintChartLayer3;
end;

procedure TTimeSlider.GenerateTestData(TestAmpl: Real; SampleCount: UInt16);
begin
  fChartData.GenerateTestData(TestAmpl, SampleCount);
  Invalidate;
end;

function TTimeSlider.GetCaptionBar: TRect;
begin
  Result  := ClientRect;
  with Result do begin
    Left   := Left  + GetLeftMargin;
    Right  := Right - GetRightMargin;
    Bottom := fCaptionHeight - 1;
  end;
end;

function TTimeSlider.GetChartRect: TRect;
begin
  Result  := ClientRect;
  with Result do begin
    Left  := Left  + GetLeftMargin;
    Right := Right - GetRightMargin;
    Top   := fCaptionHeight;
  end;
end;

function TTimeSlider.GetDelta: integer;
var
  len, hstep: integer;
  ascale: TTSHorzScale;
begin
  len := ClientRect.Width - (FLeftMargin+FRightMargin);
  len := len - 2 * (FButtonWidth + FButtonGap);
  len := len - FSliderHandleWidth;

  ascale := fChartData.GetHorzScale(len, fMinLabelWidth);

  hstep    := Round(len / ascale.Steps);

  Result := len - hstep * ascale.Steps;
end;

function TTimeSlider.GetPlotRect: TRect;
begin
  with Result do begin
    Left   := GetZeroXOffset;
    Top    := GetChartRect.Top + 1;
    Right  := GetZeroXOffset + GetSliderDiap;
    Bottom := GetBottomBar.Top - 1;
  end;
end;

function TTimeSlider.GetBottomBar: TRect;
begin
  Result := GetChartRect;
  with Result do begin
    Top   := Bottom - fBarHeight + 1;
  end;
end;

function TTimeSlider.GetSliderTrack: TRect;
begin
  Result := GetBottomBar;
  with Result do begin
    Left  := Left  + FButtonWidth + FButtonGap;
    Right := Right - (FButtonWidth + FButtonGap);
  end;
end;

function TTimeSlider.GetTimeStep: Real;
begin
  Result := fChartData.TimeStep;
end;

function TTimeSlider.GetSliderDiap: UInt16;
var
  R: TRect;
  Len: Integer;
begin
  R   := GetSliderTrack;
  Len := R.Right - R.Left + 1;
  Result := Len - FSliderHandleWidth;
end;

function TTimeSlider.GetVertScales: TTSVertScale;
begin
  Result := fChartData.GetVertScales(GetPlotRect.Height, fLabelFontHeight);
end;

function TTimeSlider.GetHorzScale: TTSHorzScale;
begin
  Result := fChartData.GetHorzScale(GetPlotRect.Width, fMinLabelWidth);
end;

function TTimeSlider.GetLeftMargin: UInt16;
begin
  Result := FLeftMargin;
end;

function TTimeSlider.GetMaxTime: Real;
begin
  Result := fChartData.TimeStep * fChartData.Count;
end;

function TTimeSlider.GetRightMargin: UInt16;
begin
  Result := FRightMargin + GetDelta;
end;

function TTimeSlider.PointsPerSecond: Real;
begin
  Result := GetSliderDiap  / GetHorzScale.Duration;
end;

function TTimeSlider.GetVisorShift: UInt16;
begin
  Result := FSliderHandleWidth div 2;
end;

function TTimeSlider.GetZeroXOffset: UInt16;
begin
  Result := GetLeftMargin + FButtonWidth + FButtonGap + GetVisorShift; // be accurate!
end;

function TTimeSlider.GetZeroYOffset: UInt16;
begin
  Result := GetBottomBar.Top-1; // be accurate!
end;

function TTimeSlider.GetZoneRect(Zone: TTSZone): TRect;

  function GetSliderHandleRect: TRect;
  var
    LeftEdgeOffset: Integer;
  begin
    Result := GetSliderTrack;
    LeftEdgeOffset := Round(Position * PointsPerSecond);

    Inc(Result.Left, LeftEdgeOffset);
    Result.Right := Result.Left + FSliderHandleWidth - 1;
  end;

const
  CEmptyRect: TRect = (Left: 0; Top: 0; Right: 0; Bottom: 0);
var
  R: TRect;
begin
  case Zone of
    zNone: Result := CEmptyRect;
    zBtnPrev:
      begin
        Result       := GetBottomBar;
        Result.Right := Result.Left + FButtonWidth - 1
      end;
    zTrackPrev..zTrackNext:
      begin
        Result := GetSliderTrack;
        R := GetSliderHandleRect;
        case Zone of
          zTrackPrev: Result.Right := R.Left;
          zHandle:    Result := R;
          zTrackNext: Result.Left := R.Right;
        end;
      end;
    zBtnNext:
      begin
        Result       := GetBottomBar;
        Result.Left  := Result.Right - FButtonWidth + 1;
      end;
  end;
end;

function TTimeSlider.GetZone(X, Y: Integer): TTSZone;
var
  P: TPoint;
  R: TRect;
begin
  Result := zNone;

  P := Point(X, Y);
  R := ClientRect;
  if not GR32.PtInrect(R, P) then Exit;
  Result := zNone;

  R := GetBottomBar;
  if not GR32.PtInRect(R, P) then Exit;

  R := GetZoneRect(zHandle);
  if GR32.PtInRect(R, P) then begin
    Result := zHandle;
    Exit;
  end;

  R := GetZoneRect(zBtnPrev);
  if GR32.PtInRect(R, P) then begin
    Result := zBtnPrev;
    Exit;
  end;

  R := GetZoneRect(zBtnNext);
  if GR32.PtInRect(R, P) then begin
    Result := zBtnNext;
    Exit;
  end;

  R := GetZoneRect(zTrackPrev);
  if GR32.PtInRect(R, P) then begin
    Result := zTrackPrev;
    Exit;
  end;

  R := GetZoneRect(zTrackNext);
  if GR32.PtInRect(R, P) then begin
    Result := zTrackNext;
    Exit;
  end;

end;

procedure TTimeSlider.StopHotTracking;
begin
  fHotZone := zNone;
  Invalidate;
end;

procedure TTimeSlider.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
  if Button <> mbLeft then Exit;

  fDragZone := GetZone(X, Y);
  Invalidate;

  fXBeforeDrag := X;
  fYBeforeDrag := Y;
  fPosBeforeDrag := Position;
end;

procedure TTimeSlider.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
var
  dzOld: TTSZone;
begin
  inherited;

  dzOld := fDragZone;
  fDragZone := zNone;

  fGenChange := true;
  try

  if  (dzOld in [zTrackPrev, zTrackNext])
  and (GetZone(X, Y) = dzOld) then begin
    Position   := ClientXToPosition(X); //will call Invalidate and DoChange
  end else if  (dzOld in [zBtnPrev, zBtnNext])
  and (GetZone(X, Y) = dzOld) then begin
    if Assigned(FOnButtonClick) then begin
      Invalidate;
      fGenChange := false; //����� � ����������� ����� ���� ������ ������� ������ ��� ������������
      FOnButtonClick(Self, dzOld=zBtnNext);
    end else begin
      if (dzOld=zBtnNext) then
        Position := Position + SmallChange
      else
        Position := Position - SmallChange
      ;
      Invalidate;
    end
    ;
  end else begin
    Invalidate;
  end
  ;

  finally
    fGenChange := false;
  end;
end;

procedure TTimeSlider.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  NewHotZone: TTSZone;
  DeltaByVal: Real;
begin
  inherited;

  if (fDragZone = zNone) then begin
    NewHotZone := GetZone(X, Y);
    if NewHotZone <> fHotZone then begin
      fHotZone := NewHotZone;
      //if fHotZone <> zNone then StartHotTracking;
      Invalidate;
    end;
  end else if (fDragZone = zHandle) then begin
    DeltaByVal := (X - fXBeforeDrag) / PointsPerSecond;

    fGenChange := true;
    Position := fPosBeforeDrag + DeltaByVal; //will call Invalidate and DoChange
    fGenChange := false;

    {
    //function ClientXToPosition must be aware of the x zero point offset
    fGenChange := true;
    Position := ClientXToPosition(X); //will call Invalidate and DoChange
    fGenChange := false;
    }
  end
  ;

end;

procedure TTimeSlider.SetPosition(Value: Real);
begin
  Value := fChartData.GetNearestTime(Value);

  if FPosition <> Value then begin
    FPosition := Value;
    Invalidate;
    DoChange;
  end;
end;

procedure TTimeSlider.SetBackColor(const Value: TColor);
begin
  FBackColor := Value;
  Invalidate;
end;

procedure TTimeSlider.SetCaptAccel(const Value: string);
begin
  FCaptAccel := Value;
  Invalidate;
end;

procedure TTimeSlider.SetCaption(const Value: string);
begin
  FCaption := Value;
  Invalidate;
end;

procedure TTimeSlider.SetCaptSpeed(const Value: string);
begin
  FCaptSpeed := Value;
  Invalidate;
end;

procedure TTimeSlider.SetData(aData: array of TTSDataSample;
  aTimeStep: Real);
begin
  fChartData.SetData(aData, aTimeStep);
  Invalidate;
end;

procedure TTimeSlider.SetFramed(const Value: boolean);
begin
  FFramed := Value;
  Invalidate;
end;

procedure TTimeSlider.SetLargeChange(const Value: Real);
begin
  FLargeChange := Value;
end;

procedure TTimeSlider.SetOnButtonClick(const Value: TTimeSliderButtonClick);
begin
  FOnButtonClick := Value;
end;

procedure TTimeSlider.SetOnUserChange(const Value: TTimeSliderChangeEvent);
begin
  FOnUserChange := Value;
end;

procedure TTimeSlider.SetSmallChange(const Value: Real);
begin
  FSmallChange := Value;
end;

end.

