unit Accel;

interface

uses
  System.SysUtils, System.Classes, System.Types,
  Vcl.Controls, Vcl.Graphics,
  GR32_Image, GR32_Layers, GR32, GR32_VPR, GR32_Lines;

type
  TAccelerometer = class(TCustomImage32)
  private
    FVectorLayer: TBitmapLayer;
    FGridLayer:   TBitmapLayer;
    FPolModule: real;
    FPolAngle: real;
    FDecXValue: real;
    FDecYValue: real;
    procedure SetPolAngle(const Value: real);
    procedure SetPolModule(const Value: real);
    procedure BeginDrawing;
    procedure EndDrawing;
    procedure DrawCircle(amp: real);
    procedure DrawArrow(amp, theta: real);
    procedure LoadPNGFromResource(Dst: TBitmap32; ResName: string);
    procedure SetDecXValue(const Value: real);
    procedure SetDecYValue(const Value: real);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property PolModule: real read FPolModule write SetPolModule;
    property PolAngle:  real read FPolAngle  write SetPolAngle;
    property DecXValue: real read FDecXValue write SetDecXValue; //�� ���������� ��� ����
    property DecYValue: real read FDecYValue write SetDecYValue;
  end;

procedure Register;

implementation

uses
  Math, GR32_PNG, GR32_VectorUtils, GR32_PolygonsEx;

{$R 'accel.res'}

{$I ..\AkenIndi.inc}

const
  cXCenter = 70;
  cYCenter = 70;

procedure Register;
begin
  RegisterComponents('Graphics32', [TAccelerometer]);
end;

{ TAccelerometer }

procedure TAccelerometer.BeginDrawing;
begin
  BeginUpdate;
  FVectorLayer.Bitmap.Clear(0);
end;

procedure TAccelerometer.EndDrawing;
begin
  EndUpdate;
  Invalidate;
end;

constructor TAccelerometer.Create(AOwner: TComponent);
begin
  inherited;

  AutoSize := true;
  LoadPNGFromResource(Bitmap, 'ACCELBACK');
  Bitmap.DrawMode := dmBlend;


  FVectorLayer := TBitmapLayer.Create(Layers);
  with FVectorLayer do begin
    Bitmap.DrawMode := dmBlend;
    Bitmap.SetSizeFrom(Self.Bitmap);
    Location := FloatRect(0, 0, Self.Bitmap.Width, Self.Bitmap.Height);
  end;


  FGridLayer   := TBitmapLayer.Create(Layers);
  LoadPNGFromResource(FGridLayer.Bitmap, 'ACCELGRID');
  with FGridLayer do begin
    Bitmap.DrawMode := dmBlend;
    Location := FloatRect(0, 0, Self.Bitmap.Width, Self.Bitmap.Height);
  end;

  FDecXValue := 0.0;
  FDecYValue := 0.0;

  FPolModule := 0;
  DrawCircle(0.0);

  FPolAngle := 0.0;
  DrawArrow(0.0, 0.0);
end;

destructor TAccelerometer.Destroy;
begin
  //FArrowLayer.Free;

  inherited;
end;

procedure TAccelerometer.DrawArrow(amp, theta: real);
var
  r: real;
  x0,y0,x,y: UInt16;
begin
  DrawCircle(0.0);
  if amp < 10 then Exit;

  with TLine32.Create do
  try
    //AntialiasMode := am4times;

    EndStyle := esSquared;
    ArrowEnd.Style := asFourPoint;
    ArrowEnd.Color := clAkGreenLight;
    ArrowEnd.Size := 8;
    ArrowEnd.Pen.Width := 1;
    ArrowEnd.Pen.Color   := clAkGreenLight;

    ArrowStart.Style := asNone;
    //ArrowStart.Color := clAkGreenLight;
    //ArrowStart.Size := 6;
    //ArrowStart.Pen.Width := 2.0;
    //ArrowStart.Pen.Color := clAkGreenLight;

    r := Round(amp * 64/100 + 3); //��� ������=3, ���� ������=67, ��� ����=0-100
    x0 := 70;
    y0 := 70;

    x := Round(x0 + r * Sin(theta));
    y := Round(y0 - r * Cos(theta));

    //SetPoints([FixedPoint(70,74), FixedPoint(70,72-r)]);
    SetPoints([FixedPoint(70,70), FixedPoint(x,y)]);

    Draw(FVectorLayer.Bitmap, 3, clAkGreenLight);
  finally
    free;
  end;
end;

procedure TAccelerometer.DrawCircle(amp: real);
var
  r: UInt16;
  aofp: TArrayOfFloatPoint;
begin
  r := Round(amp * 64/100 + 3); //��� ������=3, ���� ������=67, ��� ����=0-100

  aofp := Ellipse(70, 70, r, r);
  PolyLineFS(FVectorLayer.Bitmap, aofp, clAkGreenLight, True, 5);
end;

procedure TAccelerometer.LoadPNGFromResource(Dst: TBitmap32; ResName: string);
var
  aResStream: TResourceStream;
  aPNG32: TPortableNetworkGraphic32;
begin
  aResStream := TResourceStream.Create(HInstance, ResName, RT_RCDATA);
  try
    aPNG32 := TPortableNetworkGraphic32.Create;
    try
      aPNG32.LoadFromStream(aResStream);
      aPNG32.AssignTo(Dst);
    finally
      aPNG32.Free;
    end;
  finally
    aResStream.Free;
  end
  ;
end;

procedure TAccelerometer.SetPolAngle(const Value: real);
begin
  if Value <> FPolAngle then begin
    FPolAngle := Value;
    BeginDrawing;
    DrawCircle(FPolModule);
    DrawArrow(FPolModule, FPolAngle);
    EndDrawing;
  end;
end;

procedure TAccelerometer.SetPolModule(const Value: real);
begin
  if Value <> FPolModule then begin
    if Value>100 then
      FPolModule := 100
    else
      FPolModule := Value
    ;
    BeginDrawing;
    DrawCircle(FPolModule);
    DrawArrow(FPolModule, FPolAngle);
    EndDrawing;
  end;
end;

procedure TAccelerometer.SetDecXValue(const Value: real);
begin
  if Value <> FDecXValue then begin
    FDecXValue := Value;

    FPolModule := Sqrt(Sqr(FDecXValue) + Sqr(FDecYValue));
    FPolAngle  := ArcTan2(FDecYValue, FDecXValue);

    BeginDrawing;
    DrawCircle(FPolModule);
    DrawArrow(FPolModule, FPolAngle);
    EndDrawing;
  end;
end;

procedure TAccelerometer.SetDecYValue(const Value: real);
begin
  if Value <> FDecYValue then begin
    FDecYValue := Value;

    FPolModule := Sqrt(Sqr(FDecXValue) + Sqr(FDecYValue));
    FPolAngle  := ArcTan2(FDecYValue, FDecXValue);

    BeginDrawing;
    DrawCircle(FPolModule);
    DrawArrow(FPolModule, FPolAngle);
    EndDrawing;
  end;
end;

end.
